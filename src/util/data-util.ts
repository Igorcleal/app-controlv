export class DataUtil{
    public static convertDateString(date:Date){
        return date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
    }
}