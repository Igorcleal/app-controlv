import { Usuario } from './usuario';
import { BtpItemEstoque } from './btpItemEstoque';

export class BtpEstoque {

	idEstoque: number;

	dscEstoque: string;

	usuario: Usuario;

	qtdEstoque: number;

	ativo: boolean;

	isServidor: boolean;

	lstItemEstoque : Array<BtpItemEstoque>;

}