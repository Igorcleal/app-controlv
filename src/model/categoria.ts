import {TipoCategoria} from './tipo-categoria';
export class Categoria {

    categoriaId: number;
    usuario: any;
    categoriaDsc: string;
    tipo: TipoCategoria;
    cor: string;
    isServidor: boolean;
    ativo: boolean;

}