import { BtpItemVenda } from './btpItemVenda';
import { Usuario } from './usuario';
import { BtpCliente } from "./cliente";
import { StatusVenda } from "./status-venda";
import { BtpItemFormaPagamentoVenda } from "./btpItemFormaPagamentoVenda";

export class BtpVenda {

	idVenda: number;
	usuario: Usuario;
	btpCliente: BtpCliente;
	datVenda: Date;
	datVendaString: string;
	vlrTotal: number;
	statusVenda: StatusVenda;
	isServidor: boolean;
	ativo: boolean;

	listItemVenda:Array<BtpItemVenda>;
	listItemPagamentoVenda:Array<BtpItemFormaPagamentoVenda>;

}