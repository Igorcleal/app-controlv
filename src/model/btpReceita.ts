import { BtpConta } from './btpConta';

import { Categoria } from './categoria';
import { Usuario } from './usuario';
import { BtpVenda } from "./btpVenda";
export class BtpReceita {

    receitaId: number;
    usuario: Usuario;
    receita: string;
    receitaDta: Date;
    receitaDtaString: string;
    receitaVlr: number;
    categoria: Categoria;
    receitaPaga: boolean;
    btpVenda: BtpVenda;
    receitaVlrPago: number;
    btpReceitaPai: BtpReceita;
    btpConta: BtpConta;
    isServidor: boolean;
    ativo: boolean;

}