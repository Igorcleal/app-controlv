import { Usuario } from './usuario';
import { BtpCliente } from './cliente';
import { EnumGrauParentesco } from "./enum-grauParentesco";


export class BtpDataImportante {

	idDataImportante: number;

	dscDataImportante: string;

	tipDataImportante: string;

	dscGrauParentesco: string;

	sexo: string;

	diaMes: Date;

	cliente: BtpCliente;

	usuario: Usuario;

	enumGrauParentesco: EnumGrauParentesco;

	ativo: boolean;

	isServidor: boolean;

}