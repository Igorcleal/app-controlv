import { Categoria } from './categoria';
import { Usuario } from './usuario';
export class BtpCliente {

    cliId: number;
    nome: string;
    cpf: string;
    sexo: string;
    endereco: string;
    celular: string;
    email: string;
    dataNasc: Date;
    usuario: Usuario;
    categoria: Categoria;
    isServidor: Boolean;
    ativo: Boolean;

}