import { Usuario } from './usuario';
import { BtpVenda } from "./btpVenda";
import { Produto } from "./produto";
import { BtpEstoque } from "./btpEstoque";

export class BtpItemVenda {

	idItemVenda: number;
	usuario: Usuario;
	btpVenda: BtpVenda;
	produto: Produto;
	qtdItemVenda: number;
	vlrItemVenda: number;
	vlrTotalItemVenda: number;
	btpEstoque: BtpEstoque;
	isServidor: boolean;
	ativo: boolean;

}