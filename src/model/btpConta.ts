import { Usuario } from './usuario';
export class BtpConta {

    contaId: number;
    usuario: Usuario;
    contaDsc: string;
    isServidor: boolean;
    ativo: boolean;
}