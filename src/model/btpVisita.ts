import { BtpCliente } from './cliente';
import { Usuario } from './usuario';

export class BtpVisita {

    idVisita: number;
    btpUsuario: Usuario;
    btpCliente: BtpCliente;
    localVisita: string;
    dataVisita: Date;
    dataVisitaString: string;
    horaVisita: string;
    isServidor: boolean;
    ativo: boolean
}