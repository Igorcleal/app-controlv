import { Produto } from './produto';
import { BtpPedido } from './btpPedido';
import { Usuario } from './usuario';
export class BtpItemPedido{
    
    private _usuario : Usuario;
    private _btpPedido : BtpPedido;
    private _itemId : number;
    private _produto : Produto;
    private _qtdSolicitada : number;
    private _qtdAtendida : number;
    private _valor : number;
    
    public get valor() : number {
        return this._valor;
    }
    public set valor(v : number) {
        this._valor = v;
    }
    
    public get qtdAtendida() : number {
        return this._qtdAtendida;
    }
    public set qtdAtendida(v : number) {
        this._qtdAtendida = v;
    }
    
    public get qtdSolicitada() : number {
        return this._qtdSolicitada;
    }
    public set qtdSolicitada(v : number) {
        this._qtdSolicitada = v;
    }
    
    public get produto() : Produto {
        return this._produto;
    }
    public set produto(v : Produto) {
        this._produto = v;
    }
    
    public get itemId() : number {
        return this._itemId;
    }
    public set itemId(v : number) {
        this._itemId = v;
    }
    

    public get btpPedido() : BtpPedido {
        return this._btpPedido;
    }
    public set btpPedido(v : BtpPedido) {
        this._btpPedido = v;
    }
    
    public get usuario() : Usuario {
        return this._usuario;
    }
    public set usuario(v : Usuario) {
        this._usuario = v;
    }


}