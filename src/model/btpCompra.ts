import { Usuario } from './usuario';
import { StatusCompra } from "./status-compra";
import { BtpFornecedor } from "./fornecedor";
import { BtpItemProdutoCompra } from "./btpItemProdutoCompra";
import { BtpItemFormaPagamentoCompra } from "./btpItemFormaPagamentoCompra";

export class BtpCompra {

	idCompra: number;
	usuario: Usuario;
	datCompra: Date;
	datCompraString: string;
	vlrTotal: number;
	statusCompra: StatusCompra;
	btpFornecedor: BtpFornecedor;
	isServidor:boolean;
	ativo:boolean;

	listItemCompra:Array<BtpItemProdutoCompra>;
	listItemPagamentoCompra:Array<BtpItemFormaPagamentoCompra>;

}