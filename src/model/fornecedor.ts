import { Usuario } from './usuario';

export class BtpFornecedor {

    fornId: number;
    nomeFornecedor: string;
    usuario: Usuario;
    cnpj: number;
    endereco: string;
    telefone: string;
    ativo: boolean;
    isServidor: boolean;

    constructor(fornId?: number, nomeFornecedor?: string, usuario?: Usuario, cnpj?: number, endereco?: string, telefone?: string) {
        this.fornId = fornId;
        this.nomeFornecedor = nomeFornecedor;
        this.usuario = usuario;
        this.cnpj = cnpj;
        this.endereco = endereco;
        this.telefone = telefone;
    }

}