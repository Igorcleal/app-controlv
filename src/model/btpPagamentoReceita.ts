import { Usuario } from './usuario';
import { BtpReceita } from './btpReceita';

export class BtpPagamentoReceita {

    idPagamento: number;
    valorPagamento: number;
    datPagamento: Date;
    btpReceita: BtpReceita;
    usuario: Usuario;
    isServidor: Boolean;
    ativo: Boolean;
}