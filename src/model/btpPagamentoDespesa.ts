import { Usuario } from './usuario';
import { BtpDespesa } from './btpDespesa';
export class BtpPagamentoDespesa{
    
    idPagamento : number;
    valorPagamento : number;
    datPagamento : Date;
    btpDespesa : BtpDespesa;
    usuario : Usuario;
    isServidor:Boolean;
    ativo:Boolean;
    
}