import { BtpProdutoPedido } from './btpProdutoPedido';
import { BtpCliente } from './cliente';
import { Usuario } from './usuario';
import { StatusPedido } from './status-pedido'
export class BtpPedido {

    private _pedidoId: number;
    private _dataPedido: Date;
    private _btpCliente: BtpCliente;
    private _usuario: Usuario;
    private _statusPedido: StatusPedido;
    private _listBtpProdutopedido: Array<BtpProdutoPedido>;
    private _totalPedido : number;
    
    public get totalPedido() : number {
        return this._totalPedido;
    }
    public set totalPedido(v : number) {
        this._totalPedido = v;
    }
    

    public get listBtpProdutopedido(): Array<BtpProdutoPedido> {
        return this._listBtpProdutopedido;
    }
    public set listBtpProdutopedido(v: Array<BtpProdutoPedido>) {
        this._listBtpProdutopedido = v;
    }

    public get statusPedido(): StatusPedido {
        return this._statusPedido;
    }
    public set statusPedido(v: StatusPedido) {
        this._statusPedido = v;
    }


    public get usuario(): Usuario {
        return this._usuario;
    }
    public set usuario(v: Usuario) {
        this._usuario = v;
    }

    public get btpCliente(): BtpCliente {
        return this._btpCliente;
    }
    public set btpCliente(v: BtpCliente) {
        this._btpCliente = v;
    }

    public get dataPedido(): Date {
        return this._dataPedido;
    }
    public set dataPedido(v: Date) {
        this._dataPedido = v;
    }

    public get pedidoId(): number {
        return this._pedidoId;
    }
    public set pedidoId(v: number) {
        this._pedidoId = v;
    }

}