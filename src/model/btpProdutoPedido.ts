import { Usuario } from './usuario';
import { Produto } from './produto';
import { BtpPedido } from './btpPedido';
export class BtpProdutoPedido{
    
    private _btpPedido : BtpPedido;
    private _btpProduto : Produto;
    private _btpUsuario : Usuario;
    private _qtdProduto : number;
    private _qtdAtendidaProduto : number;
    private _valor : number;

    public get valor() : number {
        return this._valor;
    }
    public set valor(v : number) {
        this._valor = v;
    }
    
    public get qtdAtendidaProduto() : number {
        return this._qtdAtendidaProduto;
    }
    public set qtdAtendidaProduto(v : number) {
        this._qtdAtendidaProduto = v;
    }
    
    public get qtdProduto() : number {
        return this._qtdProduto;
    }
    public set qtdProduto(v : number) {
        this._qtdProduto = v;
    }
    
    
    public get btpUsuario() : Usuario 
    {
        return this._btpUsuario;
    }
    public set btpUsuario(v : Usuario)
     {
        this._btpUsuario = v;
    }
    
    public get btpProduto() : Produto {
        return this._btpProduto;
    }
    public set btpProduto(v : Produto) {
        this._btpProduto = v;
    }
    
    public get btpPedido() : BtpPedido {
        return this._btpPedido;
    }
    public set btpPedido(v : BtpPedido) {
        this._btpPedido = v;
    }
    
}