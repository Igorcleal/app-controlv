export class CodigoErroLoginFirebase {
    public static EMAIL_INVALIDO="auth/invalid-email";
    public static SENHA_ERRADA="auth/wrong-password";
    public static USUARIO_NAO_ENCONTRADO="auth/user-not-found";
}