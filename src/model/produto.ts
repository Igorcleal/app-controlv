import { Categoria } from './categoria';
import { Usuario } from './usuario';
import { EnumUnidade } from './enum-unidade';

export class Produto {

    produtoId: number;

    usuario: Usuario;

    categoria: Categoria;

    produtoDsc: string;

    produtoUnd: EnumUnidade;

    durabilidade: number;

    precoUnitario: number;

    precoUltCompra: number;

    caracteristicas: string;

    ativo: boolean;

    isServidor: boolean;

}