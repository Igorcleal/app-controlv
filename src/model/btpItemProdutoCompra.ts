import { Usuario } from './usuario';
import { Produto } from "./produto";
import { BtpCompra } from "./btpCompra";
import { BtpEstoque } from "./btpEstoque";

export class BtpItemProdutoCompra {

	idItemCompra: number;
	usuario: Usuario;
	btpCompra: BtpCompra;
	produto: Produto;
	qtdItemCompra: number;
	vlrItemCompra: number;
	vlrTotalItemCompra: number;
	btpEstoque: BtpEstoque;
	ativo:boolean;
	
}