import { BtpConta } from './btpConta';
import { Categoria } from './categoria';
import { Usuario } from './usuario';
import { BtpCompra } from "./btpCompra";
export class BtpDespesa {

    despesaId: number;
    usuario: Usuario;
    despesa: string;
    despesaDta: Date;
    despesaDtaString: string;
    despesaVlr: number;
    despesaValorPago: number;
    categoria: Categoria;
    despesaPaga: boolean;
    btpCompra: BtpCompra;
    btpDespesaPai: BtpDespesa;
    btpConta: BtpConta;
    isServidor: Boolean;
    ativo: Boolean;

}