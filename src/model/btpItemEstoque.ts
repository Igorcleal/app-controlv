import { Usuario } from './usuario';
import { BtpEstoque } from "./btpEstoque";
import { Produto } from "./produto";

export class BtpItemEstoque {

	idItemEstoque: number;

	btpEstoque: BtpEstoque;

	usuario: Usuario;

	produto: Produto;

	qtdEstoque: number;

	ativo: boolean;

	isServidor: boolean;

}