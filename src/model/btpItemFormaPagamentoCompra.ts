
import { Usuario } from "./usuario";
import { BtpFormaPagamento } from "./btpFormaPagamento";
import { BtpCompra } from "./btpCompra";

export class BtpItemFormaPagamentoCompra {

	usuario: Usuario;
	itemId: number;
	numParcela: number;
	datVencimento: Date;
	datVencimentoString: string;
	datPagamento: Date;
	datPagamentoString: string;
	vlrParcela: number;
	dscObservacao: string;
	btpCompra: BtpCompra;
	btpFormaPagamento: BtpFormaPagamento;
	ativo: boolean;

}