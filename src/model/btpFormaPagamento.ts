import { Usuario } from './usuario';
export class BtpFormaPagamento {

    idFormaPagamento: number;
    descricao: string;
    usuario: Usuario;
    isServidor:Boolean;
    ativo: Boolean;

}