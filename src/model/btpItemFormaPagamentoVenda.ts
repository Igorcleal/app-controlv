
import { Usuario } from "./usuario";
import { BtpVenda } from "./btpVenda";
import { BtpFormaPagamento } from "./btpFormaPagamento";

export class BtpItemFormaPagamentoVenda {

	usuario: Usuario;
	itemId: number;
	numParcela: number;
	datVencimento: Date;
	datVencimentoString: string;
	datPagamento: Date;
	datPagamentoString: string;
	vlrParcela: number;
	dscObservacao: string;
	btpVenda: BtpVenda;
	btpFormaPagamento: BtpFormaPagamento;
	ativo:boolean;

}