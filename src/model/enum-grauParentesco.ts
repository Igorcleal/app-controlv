export enum EnumGrauParentesco {
    
    Pai =0,
    Mãe,
    Filho,
    Filha,
    Irmão,
    Irmã,
    Tio,
    Tia,
    Sobrinho,
    Sobrinha,
    Neto,
    Neta,
    Primo,
    Prima
}