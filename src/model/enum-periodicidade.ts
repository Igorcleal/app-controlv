export enum EnumPeriodicidade {
    diario = 0,
    semanal,
    mensal,
    semestral,
    anual
}