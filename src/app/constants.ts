export class Constants {
    public static urlWs: string = "http://192.168.25.94:8100/api/controlv/rest/";
    //public static urlWs:string = "http://ec2-18-231-14-147.sa-east-1.compute.amazonaws.com:8080/controlv/rest/";
    public static timeout: number = 15000;
}