import { PagamentoDespesaWsProvider } from './../providers/despesa/pagamento-despesa-ws';
import { Device } from '@ionic-native/device';
import { VisitaWsProvider } from './../providers/visita/visita-ws';
import { Network } from '@ionic-native/network';
import { CadastroUsuarioPage } from './../pages/cadastro-usuario/cadastro-usuario';
import { ReenviarSenhaPage } from './../pages/reenviar-senha/reenviar-senha';
import { AuthFirebaseService } from './../providers/auth-firebase-service';
import { ModalMensagemPage } from './../pages/modal-mensagem/modal-mensagem';
import { AngularFire } from 'angularfire2';
import { PopoverDespesaReceita } from './../components/popover-despesa-receita/popover-despesa-receita';
import { PagamentoDespesaService } from './../providers/pagamento-despesa-provider';
import { PagamentosDespesasPage } from './../pages/despesas-receitas/pagamentos-despesas/pagamentos-despesas';
import { UtilsService } from './../providers/utils-service';
import { DashboardService } from './../providers/dashboard-service';
import { MensagemProvider } from './../providers/mensagem-provider';
import { PagamentoService } from './../providers/pagamento-provider';
import { PagamentosPage } from './../pages/despesas-receitas/pagamentos/pagamentos';
import { PopoverColorsComponent } from './../components/popover-colors/popover-colors';
import { PopoverCategoriaComponent } from './../components/popover-categoria/popover-categoria';
import { DashboardPage } from './../pages/dashboard/dashboard';
import { PopoverCalendarioComponent } from './../components/popover-calendario/popover-calendario';
import { VisitaService } from './../providers/visita/visita-service';
import { VisitaClienteAddPage } from './../pages/visita/visita-cliente-add/visita-cliente-add';
import { VisitaClientePage } from './../pages/visita/visita-cliente/visita-cliente';
import { CalendarioPage } from './../pages/calendario/calendario';
import { ContaPage } from './../pages/conta/conta/conta';
import { ContaAddPage } from './../pages/conta/conta-add/conta-add';
import { ContaService } from './../providers/conta/conta-service';
import { InputDataComponent } from './../components/input-data/input-data';
import { DatePicker } from '@ionic-native/date-picker';
import { ReceitaService } from './../providers/receita/receita-service';
import { DespesaService } from './../providers/despesa/despesa-service';
import { NgModule, ErrorHandler, LOCALE_ID } from '@angular/core';
import { IonicStorageModule } from '@ionic/storage'
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { Page1 } from '../pages/page1/page1';
import { Page2 } from '../pages/page2/page2';
import { CategoriaPage } from '../pages/categoria/categoria';
import { LoginPage } from '../pages/login/login';
import { Login2Page } from '../pages/login2/login2';
import { CadastroPage } from '../pages/cadastro/cadastro';
import { FornecedorPage } from '../pages/fornecedor/fornecedor';
import { FornecedorAddPage } from './../pages/fornecedor-add/fornecedor-add';
import { ProdutoAddPage } from './../pages/produto-add/produto-add';
import { ProdutoPage } from './../pages/produto-list/produto-list';
import { ClientePage } from './../pages/cliente/cliente';
import { FormaPagamentoPage } from './../pages/forma-pagamento/forma-pagamento';
import { PedidoPage } from './../pages/pedido/pedido/pedido';
import { PedidoAddPage } from './../pages/pedido/pedido-add/pedido-add';
import { PedidoService } from './../providers/pedido-service';
import { FornecedorService } from './../providers/fornecedor/fornecedor-service';
import { FormaPagamentoService } from './../providers/forma-pagamento/forma-pagamento-service';
import { FormaPagamentoAddPage } from './../pages/forma-pagamento-add/forma-pagamento-add';
import { ClienteService } from './../providers/cliente/cliente-service';
import { ClienteAddPage } from './../pages/cliente-add/cliente-add';

import { DbProvider } from './../providers/db-provider';
import { ModalCategoriaPage } from './../pages/categoria/modalCategoria';
import { ReceitaAddPage } from './../pages/despesas-receitas/receita-add/receita-add';
import { DespesaAddPage } from './../pages/despesas-receitas/despesa-add/despesa-add';
import { DespesasReceitasPage } from './../pages/despesas-receitas/despesas-receitas/despesas-receitas';
import { ItemPedidoAddPage } from './../pages/pedido/item-pedido-add/item-pedido-add';
import { PopupListarProdutosPage } from './../pages/popup-listar-produtos/popup-listar-produtos';
import { PopupListarClientesPage } from './../pages/popup-listar-clientes/popup-listar-clientes';
import { DataImportantePage } from './../pages/dataImportante/dataImportante-list/dataImportante';
import { DataImportanteAddPage } from '../pages/dataImportante/dataImportante-add/dataImportante-add';
import { EstoquePage } from './../pages/estoque/estoque-list/estoque';
import { EstoqueAddPage } from './../pages/estoque/estoque-add/estoque-add';
import { ItemEstoqueAddPage } from './../pages/estoque/item-estoque-add/item-estoque-add';
import { VendaService } from './../providers/venda/venda-service';
import { VendaPage } from './../pages/venda/venda-list/venda';
import { VendaAddPage } from './../pages/venda/venda-add/venda-add';
import { ItemVendaAddPage } from './../pages/venda/item-venda-add/item-venda-add';
import { ItemFormaPagamentoVendaAddPage } from './../pages/venda/item-forma-pagamento-venda-add/item-forma-pagamento-venda-add';

import { CompraPage } from './../pages/compra/compra-list/compra';
import { CompraAddPage } from './../pages/compra/compra-add/compra-add';
import { PopupListarFornecedorPage } from './../pages/popup-listar-fornecedor/popup-listar-fornecedor';
import { CompraService } from './../providers/compra/compra-service';
import { ItemFormaPagamentoCompraAddPage } from './../pages/compra/item-forma-pagamento-compra-add/item-forma-pagamento-compra-add';
import { ItemProdutoCompraAddPage } from './../pages/compra/item-produto_compra-add/item-produto_compra-add';

import { NgCalendarModule } from 'ionic2-calendar';
import { HttpModule } from '@angular/http';
import { MaskDirective } from '../directives/mask/mask';
import { TextMaskModule } from 'angular2-text-mask';
import { AngularFireModule } from 'angularfire2';
import { HttpProvider } from '../providers/http/http-provider';
import { NetworkProvider } from '../providers/network/network-provider';
import { ContaWS } from '../providers/conta/conta-ws';
import { FornecedorWsProvider } from '../providers/fornecedor/fornecedor-ws';
import { CategoriaWsProvider } from '../providers/categoria/categoria-ws';
import { CategoriaService } from './../providers/categoria/categoria-service';
import { ClienteWsProvider } from "../providers/cliente/cliente-ws";
import { FormaPagamentoWsProvider } from "../providers/forma-pagamento/forma-pagamento-ws";
import { ProdutoService } from "../providers/produto/produto-service";
import { ProdutoWsProvider } from "../providers/produto/produto-ws";
import { UsuarioWsProvider } from "../providers/usuario/usuario-ws";
import { DespesaWsProvider } from "../providers/despesa/despesa-ws";
import { ReceitaWsProvider } from "../providers/receita/receita-ws";
import { VendaWsProvider } from '../providers/venda/venda-ws';
import { ItemVendaService } from '../providers/venda/item-venda-service';
import { ItemFormaPagamentoVendaService } from "../providers/venda/item-forma-pagamento-venda-service";
import { CompraWsProvider } from "../providers/compra/compra-ws";
import { ItemCompraService } from "../providers/compra/item-compra-service";
import { ItemFormaPagamentoCompraService } from "../providers/compra/item-forma-pagamento-compra-service";
import { EstoqueService } from '../providers/estoque/estoque-service';
import { EstoqueWsProvider } from '../providers/estoque/estoque-ws';
import { DataImportanteService } from '../providers/data-importante/dataImportante-service';
import { DataImportanteWsProvider } from '../providers/data-importante/dataImportante-ws';
import { PagamentoReceitaWs } from "../providers/receita/pagamento-receita-ws";
import { DownloadWsProvider } from "../providers/download-ws";
import { EnviarDadosWs } from "../providers/enviar-dados-ws";
import { Facebook } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';

var config = {
  apiKey: "AIzaSyBS4bW770NpTX-fC1GqUSJVoW56XgZwUzM",
  authDomain: "teste-e4bd3.firebaseapp.com",
  databaseURL: "https://teste-e4bd3.firebaseio.com",
  projectId: "teste-e4bd3",
  storageBucket: "teste-e4bd3.appspot.com",
  messagingSenderId: "339941043071"
};

@NgModule({
  declarations: [
    MyApp,
    Page1,
    Page2,
    CategoriaPage,
    LoginPage, Login2Page,
    CadastroPage, ModalCategoriaPage,
    FornecedorPage, FornecedorAddPage,
    ProdutoPage, ProdutoAddPage,
    ClientePage, ClienteAddPage,
    FormaPagamentoPage, FormaPagamentoAddPage,
    PedidoPage, PedidoAddPage, ItemPedidoAddPage,
    PopupListarClientesPage,
    PopupListarProdutosPage,
    DataImportantePage, DataImportanteAddPage,
    DespesasReceitasPage, DespesaAddPage, ReceitaAddPage,
    InputDataComponent,
    ContaPage, ContaAddPage,
    DataImportantePage,
    DataImportanteAddPage,
    EstoquePage,
    VendaAddPage, ItemVendaAddPage, ItemFormaPagamentoVendaAddPage,
    EstoqueAddPage, ItemEstoqueAddPage, VendaPage, VendaAddPage,
    CalendarioPage,
    VisitaClientePage, VisitaClienteAddPage,
    PopoverCalendarioComponent,
    DashboardPage,
    PopoverCategoriaComponent, PopoverColorsComponent,
    PagamentosPage, CompraPage, CompraAddPage, PopupListarFornecedorPage, ItemFormaPagamentoCompraAddPage, ItemProdutoCompraAddPage,
    PagamentosDespesasPage,
    MaskDirective,
    PopoverDespesaReceita,
    ModalMensagemPage,
    ReenviarSenhaPage,
    CadastroUsuarioPage
  ],
  imports: [
    HttpModule,
    NgCalendarModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    TextMaskModule,
    AngularFireModule.initializeApp(config)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Page1,
    Page2,
    CategoriaPage, ModalCategoriaPage,
    LoginPage, Login2Page,
    CadastroPage,
    FornecedorPage, FornecedorAddPage,
    ProdutoPage, ProdutoAddPage,
    ClientePage, ClienteAddPage,
    FormaPagamentoPage, FormaPagamentoAddPage,
    PedidoPage, PedidoAddPage, ItemPedidoAddPage,
    PopupListarClientesPage,
    PopupListarProdutosPage,
    DataImportantePage, DataImportanteAddPage,
    DespesasReceitasPage, DespesaAddPage, ReceitaAddPage,
    ContaPage, ContaAddPage,
    DataImportantePage,
    DataImportanteAddPage,
    EstoquePage,
    EstoqueAddPage, ItemEstoqueAddPage,
    VendaPage, VendaAddPage, ItemVendaAddPage, ItemFormaPagamentoVendaAddPage,
    CalendarioPage,
    VisitaClientePage, VisitaClienteAddPage,
    PopoverCalendarioComponent,
    DashboardPage,
    PopoverCategoriaComponent, PopoverColorsComponent,
    PagamentosPage, CompraPage, CompraAddPage, PopupListarFornecedorPage, ItemFormaPagamentoCompraAddPage, ItemProdutoCompraAddPage,
    PagamentosDespesasPage,
    PopoverDespesaReceita,
    ModalMensagemPage,
    ReenviarSenhaPage,
    CadastroUsuarioPage
  ],
  providers: [{ provide: ErrorHandler, useClass: IonicErrorHandler },
  { provide: LOCALE_ID, useValue: 'pt-BR' },
    DbProvider,
    CategoriaService, CategoriaWsProvider,
    ProdutoService, ProdutoWsProvider,
    ClienteService, ClienteWsProvider,
    FormaPagamentoService, FormaPagamentoWsProvider,
    FornecedorService, FornecedorWsProvider,
    PedidoService,
    DataImportanteService, DataImportanteWsProvider,
    DespesaService, DespesaWsProvider, PagamentoDespesaWsProvider,
    ReceitaService, ReceitaWsProvider, PagamentoReceitaWs,
    DatePicker,
    ContaService, ContaWS,
    EstoqueService, VendaService,
    VisitaService, VisitaWsProvider,
    PagamentoService, MensagemProvider, 
    CompraService, CompraWsProvider,
    DashboardService, UtilsService,
    PagamentoDespesaService,
    AngularFire,
    AuthFirebaseService,
    HttpProvider,
    NetworkProvider,
    Network,
    UsuarioWsProvider,
    VendaWsProvider,
    ItemVendaService, ItemFormaPagamentoVendaService,
    ItemCompraService, ItemFormaPagamentoCompraService,
    EstoqueWsProvider,
    Device,
    DownloadWsProvider,
    EnviarDadosWs,
    Facebook,
    GooglePlus
  ]
})
export class AppModule { }
