import { AuthFirebaseService } from './../providers/auth-firebase-service';
import { Storage } from '@ionic/storage';
import { Login2Page } from './../pages/login2/login2';
import { CategoriaPage } from './../pages/categoria/categoria';
import { DashboardPage } from './../pages/dashboard/dashboard';
import { VisitaClientePage } from './../pages/visita/visita-cliente/visita-cliente';
import { CalendarioPage } from './../pages/calendario/calendario';
import { ContaPage } from './../pages/conta/conta/conta';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController, MenuController } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { Observable } from 'rxjs/Rx';

import { FornecedorPage } from '../pages/fornecedor/fornecedor';
import { ProdutoPage } from './../pages/produto-list/produto-list';
import { ClientePage } from './../pages/cliente/cliente';
import { FormaPagamentoPage } from './../pages/forma-pagamento/forma-pagamento';
import { DbProvider } from './../providers/db-provider';
import { DataImportantePage } from './../pages/dataImportante/dataImportante-list/dataImportante';
import { DespesasReceitasPage } from './../pages/despesas-receitas/despesas-receitas/despesas-receitas';
import { EstoquePage } from './../pages/estoque/estoque-list/estoque';
import { VendaPage } from './../pages/venda/venda-list/venda';
import { CompraPage } from './../pages/compra/compra-list/compra';

import * as firebase from 'firebase';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;


  rootPage: any;
  public alert: any;

  pages: Array<{ title: string, component: any, icon: string, atual: boolean, linha: boolean }>;

  constructor(public platform: Platform, public dbProvider: DbProvider, public alertCtrl: AlertController,
    public storage: Storage, public menuCtrl: MenuController) {
    firebase.initializeApp({
      apiKey: "AIzaSyBS4bW770NpTX-fC1GqUSJVoW56XgZwUzM",
      authDomain: "teste-e4bd3.firebaseapp.com",
      databaseURL: "https://teste-e4bd3.firebaseio.com",
      projectId: "teste-e4bd3",
      storageBucket: "teste-e4bd3.appspot.com",
      messagingSenderId: "339941043071"
    });

    this.initializeApp();

    this.pages = [
      { title: 'Categorias', component: CategoriaPage, icon: 'pricetags', atual: false, linha: false },
      { title: 'Clientes', component: ClientePage, icon: 'person', atual: false, linha: false },
      { title: 'Fornecedores', component: FornecedorPage, icon: 'person', atual: false, linha: false },
      { title: 'Produtos', component: ProdutoPage, icon: 'cart', atual: false, linha: false },
      { title: 'Estoque', component: EstoquePage, icon: 'cube', atual: false, linha: true },
      { title: 'Forma Pagamento', component: FormaPagamentoPage, icon: 'card', atual: false, linha: false },
      { title: 'Contas', component: ContaPage, icon: 'logo-buffer', atual: false, linha: true },
      { title: 'Compra', component: CompraPage, icon: 'thumbs-up', atual: false, linha: false },
      { title: 'Venda', component: VendaPage, icon: 'thumbs-up', atual: false, linha: false },
      { title: 'Despesas/Receitas', component: DespesasReceitasPage, icon: 'swap', atual: false, linha: true },
      { title: 'Calendário', component: CalendarioPage, icon: 'calendar', atual: false, linha: false },
      { title: 'Visitas', component: VisitaClientePage, icon: 'calendar', atual: false, linha: false },
      { title: 'Datas Importantes', component: DataImportantePage, icon: 'calendar', atual: false, linha: false }
    ];

  }

  initializeApp() {

    this.storage.get("usuarioLogado").then(data => {
      if (data != null) {
        this.rootPage = DashboardPage;
        this.menuCtrl.enable(true);
      } else {
        this.rootPage = Login2Page;
        this.menuCtrl.enable(false);
      }
    });

    this.platform.ready().then(() => {

      StatusBar.styleDefault();

      setTimeout(() => {
        Splashscreen.hide();
      }, 100);

      this.dbProvider.openDatabase()
        .then(() => this.dbProvider.createTable());

    });
  }

  openPage(page) {
    for (let page1 of this.pages) {
      page1.atual = false;
    }
    page.atual = true;
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario

    //this.nav.setRoot(page.component);
    if (this.nav.canGoBack()) {
      this.nav.pop();
    }
    this.nav.push(page.component);

  }

  isPageAtualDashboard() {
    for (let page of this.pages) {
      if (page.atual == true) {
        return false;
      }
    }
    return true;
  }

  abrirDashboard() {
    for (let page of this.pages) {
      page.atual = false;
    }
    this.nav.setRoot(DashboardPage);
  }

  showAlert() {
    this.alert = this.alertCtrl.create({
      title: 'Sair?',
      message: 'Deseja fechar ControlV?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            this.alert = null;
          }
        },
        {
          text: 'Sair',
          handler: () => {
            this.platform.exitApp();
          }
        }
      ]
    });
    this.alert.present();
  }

  deslogar() {


    let alert = this.alertCtrl.create({
      title: 'Deslogar?',
      message: 'Deseja deslogar do APP?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            this.alert = null;
          }
        },
        {
          text: 'Deslogar',
          handler: () => {
            this.storage.set('usuarioLogado', null);
            this.nav.setRoot(Login2Page);
            firebase.auth().signOut();
            this.menuCtrl.enable(false);
          }
        }
      ]
    });
    alert.present();

  }


}