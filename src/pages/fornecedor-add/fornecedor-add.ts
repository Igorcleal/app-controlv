import { Component } from '@angular/core';
import { NavController, ViewController, NavParams, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormBuilder, Validators } from '@angular/forms';

import { FornecedorWsProvider } from './../../providers/fornecedor/fornecedor-ws';
import { MensagemProvider } from './../../providers/mensagem-provider';
import { UtilsService } from './../../providers/utils-service';
import { Usuario } from './../../model/usuario';
import { FornecedorService } from './../../providers/fornecedor/fornecedor-service';
import { BtpFornecedor } from './../../model/fornecedor';

@Component({
  selector: 'page-fornecedor-add',
  templateUrl: 'fornecedor-add.html'
})
export class FornecedorAddPage {
  formCadastroFornecedor: any;
  btpFornecedor: BtpFornecedor;
  masks: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public formBuilder: FormBuilder, public viewCtrl: ViewController,
    public fornecedorService: FornecedorService, public storage: Storage,
    public alertCtrl: AlertController, public utils: UtilsService, public msgProvider: MensagemProvider,
    public fornecedorWS: FornecedorWsProvider) {

    this.inicializar();
  }

  inicializar() {
    this.btpFornecedor = this.navParams.get('fornecedor');
    if (this.btpFornecedor == null) {
      this.btpFornecedor = new BtpFornecedor();
    }

    this.masks = {
      foneMask: ['(', /[0-9]/, /\d/, ')', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/],
      cnpjMask: [/[0-9]/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/]
    };

    this.formCadastroFornecedor = this.formBuilder.group({
      nomeFornecedor: [this.btpFornecedor.nomeFornecedor, Validators.compose([Validators.required])],
      cnpj: [this.btpFornecedor.cnpj],
      telefone: [this.btpFornecedor.telefone],
      endereco: [this.btpFornecedor.endereco]
    });

  }

  salvar() {
    if (this.formCadastroFornecedor.valid) {
      this.btpFornecedor.nomeFornecedor = this.formCadastroFornecedor.value.nomeFornecedor;
      this.btpFornecedor.cnpj = this.formCadastroFornecedor.value.cnpj;
      this.btpFornecedor.telefone = this.formCadastroFornecedor.value.telefone;
      this.btpFornecedor.endereco = this.formCadastroFornecedor.value.endereco;

      if (this.btpFornecedor.fornId == null) {
        this.incluir();
      } else {
        this.alterar();
      }
    }
  }

  incluir() {
    this.btpFornecedor.isServidor = false;
    this.utils.getUsuarioLogado().then(usuario => {
      this.btpFornecedor.usuario = usuario;
      this.utils.showLoading();

      this.fornecedorService.incluir(this.btpFornecedor).then(data => {
        console.log("success incluir fornecedor sqlite");

        this.btpFornecedor.fornId = data.insertId;
        //após inserir conta no banco envia conta para Servidor
        this.enviarFornecedorServidor().then(() => {
          this.viewCtrl.dismiss();
          this.utils.closeLoading();
        }).catch((err) => {
          this.viewCtrl.dismiss();
          this.utils.closeLoading();
        });

      }).catch(error => {
        console.log("erro incluir fornecedor");
        this.utils.closeLoading();
      });
    });
  }

  alterar() {
    this.utils.showLoading();

    this.btpFornecedor.isServidor = false;

    this.fornecedorService.alterar(this.btpFornecedor).then(response => {
      console.log("success alterar fornecedor sqlite");

      this.enviarFornecedorServidor().then(() => {
        this.voltarParaListagem()
      }).catch(() => {
        this.voltarParaListagem()
      });

    }).catch(error => {
      console.log("erro alterar fornecedor");
      this.utils.closeLoading();
    });
  }

  fecharModal() {
    this.viewCtrl.dismiss();
  }

  deletar() {
    let confirm = this.alertCtrl.create({
      title: 'Excluir',
      message: 'Deseja excluir o fornecedor?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
            this.btpFornecedor.ativo = false;
            this.alterar();

          }
        }
      ]
    });
    confirm.present();
  }

  private enviarFornecedorServidor() {
    return this.fornecedorWS.enviarFornecedoresServidor().catch((err) => {
      console.log('err enviar fornecedor ws');
      console.error(err.message);
      return Promise.reject(true);
    });
  }

  voltarParaListagem() {
    this.viewCtrl.dismiss();
    this.utils.closeLoading();
  }
}