import { FirebaseAuthState } from 'angularfire2/auth';
import { EnviarDadosWs } from './../../providers/enviar-dados-ws';
import { UsuarioWsProvider } from './../../providers/usuario/usuario-ws';
import { CategoriaService } from './../../providers/categoria/categoria-service';
import { CadastroUsuarioPage } from './../cadastro-usuario/cadastro-usuario';
import { ReenviarSenhaPage } from './../reenviar-senha/reenviar-senha';
import { AuthFirebaseService } from './../../providers/auth-firebase-service';
import { CadastroPage } from './../cadastro/cadastro';
import { UtilsService } from './../../providers/utils-service';
import { ModalMensagemPage } from './../modal-mensagem/modal-mensagem';
import { CodigoErroLoginFirebase } from './../../model/codigo-erro-login-firebase';
import { FormBuilder, Validators } from '@angular/forms';
import { DashboardPage } from './../dashboard/dashboard';
import { Storage } from '@ionic/storage';
import { Usuario } from './../../model/usuario';
import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, MenuController, Platform } from 'ionic-angular';
import { FormaPagamentoService } from "../../providers/forma-pagamento/forma-pagamento-service";
import { DownloadWsProvider } from "../../providers/download-ws";
//import { GooglePlus } from 'ionic-native';
import { GooglePlus } from '@ionic-native/google-plus';
import { AngularFire } from 'angularfire2';
import * as firebase from 'firebase';

@Component({
  selector: 'page-login2',
  templateUrl: 'login2.html'
})
export class Login2Page {

  email: any;
  password: any;
  formLogin: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
    public formBuilder: FormBuilder, public modalCtrl: ModalController, public categoriaService: CategoriaService,
    public menuCtrl: MenuController, public utils: UtilsService, public authFirebaseService: AuthFirebaseService,
    public formaPagamentoService: FormaPagamentoService, public usuarioWs: UsuarioWsProvider,
    public downloadWs: DownloadWsProvider, public enviarDadosWs: EnviarDadosWs, public af: AngularFire, private platform: Platform) {
    this.formLogin = formBuilder.group({
      email: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Login2Page');
  }

  loginFacebook() {
    this.authFirebaseService.facebookLogin().then((response) => {
      this.utils.showLoading();
      let usuarioLogado: Usuario = new Usuario();
      usuarioLogado.nomeUsuario = response.displayName;
      usuarioLogado.idUsuario = response.uid;
      return this.entrarApp(usuarioLogado).catch((err) => {
        this.authFirebaseService.logout();
        return Promise.reject(err);
      });
    }).catch((error: any) => {
      console.log('aaaaa');
      console.log(error);
      console.log(error.message);

      this.modalCtrl.create(ModalMensagemPage, { mensagem: error.message, tipo: 'erro' }).present();
    });
  }

  loginGoogle() {
    this.authFirebaseService.googleLogin().then((response) => {
      let usuarioLogado: Usuario = new Usuario();
      usuarioLogado.nomeUsuario = response.displayName;
      usuarioLogado.idUsuario = response.uid;
      console.log('succcess')
      console.log(response)
      return this.entrarApp(usuarioLogado).catch((err) => {
        this.authFirebaseService.logout();
        return Promise.reject(err);
      });
    }).catch((error: any) => {
      console.log('aaaaa');

      console.log(error);
      console.log(error.message);

      this.modalCtrl.create(ModalMensagemPage, { mensagem: error.message, tipo: 'erro' }).present();
    });
  }

  public logarApp(): void {

    let usuarioLogado: Usuario = new Usuario();
    usuarioLogado.nomeUsuario = "Igor Leal";
    usuarioLogado.idUsuario = 1;
    this.storage.set('usuarioLogado', usuarioLogado);
    this.storage.set('idUsuarioLogado', usuarioLogado.idUsuario);
    console.log(usuarioLogado);

    this.navCtrl.setRoot(DashboardPage);
  }

  login() {

    if (this.formLogin.valid == false) {
      return;
    }

    this.utils.showLoading();
    this.authFirebaseService.login(this.email, this.password).then((response) => {

      if (response.auth.emailVerified == false) {
        this.utils.closeLoading();
        this.modalCtrl.create(ModalMensagemPage, { mensagem: "E-mail não verificado!", tipo: 'erro' }).present();
        return;
      }

      let usuarioLogado: Usuario = new Usuario();
      usuarioLogado.nomeUsuario = "Igor Leal";
      usuarioLogado.idUsuario = response.auth.uid;

      return this.entrarApp(usuarioLogado).catch((err) => {
        this.authFirebaseService.logout();
        return Promise.reject(err);
      });

    }).catch((error: any) => {
      console.log(error);
      console.log(error.message);
      this.utils.closeLoading();

      switch (error.code) {
        case CodigoErroLoginFirebase.EMAIL_INVALIDO: {
          this.modalCtrl.create(ModalMensagemPage, { mensagem: "E-mail inválido!", tipo: 'erro' }).present();
          break;
        }
        case CodigoErroLoginFirebase.SENHA_ERRADA: {
          this.modalCtrl.create(ModalMensagemPage, { mensagem: "Senha incorreta!", tipo: 'erro' }).present();
          break;
        }
        case CodigoErroLoginFirebase.USUARIO_NAO_ENCONTRADO: {
          this.modalCtrl.create(ModalMensagemPage, { mensagem: "E-mail não cadastrado!", tipo: 'erro' }).present();
          break;
        }
        default:
          this.modalCtrl.create(ModalMensagemPage, { mensagem: error.message, tipo: 'erro' }).present();
          break;
      }
    })
  }

  //método chamado após logar pelo firebase
  entrarApp(usuarioLogado) {

    return this.downloadDados(usuarioLogado.idUsuario).then(() => {
      console.log('dados baixados com sucesso');

      this.storage.set('usuarioLogado', usuarioLogado);
      this.storage.set('idUsuarioLogado', usuarioLogado.idUsuario).then(() => {

        let promises = [];
        promises.push(this.categoriaService.inserirCategoriasBasicas(usuarioLogado.idUsuario));
        promises.push(this.formaPagamentoService.inserirFormasPagamentosBasicas(usuarioLogado.idUsuario));

        Promise.all(promises).then(() => {
          this.enviarDadosWs.enviarDados().then(() => {
            this.menuCtrl.enable(true);
            this.navCtrl.setRoot(DashboardPage);
            this.utils.closeLoading();
          });
        });
      });
    }).catch((error) => {
      return Promise.reject(error);
    });
  }

  irCadastroPage() {
    this.navCtrl.push(CadastroUsuarioPage);
  }

  resetPassword() {
    this.navCtrl.push(ReenviarSenhaPage);
  }

  downloadDados(idUsuarioLogado: string) {
    return this.downloadWs.downloadDados(idUsuarioLogado);
  }

  teste() {
    let usuario: Usuario = new Usuario();
    usuario.idUsuario = 'sZ96rdor6IgWsxyyDTtgORFTB2Y2';

    this.usuarioWs.atualizarUidPhone(usuario);
  }

  /*googleLogin() {
    console.log('começou aquii');
    this.google.login({
      'webClientId': '997374192032-mrns461ek3k2mv6itb2csrf48dfpbr92.apps.googleusercontent.com',
      'offline': true
    }).then((response) => {
      console.log('passou');

      const googleCredential = firebase.auth.GoogleAuthProvider
        .credential(response.idToken);

      firebase.auth().signInWithCredential(googleCredential).then((res) => {

        alert('chegou aqui');
        this.navCtrl.setRoot(DashboardPage);
      }).catch((err) => {
        alert('Erro ' + err);

      })


    });
  }*/

  googlePlusLogin() {

    this.af.auth.subscribe((data: FirebaseAuthState) => {

      this.af.auth.unsubscribe()
      console.log("in auth subscribe", data)

      this.platform.ready().then(() => {
        GooglePlus.login({
          'webClientId': '902582118353-q09n2vt3v6i3slbhcrueqsivrku2rdne.apps.googleusercontent.com'
        })
          .then((userData) => {

            console.log("userData " + JSON.stringify(userData));
            console.log("firebase " + firebase);
            var provider = firebase.auth.GoogleAuthProvider.credential(userData.idToken);

            firebase.auth().signInWithCredential(provider)
              .then((success) => {
                console.log("Firebase success: " + JSON.stringify(success));


              })
              .catch((error) => {
                console.log("Firebase failure: " + JSON.stringify(error));

              });

          })
          .catch((gplusErr) => {
            console.log("GooglePlus failure: " + JSON.stringify(gplusErr));
           
          });

      })
    })

  }

}
