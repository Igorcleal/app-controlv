import { Storage } from '@ionic/storage';
import { FormaPagamentoService } from './../../providers/forma-pagamento/forma-pagamento-service';
import { FormaPagamentoAddPage } from './../forma-pagamento-add/forma-pagamento-add';
import { BtpFormaPagamento } from './../../model/btpFormaPagamento';
import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';


@Component({
  selector: 'page-forma-pagamento',
  templateUrl: 'forma-pagamento.html'
})
export class FormaPagamentoPage {

  listFormasPagamento: Array<BtpFormaPagamento>;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public modalCtrl: ModalController, public formaPagamentoService: FormaPagamentoService,
    public storage: Storage) {
    if (this.listFormasPagamento == null) {
      this.listFormasPagamento = new Array();
    }
  }

  ionViewDidLoad() {
    this.getAllFormasPagamento();
  }

  getAllFormasPagamento() {
    this.storage.get('idUsuarioLogado').then((data) => {
      this.formaPagamentoService.getAll(data)
        .then(listFormasPagamento => {
          this.listFormasPagamento = listFormasPagamento;
        });
    });

  }

  incluir() {
    let formaPagamentoModal = this.modalCtrl.create(FormaPagamentoAddPage);
    this.abrirModalFormaPagamento(formaPagamentoModal);
  }

  editar(btpFormaPagamento) {
    let formaPagamentoModal = this.modalCtrl.create(FormaPagamentoAddPage, { btpFormaPagamento: btpFormaPagamento });
    this.abrirModalFormaPagamento(formaPagamentoModal);
  }

  private abrirModalFormaPagamento(modal) {
    modal.onDidDismiss(data => {
      this.getAllFormasPagamento();
    });
    modal.present();
  }

}
