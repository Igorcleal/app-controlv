import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { Produto } from './../../model/produto';
import { ProdutoAddPage } from './../produto-add/produto-add';
import { ProdutoService } from "../../providers/produto/produto-service";

@Component({
    selector: 'page-produto-list',
    templateUrl: 'produto-list.html'
})
export class ProdutoPage {

    produtos: Produto[] = new Array();
    produtosNoFilter: Produto[] = new Array();
    renderSearchBar: Boolean = false;
    filtroProdutoDsc: string = "";

    constructor(public navCtrl: NavController, public navParams: NavParams,
        public storage: Storage, public produtoService: ProdutoService,
        public modalCtrl: ModalController) {

        if (this.produtos == null) {
            this.produtos = new Array();
        }

    }

    ionViewDidLoad() {
        this.getAllProdutos();
    }

    getAllProdutos() {

        this.storage.get('idUsuarioLogado').then((data) => {
            this.produtoService.getAll(data)
                .then(produtos => {
                    this.produtos = produtos;
                    this.produtosNoFilter = produtos;
                });
        });
    }

    adicionarProduto() {
        let addProduto = this.modalCtrl.create(ProdutoAddPage);
        this.abrirModalProduto(addProduto);
    }

    editarProduto(produto: Produto) {
        let modal = this.modalCtrl.create(ProdutoAddPage, { produto: produto });
        this.abrirModalProduto(modal);
    }

    private abrirModalProduto(modal) {
        modal.onDidDismiss(data => {
            this.getAllProdutos();
            console.log("fechado");
        });
        modal.present();
    }

    exibirSearchBar() {
        if (this.renderSearchBar) {
            this.renderSearchBar = false;
            document.getElementById("contentProdutos").style.top = "0px";
        } else {
            this.renderSearchBar = true;
            document.getElementById("contentProdutos").style.top = "50px";
        }

    }

    filtrarProdutos() {

        this.produtos = this.produtosNoFilter.filter((item) => {
            return item.produtoDsc.toLowerCase().indexOf(this.filtroProdutoDsc.toLowerCase()) > -1;
        });
    }
}
