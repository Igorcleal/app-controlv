import { DespesasReceitasPage } from './../despesas-receitas/despesas-receitas/despesas-receitas';
import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

import { CadastroPage } from '../cadastro/cadastro';
import { Usuario } from '../../model/usuario';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  public usuario: string;
  public senha: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public alertCtrl: AlertController, public storage: Storage) {

  }

  logarApp(){
    if(this.autenticarUsuario()){
      let usuarioLogado: Usuario = new Usuario();
      usuarioLogado.nomeUsuario = "Igor Leal";
      usuarioLogado.idUsuario = 1;
      this.storage.set('usuarioLogado', usuarioLogado);
      this.storage.set('idUsuarioLogado', usuarioLogado.idUsuario);
      console.log(usuarioLogado);
      
      let alert = this.alertCtrl.create({
        title: 'Success',
        subTitle: 'Bem-vindo '+usuarioLogado.idUsuario+'!',
        buttons: ['OK']
      });
      alert.present();
      this.navCtrl.setRoot(DespesasReceitasPage);
    }else{
      let alert = this.alertCtrl.create({
        title: 'Fail',
        subTitle: 'Login ou senha incorretos!',
        buttons: ['OK']
      });
      alert.present();
    }
  }

  autenticarUsuario(): Boolean{
    return true;
  }

  irCadastroPage() {
    this.navCtrl.push(CadastroPage);
  }

}
