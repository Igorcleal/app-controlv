import { Component } from '@angular/core';
import { ViewController, NavParams, AlertController, PopoverController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormBuilder, Validators } from '@angular/forms';

import { MensagemProvider } from './../../providers/mensagem-provider';
import { UtilsService } from './../../providers/utils-service';
import { PopoverColorsComponent } from './../../components/popover-colors/popover-colors';
import { Usuario } from './../../model/usuario';
import { Categoria } from '../../model/categoria';
import { CategoriaService } from './../../providers/categoria/categoria-service';
import { TipoCategoria } from '../../model/tipo-categoria';
import { CategoriaWsProvider } from "../../providers/categoria/categoria-ws";


@Component({
    selector: 'modal-categoria',
    templateUrl: 'modalCategoria.html'
})
export class ModalCategoriaPage {
    formCadastroCategoria: any;
    categoria: Categoria;
    TipoCategoria: typeof TipoCategoria = TipoCategoria;

    constructor(public formBuilder: FormBuilder, public viewCtrl: ViewController,
        public params: NavParams, public storage: Storage, public popoverCtrl: PopoverController,
        public categoriaService: CategoriaService, public alertCtrl: AlertController,
        public utils: UtilsService, public msgProvider: MensagemProvider, public categoriaWs: CategoriaWsProvider) {

        this.categoria = params.get('categoria');

        if (this.categoria == null) {
            this.categoria = new Categoria();
            this.categoria.cor = '#4767a4';
            this.categoria.tipo = params.get('tipoCategoria');
        }

        this.formCadastroCategoria = formBuilder.group({
            categoriaDsc: [this.categoria.categoriaDsc, Validators.compose([Validators.required])]
        });

    }

    salvarCategoria() {
        if (this.formCadastroCategoria.valid) {
            this.categoria.categoriaDsc = this.formCadastroCategoria.value.categoriaDsc;
            if (this.categoria.categoriaId == null) {
                this.incluirCategoria();
            } else {
                this.alterarCategoria();
            }
        }
    }

    incluirCategoria() {
        this.utils.getUsuarioLogado().then((usuario) => {
            this.categoria.usuario = usuario;
            this.categoria.isServidor = false;

            this.utils.showLoading();
            this.categoriaService.incluirCategoria(this.categoria)
                .then(response => {
                    console.log("success incluir categoria sqlite");

                    //após inserir conta no banco envia conta para Servidor
                    this.enviarServidor().then(() => {
                        this.viewCtrl.dismiss();
                        this.utils.closeLoading();
                    }).catch((err) => {
                        this.viewCtrl.dismiss();
                        this.utils.closeLoading();
                    });

                }).catch(error => {
                    console.log("erro incluir categoria");
                    this.utils.closeLoading();
                });
        });


    }

    alterarCategoria() {
        this.categoria.isServidor = false;

        this.utils.showLoading();
        this.categoriaService.update(this.categoria).then(response => {
            this.enviarServidor().then(() => {
                this.viewCtrl.dismiss();
                this.utils.closeLoading();
            }).catch((err) => {
                this.viewCtrl.dismiss();
                this.utils.closeLoading();
            });
        }).catch(error => {
            console.log("erro alterar categoria");
        });
    }

    fecharModalCategoria() {
        this.viewCtrl.dismiss();
    }

    deletarCategoria() {

        let confirm = this.alertCtrl.create({
            title: 'Excluir',
            message: 'Deseja excluir categoria?',
            buttons: [
                {
                    text: 'Cancelar',
                    handler: () => {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Confirmar',
                    handler: () => {
                        this.categoria.ativo = false;
                        this.alterarCategoria();
                    }
                }
            ]
        });
        confirm.present();
    }

    openPopoverColors(myEvent) {
        let popover = this.popoverCtrl.create(PopoverColorsComponent, {}, { cssClass: 'colors-popover' });
        popover.onDidDismiss(data => {
            if (data != null) {
                this.categoria.cor = data;
            }
        });
        popover.present({
            ev: myEvent
        });
    }

    private enviarServidor() {
        return this.categoriaWs.enviarCategoriasServidor().catch((err) => {
            console.log('err enviar fornecedor ws');
            console.error(err.message);
            return Promise.reject(true);
        });
    }
}