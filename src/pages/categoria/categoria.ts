import { Component, ViewChild } from '@angular/core';
import { NavController, AlertController, ModalController, Slides, PopoverController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { Categoria } from '../../model/categoria';
import { TipoCategoria } from '../../model/tipo-categoria';
import { PopoverCategoriaComponent } from './../../components/popover-categoria/popover-categoria';
import { CategoriaService } from './../../providers/categoria/categoria-service';
import { ModalCategoriaPage } from './modalCategoria';

@Component({
    selector: 'page-categoria',
    templateUrl: 'categoria.html'
})
export class CategoriaPage {

    categoriasProdutos: Categoria[];
    categoriasClientes: Categoria[];
    categorias: Categoria[];
    @ViewChild(Slides) slides: Slides;
    tipoCategoria: TipoCategoria;
    TipoCategoria: typeof TipoCategoria = TipoCategoria;

    constructor(public navCtrl: NavController, public popoverCtrl: PopoverController,
        public alertCtrl: AlertController, public storage: Storage,
        public modalCtrl: ModalController, public categoriaService: CategoriaService) {
        this.tipoCategoria = TipoCategoria.cliente
        this.categorias = new Array();
        if (this.categoriasProdutos == null) {
            this.categoriasProdutos = new Array();
        }
        if (this.categoriasClientes == null) {
            this.categoriasClientes = new Array();
        }
    }

    ionViewDidLoad() {
        this.getAllCategorias();
    }

    getAllCategorias() {
        this.storage.get('idUsuarioLogado').then((data) => {
            this.categoriaService.getAll(this.tipoCategoria, data)
                .then(categorias => {
                    this.categorias = categorias;
                });
        });

    }

    private abrirModalCategoria(categoriaModal) {
        console.log("abrir categoria");
        categoriaModal.onDidDismiss(data => {
            this.getAllCategorias();
        });
        categoriaModal.present();
    }

    incluir() {
        let categoriaModal = this.modalCtrl.create(ModalCategoriaPage, { tipoCategoria: this.tipoCategoria });
        this.abrirModalCategoria(categoriaModal);
    }

    editar(categoria) {
        console.log("editar");
        let categoriaModal = this.modalCtrl.create(ModalCategoriaPage, { categoria: categoria });
        this.abrirModalCategoria(categoriaModal);
    }

    deletar(categoria) {

        let confirm = this.alertCtrl.create({
            title: 'Excluir',
            message: 'Deseja excluir categoria?',
            buttons: [
                {
                    text: 'Cancelar',
                    handler: () => {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Confirmar',
                    handler: () => {
                        console.log('Agree clicked');
                        this.categoriaService.delete(categoria);
                        this.getAllCategorias();
                    }
                }
            ]
        });
        confirm.present();
    }

    /**
    * abre popOver para selecionar o tipo da categoria
    */
    abrirPopOver(myEvent) {
        let popover = this.popoverCtrl.create(PopoverCategoriaComponent);
        popover.onDidDismiss(data => {
            if (data != null) {
                this.tipoCategoria = data;
                this.getAllCategorias();
            }
        });
        popover.present({
            ev: myEvent
        });
    }

}