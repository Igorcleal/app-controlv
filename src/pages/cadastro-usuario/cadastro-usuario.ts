import { UtilsService } from './../../providers/utils-service';
import { Login2Page } from './../login2/login2';
import { Usuario } from './../../model/usuario';
import { AuthFirebaseService } from './../../providers/auth-firebase-service';
import { EmailValidator } from './../../validators/email';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-cadastro-usuario',
  templateUrl: 'cadastro-usuario.html'
})
export class CadastroUsuarioPage {

  public signupForm: FormGroup;
  public loading; Loading;
  public usuario: Usuario;

  masks = {
    foneMask: ['(', /[0-9]/, /\d/, ')', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/],
    cpfMask: [/[0-9]/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/],
    dateMask: [/[0-9]/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/],
  };

  constructor(public nav: NavController, public auth: AuthFirebaseService,
    public formBuilder: FormBuilder, public loadingCtrl: LoadingController,
    public alertCtrl: AlertController, public storage: Storage, public menuCtrl: MenuController,
    public utils: UtilsService) {

    

  }

  ngOnInit(): void {
    this.usuario = new Usuario();
    this.usuario.nomeUsuario=null;
    this.usuario.email=null;
    this.usuario.senha=null;
    this.usuario.telefone=null;
    this.usuario.cpf=null;
    
    this.signupForm = this.formBuilder.group({
      nome: [this.usuario.nomeUsuario, Validators.compose([Validators.required])],
      email: [this.usuario.email, Validators.compose([Validators.required, EmailValidator.isValid])],
      password: [this.usuario.senha, Validators.compose([Validators.required, Validators.minLength(6)])],
      telefone: [this.usuario.telefone, Validators.compose([Validators.required])],
      cpf: [this.usuario.cpf, Validators.compose([Validators.required])]
    });
  }

  /**
   * If the form is valid it will call the AuthData service to sign the user up password displaying a loading
   *  component while the user waits.
   *
   * If the form is invalid it will just log the form value, feel free to handle that as you like.
   */
  signupUser() {
    if (!this.signupForm.valid) {
      console.log("form invalid")
    } else {
      this.utils.showLoading();
      this.auth.cadastrarUsuario(this.usuario)
        .then((response) => {
          console.log(response);
          this.menuCtrl.enable(true);
          this.utils.closeLoading();
          this.nav.setRoot(Login2Page);
        }, (error) => {
          var errorMessage: string = error.message;
          let alert = this.alertCtrl.create({
            message: errorMessage,
            buttons: [
              {
                text: "Ok",
                role: 'cancel'
              }
            ]
          });
          this.utils.closeLoading();
          alert.present();
        });


    }
  }

}
