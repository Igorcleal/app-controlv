import { Storage } from '@ionic/storage';

import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { BtpEstoque } from "../../../model/btpEstoque";
import { EstoqueAddPage} from '../estoque-add/estoque-add';
import { EstoqueService } from '../../../providers/estoque/estoque-service';


@Component({
  selector: 'estoque',
  templateUrl: 'estoque.html'
})
export class EstoquePage {

  listEstoque: Array<BtpEstoque>;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public modalCtrl: ModalController, public estoqueService: EstoqueService,
    public storage: Storage) {
    if (this.listEstoque == null) {
      this.listEstoque = new Array();
    }
  }

  ionViewDidLoad() {
    this.getAllEstoque();
  }

  getAllEstoque() {
    this.storage.get('idUsuarioLogado').then((data) => {
      this.estoqueService.getAll(data)
        .then(listEstoque => {
          this.listEstoque = listEstoque;
        });
    });

  }

  incluir() {
    let estoqueModal = this.modalCtrl.create(EstoqueAddPage);
    this.abrirModalEstoque(estoqueModal);
  }

  editar(btpEstoque) {
    console.log(btpEstoque.idEstoque);
    let estoqueModal = this.modalCtrl.create(EstoqueAddPage, { btpEstoque: btpEstoque });
    this.abrirModalEstoque(estoqueModal);
  }

  private abrirModalEstoque(modal) {
    modal.onDidDismiss(data => {
      this.getAllEstoque();
    });
    modal.present();
  }


}
