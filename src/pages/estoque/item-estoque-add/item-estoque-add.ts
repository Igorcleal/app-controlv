import { UtilsService } from './../../../providers/utils-service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Produto } from './../../../model/produto';
import { Usuario } from './../../../model/usuario';
import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams, ViewController } from 'ionic-angular';

import { BtpItemEstoque } from './../../../model/btpItemEstoque';
import { EstoqueService } from '../../../providers/estoque/estoque-service';

@Component({
  selector: 'item-estoque-add',
  templateUrl: 'item-estoque-add.html'
})
export class ItemEstoqueAddPage {

  btpItemEstoque: BtpItemEstoque;
  statusCrud: string;
  form:FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public viewCtrl: ViewController, public estoqueService: EstoqueService,
    public storage: Storage, public formBuilder: FormBuilder, public utils:UtilsService) {
    this.btpItemEstoque = this.navParams.get('btpItemEstoque');

    if (this.btpItemEstoque == null) {
      this.btpItemEstoque = new BtpItemEstoque();
      let produto: Produto = this.navParams.get('produto');
      this.btpItemEstoque.produto = produto;
      this.btpItemEstoque.btpEstoque = this.navParams.get('btpEstoque');
    }

    this.form = this.formBuilder.group({
      qtdEstoque: [this.btpItemEstoque.qtdEstoque, Validators.compose([Validators.required])],
    });

  }

  ionViewDidLoad() {

  }

  fechar() {
    this.viewCtrl.dismiss();
  }

  salvarProdutoEstoque() {

    if(this.form.valid==false){
      return;
    }

    this.btpItemEstoque.qtdEstoque = this.form.value.qtdEstoque;

    console.log(this.btpItemEstoque.idItemEstoque + "salvarProdutoEstoque");
    if (this.btpItemEstoque.idItemEstoque == null) {
      this.incluirItemEstoque();
    } else {
      this.alterarItemEstoque();
    }
  }

  incluirItemEstoque() {
    this.utils.getUsuarioLogado().then((usuario) => {
      this.btpItemEstoque.usuario = usuario;

      let resposta = { 'btpItemEstoque': this.btpItemEstoque };
      this.viewCtrl.dismiss(resposta);
    });
  }

  alterarItemEstoque() {
    this.statusCrud = 'alterar';
    let resposta = { 'btpItemEstoque': this.btpItemEstoque, 'statusCrud': this.statusCrud };
    this.viewCtrl.dismiss(resposta);
  }

  deletarItemEstoque() {
    this.statusCrud = 'deletar';
    let resposta = { 'btpItemEstoque': this.btpItemEstoque, 'statusCrud': this.statusCrud };
    this.viewCtrl.dismiss(resposta);
  }

}
