import { MensagemProvider } from './../../../providers/mensagem-provider';
import { UtilsService } from './../../../providers/utils-service';
import { Usuario } from './../../../model/usuario';
import { Storage } from '@ionic/storage';
import { BtpEstoque } from './../../../model/btpEstoque';

import { Component } from '@angular/core';
import { AlertController, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { FormBuilder, Validators } from "@angular/forms";
import { BtpItemEstoque } from "../../../model/btpItemEstoque";

import { PopupListarProdutosPage } from "../../popup-listar-produtos/popup-listar-produtos";
import { Produto } from "../../../model/produto";
import { ItemEstoqueAddPage } from "../item-estoque-add/item-estoque-add";
import { EstoqueService } from '../../../providers/estoque/estoque-service';
import { EstoqueWsProvider } from '../../../providers/estoque/estoque-ws';

@Component({
  selector: 'page-estoque-add',
  templateUrl: 'estoque-add.html'
})
export class EstoqueAddPage {

  btpEstoque: BtpEstoque;
  listEstoque: Array<BtpEstoque>;
  listEstoqueAlterar: Array<BtpItemEstoque>;
  listEstoqueDeletar: Array<BtpItemEstoque>;
  formCadastroEstoque: any;
  listBtpItemEstoque: Array<BtpItemEstoque>;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public modalCtrl: ModalController, public storage: Storage,
    public estoqueService: EstoqueService, public mensagemProvider: MensagemProvider,
    public alertCtrl: AlertController, public formBuilder: FormBuilder,
    public viewCtrl: ViewController, public estoqueWs: EstoqueWsProvider, public utils: UtilsService) {

    this.inicializar();

  }


  private inicializar() {
    this.btpEstoque = this.navParams.get('btpEstoque');

    if (this.btpEstoque == null) {
      this.btpEstoque = new BtpEstoque();
    }

    if (this.listBtpItemEstoque == null) {
      this.listBtpItemEstoque = new Array<BtpItemEstoque>();
    }

    this.formCadastroEstoque = this.formBuilder.group({
      dscEstoque: [this.btpEstoque.dscEstoque, Validators.compose([Validators.required])]
    });

  }

  ionViewDidLoad() {
    this.getItensEstoque();
  }

  getItensEstoque() {
    this.utils.getUsuarioLogado().then((usuario) => {
      this.estoqueService.getItensEstoque(this.btpEstoque.idEstoque, usuario.idUsuario).then(list => {
        this.listBtpItemEstoque = list;
        console.log(this.listBtpItemEstoque);
      });
    });
  }


  fecharModalEstoque() {
    this.viewCtrl.dismiss();
  }


  salvar() {
    if (this.formCadastroEstoque.valid) {
      //recuperar valores do form
      this.btpEstoque.dscEstoque = this.formCadastroEstoque.value.dscEstoque;

      if (this.btpEstoque.idEstoque == null) {
        this.incluir();
      } else {
        this.alterar();
      }

    }
  }

  incluir() {
    
    this.utils.showLoading();

    this.utils.getUsuarioLogado().then(usuario => {
      this.btpEstoque.usuario = usuario;
      this.btpEstoque.isServidor = false;

      this.estoqueService.incluirEstoque(this.btpEstoque).then(() => {
        console.log('idInserido: ' + this.btpEstoque.idEstoque);

        this.incluirAlterarListas().then(() => {
          this.enviarEstoqueServidor();
        });

        console.log("success incluir estoque");

      }).catch(() => {
        this.utils.closeLoading();
        console.error('error inserir estoque');
      })

    });
  }

  alterar() {
    this.btpEstoque.isServidor = false;
    this.utils.showLoading();
    this.estoqueService.alterarEstoque(this.btpEstoque).then(response => {
      this.incluirAlterarListas().then(() => {
        this.enviarEstoqueServidor();
      });
      console.log("success alterar estoque");
    }).catch(error => {
      console.log("erro alterar estoque");
      this.utils.closeLoading();
    });
  }

  deletarEstoque() {

    let confirm = this.alertCtrl.create({
      title: 'Excluir',
      message: 'Deseja excluir esse estoque?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {

            this.btpEstoque.ativo = false;
            this.alterar();
          }
        }
      ]
    });
    confirm.present();
  }

  //Utilizado no alterar
  abrirModalItemEstoque(btpItemEstoque: BtpItemEstoque, indice: number) {
    console.log("abrir modal:" + btpItemEstoque.idItemEstoque);
    let modalItemEstoque = this.modalCtrl.create(ItemEstoqueAddPage, { btpItemEstoque: btpItemEstoque });
    modalItemEstoque.onDidDismiss(data => {

      if (data != null && data.btpItemEstoque != "" && data.btpItemEstoque !== null && data.statusCrud !== null) {

        if ("alterar" == data.statusCrud) {
          if (this.listEstoqueAlterar == null || this.listEstoqueAlterar[0] == null) {
            this.listEstoqueAlterar = new Array<BtpItemEstoque>();
          }

          this.listEstoqueAlterar.push(data.btpItemEstoque);
        } else if ("deletar" == data.statusCrud) {
          if (this.listEstoqueDeletar == null || this.listEstoqueDeletar[0] == null) {
            this.listEstoqueDeletar = new Array<BtpItemEstoque>();
          }

          this.listBtpItemEstoque.splice(indice, 1);
          this.listEstoqueDeletar.push(data.btpItemEstoque);
        }

      }

    });
    modalItemEstoque.present();
  }

  abrirModalProdutos() {
    let modal = this.modalCtrl.create(PopupListarProdutosPage, { listProdutosJaAdicionados: this.getProdutosEstoque() });
    modal.onDidDismiss(data => {
      if (data != null) {
        let produto: Produto = data;
        console.log("produto: " + produto.produtoDsc);

        let modalItemEstoque = this.modalCtrl.create(ItemEstoqueAddPage, { produto: produto, btpEstoque: this.btpEstoque });
        modalItemEstoque.onDidDismiss(data => {

          if (data != null && data.btpItemEstoque != "" && data.btpItemEstoque !== null) {

            if (this.listBtpItemEstoque == null || this.listBtpItemEstoque[0] == null) {
              this.listBtpItemEstoque = new Array<BtpItemEstoque>();
            }

            this.listBtpItemEstoque.push(data.btpItemEstoque);

          }

        });
        modalItemEstoque.present();
      }
    });
    modal.present();
  }

  incluirAlterarListas() {

    let promises = [];

    //Incluindo
    if (this.listBtpItemEstoque != null && this.listBtpItemEstoque[0] != null) {
      this.listBtpItemEstoque.forEach(elemento => {
        elemento.btpEstoque = this.btpEstoque;

        if (elemento.idItemEstoque == null) {
          promises.push(this.estoqueService.incluirItemEstoque(elemento));
        }
      })
    }

    //Deletar itens
    if (this.listEstoqueDeletar != null && this.listEstoqueDeletar[0] != null) {
      this.listEstoqueDeletar.forEach(elemento => {
        if (elemento.idItemEstoque != null) {
          elemento.ativo = false;
          promises.push(this.estoqueService.alterarItemEstoque(elemento));
        }
      })
    }

    //alterar itens
    if (this.listEstoqueAlterar != null && this.listEstoqueAlterar[0] != null) {
      this.listEstoqueAlterar.forEach(elemento => {
        if (elemento.idItemEstoque != null) {
          promises.push(this.estoqueService.alterarItemEstoque(elemento));
        }
      })
    }

    return Promise.all(promises);
  }

  getProdutosEstoque(): Array<Produto> {

    let listProdutos: Array<Produto>;

    this.listBtpItemEstoque.forEach(element => {

      if (listProdutos == null) {
        listProdutos = new Array();
      }

      listProdutos.push(element.produto);
    });

    return listProdutos;
  }

  private enviarEstoqueServidor() {
    return this.estoqueWs.enviarEstoqueServidorSemDependencias().then(() => {
      this.viewCtrl.dismiss();
      this.utils.closeLoading();
    }).catch((err) => {
      console.log('err enviar estoque ws');
      console.error(err.message);
      this.viewCtrl.dismiss();
      this.utils.closeLoading();
    });
  }


}
