import { MensagemProvider } from './../../providers/mensagem-provider';
import { UtilsService } from './../../providers/utils-service';
import { Usuario } from './../../model/usuario';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, NavParams, AlertController, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { Produto } from '../../model/produto';
import { CategoriaService } from './../../providers/categoria/categoria-service';
import { Categoria } from './../../model/categoria';
import { TipoCategoria } from '../../model/tipo-categoria';
import { ProdutoService } from "../../providers/produto/produto-service";
import { ProdutoWsProvider } from "../../providers/produto/produto-ws";

@Component({
  selector: 'page-produto-add',
  templateUrl: 'produto-add.html'
})
export class ProdutoAddPage {

  produto: Produto;
  produtos: Produto[];
  listCategorias: Categoria[];
  produtoForm: FormGroup;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public alertCtrl: AlertController,
    public storage: Storage,
    public produtoService: ProdutoService,
    public viewCtrl: ViewController,
    public categoriaService: CategoriaService,
    public utils: UtilsService, public msgProvider: MensagemProvider, public produtoWS: ProdutoWsProvider) {
    this.inicializar();
  }

  private inicializar() {
    this.produto = this.navParams.get('produto');

    if (this.produto == null) {
      this.produto = new Produto();
    }
    if (this.produto.categoria == null) {
      this.produto.categoria = new Categoria();
    }

    this.getListCategorias();

    this.produtoForm = this.formBuilder.group({
      produtoDsc: [this.produto.produtoDsc, Validators.compose([Validators.required])],
      produtoUnd: [this.produto.produtoUnd],
      durabilidade: [this.produto.durabilidade],
      precoUnitario: [this.produto.precoUnitario],
      precoUltCompra: [this.produto.precoUltCompra],
      categoriaId: [this.produto.categoria.categoriaId],
      caracteristicas: [this.produto.caracteristicas]
    });
  }

  getListCategorias() {
    this.storage.get('idUsuarioLogado').then((data) => {
      this.produto.usuario = new Usuario();
      this.produto.usuario.idUsuario = data;
      this.categoriaService.getAll(TipoCategoria.produto, data)
        .then(categorias => {
          this.listCategorias = categorias;
          console.log("list cat:" + categorias);
        });
    });
  }

  salvarProduto() {
    if (this.produtoForm.valid) {
      let produtoId: number = this.produto.produtoId;
      let ativo:boolean = this.produto.ativo;
      this.produto = this.produtoForm.value;
      this.produto.produtoId = produtoId;
      this.produto.ativo = ativo;

      if (this.produto.categoria == null) {
        this.produto.categoria = new Categoria();
      }
      this.produto.categoria.categoriaId = this.produtoForm.value.categoriaId;

      console.log("categoria" + this.produto.categoria.categoriaId);
      console.log("categoria" + this.produto.categoria.categoriaDsc);

      if (this.produto.produtoId == null) {
        this.incluirProduto();
      } else {
        this.alterarProduto();
      }
    }
  }

  incluirProduto() {

    this.produto.isServidor = false;
    this.utils.getUsuarioLogado().then(usuario => {

      this.produto.usuario = usuario;
      this.utils.showLoading();
      this.produtoService.incluirProduto(this.produto).then(response => {
        console.log("success");

        //após inserir produtos no banco envia produtos para Servidor
        this.enviarProdutosServidor().then(() => {
          this.viewCtrl.dismiss();
          this.utils.closeLoading();
        }).catch((err) => {
          this.viewCtrl.dismiss();
          this.utils.closeLoading();
        });

      }).catch(error => {
        console.log("erro");
        this.utils.closeLoading();

      });
    });
  }

  alterarProduto() {
    this.utils.showLoading();
    this.produto.isServidor = false;

    this.produtoService.update(this.produto).then(response => {
      console.log("success");

      this.enviarProdutosServidor().then(() => {
        this.utils.voltarParaListagem(this.viewCtrl);
      }).catch(() => {
        this.utils.voltarParaListagem(this.viewCtrl);
      });

    }).catch(error => {
      console.log("erro");
      this.utils.closeLoading();
    });
  }

  deletarProduto() {
    let confirm = this.alertCtrl.create({
      title: 'Excluir',
      message: 'Deseja excluir o produto?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
            this.produto.ativo = false;
            this.alterarProduto();
          }
        }
      ]
    });
    confirm.present();
  }

  fecharModalProduto() {
    this.viewCtrl.dismiss();
  }

  private enviarProdutosServidor() {
    return this.produtoWS.enviarProdutoServidor().catch((err) => {
      console.log('erro enviar produtos ws');
      console.error(err.message);
      return Promise.reject(true);
    });
  }

}
