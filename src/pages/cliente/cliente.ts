import { Storage } from '@ionic/storage';
import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { ClienteService } from './../../providers/cliente/cliente-service';
import { BtpCliente } from './../../model/cliente';
import { ClienteAddPage } from './../cliente-add/cliente-add';

@Component({
  selector: 'page-cliente',
  templateUrl: 'cliente.html'
})
export class ClientePage {

  lstClientes: Array<BtpCliente>;
  lstClientesNoFilter: Array<BtpCliente>;
  renderSearchBar: Boolean = false;
  filtroClienteNome: string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public modalCtrl: ModalController, public clienteService: ClienteService,
    public storage: Storage) {
    if (this.lstClientes == null) {
      this.lstClientes = new Array();
    }
  }

  ionViewDidLoad() {
    this.getAllClientes();
  }

  getAllClientes() {
    this.storage.get('idUsuarioLogado').then((data) => {
      this.clienteService.getAll(data)
        .then(clientes => {
          this.lstClientes = clientes;
          this.lstClientesNoFilter = clientes;
        });
    });
  }

  adicionarCliente() {
    let modalAddCliente = this.modalCtrl.create(ClienteAddPage);
    this.abrirModalCliente(modalAddCliente);
  }

  editarCliente(btpCliente: BtpCliente) {
    let modal = this.modalCtrl.create(ClienteAddPage, { btpCliente: btpCliente });
    this.abrirModalCliente(modal);
  }

  private abrirModalCliente(modal) {
    modal.onDidDismiss(data => {
      this.getAllClientes();
      console.log("fechado popup cliente");
    });
    console.log("abrir popup cliente");
    modal.present();
  }

  exibirSearchBar() {
    if (this.renderSearchBar) {
      this.renderSearchBar = false;
      document.getElementById("contentClientes").style.top = "0px";
    } else {
      this.renderSearchBar = true;
      document.getElementById("contentClientes").style.top = "50px";
    }
  }

  filtrarClientes() {
    this.lstClientes = this.lstClientesNoFilter.filter((item) => {
      return item.nome.toLowerCase().indexOf(this.filtroClienteNome.toLowerCase()) > -1;
    });
  }

}
