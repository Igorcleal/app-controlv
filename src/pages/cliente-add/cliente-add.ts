import { Storage } from '@ionic/storage';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';

import { Categoria } from './../../model/categoria';
import { ClienteService } from './../../providers/cliente/cliente-service';
import { BtpCliente } from './../../model/cliente';
import { TipoCategoria } from '../../model/tipo-categoria';
import { MensagemProvider } from './../../providers/mensagem-provider';
import { UtilsService } from './../../providers/utils-service';
import { Usuario } from './../../model/usuario';
import { CategoriaService } from './../../providers/categoria/categoria-service';
import { ClienteWsProvider } from "../../providers/cliente/cliente-ws";

@Component({
  selector: 'page-cliente-add',
  templateUrl: 'cliente-add.html'
})
export class ClienteAddPage {

  formCadastroCliente: any;
  masks: any;
  btpCliente: BtpCliente;
  listCategorias: Array<Categoria>;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public formBuilder: FormBuilder, public viewCtrl: ViewController,
    public clienteService: ClienteService, public storage: Storage,
    public alertCtrl: AlertController, public categoriaService: CategoriaService,
    public utils:UtilsService, public msgProvider:MensagemProvider, public clienteWS:ClienteWsProvider) {

    this.inicializar();
  }

  inicializar() {
    this.btpCliente = this.navParams.get('btpCliente');
    if (this.btpCliente == null) {
      this.btpCliente = new BtpCliente();
    }

    if (this.btpCliente.categoria == null) {
      this.btpCliente.categoria = new Categoria();
    }
    this.getListCategorias();

    this.masks = {
      foneMask: ['(', /[0-9]/, /\d/, ')', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/],
      cpfMask: [/[0-9]/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/],
      dateMask: [/[0-9]/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/],
    };

    this.formCadastroCliente = this.formBuilder.group({
      nome: [this.btpCliente.nome, Validators.compose([Validators.required])],
      email: [this.btpCliente.email, Validators.compose([])],
      celular: [this.btpCliente.celular],
      cpf: [this.btpCliente.cpf],
      sexo: [this.btpCliente.sexo],
      endereco: [this.btpCliente.endereco],
      categoriaId: [this.btpCliente.categoria.categoriaId],
      dataNasc: [this.btpCliente.dataNasc]
    });

  }

  getListCategorias() {
    this.storage.get('idUsuarioLogado').then((data) => {
      this.btpCliente.usuario = new Usuario();
      this.btpCliente.usuario.idUsuario = data;
      this.categoriaService.getAll(TipoCategoria.cliente, data)
        .then(categorias => {
          this.listCategorias = categorias;
        });
    });
  }

  salvar() {
    if (this.formCadastroCliente.valid) {
      let cliId: number = this.btpCliente.cliId;
      let ativo = this.btpCliente.ativo;
      let isServidor = this.btpCliente.isServidor;

      this.btpCliente = this.formCadastroCliente.value;
      this.btpCliente.cliId = cliId;
      this.btpCliente.isServidor = isServidor;
      this.btpCliente.ativo = ativo;


      if (this.btpCliente.categoria == null) {
        this.btpCliente.categoria = new Categoria();
      }
      this.btpCliente.categoria.categoriaId = this.formCadastroCliente.value.categoriaId;

      if (this.btpCliente.cliId == null) {
        this.incluir();
      } else {
        this.alterar();
      }
    }
  }

  incluir() {
    this.btpCliente.isServidor = false;
    this.utils.showLoading();
    
    this.utils.getUsuarioLogado().then(usuario => {
      this.btpCliente.usuario = usuario;

      this.clienteService.incluir(this.btpCliente).then(response => {
        console.log("success inserir cliente sqlite");

        //após inserir conta no banco envia cliente para Servidor
        this.enviarClienteServidor().then(() => {
          this.viewCtrl.dismiss();
          this.utils.closeLoading();
        }).catch((err) => {
          this.viewCtrl.dismiss();
          this.utils.closeLoading();
        });
      }).catch(error => {
        console.log(error.message);
        this.utils.closeLoading();
      });
    });
  }

  alterar() {

    this.btpCliente.isServidor = false;

    this.utils.showLoading();
    this.clienteService.alterar(this.btpCliente).then(response => {
      console.log("success alterar cliente sqlite");
      
      //após inserir conta no banco envia cliente para Servidor
        this.enviarClienteServidor().then(() => {
          this.viewCtrl.dismiss();
          this.utils.closeLoading();
        }).catch((err) => {
          this.viewCtrl.dismiss();
          this.utils.closeLoading();
        });

    }).catch(error => {
      console.log("erro");
      this.utils.closeLoading();
    });
  }

  fecharModal() {
    this.viewCtrl.dismiss();
  }

  deletar() {
    let confirm = this.alertCtrl.create({
      title: 'Excluir',
      message: 'Deseja excluir o cliente?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
            this.btpCliente.ativo = false;
            this.alterar();
          }
        }
      ]
    });
    confirm.present();
  }

  rangeAnosData(): Array<number> {
    let data = new Date()
    let anoAtual: number = data.getFullYear();

    let anos: Array<number> = new Array();
    anos.push(anoAtual - 1);
    anos.push(anoAtual);
    for (var _i = 0; _i < 4; _i++) {
      anoAtual++;
      anos.push(anoAtual);
    }
    return anos;
  }

  private enviarClienteServidor() {
    return this.clienteWS.enviarClientesServidor().catch((err) => {
      console.log('err enviar clientes ws');
      console.error(err.message);
      return Promise.reject(true);
    });
  }

  voltarParaListagem() {
    this.viewCtrl.dismiss();
    this.utils.closeLoading();
  }

}
