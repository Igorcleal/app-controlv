import { Component, ViewChild } from '@angular/core';
import { AlertController, NavController, NavParams, ModalController, ViewController, ToastController, Slides, FabContainer } from 'ionic-angular';
import { FormBuilder, Validators } from "@angular/forms";
import { Storage } from '@ionic/storage';
import * as moment from 'moment';

import { BtpConta } from './../../../model/btpConta';
import { FormaPagamentoService } from './../../../providers/forma-pagamento/forma-pagamento-service';
import { MensagemProvider } from './../../../providers/mensagem-provider';
import { Categoria } from './../../../model/categoria';
import { StatusCompra } from '../../../model/status-compra';
import { CompraService } from './../../../providers/compra/compra-service';
import { Usuario } from './../../../model/usuario';
import { BtpCompra } from './../../../model/btpCompra';
import { BtpFornecedor } from './../../../model/fornecedor';
import { PopupListarProdutosPage } from "../../popup-listar-produtos/popup-listar-produtos";
import { Produto } from "../../../model/produto";
import { PopupListarFornecedorPage } from "../../popup-listar-fornecedor/popup-listar-fornecedor";
import { BtpItemProdutoCompra } from "../../../model/btpItemProdutoCompra";
import { ItemProdutoCompraAddPage } from "../item-produto_compra-add/item-produto_compra-add";
import { BtpItemFormaPagamentoCompra } from "../../../model/btpItemFormaPagamentoCompra";
import { ItemFormaPagamentoCompraAddPage } from "../item-forma-pagamento-compra-add/item-forma-pagamento-compra-add";
import { BtpDespesa } from '../../../model/btpDespesa';
import { DespesaService } from "../../../providers/despesa/despesa-service";
import { BtpItemEstoque } from "../../../model/btpItemEstoque";
import { CompraWsProvider } from "../../../providers/compra/compra-ws";
import { UtilsService } from "../../../providers/utils-service";
import { EstoqueService } from '../../../providers/estoque/estoque-service';

@Component({
  selector: 'page-compra-add',
  templateUrl: 'compra-add.html'
})
export class CompraAddPage {

  @ViewChild(Slides) slides: Slides;
  btpCompra: BtpCompra;
  listCompra: Array<BtpCompra>;
  formCadastroCompra: any;
  dataCompra: string;
  showDetails: boolean;
  isBloqueiaAlteracao: boolean = false;
  slideSelecionado: number;

  listBtpItemProdutoCompra: Array<BtpItemProdutoCompra>;
  listBtpItemProdutoCompraRemover: Array<BtpItemProdutoCompra>;
  listBtpItemProdutoCompraAlterar: Array<BtpItemProdutoCompra>;

  lstBtpItemFormaPagamentoCompra: Array<BtpItemFormaPagamentoCompra>;
  lstBtpItemFormaPagamentoCompraRemover: Array<BtpItemFormaPagamentoCompra>;
  lstBtpItemFormaPagamentoCompraAlterar: Array<BtpItemFormaPagamentoCompra>;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public modalCtrl: ModalController, public storage: Storage,
    public compraService: CompraService, public toastCtrl: ToastController,
    public alertCtrl: AlertController, public despesaService: DespesaService,
    public formBuilder: FormBuilder, public viewCtrl: ViewController,
    public mensagemProvider: MensagemProvider, public formaPagamentoService: FormaPagamentoService,
    public estoqueService: EstoqueService, public compraWs: CompraWsProvider, public utils: UtilsService) {

    this.inicializar();

  }

  private inicializar() {
    this.btpCompra = this.navParams.get('btpCompra');

    if (this.btpCompra == null) {
      this.btpCompra = new BtpCompra();

      let btpFornecedor: BtpFornecedor = this.navParams.get('btpFornecedor');
      this.btpCompra.btpFornecedor = btpFornecedor;
      this.btpCompra.datCompra = new Date();

    }

    if (this.showDetails == null) {
      this.showDetails = false;
    }

    if (this.btpCompra.btpFornecedor == null) {
      this.btpCompra.btpFornecedor = new BtpFornecedor();
    }

    if (this.btpCompra != null && this.btpCompra.statusCompra != null) {

      //Bloqueia os campos caso a venda já esteja fecahda
      console.log(this.btpCompra.statusCompra)
      if (StatusCompra.FECHADO == this.btpCompra.statusCompra) {
        this.isBloqueiaAlteracao = true;
      } else {
        this.isBloqueiaAlteracao = false;
      }
    }

    if (this.btpCompra.datCompraString == null) {
      this.btpCompra.datCompraString = moment().format();
    }

    this.formCadastroCompra = this.formBuilder.group({
      datCompra: [{
        value: this.btpCompra.datCompraString,
        disabled: this.btpCompra.statusCompra == 1
      }, Validators.compose([Validators.required])]

    });
  }

  ionViewDidLoad() {
    this.getItemProdutoCompra();
    this.getAllItemFormaPgtoCompra();
  }

  fecharModalCompra() {
    this.viewCtrl.dismiss();
  }

  salvar() {
    if (this.formCadastroCompra.valid) {

      this.btpCompra.datCompra = new Date(this.btpCompra.datCompraString);

      if (this.btpCompra.idCompra == null) {
        this.incluir();
      } else {
        this.alterar();
      }
    }
  }

  incluir() {

    this.utils.showLoading();

    this.utils.getUsuarioLogado().then(usuario => {
      this.btpCompra.usuario = usuario;
      this.btpCompra.statusCompra = StatusCompra.PENDENTE;
      this.btpCompra.isServidor = false;

      this.compraService.incluirCompra(this.btpCompra)
        .then(idInserido => {

          this.btpCompra.idCompra = idInserido;

          this.incluirAlterarListas().then(() => {
            this.enviarComprasServidor();
          });

        }).catch((error: Error) => {
          console.log("erro incluir Compra");
          console.error(error.message);
        });
    });
  }

  alterar() {
    this.btpCompra.isServidor = false;
    this.utils.showLoading();
    this.compraService.alterarCompra(this.btpCompra).then(response => {
      console.log("success alterar compra");
      this.incluirAlterarListas().then(() => {
        this.enviarComprasServidor();
      });
    }).catch(error => {
      console.log("erro alterar compra");
    });
  }


  deletarCompra() {

    this.compraService.verificarExistePagamento(this.btpCompra.idCompra).then(temPagamento => {

      let confirm = this.alertCtrl.create({
        title: 'Excluir',
        message: temPagamento == true ? 'Existem pagamentos para essa compra, deseja mesmo excluí-la?' : 'Deseja excluir essa compra?',
        buttons: [
          {
            text: 'Cancelar',
            handler: () => {
              console.log('Disagree clicked');
            }
          },
          {
            text: 'Confirmar',
            handler: () => {
              this.compraService.deletarCompraCompleta(this.btpCompra).then(response => {

                this.listBtpItemProdutoCompra.forEach(elemento => {
                  this.estoqueService.removerQtdItemEstoque(elemento.produto.produtoId, elemento.btpEstoque.idEstoque, elemento.qtdItemCompra);
                })

                console.log("success deletar a compra");
                this.viewCtrl.dismiss();
              }).catch(error => {
                console.log("erro deletar a compra");
              });
            }
          }
        ]
      });
      confirm.present();
    });
  }

  abrirModalFornecedor() {
    let modal = this.modalCtrl.create(PopupListarFornecedorPage);
    modal.onDidDismiss(data => {
      if (data != null) {
        this.btpCompra.btpFornecedor = data;
      }
    });
    modal.present();
  }

  //Utilizado no alterar
  abrirModalItemProdutoCompra(btpItemProdutoCompra: BtpItemProdutoCompra, indice: number) {

    let btpItemPC: BtpItemProdutoCompra = new BtpItemProdutoCompra();

    btpItemPC = Object.assign({}, btpItemProdutoCompra);
    console.log("abrir modal" + btpItemProdutoCompra);

    btpItemPC.btpCompra = this.btpCompra;

    let modalItem = this.modalCtrl.create(ItemProdutoCompraAddPage, { btpItemProdutoCompra: btpItemProdutoCompra, statusCrud: "alterar", bloqueiaAlteracao: this.isBloqueiaAlteracao });
    modalItem.onDidDismiss(data => {

      if (data != null && data.btpItemProdutoCompra != "" && data.btpItemProdutoCompra !== null
        && data.statusCrud !== null) {

        if ("alterar" == data.statusCrud) {

          console.log(btpItemPC);
          console.log(data.btpItemProdutoCompra);

          let isEntrouValidacaoAletar: boolean = false;
          if (btpItemPC.qtdItemCompra != data.btpItemProdutoCompra.qtdItemCompra) {
            isEntrouValidacaoAletar = true;
          }

          if (btpItemPC.produto.produtoId != data.btpItemProdutoCompra.produto.produtoId) {
            isEntrouValidacaoAletar = true;
          }

          if (btpItemPC.vlrItemCompra != data.btpItemProdutoCompra.vlrItemCompra) {
            isEntrouValidacaoAletar = true;
          }

          if (btpItemPC.btpEstoque != null && data.btpItemProdutoCompra.btpEstoque != null &&
            btpItemPC.btpEstoque != data.btpItemProdutoCompra.btpEstoque.idEstoque) {
            isEntrouValidacaoAletar = true;
          }

          if (this.listBtpItemProdutoCompraAlterar == null || this.listBtpItemProdutoCompraAlterar[0] == null) {
            this.listBtpItemProdutoCompraAlterar = new Array<BtpItemProdutoCompra>();
          }

          if (isEntrouValidacaoAletar) {
            this.listBtpItemProdutoCompraAlterar.push(data.btpItemProdutoCompra);
          }

          btpItemProdutoCompra = data.btpItemProdutoCompra;

        } else if ("deletar" == data.statusCrud) {
          if (indice != null) {
            if (this.listBtpItemProdutoCompraRemover == null || this.listBtpItemProdutoCompraRemover[0] == null) {
              this.listBtpItemProdutoCompraRemover = new Array<BtpItemProdutoCompra>();
            }
            this.listBtpItemProdutoCompraRemover.push(btpItemProdutoCompra);
            this.listBtpItemProdutoCompra.splice(indice, 1);
          }
        }
      }

    });
    modalItem.present();
  }

  getItemProdutoCompra() {
    this.utils.getUsuarioLogado().then((usuario) => {
      this.compraService.getItemProdutoCompra(usuario.idUsuario, this.btpCompra.idCompra).then(list => {
        this.listBtpItemProdutoCompra = list;
      });
    });
  }

  abrirModalProdutos(fab: FabContainer) {
    let modal = this.modalCtrl.create(PopupListarProdutosPage);
    modal.onDidDismiss(data => {
      if (data != null) {
        let produto: Produto = data;
        console.log("produto: " + produto.produtoDsc);

        let modalItemProdutoCompra = this.modalCtrl.create(ItemProdutoCompraAddPage, { produto: produto, btpCompra: this.btpCompra, statusCrud: 'incluir', bloqueiaAlteracao: this.isBloqueiaAlteracao });
        modalItemProdutoCompra.onDidDismiss(data => {

          if (data != null && data.btpItemProdutoCompra != "" && data.btpItemProdutoCompra != null) {
            if (this.listBtpItemProdutoCompra == null || this.listBtpItemProdutoCompra[0] == null) {
              this.listBtpItemProdutoCompra = Array<BtpItemProdutoCompra>();
            }
            this.listBtpItemProdutoCompra.push(data.btpItemProdutoCompra);
          }

        });
        modalItemProdutoCompra.present();
      }
    });
    modal.present();
    fab.close();
  }

  private abrirModalCompra(modal) {
    modal.onDidDismiss(data => {

    });
    modal.present();
  }

  abrirModalItemFormaPagamento(btpItemFormaPagamentoCompra: BtpItemFormaPagamentoCompra, indice: number) {

    let btpItemFormaPC: BtpItemFormaPagamentoCompra = new BtpItemFormaPagamentoCompra();

    btpItemFormaPC = Object.assign({}, btpItemFormaPagamentoCompra);
    console.log("abrir modal" + btpItemFormaPagamentoCompra);

    btpItemFormaPC.btpCompra = this.btpCompra;

    let modalItem = this.modalCtrl.create(ItemFormaPagamentoCompraAddPage, { btpItemFormaPagamentoCompra: btpItemFormaPagamentoCompra, bloqueiaAlteracao: this.isBloqueiaAlteracao, dataCompra: this.btpCompra.datCompra, 'statusCrud': "alterar", });
    modalItem.onDidDismiss(data => {

      if (data != null && data.statusCrud != "" && data.statusCrud !== null) {

        if ("alterar" == data.statusCrud) {

          if (this.lstBtpItemFormaPagamentoCompraAlterar == null || this.lstBtpItemFormaPagamentoCompraAlterar[0] == null) {
            this.lstBtpItemFormaPagamentoCompraAlterar = new Array<BtpItemFormaPagamentoCompra>();
          }

          let isBloqueiaAlteracao = false;

          if (data.btpItemFormaPagamentoCompra.numParcela
            != btpItemFormaPC.numParcela) {
            isBloqueiaAlteracao = true;
          }
          if (data.btpItemFormaPagamentoCompra.datVencimentoString != btpItemFormaPC.datVencimentoString) {
            isBloqueiaAlteracao = true;
          }
          if (btpItemFormaPC.btpFormaPagamento.idFormaPagamento != data.btpItemFormaPagamentoCompra.btpFormaPagamento.idFormaPagamento) {
            isBloqueiaAlteracao = true;
          }

          if (btpItemFormaPC.vlrParcela != data.btpItemFormaPagamentoCompra.vlrParcela) {
            isBloqueiaAlteracao = true;
          }

          if (btpItemFormaPC.datPagamentoString != data.btpItemFormaPagamentoCompra.datPagamentoString) {
            isBloqueiaAlteracao = true;
          }

          if (btpItemFormaPC.dscObservacao != data.btpItemFormaPagamentoCompra.dscObservacao) {
            isBloqueiaAlteracao = true;
          }

          console.log("testando flag de inclusao teste teste");
          console.log(isBloqueiaAlteracao);
          if (isBloqueiaAlteracao) {
            this.lstBtpItemFormaPagamentoCompraAlterar.push(data.btpItemFormaPagamentoCompra);
          }

          btpItemFormaPagamentoCompra = data.btpItemFormaPagamentoCompra;
        } else if ("deletar" == data.statusCrud) {

          if (indice != null) {
            if (this.lstBtpItemFormaPagamentoCompraRemover == null || this.lstBtpItemFormaPagamentoCompraRemover[0] == null) {
              this.lstBtpItemFormaPagamentoCompraRemover = new Array<BtpItemFormaPagamentoCompra>();
            }

            this.lstBtpItemFormaPagamentoCompraRemover.push(btpItemFormaPagamentoCompra);
            this.lstBtpItemFormaPagamentoCompra.splice(indice, 1);
          }

        }
      }

    });
    modalItem.present();
  }

  abrirModalFormaPagamento(fab: FabContainer) {

    if (this.listBtpItemProdutoCompra == null || this.listBtpItemProdutoCompra.length == 0) {
      this.mensagemProvider.addMensagemErro("Adicione um produto primeiro! ", 3000, 'top');
      return;
    }

    let valorRestantePagamento = this.getTotalVlrCompra(this.listBtpItemProdutoCompra) - this.getTotalVlrParcelas();
    valorRestantePagamento = Number(valorRestantePagamento.toFixed(2));

    let modalItemFormaPg = this.modalCtrl.create(ItemFormaPagamentoCompraAddPage, { btpCompra: this.btpCompra, listBtpItemProdutoCompra: this.listBtpItemProdutoCompra, valorRestantePagamento: valorRestantePagamento, dataCompra: this.btpCompra.datCompraString });
    modalItemFormaPg.onDidDismiss(data => {

      if (this.lstBtpItemFormaPagamentoCompra == null || this.lstBtpItemFormaPagamentoCompra[0] == null) {

        this.lstBtpItemFormaPagamentoCompra = new Array<BtpItemFormaPagamentoCompra>();
      }

      if (data != null && data.lstBtpItemFormaPagamentoCompra != "" && data.lstBtpItemFormaPagamentoCompra !== null) {

        data.lstBtpItemFormaPagamentoCompra.forEach(elemento => {

          this.formaPagamentoService.getFormaPagamento(elemento.btpFormaPagamento.idFormaPagamento).then((formaPagamento) => {
            elemento.btpFormaPagamento = formaPagamento;
            this.lstBtpItemFormaPagamentoCompra.push(elemento);
          });

        })

      }
    });
    modalItemFormaPg.present();
    fab.close();
  }

  getAllItemFormaPgtoCompra() {
    this.storage.get('idUsuarioLogado').then((data) => {
      this.compraService.getItensFormaPagamento(this.btpCompra.idCompra)
        .then(list => {
          this.lstBtpItemFormaPagamentoCompra = list;
        });
    });
  }

  getTotalVlrCompra(listBtpItemProdutoCompra: Array<BtpItemProdutoCompra>) {
    if (listBtpItemProdutoCompra != null && listBtpItemProdutoCompra.length > 0) {
      let resposta: number;
      resposta = 0;
      for (let btpItemProdutoCompra of listBtpItemProdutoCompra) {
        resposta = resposta + btpItemProdutoCompra.vlrTotalItemCompra;
      }
      return resposta;
    }
    return 0;
  }

  getTotalVlrParcelas() {
    let resposta: number = 0;

    if (this.lstBtpItemFormaPagamentoCompra != null && this.lstBtpItemFormaPagamentoCompra.length > 0) {
      for (let pagamento of this.lstBtpItemFormaPagamentoCompra) {
        resposta = resposta + pagamento.vlrParcela;
      }
    }
    return resposta;
  }

  toggleDetails() {
    if (this.showDetails) {
      this.showDetails = false;
    } else {
      this.showDetails = true;
    }
    console.log(this.showDetails);
  }

  fecharCompra() {
    if (this.validarFecharCompra()) {

      this.compraService.alterarStatusCompra(StatusCompra.FECHADO, this.btpCompra.idCompra).then(response => {
        console.log("success alterar status compra");

        this.lstBtpItemFormaPagamentoCompra.forEach(elemento => {
          let btpDespesa: BtpDespesa = new BtpDespesa();
          btpDespesa.despesaVlr = elemento.vlrParcela;
          btpDespesa.despesaDta = elemento.datVencimento;
          btpDespesa.btpCompra = this.btpCompra;
          btpDespesa.despesaPaga = false;
          btpDespesa.categoria = new Categoria();
          btpDespesa.btpConta = new BtpConta();

          this.storage.get('usuarioLogado').then((data) => {
            btpDespesa.usuario = new Usuario();
            btpDespesa.usuario.idUsuario = data._idUsuario;
            btpDespesa.usuario.nomeUsuario = data._nomeUsuario;

            this.despesaService.incluir(btpDespesa);
          });

          btpDespesa.despesa = "Despesa gerada a partir de uma compra.";

          this.incluirAlterarListas();

        });

      }).catch(error => {
        console.log("erro alterar status compra");
      });

      this.viewCtrl.dismiss();

    }
  }

  fecharCompraIgor() {


    if (!this.validarFecharCompra()) {
      return;
    }

    this.utils.getUsuarioLogado().then((usuario) => {
      this.btpCompra.usuario = usuario;
      this.btpCompra.statusCompra = StatusCompra.FECHADO;
      this.btpCompra.datCompra = new Date(this.btpCompra.datCompraString);

      if (this.btpCompra.idCompra == null) {

        this.compraService.incluirCompra(this.btpCompra)
          .then(idInserido => {
            console.log("success incluir despesa");
            this.btpCompra.idCompra = idInserido;

            let promises = [];

            promises.push(this.incluirAlterarListas());
            promises.push(this.incluirDespesas());
            promises.push(this.adicionarQtdEstoque(this.listBtpItemProdutoCompra));

            Promise.all(promises).then(() => {
              this.enviarComprasServidor();
            });

          }).catch((error: Error) => {
            console.log("erro incluir despesa");
            console.error(error.message);
          });

      } else {
        this.compraService.alterarCompra(this.btpCompra).then(response => {
          console.log("success alterar compra");

          let promises = [];

          promises.push(this.incluirAlterarListas());
          promises.push(this.incluirDespesas());
          promises.push(this.adicionarQtdEstoque(this.listBtpItemProdutoCompra));

          Promise.all(promises).then(() => {
            this.enviarComprasServidor();
          });

        }).catch(error => {
          console.log("erro alterar compra");
        });
      }
    });
  }

  incluirDespesas() {
    let promises = [];

    this.lstBtpItemFormaPagamentoCompra.forEach(elemento => {
      let btpDespesa: BtpDespesa = new BtpDespesa();
      btpDespesa.despesaVlr = elemento.vlrParcela;
      btpDespesa.despesaDtaString = elemento.datVencimentoString;
      btpDespesa.btpCompra = this.btpCompra;
      btpDespesa.despesaPaga = false;
      btpDespesa.categoria = new Categoria();
      btpDespesa.btpConta = new BtpConta();
      btpDespesa.despesa = "COMPRA - " + this.btpCompra.btpFornecedor.nomeFornecedor;
      btpDespesa.isServidor = false;

      btpDespesa.usuario = new Usuario();
      btpDespesa.usuario = this.btpCompra.usuario;

      promises.push(this.compraService.incluirDespesa(btpDespesa));

    });

    return Promise.all(promises);
  }

  adicionarQtdEstoque(listBtpItemProdutoCompra: Array<BtpItemProdutoCompra>) {

    if (listBtpItemProdutoCompra != null && listBtpItemProdutoCompra[0] != null) {
      listBtpItemProdutoCompra.forEach(elemento => {

        console.log(elemento.btpEstoque.idEstoque + " estoque");

        if (elemento.btpEstoque != null && elemento.btpEstoque.idEstoque != null) {
          this.estoqueService.consultarItemEstoquePorCodProdutoCodEstoque(elemento.btpEstoque.idEstoque, elemento.produto.produtoId).then(lstBtpItemEstoque => {
            let qtdNovoEstoque: number = 0;
            let btpItemEstoque: BtpItemEstoque = new BtpItemEstoque();

            let promises = [];

            lstBtpItemEstoque.forEach(elementBtpItemEstoque => {
              qtdNovoEstoque = Number(elementBtpItemEstoque.qtdEstoque) + Number(elemento.qtdItemCompra);

              btpItemEstoque.qtdEstoque = qtdNovoEstoque;
              btpItemEstoque.idItemEstoque = elementBtpItemEstoque.idItemEstoque;
              btpItemEstoque.ativo = elementBtpItemEstoque.ativo;
              promises.push(this.estoqueService.alterarItemEstoque(btpItemEstoque));
            });

            promises.push(this.estoqueService.alterarEstoqueIsServidor(elemento.btpEstoque.idEstoque, false));

            return Promise.all(promises);
          })
        }
      })
    }
    return Promise.resolve(null);
  }

  validarFecharCompra() {

    let resposta: boolean = true;

    if (this.listBtpItemProdutoCompra == null || this.listBtpItemProdutoCompra[0] == null) {
      this.mensagemProvider.addMensagemErro("Adicione ao menos um produto!", 3000, "top");
      return false;
    }

    if (this.lstBtpItemFormaPagamentoCompra != null && this.lstBtpItemFormaPagamentoCompra[0] != null) {

      let isDataVendaCompraMaiorVencimento: boolean = false;

      let totalFormaPgt: number = 0;
      //Obtem o valor total da forma de pagamento
      this.lstBtpItemFormaPagamentoCompra.forEach(element => {
        totalFormaPgt = totalFormaPgt + element.vlrParcela;

        if (this.btpCompra.datCompra > element.datVencimento) {
          isDataVendaCompraMaiorVencimento = true;
        }
      });
      totalFormaPgt = Number(totalFormaPgt.toFixed(2));

      if (isDataVendaCompraMaiorVencimento) {
        this.mensagemProvider.addMensagemErro("A data da compra não pode ser maior que a data do vencimento de uma forma de pagamento!", 3000, "top");
        return false;
      }

      let totalCompra: number = 0;
      totalCompra = this.getTotalVlrCompra(this.listBtpItemProdutoCompra);
      totalCompra = Number(totalCompra.toFixed(2));

      //Obtem o valor total da compra
      console.log(totalFormaPgt);
      console.log(totalCompra);
      if (totalFormaPgt != totalCompra) {
        this.mensagemProvider.addMensagemErro("O valor dos pagamentos deve ser igual ao valor venda!", 3000, "top");
        return false;
      }

    } else {
      this.mensagemProvider.addMensagemErro("Adicione uma forma de pagamento!", 3000, "top");
      return false;
    }

    return resposta;
  }

  visualizarProdutos() {
    this.slides.slideTo(0, 200);
  }

  visualizarParcelas() {
    this.slides.slideTo(1, 200);
  }

  incluirAlterarListas() {

    let promises = [];

    //Incluindo valores dos produtos.
    if (this.listBtpItemProdutoCompra != null && this.listBtpItemProdutoCompra[0] != null) {
      this.listBtpItemProdutoCompra.forEach(elemento => {
        elemento.btpCompra = this.btpCompra;
        if (elemento.idItemCompra == null) {
          promises.push(this.compraService.incluirItemProdutoCompra(elemento));
        }
      })
    }

    if (this.listBtpItemProdutoCompraRemover != null && this.listBtpItemProdutoCompraRemover[0] != null) {
      this.listBtpItemProdutoCompraRemover.forEach(elemento => {
        if (elemento.idItemCompra != null) {
          elemento.ativo = false;
          promises.push(this.compraService.alterarItemProdutoCompra(elemento));
        }
      })
    }

    if (this.listBtpItemProdutoCompraAlterar != null && this.listBtpItemProdutoCompraAlterar[0] != null) {
      this.listBtpItemProdutoCompraAlterar.forEach(elemento => {
        if (elemento.idItemCompra != null) {
          elemento.ativo = true;
          promises.push(this.compraService.alterarItemProdutoCompra(elemento));
        }
      })
    }

    if (this.lstBtpItemFormaPagamentoCompra != null && this.lstBtpItemFormaPagamentoCompra[0] != null) {
      this.lstBtpItemFormaPagamentoCompra.forEach(elemento => {
        elemento.btpCompra = this.btpCompra;

        if (elemento.itemId == null) {
          promises.push(this.compraService.incluirItemFormaPagamentoCompra(elemento));
        }
      })
    }

    if (this.lstBtpItemFormaPagamentoCompraAlterar != null && this.lstBtpItemFormaPagamentoCompraAlterar[0] != null) {
      this.lstBtpItemFormaPagamentoCompraAlterar.forEach(elemento => {
        if (elemento.itemId != null) {
          elemento.ativo = true;
          promises.push(this.compraService.alterarItemFormaPagamentoCompra(elemento));
        }
      })
    }

    if (this.lstBtpItemFormaPagamentoCompraRemover != null && this.lstBtpItemFormaPagamentoCompraRemover[0] != null) {
      this.lstBtpItemFormaPagamentoCompraRemover.forEach(elemento => {
        if (elemento.itemId != null) {
          elemento.ativo = false;
          promises.push(this.compraService.alterarItemFormaPagamentoCompra(elemento));
        }
      })
    }

    return Promise.all(promises);
  }

  rangeAnosData(): Array<number> {
    let data = new Date()
    let anoAtual: number = data.getFullYear();

    let anos: Array<number> = new Array();
    anos.push(anoAtual - 1);
    anos.push(anoAtual);
    for (var _i = 0; _i < 4; _i++) {
      anoAtual++;
      anos.push(anoAtual);
    }
    return anos;
  }

  private enviarComprasServidor() {
    return this.compraWs.enviarComprasServidor().then(() => {
      this.viewCtrl.dismiss();
      this.utils.closeLoading();
    }).catch((err) => {
      console.log('err enviar Compras ws');
      console.error(err.message);
      this.viewCtrl.dismiss();
      this.utils.closeLoading();
    });
  }

}