import { PopupListarFornecedorPage } from '../../popup-listar-fornecedor/popup-listar-fornecedor';
import { CompraService } from './../../../providers/compra/compra-service';
import { Storage } from '@ionic/storage';

import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { BtpCompra } from "../../../model/btpCompra";
import { CompraAddPage } from "../compra-add/compra-add";
import { BtpFornecedor } from "../../../model/fornecedor";

@Component({
  selector: 'compra',
  templateUrl: 'compra.html'
})
export class CompraPage {

  btnSearchClicked: boolean;
  listBtpCompra: Array<BtpCompra>;
  listBtpCompraSemFiltro: Array<BtpCompra>;
  filtroCompra: string = "";
  public meses = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho",
    "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
  public filtroData: Date = new Date();

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public modalCtrl: ModalController, public compraService: CompraService,
    public storage: Storage) {
    if (this.listBtpCompra == null) {
      this.listBtpCompra = new Array();
    }
  }

  ionViewDidLoad() {
    this.getAllCompra();
  }

  getAllCompra() {
    this.storage.get('idUsuarioLogado').then((data) => {
      this.compraService.getAllByDate(data, this.filtroData)
        .then(listBtpCompra => {
          this.listBtpCompra = listBtpCompra;
          this.listBtpCompraSemFiltro = listBtpCompra;
        });
    });
  }

  incluir() {

    let modal = this.modalCtrl.create(PopupListarFornecedorPage);
    modal.onDidDismiss(data => {

      if (data != null) {
        let btpFornecedor: BtpFornecedor = data;
        let compraModal = this.modalCtrl.create(CompraAddPage, { btpFornecedor: btpFornecedor });
        this.abrirModalCompra(compraModal);
      }
    });
    modal.present();
  }

  editar(btpCompra) {
    console.log("id" + btpCompra.idCompra);
    let compraModal = this.modalCtrl.create(CompraAddPage, { btpCompra: btpCompra });
    this.abrirModalCompra(compraModal);
  }

  private abrirModalCompra(modal) {
    modal.onDidDismiss(data => {
      this.getAllCompra();
    });
    modal.present();
  }

  clickLupa() {
    if (this.btnSearchClicked) {
      this.btnSearchClicked = false;
      document.getElementById("barraBusca").style.left = '100%';
    } else {
      this.btnSearchClicked = true;
      this.animarSearchBar();
    }
  }

  animarSearchBar() {
    var elem = document.getElementById("barraBusca");
    var pos = 100;
    var id = setInterval(frame, 1);
    function frame() {
      if (pos == 0) {
        clearInterval(id);
      } else {
        pos = pos - 10;
        //elem.style.width = pos + '%';
        elem.style.left = pos + '%';
      }
    }
  }

  incrementaData() {
    this.filtroData.setMonth(this.filtroData.getMonth() + 1);
    this.getAllCompra();
  }

  decrementaData() {
    this.filtroData.setMonth(this.filtroData.getMonth() - 1);
    this.getAllCompra();
  }

  filtrarCompras() {
    this.listBtpCompra = this.listBtpCompraSemFiltro.filter((item) => {
      return item.btpFornecedor.nomeFornecedor.toLowerCase().indexOf(this.filtroCompra.toLowerCase()) > -1;

    });
  }

  calcularTotalCompras() {
    let soma: number = 0;
    this.listBtpCompra.forEach(compra => {
      soma = soma + compra.vlrTotal;
    });
    return soma;
  }

}
