import { Produto } from './../../../model/produto';
import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams, ViewController } from 'ionic-angular';

import { CompraService } from './../../../providers/compra/compra-service';
import { Validators, FormBuilder } from '@angular/forms';
import { BtpItemProdutoCompra } from "../../../model/btpItemProdutoCompra";

import { BtpEstoque } from "../../../model/btpEstoque";
import { UtilsService } from "../../../providers/utils-service";
import { EstoqueService } from '../../../providers/estoque/estoque-service';

@Component({
  selector: 'item-produto_compra-add',
  templateUrl: 'item-produto_compra-add.html'
})
export class ItemProdutoCompraAddPage {

  btpItemProdutoCompra: BtpItemProdutoCompra;
  formItemProdutoCompra: any;
  statusCrud: string;
  lstBtpEstoque: Array<BtpEstoque>
  bloqueiaAlteracao: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public viewCtrl: ViewController, public compraService: CompraService,
    public storage: Storage, public formBuilder: FormBuilder, public estoqueService: EstoqueService, 
    public utils: UtilsService) {
    this.btpItemProdutoCompra = this.navParams.get('btpItemProdutoCompra');
    this.statusCrud = this.navParams.get('statusCrud');
    this.bloqueiaAlteracao = this.navParams.get('bloqueiaAlteracao');

    if (this.btpItemProdutoCompra == null) {
      this.btpItemProdutoCompra = new BtpItemProdutoCompra();
      let produto: Produto = this.navParams.get('produto');
      this.btpItemProdutoCompra.produto = produto;
      this.btpItemProdutoCompra.vlrItemCompra = produto.precoUnitario;
      this.btpItemProdutoCompra.btpCompra = this.navParams.get('btpCompra');
    }

    if (this.btpItemProdutoCompra.btpEstoque == null) {
      this.btpItemProdutoCompra.btpEstoque = new BtpEstoque();
    }

    this.formItemProdutoCompra = this.formBuilder.group({
      qtdItemCompra: [this.btpItemProdutoCompra.qtdItemCompra, Validators.compose([Validators.required])],
      idEstoque: [this.btpItemProdutoCompra.btpEstoque.idEstoque],
      vlrItemCompra: [this.btpItemProdutoCompra.vlrItemCompra, Validators.compose([Validators.required])]
    });

  }

  ionViewDidLoad() {
    this.getLstEstoque();
  }

  fechar() {
    this.viewCtrl.dismiss();
  }

  salvarProdutoCompra() {

    if (this.btpItemProdutoCompra.idItemCompra == null) {
      this.incluirItemCompra();
    } else {
      this.alterarItemCompra();
    }
  }

  incluirItemCompra() {
    this.utils.getUsuarioLogado().then(usuario => {
      this.btpItemProdutoCompra.usuario = usuario;

      this.btpItemProdutoCompra.qtdItemCompra = this.formItemProdutoCompra.value.qtdItemCompra;
      this.btpItemProdutoCompra.vlrItemCompra = this.formItemProdutoCompra.value.vlrItemCompra;

      this.btpItemProdutoCompra.vlrTotalItemCompra = this.btpItemProdutoCompra.qtdItemCompra * this.btpItemProdutoCompra.vlrItemCompra;

      console.log(this.btpItemProdutoCompra.vlrTotalItemCompra);

      if (this.btpItemProdutoCompra.btpEstoque == null) {
        this.btpItemProdutoCompra.btpEstoque = new BtpEstoque();
      }

      this.btpItemProdutoCompra.btpEstoque.idEstoque = this.formItemProdutoCompra.value.idEstoque;

      let resposta = { 'btpItemProdutoCompra': this.btpItemProdutoCompra };
      this.viewCtrl.dismiss(resposta);

    });
  }

  alterarItemCompra() {

    this.btpItemProdutoCompra.qtdItemCompra = this.formItemProdutoCompra.value.qtdItemCompra;
    this.btpItemProdutoCompra.vlrItemCompra = this.formItemProdutoCompra.value.vlrItemCompra;
    this.btpItemProdutoCompra.vlrTotalItemCompra = this.btpItemProdutoCompra.qtdItemCompra * this.btpItemProdutoCompra.vlrItemCompra;

    if (this.btpItemProdutoCompra.btpEstoque == null) {
      this.btpItemProdutoCompra.btpEstoque = new BtpEstoque();
    }
    this.btpItemProdutoCompra.btpEstoque.idEstoque = this.formItemProdutoCompra.value.idEstoque;

    let resposta = { 'btpItemProdutoCompra': this.btpItemProdutoCompra, 'statusCrud': "alterar" };
    this.viewCtrl.dismiss(resposta);
  }

  deletarItemCompra() {

    let resposta = { 'btpItemProdutoCompra': this.btpItemProdutoCompra, 'statusCrud': "deletar" };
    this.viewCtrl.dismiss(resposta);

  }

  getLstEstoque() {
    if (this.btpItemProdutoCompra != null && this.btpItemProdutoCompra.produto != null) {

      this.estoqueService.consultarEstoquePorProduto(this.btpItemProdutoCompra.produto.produtoId).then(listEstoque => {
        this.lstBtpEstoque = listEstoque;
      }).catch((error: Error) => {
        console.log("erro consultar estoque");
        console.error(error.message);
      })
    }
  }

}