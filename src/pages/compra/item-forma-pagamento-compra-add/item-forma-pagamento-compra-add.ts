import { MensagemProvider } from './../../../providers/mensagem-provider';
import { Usuario } from './../../../model/usuario';
import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AlertController, NavController, NavParams, ViewController } from 'ionic-angular';

import { Validators, FormBuilder } from '@angular/forms';
import { BtpFormaPagamento } from "../../../model/btpFormaPagamento";
import { FormaPagamentoService } from "../../../providers/forma-pagamento/forma-pagamento-service";
import { CompraService } from "../../../providers/compra/compra-service";
import { BtpItemFormaPagamentoCompra } from "../../../model/btpItemFormaPagamentoCompra";
import { BtpItemProdutoCompra } from "../../../model/btpItemProdutoCompra";
import * as moment from 'moment';
import { UtilsService } from "../../../providers/utils-service";

@Component({
  selector: 'item-forma-pagamento-compra-add',
  templateUrl: 'item-forma-pagamento-compra-add.html'
})
export class ItemFormaPagamentoCompraAddPage {
  comporaService: any;

  btpItemFormaPagamentoCompra: BtpItemFormaPagamentoCompra;
  formItemFormaPagamentoCompra: any;
  lstBtpFormaPagamento: Array<BtpFormaPagamento>;
  isExibeQtdParcela: boolean;
  lstBtpItemFormaPagamentoCompra: Array<BtpItemFormaPagamentoCompra>;
  statusCrud: string;
  listBtpItemProdutoCompra: Array<BtpItemProdutoCompra>;
  valorRestantePagamento: number;
  bloqueiaAlteracao: boolean = false;
  dataCompra: Date;

  usuarioLogado: Usuario;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public viewCtrl: ViewController, public compraService: CompraService,
    public storage: Storage, public formBuilder: FormBuilder, public formaPagamentoService: FormaPagamentoService,
    public alertCtrl: AlertController, public mensagemProvider:MensagemProvider, public utils:UtilsService) {
    this.btpItemFormaPagamentoCompra = this.navParams.get('btpItemFormaPagamentoCompra');
    this.listBtpItemProdutoCompra = this.navParams.get('listBtpItemProdutoCompra');
    this.statusCrud = this.navParams.get('statusCrud');
    this.valorRestantePagamento = this.navParams.get('valorRestantePagamento');
    this.bloqueiaAlteracao = this.navParams.get('bloqueiaAlteracao');
    this.dataCompra = new Date(this.navParams.get('dataCompra'));

    utils.getUsuarioLogado().then((usuario) => {
      this.usuarioLogado = usuario;
    });


    if (this.btpItemFormaPagamentoCompra == null) {
      this.btpItemFormaPagamentoCompra = new BtpItemFormaPagamentoCompra();
      this.btpItemFormaPagamentoCompra.btpCompra = this.navParams.get('btpCompra');
      this.btpItemFormaPagamentoCompra.datPagamentoString = moment().format();
      this.btpItemFormaPagamentoCompra.datVencimentoString = moment().format();
      this.btpItemFormaPagamentoCompra.numParcela = 1;
      this.btpItemFormaPagamentoCompra.vlrParcela = this.valorRestantePagamento
    }

    if (this.lstBtpItemFormaPagamentoCompra == null) {
      this.lstBtpItemFormaPagamentoCompra = new Array<BtpItemFormaPagamentoCompra>();
    }

    if (this.btpItemFormaPagamentoCompra.btpFormaPagamento == null) {
      this.btpItemFormaPagamentoCompra.btpFormaPagamento = new BtpFormaPagamento();
    }

    if (this.btpItemFormaPagamentoCompra.itemId != null && this.btpItemFormaPagamentoCompra.itemId != 0) {
      this.btpItemFormaPagamentoCompra.datPagamentoString = new Date(this.btpItemFormaPagamentoCompra.datPagamento).toISOString();
    }

    if (this.isExibeQtdParcela == null) {
      this.isExibeQtdParcela = false;
    }

    this.formItemFormaPagamentoCompra = this.formBuilder.group({
      numParcela: [this.btpItemFormaPagamentoCompra.numParcela, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(24)])],
      datVencimento: [this.btpItemFormaPagamentoCompra.datVencimentoString, Validators.compose([Validators.required])],
      datPagamento: [this.btpItemFormaPagamentoCompra.datPagamentoString, Validators.compose([Validators.required])],
      vlrParcela: [this.btpItemFormaPagamentoCompra.vlrParcela, Validators.compose([Validators.required])],
      idFormaPagamento: [this.btpItemFormaPagamentoCompra.btpFormaPagamento.idFormaPagamento, Validators.compose([Validators.required])],
      dscObservacao: [this.btpItemFormaPagamentoCompra.dscObservacao],
      isExibeQtdParcela: [this.isExibeQtdParcela]
    });
  }

  getTotalVlrCompra(listBtpItemProdutoCompra: Array<BtpItemProdutoCompra>) {
    if (listBtpItemProdutoCompra != null && listBtpItemProdutoCompra.length > 0) {
      let resposta: number;
      resposta = 0;
      for (let btpItemProdutoCompra of listBtpItemProdutoCompra) {
        resposta = resposta + btpItemProdutoCompra.vlrTotalItemCompra;
      }
      return resposta;
    }
    return 0;
  }

  ionViewDidLoad() {
    this.getLstBtpFormaPagamento();
  }

  fechar() {
    this.viewCtrl.dismiss();
  }

  salvaItemFormaPagamentoCompra() {

    //recuperar valores do form
    this.btpItemFormaPagamentoCompra.numParcela = this.formItemFormaPagamentoCompra.value.numParcela;
    this.btpItemFormaPagamentoCompra.datVencimentoString = this.formItemFormaPagamentoCompra.value.datVencimento;
    this.btpItemFormaPagamentoCompra.datPagamentoString = this.formItemFormaPagamentoCompra.value.datPagamento;
    this.btpItemFormaPagamentoCompra.vlrParcela = Number(this.formItemFormaPagamentoCompra.value.vlrParcela);
    this.btpItemFormaPagamentoCompra.dscObservacao = this.formItemFormaPagamentoCompra.value.dscObservacao;

    this.btpItemFormaPagamentoCompra.datVencimento = new Date(this.btpItemFormaPagamentoCompra.datVencimentoString);
    this.btpItemFormaPagamentoCompra.datPagamento = new Date(this.btpItemFormaPagamentoCompra.datPagamentoString);

    if (this.dataCompra > this.btpItemFormaPagamentoCompra.datVencimento) {
      this.mensagemProvider.addMensagemErro("A data da compra não pode ser do maior que a data do vencimento!", 3000, "top");
      return;
    }

    if (this.btpItemFormaPagamentoCompra.btpFormaPagamento == null) {
      this.btpItemFormaPagamentoCompra.btpFormaPagamento = new BtpFormaPagamento();
    }

    this.btpItemFormaPagamentoCompra.btpFormaPagamento.idFormaPagamento = this.formItemFormaPagamentoCompra.value.idFormaPagamento;

    if (this.btpItemFormaPagamentoCompra.itemId == null) {

      if (this.btpItemFormaPagamentoCompra.numParcela == null || this.btpItemFormaPagamentoCompra.numParcela == 0) {
        this.btpItemFormaPagamentoCompra.numParcela = 1;
      }

      let btpItemFormaPagamentoCompraIncluir: BtpItemFormaPagamentoCompra;
      if (this.btpItemFormaPagamentoCompra.numParcela > 1) {
        let qtdParcela: number;
        let vlrParcela: number;
        let index: number = 0;

        qtdParcela = this.btpItemFormaPagamentoCompra.numParcela;
        let vlrTotal: number = this.btpItemFormaPagamentoCompra.vlrParcela;
        vlrParcela = this.btpItemFormaPagamentoCompra.vlrParcela;
        vlrParcela = vlrParcela / qtdParcela;

        vlrParcela = Number(vlrParcela.toFixed(2));

        let somaParcela: number = 0;

        let dataVenc: Date;

        do {
          //Realizando o parcelamento
          index++;

          btpItemFormaPagamentoCompraIncluir = this.atribuirValoresBtpItemFormaPagamento();
          btpItemFormaPagamentoCompraIncluir.vlrParcela = vlrParcela;
          btpItemFormaPagamentoCompraIncluir.itemId = null;
          btpItemFormaPagamentoCompraIncluir.dscObservacao = "Parcela " + index + "." + (btpItemFormaPagamentoCompraIncluir.dscObservacao == null ? '' : btpItemFormaPagamentoCompraIncluir.dscObservacao);
          qtdParcela = qtdParcela - 1;

          if (qtdParcela == 0) {
            btpItemFormaPagamentoCompraIncluir.vlrParcela = Number((vlrTotal - somaParcela).toFixed(2));

          } else {
            somaParcela = somaParcela + vlrParcela;
          }

          //Somando 1 mes para cada parcela
          dataVenc = new Date();
          dataVenc.setUTCFullYear(this.btpItemFormaPagamentoCompra.datVencimento.getUTCFullYear());
          dataVenc.setUTCDate(this.btpItemFormaPagamentoCompra.datVencimento.getUTCDate());
          dataVenc.setUTCMonth(this.btpItemFormaPagamentoCompra.datVencimento.getUTCMonth());

          //primeira parcela não deve somar
          if (btpItemFormaPagamentoCompraIncluir.numParcela != qtdParcela) {
            dataVenc.setUTCMonth(this.btpItemFormaPagamentoCompra.datVencimento.getUTCMonth() + (index - 1));

            btpItemFormaPagamentoCompraIncluir.datVencimento = dataVenc;
            btpItemFormaPagamentoCompraIncluir.datVencimentoString = moment(dataVenc).format();
          }
          this.incluirItemFormaPagamentoCompra(btpItemFormaPagamentoCompraIncluir);

        } while (qtdParcela > 0);
      } else {
        btpItemFormaPagamentoCompraIncluir = this.atribuirValoresBtpItemFormaPagamento();
        btpItemFormaPagamentoCompraIncluir.datVencimento = this.btpItemFormaPagamentoCompra.datVencimento;
        this.incluirItemFormaPagamentoCompra(btpItemFormaPagamentoCompraIncluir);
      }
      let retorno = { 'lstBtpItemFormaPagamentoCompra': this.lstBtpItemFormaPagamentoCompra };

      this.viewCtrl.dismiss(retorno);

    } else {
      let retorno = { 'btpItemFormaPagamentoCompra': this.btpItemFormaPagamentoCompra, 'statusCrud': this.statusCrud };

      this.viewCtrl.dismiss(retorno);
    }
  }

  atribuirValoresBtpItemFormaPagamento() {

    let btpItemFormaPagamentoCompraIncluir: BtpItemFormaPagamentoCompra;
    btpItemFormaPagamentoCompraIncluir = new BtpItemFormaPagamentoCompra();
    btpItemFormaPagamentoCompraIncluir.btpFormaPagamento = this.btpItemFormaPagamentoCompra.btpFormaPagamento;
    btpItemFormaPagamentoCompraIncluir.btpCompra = this.btpItemFormaPagamentoCompra.btpCompra;
    btpItemFormaPagamentoCompraIncluir.datPagamento = this.btpItemFormaPagamentoCompra.datPagamento;
    btpItemFormaPagamentoCompraIncluir.datPagamentoString = this.btpItemFormaPagamentoCompra.datPagamentoString;
    //btpItemFormaPagamentoCompraIncluir.datVencimento = this.btpItemFormaPagamentoCompra.datVencimento;
    btpItemFormaPagamentoCompraIncluir.datVencimentoString = this.btpItemFormaPagamentoCompra.datVencimentoString;
    btpItemFormaPagamentoCompraIncluir.dscObservacao = this.btpItemFormaPagamentoCompra.dscObservacao;
    btpItemFormaPagamentoCompraIncluir.itemId = this.btpItemFormaPagamentoCompra.itemId;
    btpItemFormaPagamentoCompraIncluir.numParcela = this.btpItemFormaPagamentoCompra.numParcela;
    btpItemFormaPagamentoCompraIncluir.usuario = this.btpItemFormaPagamentoCompra.usuario;
    btpItemFormaPagamentoCompraIncluir.vlrParcela = this.btpItemFormaPagamentoCompra.vlrParcela;
    btpItemFormaPagamentoCompraIncluir.ativo = this.btpItemFormaPagamentoCompra.ativo;

    return btpItemFormaPagamentoCompraIncluir;
  }

  incluirItemFormaPagamentoCompra(btpItemFormaPagamentoCompraIncluir: BtpItemFormaPagamentoCompra) {
    btpItemFormaPagamentoCompraIncluir.usuario = this.usuarioLogado;
    this.lstBtpItemFormaPagamentoCompra.push(btpItemFormaPagamentoCompraIncluir);
  }

  alterarItemFormaPagamentocompra() {

    let retorno = { 'btpItemFormaPagamentoCompra': this.btpItemFormaPagamentoCompra, 'statusCrud': "alterar" };

    this.viewCtrl.dismiss(retorno);
  }

  deletarItemFormaPagamentocompra() {
    this.btpItemFormaPagamentoCompra.ativo = false;
    let retorno = { 'btpItemFormaPagamentoCompra': this.btpItemFormaPagamentoCompra, 'statusCrud': "deletar" };

    this.viewCtrl.dismiss(retorno);
  }

  getLstBtpFormaPagamento() {
    this.storage.get('idUsuarioLogado').then((data) => {
      this.btpItemFormaPagamentoCompra.usuario = new Usuario();
      this.btpItemFormaPagamentoCompra.usuario.idUsuario = data;
      this.formaPagamentoService.getAll(this.btpItemFormaPagamentoCompra.usuario.idUsuario)
        .then(lst => {
          this.lstBtpFormaPagamento = lst;
        });
    });

  }

  exibirParcelamento() {

    if (this.isExibeQtdParcela == null || !this.isExibeQtdParcela) {
      this.isExibeQtdParcela = true;
    } else {
      this.isExibeQtdParcela = false;
    }
  }

  rangeAnosData(): Array<number> {
    let data = new Date()
    let anoAtual: number = data.getFullYear();

    let anos: Array<number> = new Array();
    anos.push(anoAtual - 1);
    anos.push(anoAtual);
    for (var _i = 0; _i < 4; _i++) {
      anoAtual++;
      anos.push(anoAtual);
    }
    return anos;
  }
}