import { ClienteAddPage } from './../cliente-add/cliente-add';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController } from 'ionic-angular';

import { BtpCliente } from './../../model/cliente';
import { Storage } from '@ionic/storage';
import { ClienteService } from './../../providers/cliente/cliente-service';

@Component({
  selector: 'page-listar-clientes',
  templateUrl: 'popup-listar-clientes.html'
})
export class PopupListarClientesPage {

  lstClientes: Array<BtpCliente>;
  lstClientesNoFilter: Array<BtpCliente>;
  filtroClienteNome: string = "";

  @ViewChild('search') myInput ;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public clienteService: ClienteService, public storage: Storage,
    public viewCtrl: ViewController, public modalCtrl: ModalController) {
    if (this.lstClientes == null) {
      this.lstClientes = new Array();
    }
  }

  ionViewDidLoad() {
    console.log("aberto popup listar clientes");
    this.getAllClientes();

    setTimeout(() => {
      this.myInput.setFocus();
    },150);
  }

  getAllClientes() {
    this.storage.get('idUsuarioLogado').then((data) => {
      this.clienteService.getAll(data)
        .then(clientes => {
          this.lstClientes = clientes;
          this.lstClientesNoFilter = clientes;
        });
    });
  }

  escolherCliente(btpCliente) {
    console.log("fechar popup listar clientes");
    this.viewCtrl.dismiss(btpCliente);
  }

  filtrarClientes() {
    this.lstClientes = this.lstClientesNoFilter.filter((item) => {
      return item.nome.toLowerCase().indexOf(this.filtroClienteNome.toLowerCase()) > -1;
    });
  }

  fechar() {
    this.viewCtrl.dismiss();
  }

  incluirNovoCliente() {
    let modal = this.modalCtrl.create(ClienteAddPage);
    modal.onDidDismiss(data => {
      this.getAllClientes();
    });
    modal.present();
  }

}
