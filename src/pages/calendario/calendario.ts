import { Storage } from '@ionic/storage';
import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController } from 'ionic-angular';
import * as moment from 'moment';

import { VisitaService } from './../../providers/visita/visita-service';
import { BtpReceita } from './../../model/btpReceita';
import { BtpDespesa } from './../../model/btpDespesa';
import { ReceitaService } from './../../providers/receita/receita-service';
import { DespesaService } from './../../providers/despesa/despesa-service';
import { BtpDataImportante } from './../../model/btpDataImportante';
import { PopoverCalendarioComponent } from './../../components/popover-calendario/popover-calendario';
import { BtpVisita } from './../../model/btpVisita';
import { EnumTipoCalendario } from '../../model/enum-tipo-calendario';
import { DataImportanteService } from '../../providers/data-importante/dataImportante-service';

@Component({
  selector: 'page-calendario',
  templateUrl: 'calendario.html'
})
export class CalendarioPage {

  tipoCalendario: EnumTipoCalendario;
  EnumTipoCalendario: typeof EnumTipoCalendario = EnumTipoCalendario;
  idUsuarioLogado: number;
  eventSource = [];
  viewTitle;
  currentMonth;
  colorVisita: 'red';
  colorDataImportante: 'blue';

  isToday: boolean;
  calendar = {
    mode: 'month',
    currentDate: new Date(),
    dateFormatter: {
      formatMonthViewDay: function (date: Date) {
        return date.getDate().toString();
      },
      formatMonthViewDayHeader: function (date: Date) {
        return 'MonMH';
      },
      formatMonthViewTitle: function (date: Date) {
        return 'testMT';
      }
    }
  };

  constructor(public navParams: NavParams, private navController: NavController, public visitaService: VisitaService,
    public despesaService: DespesaService, public receitaService: ReceitaService,
    public storage: Storage, public popoverCtrl: PopoverController, public dataImportanteService: DataImportanteService) {
    this.currentMonth = this.calendar.currentDate.getUTCMonth();
    this.inicializar();

    //carrega eventos aleatórios sem usar SQLite para teste 
    //this.loadEvents();
  }

  inicializar() {
    this.storage.get('idUsuarioLogado').then((data) => {
      this.idUsuarioLogado = data;
      this.tipoCalendario = EnumTipoCalendario.visita;
      this.carregarEventosPorTipo();
    });
  }

  /**
   * abre popOver para selecionar o tipo do calendário
   */
  abrirPopOver(myEvent) {
    let popover = this.popoverCtrl.create(PopoverCalendarioComponent);
    popover.onDidDismiss(data => {
      if (data != null) {
        this.tipoCalendario = data;
        this.carregarEventosPorTipo();
      }
    });
    popover.present({
      ev: myEvent
    });
  }

  /**
   * ao clicar em um dia verifica se possui algum evento
   */
  veriricarTemEvento(date: any) {
    return date.hasEvent;
  }

  /**
   * cria vários eventos randômicamente para teste
   */
  loadEvents() {
    this.eventSource = this.createRandomEvents();
  }

  createRandomEvents() {

    var events = [];
    for (var i = 0; i < 50; i += 1) {
      var date = new Date();
      var eventType = Math.floor(Math.random() * 2);
      var startDay = Math.floor(Math.random() * 90) - 45;
      var endDay = Math.floor(Math.random() * 2) + startDay;
      var startTime;
      var endTime;
      if (eventType === 0) {
        startTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + startDay));
        if (endDay === startDay) {
          endDay += 1;
        }
        endTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + endDay));
        events.push({
          title: 'All Day - ' + i,
          startTime: startTime,
          endTime: endTime,
          allDay: true,
          color: 'red'
        });
      } else {
        var startMinute = Math.floor(Math.random() * 24 * 60);
        var endMinute = Math.floor(Math.random() * 180) + startMinute;
        startTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + startDay, 0, date.getMinutes() + startMinute);
        endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + endDay, 0, date.getMinutes() + endMinute);
        events.push({
          title: 'Event - ' + i,
          startTime: startTime,
          endTime: endTime,
          allDay: false,
          color: 'blue'
        });
      }
    }
    return events;
  }

  diaTemEventoTipo(date: any, tipoCalendario: EnumTipoCalendario) {
    for (let event of date.events) {
      if (event.tipo == tipoCalendario) {
        return true;
      }
    }
    return false;
  }

  /**
   * função chamada ao clicar na seta para adicionar mês
   */
  adicionarMes() {
    this.calendar.currentDate = new Date(this.calendar.currentDate.getUTCFullYear(), this.calendar.currentDate.getUTCMonth() + 1, 1);
  }

  /**
     * função chamada ao clicar na seta para subtrair mês
     */
  subtrairMes() {
    this.calendar.currentDate = new Date(this.calendar.currentDate.getUTCFullYear(), this.calendar.currentDate.getUTCMonth() - 1, 1);
  }

  onViewTitleChanged(title) {
    this.viewTitle = title;
  }

  today() {
    this.calendar.currentDate = new Date();
  }

  onCurrentDateChanged(event: Date) {
    this.calendar.currentDate = event;

    var today = new Date();
    today.setHours(0, 0, 0, 0);
    event.setHours(0, 0, 0, 0);
    this.isToday = today.getTime() === event.getTime();

    if (this.currentMonth != this.calendar.currentDate.getUTCMonth()) {
      this.carregarEventosPorTipo();
      this.currentMonth = this.calendar.currentDate.getUTCMonth();
    }
  }

  /**
   * carrega os eventos pelo tipo do calendário
   */
  carregarEventosPorTipo() {
    switch (this.tipoCalendario) {
      case EnumTipoCalendario.visita: {
        this.carregarEventosVisitas().then(events => {
          this.eventSource = events;
        });
        break;
      }
      case EnumTipoCalendario.dataImportante: {
        this.carregarEventosDatasImportantes().then(events => {
          this.eventSource = events;
        });
        break;
      }
      case EnumTipoCalendario.despesa: {
        this.carregarEventosDespesas().then(events => {
          this.eventSource = events;
        });
        break;
      }
      case EnumTipoCalendario.receita: {
        this.carregarEventosReceitas().then(events => {
          this.eventSource = events;
        });
        break;
      }
      case EnumTipoCalendario.todos: {
        var events = [];
        this.carregarEventosVisitas().then(eventsVisitas => {
          for (let event of eventsVisitas) {
            events.push(event);
          }
          this.carregarEventosDatasImportantes().then(eventsDatasImportantes => {
            for (let event of eventsDatasImportantes) {
              events.push(event);
            }
            this.carregarEventosDespesas().then(eventsDespesas => {
              for (let event of eventsDespesas) {
                events.push(event);
              }
              this.carregarEventosReceitas().then(eventsReceitas => {
                for (let event of eventsReceitas) {
                  events.push(event);
                }
                this.eventSource = events;
              });
            });
          });
        });
      }
    }
  }

  /**
   * carrega eventos pelo CRUD de visitas
   */
  carregarEventosVisitas() {
    var events = [];

    return this.visitaService.getVisitasPorData(this.idUsuarioLogado, this.calendar.currentDate)
      .then(listVisitas => {
        let listEventosVisitas: Array<BtpVisita>;
        listEventosVisitas = listVisitas;
        for (let visita of listEventosVisitas) {

          events.push({
            title: 'Cliente ' + visita.btpCliente.nome,
            local: visita.localVisita,
            startTime: new Date(visita.dataVisita),
            endTime: new Date(visita.dataVisita),
            tipo: EnumTipoCalendario.visita
          });
        }
        return Promise.resolve(events);
      });
  }

  carregarEventosDespesas() {
    var events = [];

    return this.despesaService.getAllByDate(this.idUsuarioLogado, this.calendar.currentDate)
      .then(listDespesas => {
        let listEventosDespesas: Array<BtpDespesa>;
        listEventosDespesas = listDespesas;
        console.log("tamaho listA:" + listEventosDespesas.length);
        for (let despesa of listEventosDespesas) {

          events.push({
            title: "Despesa: " + despesa.despesa,
            valor: despesa.despesaVlr,
            startTime: new Date(despesa.despesaDta),
            endTime: new Date(despesa.despesaDta),
            tipo: EnumTipoCalendario.despesa
          });
        }
        return Promise.resolve(events);
      });
  }

  carregarEventosReceitas() {
    var events = [];

    return this.receitaService.getAllByDate(this.idUsuarioLogado, this.calendar.currentDate)
      .then(listReceitas => {
        let listEventosReceitas: Array<BtpReceita>;
        listEventosReceitas = listReceitas;
        console.log("tamanjo lista:" + listEventosReceitas.length);
        for (let receita of listEventosReceitas) {

          events.push({
            title: "Receita: " + receita.receita,
            valor: receita.receitaVlr,
            startTime: new Date(receita.receitaDta),
            endTime: new Date(receita.receitaDta),
            tipo: EnumTipoCalendario.receita
          });
        }
        return Promise.resolve(events);
      });
  }

  /**
   * carrega eventos pelo CRUD de datas importantes
   */
  carregarEventosDatasImportantes() {
    var events = [];

    return this.dataImportanteService.getDatasImportantesPorData(this.idUsuarioLogado, this.calendar.currentDate)
      .then(listDatasImportantes => {
        let listEventos: Array<BtpDataImportante>;
        listEventos = listDatasImportantes;
        for (let dataImportante of listEventos) {

          events.push({
            title: dataImportante.cliente.nome + ': ' + dataImportante.dscDataImportante,
            startTime: new Date(dataImportante.diaMes),
            endTime: new Date(dataImportante.diaMes),
            tipo: EnumTipoCalendario.dataImportante
          });
        }
        return Promise.resolve(events);
      });
  }

  /**
   * label no header do evento selecionado
   */
  headerEvents() {
    switch (this.tipoCalendario) {
      case (EnumTipoCalendario.visita): {
        return "Visitas agendadas"
      }
      case (EnumTipoCalendario.dataImportante): {
        return "Datas importantes"
      }
    }
  }

  /**
   * label no header da página
   */
  headerTitle() {
    switch (this.tipoCalendario) {
      case (EnumTipoCalendario.visita): {
        return " - Visitas"
      }
      case (EnumTipoCalendario.dataImportante): {
        return "- Datas importantes"
      }
      case (EnumTipoCalendario.despesa): {
        return "- Despesas"
      } 
      case (EnumTipoCalendario.receita): {
        return "- Receitas"
      }
    }
  }

  markDisabled = (date: Date) => {
    var current = new Date();
    current.setHours(0, 0, 0);
    return date < current;
  };

}
