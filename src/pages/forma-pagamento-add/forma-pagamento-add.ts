import { Storage } from '@ionic/storage';
import { FormBuilder, Validators } from '@angular/forms';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController } from 'ionic-angular';

import { BtpFormaPagamento } from './../../model/btpFormaPagamento';
import { MensagemProvider } from './../../providers/mensagem-provider';
import { UtilsService } from './../../providers/utils-service';
import { FormaPagamentoService } from './../../providers/forma-pagamento/forma-pagamento-service';
import { Usuario } from './../../model/usuario';
import { FormaPagamentoWsProvider } from "../../providers/forma-pagamento/forma-pagamento-ws";

@Component({
  selector: 'page-forma-pagamento-add',
  templateUrl: 'forma-pagamento-add.html'
})
export class FormaPagamentoAddPage {

  formCadastroFormaPagamento: any;
  btpFormaPagamento: BtpFormaPagamento;

  constructor(public navCtrl: NavController, public params: NavParams,
    public formBuilder: FormBuilder, public storage: Storage,
    public viewCtrl: ViewController, public alertCtrl: AlertController,
    public formaPagamentoService: FormaPagamentoService, public utils: UtilsService,
    public msgProvider: MensagemProvider, public formaPagamentoWS: FormaPagamentoWsProvider) {
    this.btpFormaPagamento = params.get('btpFormaPagamento');

    if (this.btpFormaPagamento == null) {
      this.btpFormaPagamento = new BtpFormaPagamento();
    }

    this.formCadastroFormaPagamento = formBuilder.group({
      descricao: [this.btpFormaPagamento.descricao, Validators.compose([Validators.required])]
    });
  }

  salvar() {
    if (this.formCadastroFormaPagamento.valid) {
      this.btpFormaPagamento.descricao = this.formCadastroFormaPagamento.value.descricao;
      if (this.btpFormaPagamento.idFormaPagamento == null) {
        this.incluir();
      } else {
        this.alterar();
      }
    }
  }

  incluir() {
    this.btpFormaPagamento.isServidor = false;
    this.utils.showLoading();

    this.utils.getUsuarioLogado().then(usuario => {
      this.btpFormaPagamento.usuario = usuario;

      this.formaPagamentoService.incluir(this.btpFormaPagamento)
        .then(response => {
          console.log("success incluir Forma pagamento");

          //após inserir conta no banco envia cliente para Servidor
          this.enviarFormasPagamentosServidor().then(() => {
            this.viewCtrl.dismiss();
            this.utils.closeLoading();
          }).catch((err) => {
            this.viewCtrl.dismiss();
            this.utils.closeLoading();
          });

        }).catch(error => {
          console.log("erro incluir forma pagamento");
          console.log(error.message)
          this.utils.closeLoading();

        });
    });


  }

  alterar() {

    this.btpFormaPagamento.isServidor = false;
    this.utils.showLoading();

    this.formaPagamentoService.update(this.btpFormaPagamento).then(response => {
      console.log("success alterar forma pagamento");

      //após inserir no banco envia FP para Servidor
      this.enviarFormasPagamentosServidor().then(() => {
        this.viewCtrl.dismiss();
        this.utils.closeLoading();
      }).catch((err) => {
        this.viewCtrl.dismiss();
        this.utils.closeLoading();
      });

    }).catch(error => {
      console.log("erro alterar forma pagamento");
    });
  }

  fecharModal() {
    this.viewCtrl.dismiss();
  }

  deletar() {

    let confirm = this.alertCtrl.create({
      title: 'Excluir',
      message: 'Deseja excluir forma de pagamento?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
            
            this.btpFormaPagamento.ativo = false;
            this.alterar();
          }
        }
      ]
    });
    confirm.present();
  }

  private enviarFormasPagamentosServidor() {
    return this.formaPagamentoWS.enviarFormasPagamentosServidor().catch((err) => {
      console.log('err enviar forma Pagamento ws');
      console.error(err.message);
      return Promise.reject(true);
    });
  }

}
