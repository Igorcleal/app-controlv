import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams, ViewController, AlertController } from 'ionic-angular';

import { Produto } from './../../../model/produto';
import { Usuario } from './../../../model/usuario';
import { VendaService } from './../../../providers/venda/venda-service';
import { BtpItemVenda } from './../../../model/btpItemVenda';
import { Validators, FormBuilder } from '@angular/forms';
import { BtpEstoque } from "../../../model/btpEstoque";
import { EstoqueService } from '../../../providers/estoque/estoque-service';

@Component({
  selector: 'item-venda-add',
  templateUrl: 'item-venda-add.html'
})
export class ItemVendaAddPage {

  btpItemVenda: BtpItemVenda;
  formItemVenda: any;
  statusCrud: string;
  lstBtpEstoque: Array<BtpEstoque>
  bloqueiaAlteracao: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public viewCtrl: ViewController, public vendaService: VendaService,
    public storage: Storage, public formBuilder: FormBuilder, public estoqueService: EstoqueService,
    public alertCtrl: AlertController) {
    this.btpItemVenda = this.navParams.get('btpItemVenda');
    this.statusCrud = this.navParams.get('statusCrud');
    this.bloqueiaAlteracao = this.navParams.get('bloqueiaAlteracao');


    if (this.btpItemVenda == null) {
      this.btpItemVenda = new BtpItemVenda();
      let produto: Produto = this.navParams.get('produto');
      this.btpItemVenda.produto = produto;
      this.btpItemVenda.vlrItemVenda = produto.precoUnitario;
      this.btpItemVenda.btpVenda = this.navParams.get('btpVenda');
    }

    if (this.btpItemVenda.btpEstoque == null) {
      this.btpItemVenda.btpEstoque = new BtpEstoque();
    }

    this.formItemVenda = this.formBuilder.group({
      qtdItemVenda: [this.btpItemVenda.qtdItemVenda, Validators.compose([Validators.required])],
      idEstoque: [this.btpItemVenda.btpEstoque.idEstoque],
      vlrItemVenda: [this.btpItemVenda.vlrItemVenda, Validators.compose([Validators.required])]
    });
  }

  ionViewDidLoad() {
    this.getLstEstoque();
  }

  fechar() {
    this.viewCtrl.dismiss();
  }

  salvarProdutoVenda() {

    if (this.btpItemVenda.idItemVenda == null) {
      this.incluirItemVenda();
    } else {
      this.alterarItemVenda();
    }
  }

  incluirItemVenda() {
    this.storage.get('usuarioLogado').then((data) => {
      this.btpItemVenda.usuario = new Usuario();
      this.btpItemVenda.usuario.idUsuario = data._idUsuario;
      this.btpItemVenda.usuario.nomeUsuario = data._nomeUsuario;
      this.btpItemVenda.ativo = true;

      this.btpItemVenda.qtdItemVenda = this.formItemVenda.value.qtdItemVenda;
      this.btpItemVenda.vlrItemVenda = this.formItemVenda.value.vlrItemVenda;

      this.btpItemVenda.vlrTotalItemVenda = this.btpItemVenda.qtdItemVenda * this.btpItemVenda.vlrItemVenda;

      if (this.btpItemVenda.btpEstoque == null) {
        this.btpItemVenda.btpEstoque = new BtpEstoque();
      }
      this.btpItemVenda.btpEstoque.idEstoque = this.formItemVenda.value.idEstoque;

      console.log(this.btpItemVenda.vlrTotalItemVenda);

      let resposta = { 'btpItemVenda': this.btpItemVenda };
      this.viewCtrl.dismiss(resposta);

    });
  }

  alterarItemVenda() {

    this.btpItemVenda.qtdItemVenda = this.formItemVenda.value.qtdItemVenda;
    this.btpItemVenda.vlrItemVenda = this.formItemVenda.value.vlrItemVenda;
    this.btpItemVenda.vlrTotalItemVenda = this.btpItemVenda.qtdItemVenda * this.btpItemVenda.vlrItemVenda;

    if (this.btpItemVenda.btpEstoque == null) {
      this.btpItemVenda.btpEstoque = new BtpEstoque();
    }
    this.btpItemVenda.btpEstoque.idEstoque = this.formItemVenda.value.idEstoque;
    console.log('idEstoque' + this.btpItemVenda.btpEstoque.idEstoque+'-------------');
    let resposta = { 'btpItemVenda': this.btpItemVenda, 'statusCrud': "alterar" };
    this.viewCtrl.dismiss(resposta);
  }

  deletarItemVenda() {
    let confirm = this.alertCtrl.create({
      title: 'Excluir',
      message: 'Deseja excluir esse produto?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
            this.btpItemVenda.ativo = false;
            let resposta = { 'btpItemVenda': this.btpItemVenda, 'statusCrud': "deletar" };
            this.viewCtrl.dismiss(resposta);
          }
        }
      ]
    });
    confirm.present();
  }

  getLstEstoque() {
    if (this.btpItemVenda != null && this.btpItemVenda.produto != null) {

      this.estoqueService.consultarEstoquePorProduto(this.btpItemVenda.produto.produtoId).then(listEstoque => {
        this.lstBtpEstoque = listEstoque;
      }).catch((error: Error) => {
        console.log("erro consultar estoque");
        console.error(error.message);
      })
    }
  }

}
