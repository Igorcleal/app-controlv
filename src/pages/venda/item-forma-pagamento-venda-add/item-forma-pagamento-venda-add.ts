import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AlertController, NavController, NavParams, ViewController } from 'ionic-angular';

import { MensagemProvider } from './../../../providers/mensagem-provider';
import { Usuario } from './../../../model/usuario';
import { VendaService } from './../../../providers/venda/venda-service';
import { BtpItemFormaPagamentoVenda } from './../../../model/btpItemFormaPagamentoVenda';
import { Validators, FormBuilder } from '@angular/forms';
import { BtpFormaPagamento } from "../../../model/btpFormaPagamento";
import { FormaPagamentoService } from "../../../providers/forma-pagamento/forma-pagamento-service";
import { BtpItemVenda } from "../../../model/btpItemVenda";
import * as moment from 'moment';
import { UtilsService } from "../../../providers/utils-service";


@Component({
  selector: 'item-forma-pagamento-venda-add',
  templateUrl: 'item-forma-pagamento-venda-add.html'
})
export class ItemFormaPagamentoVendaAddPage {

  btpItemFormaPagamentoVenda: BtpItemFormaPagamentoVenda;
  formItemFormaPagamentoVenda: any;
  lstBtpFormaPagamento: Array<BtpFormaPagamento>;
  isExibeQtdParcela: boolean;
  lstBtpItemFormaPagamentoVenda: Array<BtpItemFormaPagamentoVenda>;
  statusCrud: string;
  listBtpItemVenda: Array<BtpItemVenda>;
  valorRestantePagamento: number;
  bloqueiaAlteracao: boolean;
  dataVenda: Date;

  usuarioLogado: Usuario;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public viewCtrl: ViewController, public vendaService: VendaService,
    public storage: Storage, public formBuilder: FormBuilder, public formaPagamentoService: FormaPagamentoService,
    public alertCtrl: AlertController, public mensagemProvider: MensagemProvider, public utils:UtilsService) {
    this.btpItemFormaPagamentoVenda = this.navParams.get('btpItemFormaPagamentoVenda');
    this.listBtpItemVenda = this.navParams.get('listBtpItemVenda');
    this.statusCrud = this.navParams.get('statusCrud');
    this.valorRestantePagamento = this.navParams.get('valorRestantePagamento');
    this.bloqueiaAlteracao = this.navParams.get('bloqueiaAlteracao');
    this.dataVenda = new Date(this.navParams.get('dataVenda'));

    this.utils.getUsuarioLogado().then(usuario => {
      this.usuarioLogado = usuario;
    });

    if (this.btpItemFormaPagamentoVenda == null) {
      this.btpItemFormaPagamentoVenda = new BtpItemFormaPagamentoVenda();
      this.btpItemFormaPagamentoVenda.btpVenda = this.navParams.get('btpVenda');
      this.btpItemFormaPagamentoVenda.datPagamentoString = moment().format();
      this.btpItemFormaPagamentoVenda.datVencimentoString = moment().format();
      this.btpItemFormaPagamentoVenda.numParcela = 1;
      this.btpItemFormaPagamentoVenda.vlrParcela = this.valorRestantePagamento;
      console.log("valorRestante" + this.valorRestantePagamento)
    }

    if (this.lstBtpItemFormaPagamentoVenda == null) {
      this.lstBtpItemFormaPagamentoVenda = new Array<BtpItemFormaPagamentoVenda>();
    }

    if (this.btpItemFormaPagamentoVenda.btpFormaPagamento == null) {
      this.btpItemFormaPagamentoVenda.btpFormaPagamento = new BtpFormaPagamento();
    }

    if (this.btpItemFormaPagamentoVenda.itemId != null && this.btpItemFormaPagamentoVenda.itemId != 0) {
      this.btpItemFormaPagamentoVenda.datPagamentoString = new Date(this.btpItemFormaPagamentoVenda.datPagamento).toISOString();
    }

    if (this.isExibeQtdParcela == null) {
      this.isExibeQtdParcela = false;
    }

    this.formItemFormaPagamentoVenda = this.formBuilder.group({
      numParcela: [this.btpItemFormaPagamentoVenda.numParcela, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(24)])],
      datVencimento: [this.btpItemFormaPagamentoVenda.datVencimentoString, Validators.compose([Validators.required])],
      vlrParcela: [this.btpItemFormaPagamentoVenda.vlrParcela, Validators.compose([Validators.required])],
      idFormaPagamento: [this.btpItemFormaPagamentoVenda.btpFormaPagamento.idFormaPagamento, Validators.compose([Validators.required])],
      dscObservacao: [this.btpItemFormaPagamentoVenda.dscObservacao],
      isExibeQtdParcela: [this.isExibeQtdParcela]
    });
  }

  ionViewDidLoad() {
    this.getLstBtpFormaPagamento();
  }

  fechar() {
    this.viewCtrl.dismiss();
  }

  salvaItemFormaPagamentoVenda() {

    //recuperar valores do form
    this.btpItemFormaPagamentoVenda.numParcela = this.formItemFormaPagamentoVenda.value.numParcela;
    this.btpItemFormaPagamentoVenda.datVencimentoString = this.formItemFormaPagamentoVenda.value.datVencimento;
    this.btpItemFormaPagamentoVenda.vlrParcela = Number(this.formItemFormaPagamentoVenda.value.vlrParcela);
    this.btpItemFormaPagamentoVenda.dscObservacao = this.formItemFormaPagamentoVenda.value.dscObservacao;
    this.btpItemFormaPagamentoVenda.ativo = true;

    this.btpItemFormaPagamentoVenda.datVencimento = new Date(this.btpItemFormaPagamentoVenda.datVencimentoString);

    if (this.dataVenda > this.btpItemFormaPagamentoVenda.datVencimento) {
      this.mensagemProvider.addMensagemErro("A data da venda não pode ser maior que a data do vencimento!", 3000, "top");
      return;
    }

    if (this.btpItemFormaPagamentoVenda.btpFormaPagamento == null) {
      this.btpItemFormaPagamentoVenda.btpFormaPagamento = new BtpFormaPagamento();
    }

    this.btpItemFormaPagamentoVenda.btpFormaPagamento.idFormaPagamento = this.formItemFormaPagamentoVenda.value.idFormaPagamento;

    if ("alterar" != this.statusCrud) {

      if (this.btpItemFormaPagamentoVenda.numParcela == null || this.btpItemFormaPagamentoVenda.numParcela == 0) {
        this.btpItemFormaPagamentoVenda.numParcela = 1;
      }


      if (this.btpItemFormaPagamentoVenda.numParcela > 1) {
        let qtdParcela: number;
        let vlrParcela: number;
        let index: number = 0;

        qtdParcela = this.btpItemFormaPagamentoVenda.numParcela;
        let vlrTotal: number = this.btpItemFormaPagamentoVenda.vlrParcela;
        vlrParcela = this.btpItemFormaPagamentoVenda.vlrParcela;
        vlrParcela = vlrParcela / qtdParcela;

        vlrParcela = Number(vlrParcela.toFixed(2));

        let somaParcela: number = 0;

        let dataVenc: Date;

        do {

          //Realizando o parcelamento
          index++;

          let btpItemFormaPagamentoVendaIncluir: BtpItemFormaPagamentoVenda;
          btpItemFormaPagamentoVendaIncluir = this.atribuirValoresBtpItemFormaPagamento();
          btpItemFormaPagamentoVendaIncluir.vlrParcela = vlrParcela;
          btpItemFormaPagamentoVendaIncluir.itemId = null;
          btpItemFormaPagamentoVendaIncluir.dscObservacao = "Parcela " + index + "." + (btpItemFormaPagamentoVendaIncluir.dscObservacao == null ? '' : btpItemFormaPagamentoVendaIncluir.dscObservacao);
          qtdParcela = qtdParcela - 1;


          if (qtdParcela == 0) {
            btpItemFormaPagamentoVendaIncluir.vlrParcela = Number((vlrTotal - somaParcela).toFixed(2));
          } else {
            somaParcela = somaParcela + vlrParcela;
          }

          //Somando 1 mes para cada parcela
          dataVenc = new Date();
          dataVenc.setUTCFullYear(this.btpItemFormaPagamentoVenda.datVencimento.getUTCFullYear());
          dataVenc.setUTCDate(this.btpItemFormaPagamentoVenda.datVencimento.getUTCDate());
          dataVenc.setUTCMonth(this.btpItemFormaPagamentoVenda.datVencimento.getUTCMonth());


          //primeira parcela não deve somar
          if (btpItemFormaPagamentoVendaIncluir.numParcela != qtdParcela) {
            dataVenc.setUTCMonth(this.btpItemFormaPagamentoVenda.datVencimento.getUTCMonth() + (index - 1));

            btpItemFormaPagamentoVendaIncluir.datVencimento = dataVenc;
            btpItemFormaPagamentoVendaIncluir.datVencimentoString = moment(dataVenc).format();
          }
          this.incluirItemFormaPagamentoVenda(btpItemFormaPagamentoVendaIncluir);

        } while (qtdParcela > 0);
      } else {
        let btpItemFormaPagamentoVendaIncluir: BtpItemFormaPagamentoVenda;

        btpItemFormaPagamentoVendaIncluir = this.atribuirValoresBtpItemFormaPagamento();

        btpItemFormaPagamentoVendaIncluir.datVencimento = this.btpItemFormaPagamentoVenda.datVencimento;
        this.incluirItemFormaPagamentoVenda(btpItemFormaPagamentoVendaIncluir);
      }

      console.log(this.lstBtpItemFormaPagamentoVenda);
      let retorno = { 'lstBtpItemFormaPagamentoVenda': this.lstBtpItemFormaPagamentoVenda };

      this.viewCtrl.dismiss(retorno);

    } else {

      let retorno = { 'btpItemFormaPagamentoVenda': this.btpItemFormaPagamentoVenda, 'statusCrud': this.statusCrud };

      this.viewCtrl.dismiss(retorno);
    }
  }

  atribuirValoresBtpItemFormaPagamento() {

    let btpItemFormaPagamentoVendaIncluir: BtpItemFormaPagamentoVenda;
    btpItemFormaPagamentoVendaIncluir = new BtpItemFormaPagamentoVenda();
    btpItemFormaPagamentoVendaIncluir.btpFormaPagamento = this.btpItemFormaPagamentoVenda.btpFormaPagamento;
    btpItemFormaPagamentoVendaIncluir.btpVenda = this.btpItemFormaPagamentoVenda.btpVenda;
    btpItemFormaPagamentoVendaIncluir.datPagamento = this.btpItemFormaPagamentoVenda.datPagamento;
    btpItemFormaPagamentoVendaIncluir.datPagamentoString = this.btpItemFormaPagamentoVenda.datPagamentoString;
    //btpItemFormaPagamentoVendaIncluir.datVencimento = this.btpItemFormaPagamentoVenda.datVencimento;
    btpItemFormaPagamentoVendaIncluir.datVencimentoString = this.btpItemFormaPagamentoVenda.datVencimentoString;
    btpItemFormaPagamentoVendaIncluir.dscObservacao = this.btpItemFormaPagamentoVenda.dscObservacao;
    btpItemFormaPagamentoVendaIncluir.itemId = this.btpItemFormaPagamentoVenda.itemId;
    btpItemFormaPagamentoVendaIncluir.numParcela = this.btpItemFormaPagamentoVenda.numParcela;
    btpItemFormaPagamentoVendaIncluir.usuario = this.btpItemFormaPagamentoVenda.usuario;
    btpItemFormaPagamentoVendaIncluir.vlrParcela = this.btpItemFormaPagamentoVenda.vlrParcela;
    btpItemFormaPagamentoVendaIncluir.ativo = true;

    return btpItemFormaPagamentoVendaIncluir;
  }

  incluirItemFormaPagamentoVenda(btpItemFormaPagamentoVendaIncluir: BtpItemFormaPagamentoVenda) {

    btpItemFormaPagamentoVendaIncluir.usuario = this.usuarioLogado;
    btpItemFormaPagamentoVendaIncluir.ativo = true;
    this.lstBtpItemFormaPagamentoVenda.push(btpItemFormaPagamentoVendaIncluir);
  }

  alterarItemFormaPagamentoVenda() {
    let retorno = { 'btpItemFormaPagamentoVenda': this.btpItemFormaPagamentoVenda, 'statusCrud': "alterar" };

    this.viewCtrl.dismiss(retorno);
  }

  deletarItemFormaPagamentoVenda() {
    let retorno = { 'btpItemFormaPagamentoVenda': this.btpItemFormaPagamentoVenda, 'statusCrud': "deletar" };

    this.viewCtrl.dismiss(retorno);
  }

  getLstBtpFormaPagamento() {
    this.storage.get('idUsuarioLogado').then((data) => {
      this.btpItemFormaPagamentoVenda.usuario = new Usuario();
      this.btpItemFormaPagamentoVenda.usuario.idUsuario = data;
      this.formaPagamentoService.getAll(this.btpItemFormaPagamentoVenda.usuario.idUsuario)
        .then(lst => {
          this.lstBtpFormaPagamento = lst;
        });
    });

  }


  exibirParcelamento() {

    if (this.isExibeQtdParcela == null || !this.isExibeQtdParcela) {
      this.isExibeQtdParcela = true;
    } else {
      this.isExibeQtdParcela = false;
    }

  }

  rangeAnosData(): Array<number> {
    let data = new Date()
    let anoAtual: number = data.getFullYear();

    let anos: Array<number> = new Array();
    anos.push(anoAtual - 1);
    anos.push(anoAtual);
    for (var _i = 0; _i < 4; _i++) {
      anoAtual++;
      anos.push(anoAtual);
    }
    return anos;
  }

  getTotalVlrVenda(listBtpItemVenda: Array<BtpItemVenda>) {
    if (listBtpItemVenda != null && listBtpItemVenda.length > 0) {
      let resposta: number;
      resposta = 0;
      for (let btpItemVenda of listBtpItemVenda) {
        resposta = resposta + btpItemVenda.vlrTotalItemVenda
      }
      return resposta;
    }
    return 0;
  }

}
