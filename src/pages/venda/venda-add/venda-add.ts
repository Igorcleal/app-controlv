import { BtpEstoque } from './../../../model/btpEstoque';
import { BtpConta } from './../../../model/btpConta';
import { FormaPagamentoService } from './../../../providers/forma-pagamento/forma-pagamento-service';
import { MensagemProvider } from './../../../providers/mensagem-provider';
import { Categoria } from './../../../model/categoria';
import { BtpReceita } from '../../../model/btpReceita';

import { StatusVenda } from '../../../model/status-venda';
import { VendaService } from './../../../providers/venda/venda-service';
import { ReceitaService } from './../../../providers/receita/receita-service';
import { Usuario } from './../../../model/usuario';
import { Storage } from '@ionic/storage';
import { BtpVenda } from './../../../model/btpVenda';
import { BtpCliente } from './../../../model/cliente';

import { Component, ViewChild } from '@angular/core';
import { AlertController, NavController, NavParams, ModalController, ViewController, ToastController, Slides, FabContainer } from 'ionic-angular';
import { FormBuilder, Validators } from "@angular/forms";
import { BtpItemVenda } from "../../../model/btpItemVenda";

import { PopupListarProdutosPage } from "../../popup-listar-produtos/popup-listar-produtos";
import { Produto } from "../../../model/produto";
import { ItemVendaAddPage } from "../item-venda-add/item-venda-add";
import { ItemFormaPagamentoVendaAddPage } from "../item-forma-pagamento-venda-add/item-forma-pagamento-venda-add";
import { PopupListarClientesPage } from "../../popup-listar-clientes/popup-listar-clientes";
import { BtpItemFormaPagamentoVenda } from "../../../model/btpItemFormaPagamentoVenda";
import { BtpItemEstoque } from "../../../model/btpItemEstoque";
import * as moment from 'moment';
import { VendaWsProvider } from "../../../providers/venda/venda-ws";
import { UtilsService } from "../../../providers/utils-service";
import { EstoqueService } from '../../../providers/estoque/estoque-service';

@Component({
  selector: 'page-venda-add',
  templateUrl: 'venda-add.html'
})
export class VendaAddPage {

  @ViewChild(Slides) slides: Slides;
  btpVenda: BtpVenda;
  listVenda: Array<BtpVenda>;
  formCadastroVenda: any;

  listBtpItemVenda: Array<BtpItemVenda>;
  listBtpItemVendaRemover: Array<BtpItemVenda>;
  listBtpItemVendaAlterar: Array<BtpItemVenda>;

  lstBtpItemFormaPagamentoVenda: Array<BtpItemFormaPagamentoVenda>;
  lstBtpItemFormaPagamentoVendaRemover: Array<BtpItemFormaPagamentoVenda>;
  lstBtpItemFormaPagamentoVendaAlterar: Array<BtpItemFormaPagamentoVenda>;

  dataVenda: string;
  showDetails: boolean;
  isBloqueiaAlteracao: boolean = false;
  slideSelecionado: number;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public modalCtrl: ModalController, public storage: Storage,
    public vendaService: VendaService, public toastCtrl: ToastController,
    public alertCtrl: AlertController, public receitaService: ReceitaService,
    public formBuilder: FormBuilder, public viewCtrl: ViewController,
    public mensagemProvider: MensagemProvider, public formaPagamentoService: FormaPagamentoService,
    public estoqueService: EstoqueService, public vendaWs: VendaWsProvider, public utils: UtilsService) {
    this.inicializar();

  }

  private inicializar() {
    this.btpVenda = this.navParams.get('btpVenda');

    if (this.btpVenda == null) {
      this.btpVenda = new BtpVenda();

      let btpCliente: BtpCliente = this.navParams.get('btpCliente');
      this.btpVenda.btpCliente = btpCliente;
      this.btpVenda.datVenda = new Date();
    }

    if (this.showDetails == null) {
      this.showDetails = false;
    }

    if (this.btpVenda.btpCliente == null) {
      this.btpVenda.btpCliente = new BtpCliente();
    }

    if (this.btpVenda != null && this.btpVenda.statusVenda != null) {

      //Bloqueia os campos caso a venda já esteja fecahda
      if (StatusVenda.FECHADO == this.btpVenda.statusVenda) {
        this.isBloqueiaAlteracao = true;
      } else {
        this.isBloqueiaAlteracao = false;
      }
    }

    if (this.listBtpItemVenda == null) {
      this.listBtpItemVenda = new Array<BtpItemVenda>();
    }

    if (this.lstBtpItemFormaPagamentoVenda == null) {
      this.lstBtpItemFormaPagamentoVenda = new Array<BtpItemFormaPagamentoVenda>();
    }

    if (this.btpVenda.datVendaString == null) {
      this.btpVenda.datVendaString = moment().format();
    }

    this.formCadastroVenda = this.formBuilder.group({
      datVenda: [{
        value: this.btpVenda.datVendaString,
        disabled: this.btpVenda.statusVenda == 1
      }, Validators.compose([Validators.required])]

    });
  }

  ionViewDidLoad() {
    this.getItensVenda();
    this.getAllItemFormaPgtoVenda();
  }

  visualizarProdutos() {
    this.slides.slideTo(0, 200);
  }

  visualizarParcelas() {
    this.slides.slideTo(1, 200);
  }

  getItensVenda() {
    this.vendaService.getItensVendaAtivo(this.btpVenda.idVenda).then(list => {
      this.listBtpItemVenda = list;
    });
  }

  fecharModalVenda() {
    this.viewCtrl.dismiss();
  }

  salvar() {
    if (this.formCadastroVenda.valid) {

      this.btpVenda.datVenda = new Date(this.btpVenda.datVendaString);

      if (this.btpVenda.idVenda == null) {
        this.incluir();
      } else {
        this.alterar();
      }
    }
  }

  incluir() {
    this.utils.getUsuarioLogado().then(usuario => {
      this.btpVenda.usuario = usuario;
      this.btpVenda.statusVenda = StatusVenda.PENDENTE;
      this.btpVenda.vlrTotal = this.getTotalVlrVenda(this.listBtpItemVenda);

      let idInserido: number;

      this.btpVenda.isServidor = false;
      this.utils.showLoading();

      this.vendaService.incluirVenda(this.btpVenda)
        .then(idInserido => {

          this.btpVenda.idVenda = idInserido;

          this.incluirAlterarListas().then(() => {
            this.enviarVendasServidor();
          });

        }).catch((error: Error) => {
          console.log("erro incluir despesa");
          console.error(error.message);
        });
    });
  }

  incluirAlterarListas() {

    let promises = []

    //Incluindo valores dos produtos.
    if (this.listBtpItemVenda != null && this.listBtpItemVenda[0] != null) {
      this.listBtpItemVenda.forEach(elemento => {
        elemento.btpVenda = this.btpVenda;

        if (elemento.idItemVenda == null) {
          promises.push(this.vendaService.incluirItemVenda(elemento));
        }
      })
    }

    if (this.listBtpItemVendaRemover != null && this.listBtpItemVendaRemover[0] != null) {
      this.listBtpItemVendaRemover.forEach(elemento => {
        if (elemento.idItemVenda != null) {
          elemento.ativo = false;
          promises.push(this.vendaService.alterarItemVenda(elemento));
        }
      })
    }

    console.log('Alterar item venda');
    if (this.listBtpItemVendaAlterar != null && this.listBtpItemVendaAlterar[0] != null) {
      console.log('Entrou');

      this.listBtpItemVendaAlterar.forEach(elemento => {
        if (elemento.idItemVenda != null) {
          promises.push(this.vendaService.alterarItemVenda(elemento));
        }
      })
    }

    if (this.lstBtpItemFormaPagamentoVenda != null && this.lstBtpItemFormaPagamentoVenda[0] != null) {
      this.lstBtpItemFormaPagamentoVenda.forEach(elemento => {
        elemento.btpVenda = this.btpVenda;

        if (elemento.itemId == null) {
          promises.push(this.vendaService.incluirItemFormaPagamentoVenda(elemento));
        }
      })
    }

    if (this.lstBtpItemFormaPagamentoVendaAlterar != null && this.lstBtpItemFormaPagamentoVendaAlterar[0] != null) {
      this.lstBtpItemFormaPagamentoVendaAlterar.forEach(elemento => {
        if (elemento.itemId != null) {
          promises.push(this.vendaService.alterarItemFormaPagamentoVenda(elemento));
        }
      })
    }

    if (this.lstBtpItemFormaPagamentoVendaRemover != null && this.lstBtpItemFormaPagamentoVendaRemover[0] != null) {
      this.lstBtpItemFormaPagamentoVendaRemover.forEach(elemento => {
        if (elemento.itemId != null) {
          elemento.ativo = false;
          promises.push(this.vendaService.alterarItemFormaPagamentoVenda(elemento));
        }
      })
    }

    return Promise.all(promises);
  }

  removerQtdEstoque(listBtpItemVenda: Array<BtpItemVenda>) {


    if (listBtpItemVenda != null && listBtpItemVenda[0] != null) {
      listBtpItemVenda.forEach(elemento => {

        console.log(elemento.btpEstoque.idEstoque + " estoque");

        if (elemento.btpEstoque != null && elemento.btpEstoque.idEstoque != null) {
          return this.estoqueService.consultarItemEstoquePorCodProdutoCodEstoque(elemento.btpEstoque.idEstoque, elemento.produto.produtoId).then(lstBtpItemEstoque => {
            let qtdNovoEstoque: number = 0;
            let btpItemEstoque: BtpItemEstoque = new BtpItemEstoque();

            let promises = [];

            lstBtpItemEstoque.forEach(elementBtpItemEstoque => {
              qtdNovoEstoque = elementBtpItemEstoque.qtdEstoque - elemento.qtdItemVenda;
              btpItemEstoque.qtdEstoque = qtdNovoEstoque;
              btpItemEstoque.idItemEstoque = elementBtpItemEstoque.idItemEstoque;
              btpItemEstoque.ativo = elementBtpItemEstoque.ativo;
              promises.push(this.estoqueService.alterarItemEstoque(btpItemEstoque));
            });

            promises.push(this.estoqueService.alterarEstoqueIsServidor(elemento.btpEstoque.idEstoque, false));

            return Promise.all(promises);

          })
        }
      })
    }
    return Promise.resolve(null);
  }

  alterar() {
    this.btpVenda.vlrTotal = this.getTotalVlrVenda(this.listBtpItemVenda);
    console.log('**************************************');
    console.log(this.btpVenda.vlrTotal);
    this.btpVenda.isServidor = false;
    this.utils.showLoading()
    this.vendaService.alterarVenda(this.btpVenda).then(response => {
      console.log("success alterar venda");
      this.incluirAlterarListas().then(() => {
        this.enviarVendasServidor();
      });
    }).catch(error => {
      this.utils.closeLoading();
      console.log("erro alterar venda");
    });
  }


  deletarVenda() {

    this.vendaService.verificarExistePagamento(this.btpVenda.idVenda).then(temPagamento => {

      let confirm = this.alertCtrl.create({
        title: 'Excluir',
        message: temPagamento == true ? 'Existem pagamentos para essa venda, deseja mesmo excluí-la?' : 'Deseja excluir essa venda?',
        buttons: [
          {
            text: 'Cancelar',
            handler: () => {
              console.log('Disagree clicked');
            }
          },
          {
            text: 'Confirmar',
            handler: () => {

              this.btpVenda.ativo = false;
              this.alterar();

              /*this.vendaService.deletarVendaCompleta(this.btpVenda).then(response => {
  
                this.listBtpItemVenda.forEach(elemento => {
  
                  console.log(elemento.qtdItemVenda);
                  console.log(elemento.btpEstoque.dscEstoque);
                  console.log(elemento.produto.produtoDsc);
  
                  this.estoqueService.adicionarQtdItemEstoque(elemento.produto.produtoId, elemento.btpEstoque.idEstoque, elemento.qtdItemVenda);
                })
  
                console.log("success deletar a venda");
                this.viewCtrl.dismiss();
              }).catch(error => {
                console.log("erro deletar a venda");
              });*/
            }
          }
        ]
      });
      confirm.present();
    });

  }

  //Utilizado no alterar
  abrirModalItemVenda(btpItemVenda: BtpItemVenda, indice: number) {
    console.log("abrir modal" + btpItemVenda);
    let btpItemV: BtpItemVenda = new BtpItemVenda();
    btpItemV.btpVenda = this.btpVenda;
    btpItemV.produto = btpItemVenda.produto;
    btpItemV.qtdItemVenda = btpItemVenda.qtdItemVenda;
    btpItemV.vlrItemVenda = btpItemVenda.vlrItemVenda;
    btpItemV.idItemVenda = btpItemVenda.idItemVenda;
    if (btpItemVenda.btpEstoque != null) {
      btpItemV.btpEstoque = new BtpEstoque();
      btpItemV.btpEstoque.idEstoque = btpItemVenda.btpEstoque.idEstoque;
    }

    let modalItemEstoque = this.modalCtrl.create(ItemVendaAddPage, { btpItemVenda: btpItemVenda, statusCrud: "alterar", bloqueiaAlteracao: this.isBloqueiaAlteracao });
    modalItemEstoque.onDidDismiss(data => {

      if (data != null && data.btpItemVenda != "" && data.btpItemVenda !== null
        && data.statusCrud !== null) {

        if ("alterar" == data.statusCrud) {

          console.log(btpItemV);
          console.log(data.btpItemVenda);

          let isEntrouValidacaoAletar: boolean = false;
          if (btpItemV.qtdItemVenda != data.btpItemVenda.qtdItemVenda) {
            isEntrouValidacaoAletar = true;
          }

          if (btpItemV.produto.produtoId != data.btpItemVenda.produto.produtoId) {
            isEntrouValidacaoAletar = true;
          }

          if (btpItemV.vlrItemVenda != data.btpItemVenda.vlrItemVenda) {
            isEntrouValidacaoAletar = true;
          }

          if (btpItemV.btpEstoque != null && data.btpItemVenda.btpEstoque != null &&
            btpItemV.btpEstoque != data.btpItemVenda.btpEstoque.idEstoque) {
            isEntrouValidacaoAletar = true;
          }

          if (this.listBtpItemVendaAlterar == null || this.listBtpItemVendaAlterar[0] == null) {
            this.listBtpItemVendaAlterar = new Array<BtpItemVenda>();
          }

          if (isEntrouValidacaoAletar) {
            console.log('adicionou para alteração');
            this.listBtpItemVendaAlterar.push(data.btpItemVenda);
          }

          btpItemVenda = data.btpItemVenda;
          btpItemVenda.btpEstoque = data.btpItemVenda.btpEstoque;
          console.log(btpItemVenda);

        } else if ("deletar" == data.statusCrud) {

          if (indice != null) {

            if (this.listBtpItemVendaRemover == null || this.listBtpItemVendaRemover[0] == null) {
              this.listBtpItemVendaRemover = new Array<BtpItemVenda>();
            }
            this.listBtpItemVendaRemover.push(btpItemVenda);
            this.listBtpItemVenda.splice(indice, 1);
          }
        }
      }

    });
    modalItemEstoque.present();
  }

  //Utilizado no alterar 
  abrirModalItemFormaPagamento(btpItemFormaPagamentoVenda: BtpItemFormaPagamentoVenda, indice: number) {
    let btpItemFormaPgtoV: BtpItemFormaPagamentoVenda = new BtpItemFormaPagamentoVenda();

    btpItemFormaPgtoV.vlrParcela = btpItemFormaPagamentoVenda.vlrParcela;
    btpItemFormaPgtoV.numParcela = btpItemFormaPagamentoVenda.numParcela;
    btpItemFormaPgtoV.datVencimentoString = btpItemFormaPagamentoVenda.datVencimentoString;
    btpItemFormaPgtoV.btpFormaPagamento = btpItemFormaPagamentoVenda.btpFormaPagamento;
    btpItemFormaPgtoV.datPagamentoString = btpItemFormaPagamentoVenda.datPagamentoString;
    btpItemFormaPgtoV.dscObservacao = btpItemFormaPagamentoVenda.dscObservacao;

    console.log("abrir modal forma de pagamento");
    console.log(this.btpVenda.datVendaString);
    let modalItemEstoque = this.modalCtrl.create(ItemFormaPagamentoVendaAddPage, { btpItemFormaPagamentoVenda: btpItemFormaPagamentoVenda, 'statusCrud': "alterar", bloqueiaAlteracao: this.isBloqueiaAlteracao, dataVenda: this.btpVenda.datVendaString });
    modalItemEstoque.onDidDismiss(data => {

      if (data != null && data.statusCrud != "" && data.statusCrud !== null) {

        if ("alterar" == data.statusCrud) {

          if (this.lstBtpItemFormaPagamentoVendaAlterar == null || this.lstBtpItemFormaPagamentoVendaAlterar[0] == null) {
            this.lstBtpItemFormaPagamentoVendaAlterar = new Array<BtpItemFormaPagamentoVenda>();
          }

          let isBloqueiaAlteracao = false;

          if (data.btpItemFormaPagamentoVenda.numParcela
            != btpItemFormaPgtoV.numParcela) {
            isBloqueiaAlteracao = true;
          }
          if (data.btpItemFormaPagamentoVenda.datVencimentoString != btpItemFormaPgtoV.datVencimentoString) {
            isBloqueiaAlteracao = true;
          }
          if (btpItemFormaPgtoV.btpFormaPagamento.idFormaPagamento != data.btpItemFormaPagamentoVenda.btpFormaPagamento.idFormaPagamento) {
            isBloqueiaAlteracao = true;
          }

          if (btpItemFormaPgtoV.vlrParcela != data.btpItemFormaPagamentoVenda.vlrParcela) {
            isBloqueiaAlteracao = true;
          }

          if (btpItemFormaPgtoV.datPagamentoString != data.btpItemFormaPagamentoVenda.datPagamentoString) {
            isBloqueiaAlteracao = true;
          }

          if (btpItemFormaPgtoV.dscObservacao != data.btpItemFormaPagamentoVenda.dscObservacao) {
            isBloqueiaAlteracao = true;
          }

          console.log("testando flag de inclusao teste teste")
          console.log(isBloqueiaAlteracao);
          if (isBloqueiaAlteracao) {
            this.lstBtpItemFormaPagamentoVendaAlterar.push(data.btpItemFormaPagamentoVenda);
          }

          btpItemFormaPagamentoVenda = data.btpItemFormaPagamentoVenda;
        } else if ("deletar" == data.statusCrud) {
          if (indice != null) {
            if (this.lstBtpItemFormaPagamentoVendaRemover == null || this.lstBtpItemFormaPagamentoVendaRemover[0] == null) {
              this.lstBtpItemFormaPagamentoVendaRemover = new Array<BtpItemFormaPagamentoVenda>();
            }

            this.lstBtpItemFormaPagamentoVendaRemover.push(btpItemFormaPagamentoVenda);
            this.lstBtpItemFormaPagamentoVenda.splice(indice, 1);
          }
        }
      }
    });
    modalItemEstoque.present();
  }

  extend<T, U>(obj: T, extension: U) {
    Object.keys(obj).forEach((key) => {
      extension[key] = obj[key];
    });

    return extension as T & U;
  }

  abrirModalProdutos(fab: FabContainer) {
    let modal = this.modalCtrl.create(PopupListarProdutosPage);
    modal.onDidDismiss(data => {
      if (data != null) {
        let produto: Produto = data;
        console.log("produto: " + produto.produtoDsc);

        let modalItemVenda = this.modalCtrl.create(ItemVendaAddPage, { produto: produto, btpVenda: this.btpVenda, statusCrud: 'incluir' });
        modalItemVenda.onDidDismiss(data => {

          if (data != null && data.btpItemVenda != "" && data.btpItemVenda != null) {

            if (this.listBtpItemVenda == null || this.listBtpItemVenda[0] == null) {
              this.listBtpItemVenda = Array<BtpItemVenda>();
            }

            this.listBtpItemVenda.push(data.btpItemVenda);

          }
        })
        modalItemVenda.present();
      }
    });
    modal.present();
    fab.close();
  }

  abrirModalFormaPagamento(fab: FabContainer) {

    if (this.listBtpItemVenda == null || this.listBtpItemVenda.length == 0) {
      this.mensagemProvider.addMensagemErro("Adicione um produto primeiro! ", 3000, 'top');
      return;
    }

    let valorRestantePagamento = this.getTotalVlrVenda(this.listBtpItemVenda) - this.getTotalVlrParcelas();
    valorRestantePagamento = Number(valorRestantePagamento.toFixed(2));

    let modalItemFormaPg = this.modalCtrl.create(ItemFormaPagamentoVendaAddPage, { btpVenda: this.btpVenda, listBtpItemVenda: this.listBtpItemVenda, valorRestantePagamento: valorRestantePagamento, dataVenda: this.btpVenda.datVendaString });
    modalItemFormaPg.onDidDismiss(data => {

      if (this.lstBtpItemFormaPagamentoVenda == null || this.lstBtpItemFormaPagamentoVenda[0] == null) {

        this.lstBtpItemFormaPagamentoVenda = new Array<BtpItemFormaPagamentoVenda>();
      }

      if (data != null && data.lstBtpItemFormaPagamentoVenda != "" && data.lstBtpItemFormaPagamentoVenda !== null) {

        data.lstBtpItemFormaPagamentoVenda.forEach(elemento => {

          //recupera a forma de pagamento, pois só está vindo o ID
          this.formaPagamentoService.getFormaPagamento(elemento.btpFormaPagamento.idFormaPagamento).then((formaPagamento) => {
            console.log('teste');
            elemento.btpFormaPagamento = formaPagamento;
            this.lstBtpItemFormaPagamentoVenda.push(elemento);
          });

        });

      }
    });
    modalItemFormaPg.present();
    fab.close();
  }

  getAllItemFormaPgtoVenda() {
    this.storage.get('idUsuarioLogado').then((data) => {
      this.vendaService.getItensFormaPagamento(this.btpVenda.idVenda)
        .then(list => {
          this.lstBtpItemFormaPagamentoVenda = list;
        });
    });
  }

  abrirModalClientes() {
    let modal = this.modalCtrl.create(PopupListarClientesPage);
    modal.onDidDismiss(data => {
      if (data != null) {
        this.btpVenda.btpCliente = data;
      }
    });
    modal.present();
  }

  getTotalVlrVenda(listBtpItemVenda: Array<BtpItemVenda>) {
    if (listBtpItemVenda != null && listBtpItemVenda.length > 0) {
      let resposta: number;
      resposta = 0;
      for (let btpItemVenda of listBtpItemVenda) {
        resposta = resposta + btpItemVenda.vlrTotalItemVenda
      }
      return resposta;
    }
    return 0;
  }

  getTotalVlrParcelas() {

    if (this.lstBtpItemFormaPagamentoVenda != null && this.lstBtpItemFormaPagamentoVenda.length > 0) {
      let resposta: number;
      resposta = 0;
      for (let pagamento of this.lstBtpItemFormaPagamentoVenda) {
        resposta = resposta + pagamento.vlrParcela;
      }
      return resposta;
    }
    return 0;
  }

  toggleDetails() {
    if (this.showDetails) {
      this.showDetails = false;
    } else {
      this.showDetails = true;
    }
    console.log(this.showDetails);
  }

  fecharVenda() {
    if (this.validarFecharVenda()) {

      this.vendaService.alterarStatusVenda(StatusVenda.FECHADO, this.btpVenda.idVenda).then(response => {
        console.log("success alterar status venda");

        this.lstBtpItemFormaPagamentoVenda.forEach(elemento => {
          let btpReceita: BtpReceita = new BtpReceita();
          btpReceita.receitaVlr = elemento.vlrParcela;
          btpReceita.receitaDta = elemento.datVencimento;
          btpReceita.btpVenda = this.btpVenda;
          btpReceita.receitaPaga = false;
          btpReceita.categoria = new Categoria();
          btpReceita.btpConta = new BtpConta();

          this.storage.get('usuarioLogado').then((data) => {
            btpReceita.usuario = new Usuario();
            btpReceita.usuario.idUsuario = data._idUsuario;
            btpReceita.usuario.nomeUsuario = data._nomeUsuario;

            this.receitaService.incluir(btpReceita);
          });

          btpReceita.receita = "VENDA - " + this.btpVenda.btpCliente.nome;

          this.salvar();

        });

      }).catch(error => {
        console.log("erro alterar status venda");
      });
    }

  }

  fecharVendaIgor() {
    if (!this.validarFecharVenda()) {
      return;
    }

    this.utils.getUsuarioLogado().then((usuario) => {
      this.btpVenda.usuario = usuario;
      this.btpVenda.statusVenda = StatusVenda.FECHADO;
      this.btpVenda.datVenda = new Date(this.btpVenda.datVendaString);

      if (this.btpVenda.idVenda == null) {

        this.btpVenda.isServidor = false;

        this.vendaService.incluirVenda(this.btpVenda)
          .then(idInserido => {
            console.log("success incluir venda");

            let promises = [];

            this.btpVenda.idVenda = idInserido;
            
            promises.push(this.incluirAlterarListas());
            promises.push(this.incluirReceitas());
            promises.push(this.removerQtdEstoque(this.listBtpItemVenda));

            Promise.all(promises).then(()=>{
              this.enviarVendasServidor();
            });

          }).catch((error: Error) => {
            console.log("erro incluir despesa");
            console.error(error.message);
            this.utils.closeLoading();
          });

      } else {

        this.btpVenda.isServidor = false;

        this.vendaService.alterarVenda(this.btpVenda).then(response => {
          console.log("success alterar venda");
          
          let promises = [];
          
          promises.push(this.incluirAlterarListas());
          promises.push(this.incluirReceitas());
          promises.push(this.removerQtdEstoque(this.listBtpItemVenda));

          Promise.all(promises).then(()=>{
            this.enviarVendasServidor();
          });
        }).catch(error => {
          console.log("erro alterar venda");
        });
      }
    });
  }

  incluirReceitas() {
    let promises = [];
    this.lstBtpItemFormaPagamentoVenda.forEach(elemento => {
      let btpReceita: BtpReceita = new BtpReceita();
      btpReceita.receitaVlr = elemento.vlrParcela;
      btpReceita.receitaDtaString = moment(elemento.datVencimentoString).format();
      btpReceita.btpVenda = this.btpVenda;
      btpReceita.receitaPaga = false;
      btpReceita.categoria = new Categoria();
      btpReceita.btpConta = new BtpConta();
      btpReceita.receita = "VENDA - " + this.btpVenda.btpCliente.nome;
      btpReceita.isServidor = false;

      btpReceita.usuario = new Usuario();
      btpReceita.usuario = this.btpVenda.usuario;

      promises.push(this.vendaService.incluirReceita(btpReceita));

    });
    return Promise.all(promises);
  }

  validarFecharVenda() {

    let resposta: boolean = true;

    if (this.listBtpItemVenda == null || this.listBtpItemVenda[0] == null) {
      this.mensagemProvider.addMensagemErro("Adicione ao menos um produto!", 3000, "top");
      return false;
    }

    if (this.lstBtpItemFormaPagamentoVenda != null && this.lstBtpItemFormaPagamentoVenda[0] != null) {

      let isDataVendaCompraMaiorVencimento: boolean = false;

      let totalFormaPgt: number = 0;
      //Obtem o valor total da forma de pagamento
      this.lstBtpItemFormaPagamentoVenda.forEach(element => {
        totalFormaPgt = totalFormaPgt + element.vlrParcela;

        if (this.btpVenda.datVenda > element.datVencimento) {
          isDataVendaCompraMaiorVencimento = true;
        }

      });

      if (isDataVendaCompraMaiorVencimento) {
        this.mensagemProvider.addMensagemErro("A data da venda não pode ser maior que a data do vencimento de uma forma de pagamento!", 3000, "top");
        return false;
      }

      let totalVenda: number = 0;
      totalVenda = this.getTotalVlrVenda(this.listBtpItemVenda);
      totalVenda = Number(totalVenda.toFixed(2));
      totalFormaPgt = Number(totalFormaPgt.toFixed(2));
      //Obtem o valor total da venda
      console.log(totalFormaPgt);
      console.log(totalVenda);
      if (totalFormaPgt != totalVenda) {
        this.mensagemProvider.addMensagemErro("O valor dos pagamentos deve ser igual ao valor venda", 3000, "top");
        return false;
      }

    } else {
      this.mensagemProvider.addMensagemErro("Adicione uma forma de pagamento!", 3000, "top");
      return false;
    }

    return resposta;
  }

  rangeAnosData(): Array<number> {
    let data = new Date()
    let anoAtual: number = data.getFullYear();

    let anos: Array<number> = new Array();
    anos.push(anoAtual - 1);
    anos.push(anoAtual);
    for (var _i = 0; _i < 4; _i++) {
      anoAtual++;
      anos.push(anoAtual);
    }
    return anos;
  }

  abrirPopupCliente() {
    let modal = this.modalCtrl.create(PopupListarClientesPage);
    modal.onDidDismiss(data => {
      if (data != null) {
        this.btpVenda.btpCliente = data;
      }
    });
    modal.present();
  }

  private enviarVendasServidor() {
    return this.vendaWs.enviarVendasServidor().then(() => {
      this.viewCtrl.dismiss();
      this.utils.closeLoading();
    }).catch((err) => {
      console.log('err enviar Vendas ws');
      console.error(err.message);
      this.viewCtrl.dismiss();
      this.utils.closeLoading();
    });
  }

}