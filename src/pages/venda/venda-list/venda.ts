import { Storage } from '@ionic/storage';
import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { PopupListarClientesPage } from '../../popup-listar-clientes/popup-listar-clientes';
import { VendaService } from './../../../providers/venda/venda-service';
import { BtpVenda } from "../../../model/btpVenda";
import { VendaAddPage } from "../venda-add/venda-add";
import { BtpCliente } from "../../../model/cliente";

@Component({
  selector: 'venda',
  templateUrl: 'venda.html'
})
export class VendaPage {

  btnSearchClicked: boolean;
  listBtpVenda: Array<BtpVenda>;
  listBtpVendaSemFiltro: Array<BtpVenda>;
  filtroVenda: string = "";
  public meses = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho",
        "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
  public filtroData: Date = new Date();

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public modalCtrl: ModalController, public vendaService: VendaService,
    public storage: Storage) {
    if (this.listBtpVenda == null) {
      this.listBtpVenda = new Array();
    }
  }

  ionViewDidLoad() {
    this.getAllVenda();
  }

  getAllVenda() {
    this.storage.get('idUsuarioLogado').then((data) => {
      this.vendaService.getAllByDate(data, this.filtroData)
        .then(listBtpVenda => {
          this.listBtpVenda = listBtpVenda;
          this.listBtpVendaSemFiltro = listBtpVenda;
        });
    });

  }

  incluir() {

    let modal = this.modalCtrl.create(PopupListarClientesPage);
    modal.onDidDismiss(data => {

      if (data != null) {
        let btpCliente: BtpCliente = data;
        let vendaModal = this.modalCtrl.create(VendaAddPage, { btpCliente: btpCliente });
        this.abrirModalVenda(vendaModal);
      }
    });
    modal.present();
  }

  editar(btpVenda) {
    console.log("id" + btpVenda.idVenda);
    let vendaModal = this.modalCtrl.create(VendaAddPage, { btpVenda: btpVenda });
    this.abrirModalVenda(vendaModal);
  }

  private abrirModalVenda(modal) {
    modal.onDidDismiss(data => {
      this.getAllVenda();
    });
    modal.present();
  }

  clickLupa() {
    if (this.btnSearchClicked) {
      this.btnSearchClicked = false;
      document.getElementById("barraBusca").style.left = '100%';
    } else {
      this.btnSearchClicked = true;
      this.animarSearchBar();
    }
  }

  animarSearchBar() {
    var elem = document.getElementById("barraBusca");
    var pos = 100;
    var id = setInterval(frame, 1);
    function frame() {
      if (pos == 0) {
        clearInterval(id);
      } else {
        pos = pos - 10;
        //elem.style.width = pos + '%';
        elem.style.left = pos + '%';
      }
    }
  }

  incrementaData() {
    this.filtroData.setMonth(this.filtroData.getMonth() + 1);
    this.getAllVenda();

  }

  decrementaData() {
    this.filtroData.setMonth(this.filtroData.getMonth() - 1);
    this.getAllVenda();
  }

  filtrarVendas() {
      this.listBtpVenda = this.listBtpVendaSemFiltro.filter((item) => {
        return item.btpCliente.nome.toLowerCase().indexOf(this.filtroVenda.toLowerCase()) > -1;
    
    });
  }

  calcularTotalVendas(){
    let soma:number = 0;
    this.listBtpVenda.forEach(venda => {
      soma = soma + venda.vlrTotal;
    });
    return soma;
  }

}
