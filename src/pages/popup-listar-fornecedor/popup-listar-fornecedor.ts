import { FornecedorAddPage } from './../fornecedor-add/fornecedor-add';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController } from 'ionic-angular';

import { BtpFornecedor } from './../../model/fornecedor';
import { Storage } from '@ionic/storage';
import { FornecedorService } from './../../providers/fornecedor/fornecedor-service';

@Component({
  selector: 'page-listar-fornecedor',
  templateUrl: 'popup-listar-fornecedor.html'
})
export class PopupListarFornecedorPage {

  lstFornecedor: Array<BtpFornecedor>;
  lstFornecedorNoFilter: Array<BtpFornecedor>;
  filtroFornecedorNome: string = "";

  @ViewChild('search') myInput ;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public fornecedorService: FornecedorService, public storage: Storage,
    public viewCtrl: ViewController, public modalCtrl: ModalController) {
    if (this.lstFornecedor == null) {
      this.lstFornecedor = new Array();
    }
  }

  ionViewDidLoad() {
    console.log("aberto popup listar fornecedores");
    this.getAllFornecedores();

    setTimeout(() => {
      this.myInput.setFocus();
    },150);
  }

  getAllFornecedores() {
    this.storage.get('idUsuarioLogado').then((data) => {
      this.fornecedorService.getAll(data)
        .then(fornecedores => {
          this.lstFornecedor = fornecedores;
          this.lstFornecedorNoFilter = fornecedores;
        });
    });
  }

  escolherFornecedor(btpFornecedor) {
    console.log("fechar popup listar fornecedores");
    this.viewCtrl.dismiss(btpFornecedor);
  }

  filtrarFornecedores() {
    this.lstFornecedor = this.lstFornecedorNoFilter.filter((item) => {
      return item.nomeFornecedor.toLowerCase().indexOf(this.filtroFornecedorNome.toLowerCase()) > -1;
    });
  }

  fechar() {
    this.viewCtrl.dismiss();
  }

  incluirNovoFornecedor() {
    let modal = this.modalCtrl.create(FornecedorAddPage);
    modal.onDidDismiss(data => {
      this.getAllFornecedores();
    });
    modal.present();
  }

}
