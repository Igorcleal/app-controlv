import { BtpCliente } from './../../../model/cliente';
import { PopupListarClientesPage } from './../../popup-listar-clientes/popup-listar-clientes';
import { BtpPedido } from './../../../model/btpPedido';
import { StatusPedido } from './../../../model/status-pedido';
import { PedidoService } from './../../../providers/pedido-service';
import { Storage } from '@ionic/storage';
import { PedidoAddPage } from './../pedido-add/pedido-add';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ModalController, Slides } from 'ionic-angular';

@Component({
  selector: 'page-pedido',
  templateUrl: 'pedido.html'
})
export class PedidoPage {

  listPedidosAbertos: Array<BtpPedido>;
  listPedidosFechados: Array<BtpPedido>;
  @ViewChild(Slides) slides: Slides;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public modalCtrl: ModalController, public pedidoService: PedidoService,
    public storage: Storage) {
    if (this.listPedidosAbertos == null) {
      this.listPedidosAbertos = new Array();
    }
    if (this.listPedidosFechados == null) {
      this.listPedidosFechados = new Array();
    }
  }

  ionViewDidLoad() {
    this.getAllPedidos();
  }

  getAllPedidos() {
    this.storage.get('idUsuarioLogado').then((data) => {
      this.pedidoService.getAllPorStatus(data, StatusPedido.ABERTO)
        .then(listPedidos => {
          this.listPedidosAbertos = listPedidos;
        });

      this.pedidoService.getAllPorStatus(data, StatusPedido.ENCERRADO)
        .then(listPedidos => {
          this.listPedidosFechados = listPedidos;
        });
    });

  }

  incluir() {
    let modal = this.modalCtrl.create(PopupListarClientesPage);
    modal.onDidDismiss(data => {
      if (data != null) {
        let btpCliente: BtpCliente = data;
        let pedidoModal = this.modalCtrl.create(PedidoAddPage, { btpCliente: btpCliente });
        this.abrirModalPedido(pedidoModal);
      }
    });
    modal.present();
  }

  editar(btpPedido) {
    let pedidoModal = this.modalCtrl.create(PedidoAddPage, { btpPedido: btpPedido });
    this.abrirModalPedido(pedidoModal);
  }

  private abrirModalPedido(modal) {
    modal.onDidDismiss(data => {
      this.getAllPedidos();
    });
    modal.present();
  }

  selecionarPedidosFechados() {
    document.getElementById("divBarPedidoSelecionado").style.left = "55%";
    this.slides.slideTo(1, 200);

  }

  selecionarPedidosAbertos() {
    document.getElementById("divBarPedidoSelecionado").style.left = "5%";
    this.slides.slideTo(0, 200);
  }

  slideChanged() {
    let currentIndex = this.slides.getActiveIndex();

    if (currentIndex == 0) {
      document.getElementById("divBarPedidoSelecionado").style.left = "5%";
    } else {
      document.getElementById("divBarPedidoSelecionado").style.left = "55%";
    }
  }

  getPrecoFormatado(price: number) {
    return new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(price);
  }

}
