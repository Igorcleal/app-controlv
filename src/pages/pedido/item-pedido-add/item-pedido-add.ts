import { Produto } from './../../../model/produto';
import { Usuario } from './../../../model/usuario';
import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams, ViewController } from 'ionic-angular';

import { PedidoService } from './../../../providers/pedido-service';
import { BtpItemPedido } from './../../../model/btpItemPedido';

@Component({
  selector: 'item-pedido-add',
  templateUrl: 'item-pedido-add.html'
})
export class ItemPedidoAddPage {

  btpItemPedido: BtpItemPedido;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public viewCtrl: ViewController, public pedidoService: PedidoService,
    public storage: Storage) {
    this.btpItemPedido = this.navParams.get('btpItemPedido');

    if (this.btpItemPedido == null) {
      this.btpItemPedido = new BtpItemPedido();
      let produto:Produto = this.navParams.get('produto');
      this.btpItemPedido.produto = produto;
      this.btpItemPedido.valor = produto.precoUnitario;
      this.btpItemPedido.btpPedido = this.navParams.get('btpPedido');
    }

  }

  ionViewDidLoad() {

  }

  fechar() {
    this.viewCtrl.dismiss();
  }

  salvarProdutoPedido() {
    if (this.btpItemPedido.itemId == null) {
      this.incluirItemPedido();
    } else {
      this.alterarItemPedido();
    }
  }

  incluirItemPedido() {
    this.storage.get('usuarioLogado').then((data) => {
      this.btpItemPedido.usuario = new Usuario();
      this.btpItemPedido.usuario.idUsuario = data._idUsuario;
      this.btpItemPedido.usuario.nomeUsuario = data._nomeUsuario;
      this.pedidoService.incluirItemPedido(this.btpItemPedido)
        .then(response => {
          console.log("success incluir Item Pedido");
          this.viewCtrl.dismiss();
        }).catch(error => {
          console.log("erro incluir item pedido");
        });
    });
  }

  alterarItemPedido() {
    console.log("qtd Atendida: "+this.btpItemPedido.qtdAtendida)
    this.pedidoService.alterarItemPedido(this.btpItemPedido)
      .then(response => {
        console.log("success alterar Item Pedido");
        this.viewCtrl.dismiss();
      }).catch(error => {
        console.log("erro alterar item pedido");
      });
  }

  deletarItemPedido() {
    this.pedidoService.deletarItemPedido(this.btpItemPedido.itemId);
    this.viewCtrl.dismiss();
  }

}
