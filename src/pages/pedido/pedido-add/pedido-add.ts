import { BtpItemPedido } from './../../../model/btpItemPedido';
import { ItemPedidoAddPage } from './../item-pedido-add/item-pedido-add';
import { PedidoService } from './../../../providers/pedido-service';
import { Usuario } from './../../../model/usuario';
import { Storage } from '@ionic/storage';
import { BtpPedido } from './../../../model/btpPedido';
import { StatusPedido } from './../../../model/status-pedido';
import { Produto } from './../../../model/produto';
import { PopupListarProdutosPage } from './../../popup-listar-produtos/popup-listar-produtos';
import { PopupListarClientesPage } from './../../popup-listar-clientes/popup-listar-clientes';
import { BtpCliente } from './../../../model/cliente';
import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, ViewController, ToastController } from 'ionic-angular';

@Component({
  selector: 'page-pedido-add',
  templateUrl: 'pedido-add.html'
})
export class PedidoAddPage {

  btpPedido: BtpPedido;
  listBtpItemPedido: Array<BtpItemPedido>;
  statusPedidoString: string;
  StatusPedido : typeof StatusPedido = StatusPedido;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public modalCtrl: ModalController, public viewCtrl: ViewController,
    public storage: Storage, public pedidoService: PedidoService, public toastCtrl: ToastController) {
    this.btpPedido = this.navParams.get('btpPedido');

    if (this.btpPedido != null) {
      this.statusPedidoString = StatusPedido[this.btpPedido.statusPedido];
    }
    if (this.btpPedido == null) {
      this.btpPedido = new BtpPedido();
      let btpCliente:BtpCliente = this.navParams.get('btpCliente');
      this.btpPedido.btpCliente = btpCliente;
      this.incluir();
    }
    if (this.btpPedido.btpCliente == null) {
      this.btpPedido.btpCliente = new BtpCliente();
    }
    if (this.listBtpItemPedido == null) {
      this.listBtpItemPedido = new Array<BtpItemPedido>();
    }
  }

  ionViewDidLoad() {
    this.getItensPedidos();
  }

  getItensPedidos() {
    this.pedidoService.getItensPedido(this.btpPedido.pedidoId).then(list => {
      this.listBtpItemPedido = list;
    });
  }

  abrirModalItemPedido(btpItemPedido: BtpItemPedido) {
    let modalItemPedido = this.modalCtrl.create(ItemPedidoAddPage, { btpItemPedido: btpItemPedido });
    modalItemPedido.onDidDismiss(data => {
      this.getItensPedidos();
    });
    modalItemPedido.present();
  }

  abrirModalClientes() {
    let modal = this.modalCtrl.create(PopupListarClientesPage);
    modal.onDidDismiss(data => {
      if (data != null) {
        this.btpPedido.btpCliente = data;
        if (this.btpPedido.pedidoId == null) {
          this.incluir();
        }else{
          this.alterarPedido(this.btpPedido);
        }
      }
    });
    modal.present();
  }

  toastPedidoCriado() {
    let toast = this.toastCtrl.create({
      message: 'Pré-Venda criada',
      duration: 3000,
      cssClass: 'toasts'
    });
    toast.present();
  }

  abrirModalProdutos() {
    let modal = this.modalCtrl.create(PopupListarProdutosPage);
    modal.onDidDismiss(data => {
      if (data != null) {
        let produto: Produto = data;
        console.log("produto: " + produto.produtoDsc);

        let modalItemPedido = this.modalCtrl.create(ItemPedidoAddPage, { produto: produto, btpPedido: this.btpPedido });
        modalItemPedido.onDidDismiss(data => {
          this.getItensPedidos();
        });
        modalItemPedido.present();
      }
    });
    modal.present();
  }

  fecharPedido() {
    this.pedidoService.fecharPedido(this.btpPedido.pedidoId).then(response => {
      console.log("success fechar Pedido");
      this.viewCtrl.dismiss();
      this.toastPedidoFechado();
    }).catch(error => {
      console.log("erro fechar pedido");
    });
  }
  
  toastPedidoFechado() {
    let toast = this.toastCtrl.create({
      message: 'Pré-Venda Fechada!',
      duration: 3000,
      position: 'bottom',
      cssClass: 'toasts'
    });
    toast.present();
  }

  fecharModalProduto() {
    this.viewCtrl.dismiss();
  }

  salvar() {
    this.incluir();
  }

  incluir() {
    this.storage.get('usuarioLogado').then((data) => {
      this.btpPedido.usuario = new Usuario();
      this.btpPedido.usuario.idUsuario = data._idUsuario;
      this.btpPedido.usuario.nomeUsuario = data._nomeUsuario;
      this.btpPedido.dataPedido = new Date();
      this.btpPedido.statusPedido = StatusPedido.ABERTO;
      this.pedidoService.incluir(this.btpPedido);
      this.toastPedidoCriado();
        
    });
  }

  alterarPedido(btpPedido:BtpPedido) {
    this.pedidoService.alterarPedido(btpPedido)
      .then(response => {
        console.log("success alterar Pedido");
        this.toastClienteAlterado();
      }).catch(error => {
        console.log("erro alterar pedido");
      });
  }

  toastClienteAlterado() {
    let toast = this.toastCtrl.create({
      message: 'Cliente alterado com sucesso!',
      duration: 3000,
      position: 'bottom',
      cssClass: 'toasts'
    });
    toast.present();
  }

  deletarPedido() {
    this.pedidoService.deletarPedido(this.btpPedido.pedidoId);
    this.viewCtrl.dismiss();
  }

  getPrecoFormatado(price: number) {
    return new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(price);
  }

  getValorTotalPreVenda():number{
    let total:number = 0;
    for(let btpItemPedido of this.listBtpItemPedido){
      total = total + btpItemPedido.qtdSolicitada*btpItemPedido.valor;
    }
    return total;
  }

}
