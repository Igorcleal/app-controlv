import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';
import { DatePicker } from '@ionic-native/date-picker';

@Component({
  selector: 'page-page1',
  templateUrl: 'page1.html'
})
export class Page1 {

  data:Date = new Date();

  constructor(public navCtrl: NavController, private datePicker: DatePicker) {

  }

  clicou(){
    console.log(this.data);
  }

  setarData(data){
    console.log(data);
  }

}
