import { UtilsService } from './../../../providers/utils-service';
import { BtpConta } from './../../../model/btpConta';
import { ContaService } from './../../../providers/conta-service';
import { ReceitaService } from './../../../providers/receita/receita-service';
import { MensagemProvider } from './../../../providers/mensagem-provider';
import { Usuario } from './../../../model/usuario';
import { BtpReceita } from './../../../model/btpReceita';
import { Storage } from '@ionic/storage';
import { PagamentoService } from './../../../providers/pagamento-provider';
import { BtpPagamentoReceita } from './../../../model/btpPagamentoReceita';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { PagamentoReceitaWs } from "../../../providers/receita/pagamento-receita-ws";

@Component({
  selector: 'page-pagamentos',
  templateUrl: 'pagamentos.html'
})
export class PagamentosPage {

  formPagamento: any;
  dataPagamento: number;
  btpReceita: BtpReceita;
  idUsuario: number;
  valorPago: number;
  valorRestante: number;

  listPagamentos: BtpPagamentoReceita[] = new Array();
  listPagamentosRemover: BtpPagamentoReceita[] = new Array();
  listContas:BtpConta[] = new Array();

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
    public formBuilder: FormBuilder, public pagamentoService: PagamentoService, public storage: Storage,
    public msgProvider: MensagemProvider, public receitaService: ReceitaService,
  public utils:UtilsService, public pagamentoReceitaWs:PagamentoReceitaWs) {

    this.btpReceita = this.navParams.get("btpReceita");


    this.formPagamento = this.formBuilder.group({
      valorPagamento: ['', Validators.compose([Validators.required])],
      valorPago: [{ value: '', disabled: true }],
      dataPagamento: [new Date().toISOString(), Validators.compose([Validators.required])]
    });

   this.utils.getUsuarioLogado().then((usuario) => {

      this.idUsuario = usuario.idUsuario;

      this.pagamentoService.getPagamentosReceitas(this.btpReceita.receitaId, this.idUsuario).then(pagamentos => {
        this.listPagamentos = pagamentos;
        this.valorPago = this.calcularValorPago();
        this.valorRestante = this.calcularValorRestante();
        console.log(this.valorRestante);
      });
      
    });

  }

  ionViewDidLoad() {

  }

  adicionarPagamento() {

    let pagamento = new BtpPagamentoReceita();
    pagamento.valorPagamento = Number(this.formPagamento.value.valorPagamento);
    pagamento.valorPagamento = Number(pagamento.valorPagamento.toFixed(2));
    pagamento.datPagamento = this.formPagamento.value.dataPagamento;
    pagamento.btpReceita = this.btpReceita;
    pagamento.usuario = new Usuario();
    pagamento.usuario.idUsuario = this.idUsuario;


    if (this.calcularValorPago() + pagamento.valorPagamento > this.btpReceita.receitaVlr) {
      this.msgProvider.addMensagemAdvertencia("Valor adicionado supera valor da receita", 3000, MensagemProvider.TOP);
      return;
    }
    if (pagamento.valorPagamento == 0) {
      this.msgProvider.addMensagemAdvertencia("Digite um número maior do que ZERO!", 3000, MensagemProvider.TOP);
      return;
    }

    this.listPagamentos.push(pagamento);

    this.valorRestante = this.calcularValorRestante();
    this.valorPago = this.calcularValorPago();
  }

  calcularValorPago(): number {
    let somaValorPago: number = 0;

    this.listPagamentos.forEach(element => {
      somaValorPago = element.valorPagamento + somaValorPago;
    });
    return Number(somaValorPago.toFixed(2));
  }

  calcularValorRestante(): number {
    return Number((this.btpReceita.receitaVlr - this.calcularValorPago()).toFixed(2));
  }

  desabilitarBotaoAdicionarPagamento(): boolean {
    return this.btpReceita.receitaVlr == this.calcularValorPago();
  }

  excluirPagamento(index) {
    let btpPagamentoReceita: BtpPagamentoReceita = this.listPagamentos[index];
    if (btpPagamentoReceita.idPagamento != null) {
      this.listPagamentosRemover.push(btpPagamentoReceita);
    }
    this.listPagamentos.splice(index, 1);

    this.valorRestante = this.calcularValorRestante();
    this.valorPago = this.calcularValorPago();
  }

  salvar() {
    this.utils.showLoading();
    let promises = [];
    promises.push(this.pagamentoService.incluirPagamentosReceita(this.listPagamentos));
    promises.push(this.pagamentoService.excluirPagamentosReceita(this.listPagamentosRemover));

    if (this.calcularValorPago() == this.btpReceita.receitaVlr) {
      this.btpReceita.receitaPaga = true;
    } else {
      this.btpReceita.receitaPaga = false;
    }
    
    this.btpReceita.receitaVlrPago = this.valorPago;
    this.btpReceita.isServidor = false;
    promises.push(this.receitaService.alterar(this.btpReceita));

     Promise.all(promises).then(()=>{
      
      this.enviarPagamentosServidor();

    }).catch((err)=>{
      console.error(err.message);
      this.utils.closeLoading();
    })
  }

  fecharModal() {
    this.viewCtrl.dismiss();
  }

  private enviarPagamentosServidor() {
    return this.pagamentoReceitaWs.enviarPagamentosReceitasServidor().then(() => {
      this.viewCtrl.dismiss(this.btpReceita);
      this.utils.closeLoading();
    }).catch((err) => {
      console.log('err enviar Pagamentos receitas ws');
      console.error(err.message);
      this.viewCtrl.dismiss(this.btpReceita);
      this.utils.closeLoading();
    });
  }

}
