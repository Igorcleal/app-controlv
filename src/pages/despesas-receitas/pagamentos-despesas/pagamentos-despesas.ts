import { PagamentoDespesaWsProvider } from './../../../providers/despesa/pagamento-despesa-ws';
import { UtilsService } from './../../../providers/utils-service';
import { Storage } from '@ionic/storage';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

import { DespesaService } from './../../../providers/despesa/despesa-service';
import { PagamentoDespesaService } from './../../../providers/pagamento-despesa-provider';
import { BtpPagamentoDespesa } from './../../../model/btpPagamentoDespesa';
import { BtpDespesa } from './../../../model/btpDespesa';
import { MensagemProvider } from './../../../providers/mensagem-provider';
import { Usuario } from './../../../model/usuario';

@Component({
  selector: 'page-pagamentos-despesas',
  templateUrl: 'pagamentos-despesas.html'
})
export class PagamentosDespesasPage {

  formPagamento: any;
  dataPagamento: number;
  btpDespesa: BtpDespesa;
  idUsuario: number;
  valorPago: number;
  valorRestante: number;

  listPagamentos: BtpPagamentoDespesa[] = new Array();
  listPagamentosRemover: BtpPagamentoDespesa[] = new Array();

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
    public formBuilder: FormBuilder, public pagamentoService: PagamentoDespesaService, public storage: Storage,
    public msgProvider: MensagemProvider, public despesaService: DespesaService, public utils:UtilsService,
    public pagamentoDespesaWs:PagamentoDespesaWsProvider) {

    this.btpDespesa = this.navParams.get("btpDespesa");


    this.formPagamento = this.formBuilder.group({
      valorPagamento: ['', Validators.compose([Validators.required])],
      valorRestante: [{ value: '', disabled: true }],
      dataPagamento: [new Date().toISOString(), Validators.compose([Validators.required])]
    });

    this.utils.getUsuarioLogado().then((usuario) => {

      this.idUsuario = usuario.idUsuario;
      this.pagamentoService.getPagamentosDespesas(this.btpDespesa.despesaId, this.idUsuario).then(pagamentos => {
        console.log(2);
        this.listPagamentos = pagamentos;
        this.valorPago = this.calcularValorPago();
        this.valorRestante = this.calcularValorRestante();
      });
    });

  }

  ionViewDidLoad() {

  }

  adicionarPagamento() {

    let pagamento = new BtpPagamentoDespesa();
    pagamento.valorPagamento = Number(this.formPagamento.value.valorPagamento);
    pagamento.valorPagamento = Number(pagamento.valorPagamento.toFixed(2));
    pagamento.datPagamento = this.formPagamento.value.dataPagamento;
    pagamento.btpDespesa = this.btpDespesa;
    pagamento.usuario = new Usuario();
    pagamento.usuario.idUsuario = this.idUsuario;
    pagamento.isServidor = false;
    pagamento.ativo = true;


    if (this.calcularValorPago() + pagamento.valorPagamento > this.btpDespesa.despesaVlr) {
      this.msgProvider.addMensagemAdvertencia("Valor adicionado supera valor da receita", 3000, MensagemProvider.TOP);
      return;
    }
    if (pagamento.valorPagamento == 0) {
      this.msgProvider.addMensagemAdvertencia("Digite um número maior do que ZERO!", 3000, MensagemProvider.TOP);
      return;
    }

    this.listPagamentos.push(pagamento);

    this.valorRestante = this.calcularValorRestante();
    this.valorPago = this.calcularValorPago();
  }

  calcularValorPago(): number {
    let somaValorPago: number = 0;

    this.listPagamentos.forEach(element => {
      somaValorPago = element.valorPagamento + somaValorPago;
    });
    return Number(somaValorPago.toFixed(2));
  }

  calcularValorRestante(): number {
    return Number((this.btpDespesa.despesaVlr - this.calcularValorPago()).toFixed(2));
  }

  desabilitarBotaoAdicionarPagamento(): boolean {
    return this.btpDespesa.despesaVlr == this.calcularValorPago();
  }

  excluirPagamento(index) {
    let btpPagamentoDespesa: BtpPagamentoDespesa = this.listPagamentos[index];

    if (btpPagamentoDespesa.idPagamento != null) {
      this.listPagamentosRemover.push(btpPagamentoDespesa);
    }
    this.listPagamentos.splice(index, 1);

    this.valorRestante = this.calcularValorRestante();
    this.valorPago = this.calcularValorPago();
  }

  salvar() {
    this.utils.showLoading();
    let promises = [];
    promises.push(this.pagamentoService.incluirPagamentosDespesa(this.listPagamentos));
    promises.push(this.pagamentoService.excluirPagamentosDespesa(this.listPagamentosRemover));

    if (this.calcularValorPago() == this.btpDespesa.despesaVlr) {
      this.btpDespesa.despesaPaga = true;
    } else {
      this.btpDespesa.despesaPaga = false;
    }
    this.btpDespesa.despesaValorPago = this.valorPago;
    this.btpDespesa.isServidor = false;
    promises.push(this.despesaService.alterar(this.btpDespesa));

    Promise.all(promises).then(()=>{
      
      this.enviarPagamentosServidor();

    }).catch((err)=>{
      console.error(err.message);
      this.utils.closeLoading();
    })

  }

  fecharModal() {
    this.viewCtrl.dismiss();
  }

  private enviarPagamentosServidor() {
    return this.pagamentoDespesaWs.enviarPagamentosDespesasServidor().then(() => {
      this.viewCtrl.dismiss(this.btpDespesa);
      this.utils.closeLoading();
    }).catch((err) => {
      console.log('err enviar Despesas ws');
      console.error(err.message);
      this.viewCtrl.dismiss(this.btpDespesa);
      this.utils.closeLoading();
    });
  }

}
