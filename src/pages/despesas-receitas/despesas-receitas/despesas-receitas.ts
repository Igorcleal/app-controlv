import { Storage } from '@ionic/storage';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Slides, ModalController, PopoverController } from 'ionic-angular';

import { BtpDespesa } from './../../../model/btpDespesa';
import { DespesaService } from './../../../providers/despesa/despesa-service';
import { PopoverDespesaReceita } from './../../../components/popover-despesa-receita/popover-despesa-receita';
import { ReceitaService } from './../../../providers/receita/receita-service';
import { BtpReceita } from './../../../model/btpReceita';
import { DespesaAddPage } from './../despesa-add/despesa-add';
import { ReceitaAddPage } from './../receita-add/receita-add';

@Component({
    selector: 'page-despesas-receitas',
    templateUrl: 'despesas-receitas.html'
})
export class DespesasReceitasPage {

    @ViewChild(Slides) slides: Slides;
    public listDespesas: Array<BtpDespesa>;
    public listDespesasSemFiltro: Array<BtpDespesa>;
    public listReceitas: Array<BtpReceita>;
    public listReceitasSemFiltro: Array<BtpReceita>;
    public totalDespesas: number;
    public totalReceitas: number;
    public filtroData: Date = new Date();
    public filtroDespesaReceita: string = "";
    public filtroReceita: string = "";
    public btnSearchClicked: boolean = false;
    public meses = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho",
        "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
    public exibirTodos: boolean = false;

    constructor(public navCtrl: NavController, public navParams: NavParams,
        public modalCtrl: ModalController, public storage: Storage,
        public despesaService: DespesaService, public receitaService: ReceitaService,
        public popoverCtrl: PopoverController) {

        if (this.listDespesas == null) {
            this.listDespesas = new Array();
        }
        if (this.listReceitas == null) {
            this.listReceitas = new Array();
        }

    }

    ionViewDidLoad() {
         this.carregarDespesasReceitas();
    }

    carregarDespesasReceitas() {
        this.getAllDespesas();
        this.getAllReceitas();
    }

    showPopover(myEvent) {
        let popover = this.popoverCtrl.create(PopoverDespesaReceita, {exibirTodos: !this.exibirTodos});
        popover.onDidDismiss(data => {
            if (data && data == "consultar") {
                this.filtrar();
            }
            else if (data && data == "exibir") {
                this.exibirTodos = !this.exibirTodos;
                this.carregarDespesasReceitas();
            }
        });
        popover.present({
            ev: myEvent
        });
    }

    getAllDespesas() {
        this.storage.get('idUsuarioLogado').then((data) => {

            if (this.exibirTodos == false) {
                this.despesaService.getAllByDate(data, this.filtroData)
                    .then(despesas => {
                        this.listDespesas = despesas;
                        this.listDespesasSemFiltro = despesas
                        this.totalDespesas = 0;
                        for (let despesa of this.listDespesas) {
                            this.totalDespesas = this.totalDespesas + Number(despesa.despesaVlr);
                        }
                    });
            } else {
                this.despesaService.getAll(data)
                    .then(despesas => {
                        this.listDespesas = despesas;
                        this.listDespesasSemFiltro = despesas
                        this.totalDespesas = 0;
                        for (let despesa of this.listDespesas) {
                            this.totalDespesas = this.totalDespesas + Number(despesa.despesaVlr);
                        }
                    });
            }
        });
    }

    getAllReceitas() {
        this.storage.get('idUsuarioLogado').then((data) => {
            if (this.exibirTodos == false) {
            this.receitaService.getAllByDate(data, this.filtroData)
                .then(receitas => {
                    this.listReceitas = receitas;
                    this.listReceitasSemFiltro = receitas;
                    this.totalReceitas = 0;
                    for (let receita of this.listReceitas) {
                        console.log(receita.btpConta.contaId);
                        this.totalReceitas = this.totalReceitas + Number(receita.receitaVlr);
                    }

                });
            }
            else{
                this.receitaService.getAll(data)
                .then(receitas => {
                    this.listReceitas = receitas;
                    this.listReceitasSemFiltro = receitas;
                    this.totalReceitas = 0;
                    for (let receita of this.listReceitas) {
                        this.totalReceitas = this.totalReceitas + Number(receita.receitaVlr);
                    }

                });
            }
        });
    }

    adicionarDespesaReceita() {
        if (this.slides.getActiveIndex() == 0) {
            let modalDespesa = this.modalCtrl.create(DespesaAddPage);
            this.abrirModalDespesa(modalDespesa);
        } else {
            let modalReceita = this.modalCtrl.create(ReceitaAddPage);
            this.abrirModalReceita(modalReceita);
        }
    }

    editarDespesa(btpDespesa: BtpDespesa) {
        let modal = this.modalCtrl.create(DespesaAddPage, { btpDespesa: btpDespesa });
        this.abrirModalDespesa(modal);
    }

    editarReceita(btpReceita: BtpReceita) {
        let modal = this.modalCtrl.create(ReceitaAddPage, { btpReceita: btpReceita });
        this.abrirModalReceita(modal);
    }

    private abrirModalDespesa(modal) {
        modal.onDidDismiss(data => {
            this.getAllDespesas();
        });
        modal.present();
    }

    private abrirModalReceita(modal) {
        modal.onDidDismiss(data => {
            this.getAllReceitas();
        });
        modal.present();
    }

    selecionarReceitas() {
        document.getElementById("divTipoSelecionado").style.left = "50%";
        this.slides.slideTo(1, 200);
        this.limparCampos();
    }

    selecionarDespesas() {
        document.getElementById("divTipoSelecionado").style.left = "20%";
        this.slides.slideTo(0, 200);
        this.limparCampos();
    }

    slideChanged() {
        let currentIndex = this.slides.getActiveIndex();
        if (currentIndex == 0) {
            document.getElementById("divTipoSelecionado").style.left = "20%";
        } else {
            document.getElementById("divTipoSelecionado").style.left = "50%";
        }

        this.limparCampos();
    }

    getPrecoFormatado(price: number) {
        return new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(price);
    }

    incrementaData() {
        this.filtroData.setMonth(this.filtroData.getMonth() + 1);
        this.carregarDespesasReceitas();
    }

    decrementaData() {
        this.filtroData.setMonth(this.filtroData.getMonth() - 1);
        this.carregarDespesasReceitas();
    }

    filtrarDespesasReceitas() {
        let currentIndex = this.slides.getActiveIndex();
        if (currentIndex == 0) {
            this.listDespesas = this.listDespesasSemFiltro.filter((item) => {
                return item.despesa.toLowerCase().indexOf(this.filtroDespesaReceita.toLowerCase()) > -1;
            });
        }

        if (currentIndex == 1) {
            this.listReceitas = this.listReceitasSemFiltro.filter((item) => {
                return item.receita.toLowerCase().indexOf(this.filtroDespesaReceita.toLowerCase()) > -1;
            });
        }

    }

    limparCampos() {
        this.filtroDespesaReceita = "";
        this.btnSearchClicked = false;
        this.listDespesas = this.listDespesasSemFiltro;
        this.listReceitas = this.listReceitasSemFiltro;
    }

    filtrar() {
        if (this.btnSearchClicked) {
            this.btnSearchClicked = false;
            document.getElementById("barraBusca").style.left = '100%';
        } else {
            this.btnSearchClicked = true;
            this.animarSearchBar();
        }

    }

    animarSearchBar() {
        var elem = document.getElementById("barraBusca");
        var pos = 100;
        var id = setInterval(frame, 1);
        function frame() {
            if (pos == 0) {
                clearInterval(id);
            } else {
                pos = pos - 10;
                //elem.style.width = pos + '%';
                elem.style.left = pos + '%';
            }
        }
    }
}