import { Storage } from '@ionic/storage';
import { Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController, ModalController } from 'ionic-angular';

import { BtpConta } from './../../../model/btpConta';
import { ContaService } from './../../../providers/conta/conta-service';
import { UtilsService } from './../../../providers/utils-service';
import { BtpVenda } from './../../../model/btpVenda';
import { PagamentosPage } from './../pagamentos/pagamentos';
import { Categoria } from './../../../model/categoria';
import { CategoriaService } from './../../../providers/categoria/categoria-service';
import { ReceitaService } from './../../../providers/receita/receita-service';
import { BtpReceita } from './../../../model/btpReceita';
import { TipoCategoria } from '../../../model/tipo-categoria';
import { Usuario } from './../../../model/usuario';
import { EnumPeriodicidade } from '../../../model/enum-periodicidade';
import * as moment from 'moment';
import { ReceitaWsProvider } from "../../../providers/receita/receita-ws";

@Component({
  selector: 'page-receita-add',
  templateUrl: 'receita-add.html'
})
export class ReceitaAddPage {

  formReceita: FormGroup;
  btpReceita: BtpReceita;
  showPagarReceita: boolean = false;
  repetir: boolean = false;
  periodicidade: EnumPeriodicidade;
  qtdPeriodicidade: number;
  masks: any;

  listCategorias: Categoria[];
  listContas: BtpConta[] = new Array();

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public formBuilder: FormBuilder, public viewCtrl: ViewController,
    public receitaService: ReceitaService, public storage: Storage,
    public alertCtrl: AlertController, public categoriaService: CategoriaService,
    public modalCtrl: ModalController, public utils: UtilsService, public contaService: ContaService,
    public receitaWS: ReceitaWsProvider) {
    this.inicializar();
  }

  inicializar() {

    this.btpReceita = this.navParams.get('btpReceita');
    if (this.btpReceita == null) {
      this.btpReceita = new BtpReceita();
      this.btpReceita.receitaPaga = true;
      this.btpReceita.btpVenda = new BtpVenda();
    }

    if (this.btpReceita.categoria == null) {
      this.btpReceita.categoria = new Categoria();
    }

    if (this.btpReceita.btpConta == null) {
      this.btpReceita.btpConta = new BtpConta();
    }

    this.carregarListas();

    this.formReceita = this.formBuilder.group({
      receita: [this.btpReceita.receita, Validators.compose([Validators.required])],
      receitaVlr: [this.btpReceita.receitaVlr, Validators.compose([Validators.required])],
      categoriaId: [this.btpReceita.categoria.categoriaId, Validators.compose([Validators.required])],
      contaId: [this.btpReceita.btpConta.contaId],
      receitaDta: [this.btpReceita.receitaDta == null ? moment().format() : this.btpReceita.receitaDta, Validators.compose([Validators.required])],
      receitaPaga: [this.btpReceita.receitaPaga],
      receitaVlrPago: [this.btpReceita.receitaVlrPago],
      repetir: [],
      periodicidade: [],
      qtdPeriodicidade: []
    });

    this.masks = {
      doisNumeros: [/[0-9]/, /\d/]
    };

  }

  abrirPagamentos() {
    let modalPagamento = this.modalCtrl.create(PagamentosPage, { btpReceita: this.btpReceita });
    modalPagamento.onDidDismiss(btpReceita => {
      if (btpReceita != null) {
        this.viewCtrl.dismiss();
      }
    })
    modalPagamento.present();
  }

  carregarListas() {
    this.storage.get('idUsuarioLogado').then((data) => {
      this.categoriaService.getAll(TipoCategoria.despesas_receitas, data)
        .then(categorias => {
          this.listCategorias = categorias;
        });

      this.contaService.getAll(data).then(contas => {
        this.listContas = contas;
      })
    });
  }

  salvar() {
    if (this.formReceita.valid) {

      let receitaId: number = this.btpReceita.receitaId;
      let isServidor = this.btpReceita.isServidor;
      let ativo = this.btpReceita.ativo;
      this.btpReceita = this.formReceita.value;
      this.btpReceita.receitaId = receitaId;
      this.btpReceita.isServidor = isServidor;
      this.btpReceita.ativo = ativo;

      if (this.btpReceita.categoria == null) {
        this.btpReceita.categoria = new Categoria();
      }
      this.btpReceita.categoria.categoriaId = this.formReceita.value.categoriaId;

      if (this.btpReceita.btpConta == null) {
        this.btpReceita.btpConta = new BtpConta();
      }
      this.btpReceita.btpConta.contaId = this.formReceita.value.contaId;

      if (this.btpReceita.receitaId == null) {
        this.incluirReceita();
      } else {
        this.alterarReceita();
      }
    }
  }

  incluirReceita() {
    this.btpReceita.isServidor = false;
    this.utils.showLoading();

    this.utils.getUsuarioLogado().then(usuario => {
      this.btpReceita.usuario = usuario;

      this.receitaService.incluir(this.btpReceita)
        .then(idReceitaInserida => {
          console.log("success incluir receita");

          if (this.repetir == true) {
            this.incluirReceitasPeriodicas(idReceitaInserida).then(() => {
              this.enviarReceitasServidor();
            });
          } else {
            this.enviarReceitasServidor();
          }
        }).catch((error: Error) => {
          console.log("erro incluir receita");
          console.log(error.message);
          this.utils.closeLoading();
        });
    });
  }

  incluirReceitasPeriodicas(idReceitaInserida: number) {
    let data = new Date(this.btpReceita.receitaDta);
    let promises = [];
    for (var i = 1; i < this.qtdPeriodicidade; i++) {

      let novaReceita = this.btpReceita;
      novaReceita.receitaDta = new Date(this.btpReceita.receitaDta);
      novaReceita.receitaPaga = false;
      novaReceita.btpReceitaPai = new BtpReceita();
      novaReceita.btpReceitaPai.receitaId = idReceitaInserida;

      if (this.periodicidade == EnumPeriodicidade.diario || this.periodicidade == EnumPeriodicidade.semanal) {
        data.setUTCDate(data.getUTCDate() + this.calcularDiasIncremento());
        novaReceita.receitaDta = new Date(data);
      } else if (this.periodicidade == EnumPeriodicidade.mensal || this.periodicidade == EnumPeriodicidade.semestral) {
        data.setUTCMonth(data.getUTCMonth() + this.calcularMesIncremento());
        novaReceita.receitaDta = new Date(data);
      } else if (this.periodicidade == EnumPeriodicidade.anual) {
        data.setUTCFullYear(data.getUTCFullYear() + this.calcularAnoIncremento());
        novaReceita.receitaDta = new Date(data);
      }

      promises.push(this.receitaService.incluirReceitaPeriodica(novaReceita))
    }
    return Promise.all(promises);
  }

  calcularAnoIncremento(): number {
    if (this.periodicidade == EnumPeriodicidade.anual) {
      return 1;
    }
  }

  calcularMesIncremento(): number {
    if (this.periodicidade == EnumPeriodicidade.mensal) {
      return 1;
    }
    if (this.periodicidade == EnumPeriodicidade.semestral) {
      return 6;
    }
  }

  calcularDiasIncremento(): number {
    if (this.periodicidade == EnumPeriodicidade.diario) {
      return 1;
    }
    if (this.periodicidade == EnumPeriodicidade.semanal) {
      return 7;
    }

    if (this.periodicidade == EnumPeriodicidade.anual) {
      return 360;
    }
  }

  alterarReceita() {
    this.btpReceita.isServidor = false;
    this.utils.showLoading();
    this.receitaService.alterar(this.btpReceita).then(response => {
      console.log("success alterar receita");

      this.enviarReceitasServidor();

    }).catch(error => {
      console.log("erro alterar receita");
      this.utils.closeLoading();
    });
  }

  fecharModal() {
    this.viewCtrl.dismiss();
  }

  deletar() {

    let confirm = this.alertCtrl.create({
      title: 'Excluir',
      message: 'Deseja excluir receita?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: (data) => {
            
            this.btpReceita.ativo = false;
            this.alterarReceita();

          }
        }
      ]
    });
    confirm.present();
  }

  rangeAnosData(): Array<number> {
    let data = new Date()
    let anoAtual: number = data.getFullYear();

    let anos: Array<number> = new Array();
    anos.push(anoAtual - 1);
    anos.push(anoAtual);
    for (var _i = 0; _i < 4; _i++) {
      anoAtual++;
      anos.push(anoAtual);
    }
    return anos;
  }

  private enviarReceitasServidor() {
    return this.receitaWS.enviarReceitasServidor().then(() => {
      this.viewCtrl.dismiss();
      this.utils.closeLoading();
    }).catch((err) => {
      console.log('err enviar Despesas ws');
      console.error(err.message);
      this.viewCtrl.dismiss();
      this.utils.closeLoading();
    });
  }

}
