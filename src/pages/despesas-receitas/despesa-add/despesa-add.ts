import { DatePicker } from '@ionic-native/date-picker';
import { Storage } from '@ionic/storage';
import { Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController, ModalController } from 'ionic-angular';

import { ContaService } from './../../../providers/conta/conta-service';
import { BtpConta } from './../../../model/btpConta';
import { BtpCompra } from './../../../model/btpCompra';
import { PagamentosDespesasPage } from './../pagamentos-despesas/pagamentos-despesas';
import { UtilsService } from './../../../providers/utils-service';
import { Categoria } from './../../../model/categoria';
import { CategoriaService } from './../../../providers/categoria/categoria-service';
import { DespesaService } from './../../../providers/despesa/despesa-service';
import { Usuario } from './../../../model/usuario';
import { BtpDespesa } from './../../../model/btpDespesa';
import { TipoCategoria } from '../../../model/tipo-categoria';
import { EnumPeriodicidade } from '../../../model/enum-periodicidade';
import * as moment from 'moment';
import { DespesaWsProvider } from "../../../providers/despesa/despesa-ws";

@Component({
  selector: 'page-despesa-add',
  templateUrl: 'despesa-add.html'
})
export class DespesaAddPage {

  formDespesa: FormGroup;
  btpDespesa: BtpDespesa;
  periodicidade: EnumPeriodicidade;
  qtdPeriodicidade: number = null;
  repetir: boolean = false;
  masks: any;

  listCategorias: Categoria[];
  listContas: BtpConta[] = new Array();

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public formBuilder: FormBuilder, public viewCtrl: ViewController,
    public despesaService: DespesaService, public storage: Storage,
    public alertCtrl: AlertController, public datePicker: DatePicker,
    public categoriaService: CategoriaService, public utils: UtilsService,
    public modalCtrl: ModalController, public contaService: ContaService,
    public despesaWS: DespesaWsProvider) {
    this.inicializar();
  }

  inicializar() {
    this.btpDespesa = this.navParams.get('btpDespesa');

    if (this.btpDespesa == null) {
      this.btpDespesa = new BtpDespesa();
      this.btpDespesa.despesaDta = null;
      this.btpDespesa.despesaPaga = true;
      this.btpDespesa.btpCompra = new BtpCompra();
    }

    if (this.btpDespesa.categoria == null) {
      this.btpDespesa.categoria = new Categoria();
    }

    if (this.btpDespesa.btpConta == null) {
      this.btpDespesa.btpConta = new BtpConta();
    }

    this.carregarListas();

    this.formDespesa = this.formBuilder.group({
      despesa: [this.btpDespesa.despesa, Validators.compose([Validators.required])],
      despesaVlr: [this.btpDespesa.despesaVlr, Validators.compose([Validators.required])],
      categoriaId: [this.btpDespesa.categoria.categoriaId, Validators.compose([Validators.required])],
      contaId: [this.btpDespesa.btpConta.contaId],
      despesaDta: [this.btpDespesa.despesaDta == null ? moment().format() : this.btpDespesa.despesaDta, Validators.compose([Validators.required])],
      despesaPaga: [this.btpDespesa.despesaPaga],
      despesaVlrPago: [this.btpDespesa.despesaValorPago],
      repetir: [],
      periodicidade: [],
      qtdPeriodicidade: []
    });

    this.masks = {
      doisNumeros: [/[0-9]/, /\d/]
    };
  }

  carregarListas() {
    this.storage.get('idUsuarioLogado').then((data) => {
      this.categoriaService.getAll(TipoCategoria.despesas_receitas, data)
        .then(categorias => {
          this.listCategorias = categorias;
        });

      this.contaService.getAll(data).then(contas => {
        this.listContas = contas;
      })
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DespesaAddPage');
  }

  salvar() {
    console.log(this.formDespesa.value.despesaDta);
    if (this.formDespesa.valid) {
      let despesaId: number = this.btpDespesa.despesaId;
      let isServidor: Boolean = this.btpDespesa.isServidor;
      let ativo: Boolean = this.btpDespesa.ativo;
      this.btpDespesa = this.formDespesa.value;
      this.btpDespesa.despesaId = despesaId;
      this.btpDespesa.isServidor = isServidor;
      this.btpDespesa.ativo = ativo;

      if (this.btpDespesa.categoria == null) {
        this.btpDespesa.categoria = new Categoria();
      }
      this.btpDespesa.categoria.categoriaId = this.formDespesa.value.categoriaId;

      if (this.btpDespesa.btpConta == null) {
        this.btpDespesa.btpConta = new BtpConta();
      }
      this.btpDespesa.btpConta.contaId = this.formDespesa.value.contaId;

      if (this.btpDespesa.despesaId == null) {
        this.incluirDespesa();
      } else {
        this.alterarDespesa();
      }
    }
  }

  incluirDespesa() {
    this.btpDespesa.isServidor = false;
    this.utils.showLoading();

    this.utils.getUsuarioLogado().then(usuario => {
      this.btpDespesa.usuario = usuario;

      this.despesaService.incluir(this.btpDespesa)
        .then(idInserido => {
          console.log("success incluir despesa");
          console.log(idInserido);
          
          if (this.repetir == true) {
            this.incluirDespesasPeriodicas(idInserido).then(() => {
              this.enviarDespesasServidor();
            })
          }
          else {
            this.enviarDespesasServidor();
          }
        }).catch((error: Error) => {
          console.log("erro incluir despesa");
          console.error(error.message);
          this.utils.closeLoading();
        });
    });
  }

  incluirDespesasPeriodicas(idDespesaInserida: number) {
    let data = new Date(this.btpDespesa.despesaDta);
    let promises = [];
    for (var i = 1; i < this.qtdPeriodicidade; i++) {

      let novaDespesa = this.btpDespesa;
      novaDespesa.despesaDta = new Date(this.btpDespesa.despesaDta);
      novaDespesa.despesaPaga = false;
      novaDespesa.btpDespesaPai = new BtpDespesa();
      novaDespesa.btpDespesaPai.despesaId = idDespesaInserida;
      novaDespesa.isServidor = false;

      if (this.periodicidade == EnumPeriodicidade.diario || this.periodicidade == EnumPeriodicidade.semanal) {
        data.setUTCDate(data.getUTCDate() + this.calcularDiasIncremento());
        novaDespesa.despesaDta = new Date(data);
      } else if (this.periodicidade == EnumPeriodicidade.mensal || this.periodicidade == EnumPeriodicidade.semestral) {
        data.setUTCMonth(data.getUTCMonth() + this.calcularMesIncremento());
        novaDespesa.despesaDta = new Date(data);
      } else if (this.periodicidade == EnumPeriodicidade.anual) {
        data.setUTCFullYear(data.getUTCFullYear() + this.calcularAnoIncremento());
        novaDespesa.despesaDta = new Date(data);
      }

      promises.push(this.despesaService.incluirDespesasPeriodicas(novaDespesa))
    }
    return Promise.all(promises);
  }

  calcularAnoIncremento(): number {
    if (this.periodicidade == EnumPeriodicidade.anual) {
      return 1;
    }
  }

  calcularMesIncremento(): number {
    if (this.periodicidade == EnumPeriodicidade.mensal) {
      return 1;
    }
    if (this.periodicidade == EnumPeriodicidade.semestral) {
      return 6;
    }
  }

  calcularDiasIncremento(): number {
    if (this.periodicidade == EnumPeriodicidade.diario) {
      return 1;
    }
    if (this.periodicidade == EnumPeriodicidade.semanal) {
      return 7;
    }

    if (this.periodicidade == EnumPeriodicidade.anual) {
      return 360;
    }
  }

  alterarDespesa() {
    this.btpDespesa.isServidor = false;
    this.utils.showLoading();
    this.despesaService.alterar(this.btpDespesa).then(response => {
      console.log("success alterar despesa");

      this.enviarDespesasServidor();

    }).catch(error => {
      console.log("erro alterar despesa");
      this.utils.closeLoading();
    });
  }

  fecharModal() {
    this.viewCtrl.dismiss();
  }

  deletarDespesa() {

    let confirm = this.alertCtrl.create({
      title: 'Excluir',
      message: 'Deseja excluir despesa?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
            this.btpDespesa.ativo = false;
            this.alterarDespesa();
          }
        }
      ]
    });
    confirm.present();
  }

  abrirPagamentos() {
    let modalPagamento = this.modalCtrl.create(PagamentosDespesasPage, { btpDespesa: this.btpDespesa });
    modalPagamento.onDidDismiss(btpDespsa => {
      if (btpDespsa != null) {
        this.viewCtrl.dismiss();
      }
    })
    modalPagamento.present();
  }

  rangeAnosData(): Array<number> {
    let data = new Date()
    let anoAtual: number = data.getFullYear();

    let anos: Array<number> = new Array();
    anos.push(anoAtual - 1);
    anos.push(anoAtual);
    for (var _i = 0; _i < 4; _i++) {
      anoAtual++;
      anos.push(anoAtual);
    }
    return anos;
  }

  private enviarDespesasServidor() {
    return this.despesaWS.enviarDespesasServidor().then(() => {
      this.viewCtrl.dismiss();
      this.utils.closeLoading();
    }).catch((err) => {
      console.log('err enviar Despesas ws');
      console.error(err.message);
      this.viewCtrl.dismiss();
      this.utils.closeLoading();
    });
  }

}
