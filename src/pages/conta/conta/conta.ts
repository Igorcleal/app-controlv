import { ContaAddPage } from './../conta-add/conta-add';
import { ContaService } from './../../../providers/conta/conta-service';
import { BtpConta } from './../../../model/btpConta';
import { Storage } from '@ionic/storage';
import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

@Component({
  selector: 'page-conta',
  templateUrl: 'conta.html'
})
export class ContaPage {

  lstContas: Array<BtpConta>;
  lstContasNoFilter: Array<BtpConta>;
  renderSearchBar: Boolean = false;
  filtroContaNome: string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public modalCtrl: ModalController, public contaService: ContaService,
    public storage: Storage) {
    if (this.lstContas == null) {
      this.lstContas = new Array();
    }
  }

  ionViewDidLoad() {
    this.getAllContas();
  }

  getAllContas() {
    this.storage.get('idUsuarioLogado').then((data) => {
      this.contaService.getAll(data)
        .then(lst => {
          this.lstContas = lst;
          this.lstContasNoFilter = lst;
        });
    });
  }

  adicionarConta() {
    let modal = this.modalCtrl.create(ContaAddPage);
    this.abrirModalConta(modal);
  }

  editarConta(btpConta: BtpConta) {
    let modal = this.modalCtrl.create(ContaAddPage, { btpConta: btpConta });
    this.abrirModalConta(modal);
  }

  private abrirModalConta(modal) {
    modal.onDidDismiss(data => {
      this.getAllContas();
      console.log("fechado popup contas");
    });
    console.log("abrir popup contas");
    modal.present();
  }

  exibirSearchBar() {
    if (this.renderSearchBar) {
      this.renderSearchBar = false;
      document.getElementById("contentConta").style.top = "0px";
    } else {
      this.renderSearchBar = true;
      document.getElementById("contentConta").style.top = "50px";
    }
  }

  filtrarContas() {
    this.lstContas = this.lstContasNoFilter.filter((item) => {
      return item.contaDsc.toLowerCase().indexOf(this.filtroContaNome.toLowerCase()) > -1;
    });
  }

}
