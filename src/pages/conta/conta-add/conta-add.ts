import { ContaWS } from './../../../providers/conta/conta-ws';
import { ContaService } from '../../../providers/conta/conta-service';
import { MensagemProvider } from './../../../providers/mensagem-provider';
import { UtilsService } from './../../../providers/utils-service';
import { Storage } from '@ionic/storage';
import { Usuario } from './../../../model/usuario';
import { FormBuilder, Validators } from '@angular/forms';
import { BtpConta } from './../../../model/btpConta';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController } from 'ionic-angular';

@Component({
  selector: 'page-conta-add',
  templateUrl: 'conta-add.html'
})
export class ContaAddPage {

  formConta: any;
  btpConta: BtpConta;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public formBuilder: FormBuilder, public viewCtrl: ViewController,
    public contaService: ContaService, public contaWS: ContaWS, public storage: Storage,
    public alertCtrl: AlertController, public utils: UtilsService,
    public msgProvider: MensagemProvider) {

    this.inicializar();
  }

  inicializar() {
    this.btpConta = this.navParams.get('btpConta');
    if (this.btpConta == null) {
      this.btpConta = new BtpConta();
      this.btpConta.contaDsc = null;
    }

    this.formConta = this.formBuilder.group({
      contaDsc: [this.btpConta.contaDsc, Validators.compose([Validators.required])]
    });
  }

  salvar() {
    if (this.formConta.valid) {

      if (this.btpConta.contaId == null) {
        this.incluir();
      } else {
        this.alterar();
      }
    }
  }

  incluir() {
    this.utils.showLoading();
    this.storage.get('idUsuarioLogado').then((data) => {
      this.btpConta.usuario = new Usuario();
      this.btpConta.usuario.idUsuario = data;

      this.contaService.incluir(this.btpConta).then((data: any) => {
        this.btpConta.contaId = data.insertId;
        //após inserir conta no banco envia conta para Servidor
        this.enviarContaServidor().then(() => {
          this.viewCtrl.dismiss();
          this.utils.closeLoading();
        }).catch(error => {
          this.viewCtrl.dismiss();
          this.utils.closeLoading();
        });

      }).catch((err) => {
        console.log('err incluir conta sqlite');
        console.error(err.message);
        this.utils.closeLoading();
      });

    });
  }



  alterar() {
    this.utils.showLoading();

    this.btpConta.isServidor = false;
    this.contaService.alterar(this.btpConta).then(response => {
      console.log("success alterar fornecedor");

      this.enviarContaServidor().then(() => {
        this.viewCtrl.dismiss();
        this.utils.closeLoading();
      }).catch(error => {
        this.viewCtrl.dismiss();
        this.utils.closeLoading();
      });

    }).catch(error => {
      console.log("erro alterar conta");
      this.utils.closeLoading();
    });
  }

  private enviarContaServidor() {
    return this.contaWS.enviarContasServidor().catch((err) => {
      console.log('err enviar contas ws');
      console.error(err.message);
      return Promise.reject(true);
    });
  }

  fecharModal() {
    this.viewCtrl.dismiss();
  }

  deletar() {
    let confirm = this.alertCtrl.create({
      title: 'Excluir',
      message: 'Deseja excluir conta?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
            this.btpConta.ativo = false;
            this.alterar();
          }
        }
      ]
    });
    confirm.present();
  }

}
