import { Storage } from '@ionic/storage';
import { Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, ViewController, AlertController } from 'ionic-angular';
import * as moment from 'moment';

import { VisitaWsProvider } from './../../../providers/visita/visita-ws';
import { VisitaService } from './../../../providers/visita/visita-service';
import { BtpVisita } from './../../../model/btpVisita';
import { BtpCliente } from './../../../model/cliente';
import { PopupListarClientesPage } from './../../popup-listar-clientes/popup-listar-clientes';
import { UtilsService } from './../../../providers/utils-service';
import { Usuario } from './../../../model/usuario';

@Component({
  selector: 'page-visita-cliente-add',
  templateUrl: 'visita-cliente-add.html'
})
export class VisitaClienteAddPage {

  btpVisita: BtpVisita;
  btpCliente: BtpCliente;
  formCadastroVisita: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController,
    public viewCtrl: ViewController, public formBuilder: FormBuilder, public visitaService: VisitaService,
    public storage: Storage, public alertCtrl: AlertController, public utils: UtilsService,
    public visitaWS: VisitaWsProvider) {
    this.inicializar();
  }

  private inicializar() {
    this.btpVisita = this.navParams.get('btpVisita');

    if (this.btpVisita == null) {
      this.btpVisita = new BtpVisita();
      this.btpVisita.localVisita = "";
      this.btpVisita.dataVisitaString = moment().format();
    }

    if (this.btpVisita.btpCliente == null) {
      this.btpVisita.btpCliente = new BtpCliente();
      this.btpVisita.btpCliente.nome = "";
    }

    this.formCadastroVisita = this.formBuilder.group({
      nomeCliente: [this.btpVisita.btpCliente.nome, Validators.compose([Validators.required])],
      localVisita: [this.btpVisita.localVisita, Validators.compose([Validators.required])],
      dataVisita: [this.btpVisita.dataVisitaString, Validators.compose([Validators.required])],
      horaVisita: [this.btpVisita.horaVisita, Validators.compose([Validators.required])]
    });

  }

  abrirPopupCliente() {
    let modal = this.modalCtrl.create(PopupListarClientesPage);
    modal.onDidDismiss(data => {
      if (data != null) {
        this.btpVisita.btpCliente = data;
      }
    });
    modal.present();
  }

  fecharModal() {
    this.viewCtrl.dismiss();
  }

  salvar() {
    if (this.formCadastroVisita.valid) {

      if (this.btpVisita.idVisita == null) {
        this.incluir();
      } else {
        this.alterar();
      }
    }
  }

  incluir() {
    this.btpVisita.isServidor = false;

    this.utils.showLoading();
    this.utils.getUsuarioLogado().then(usuario => {
      this.btpVisita.btpUsuario = usuario;

      this.visitaService.incluir(this.btpVisita)
        .then(response => {
          console.log("success incluir visita");

          this.enviarVisitasServidor().then(() => {
            this.viewCtrl.dismiss();
            this.utils.closeLoading();
          }).catch((err) => {
            this.viewCtrl.dismiss();
            this.utils.closeLoading();
          });

        }).catch(error => {
          console.log("erro incluir visita");
          console.error(error.message);
          this.utils.closeLoading();
        });
    });


  }

  alterar() {

    this.btpVisita.isServidor = false;
    this.utils.showLoading();

    this.visitaService.update(this.btpVisita).then(response => {
      console.log("success alterar visita");
      this.enviarVisitasServidor().then(() => {
        this.viewCtrl.dismiss();
        this.utils.closeLoading();
      }).catch((err) => {
        this.viewCtrl.dismiss();
        this.utils.closeLoading();
      });

    }).catch(error => {
      console.log("erro alterar visita");
      this.utils.closeLoading();
    });
  }


  deletar() {

    let confirm = this.alertCtrl.create({
      title: 'Excluir',
      message: 'Deseja excluir essa visita?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
           this.btpVisita.ativo = false;
           this.alterar();
          }
        }
      ]
    });
    confirm.present();
  }

  rangeAnosData(): Array<number> {
    let data = new Date()
    let anoAtual: number = data.getFullYear();

    let anos: Array<number> = new Array();
    anos.push(anoAtual);
    for (var _i = 0; _i < 4; _i++) {
      anoAtual++;
      anos.push(anoAtual);
    }
    return anos;
  }

  private enviarVisitasServidor() {
    return this.visitaWS.enviarVisitasServidor().catch((err) => {
      console.log('err enviar Visitas ws');
      console.error(err.message);
      return Promise.reject(true);
    });
  }

}
