import { Storage } from '@ionic/storage';
import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { VisitaService } from './../../../providers/visita/visita-service';
import { BtpVisita } from './../../../model/btpVisita';
import { VisitaClienteAddPage } from './../../visita/visita-cliente-add/visita-cliente-add';

@Component({
  selector: 'page-visita-cliente',
  templateUrl: 'visita-cliente.html'
})
export class VisitaClientePage {

  listVisitas: Array<BtpVisita>;
  public filtroData: Date = new Date();
  public meses = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho",
    "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController,
    public visitaService: VisitaService, public storage: Storage) {
    if (this.listVisitas == null) {
      this.listVisitas = new Array();
    }
  }

  ionViewDidLoad() {
    this.getAllVisitas();
  }

  incrementaData() {
    this.filtroData.setMonth(this.filtroData.getMonth() + 1);
    this.getAllVisitas();
  }

  decrementaData() {
    this.filtroData.setMonth(this.filtroData.getMonth() - 1);
    this.getAllVisitas();
  }

  adicionarVisita() {
    let addProduto = this.modalCtrl.create(VisitaClienteAddPage);
    this.abrirModalVisita(addProduto);
  }

  editarVisita(btpVisita: BtpVisita) {
    let modal = this.modalCtrl.create(VisitaClienteAddPage, { btpVisita: btpVisita });
    this.abrirModalVisita(modal);
  }

  getAllVisitas() {
    this.storage.get('idUsuarioLogado').then((data) => {
      this.visitaService.getVisitasPorData(data, this.filtroData)
        .then(listVisitas => {
          this.listVisitas = listVisitas;
        }).catch(err=>{
          console.log('erro getall visitas');
          console.log(err.message);
        });
    });
  }

  private abrirModalVisita(modal) {
    modal.onDidDismiss(data => {
      this.getAllVisitas();
      console.log("fechado");
    });
    modal.present();
  }

}
