import { UtilsService } from './../../../providers/utils-service';
import { Storage } from '@ionic/storage';
import { BtpDataImportante } from './../../../model/btpDataImportante';

import { PopupListarClientesPage } from './../../popup-listar-clientes/popup-listar-clientes';
import { BtpCliente } from './../../../model/cliente';
import { Component } from '@angular/core';
import { AlertController, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { FormBuilder, Validators } from "@angular/forms";
import { EnumGrauParentesco } from "../../../model/enum-grauParentesco";
import * as moment from 'moment';
import { DataImportanteService } from '../../../providers/data-importante/dataImportante-service';
import { DataImportanteWsProvider } from '../../../providers/data-importante/dataImportante-ws';

@Component({
  selector: 'page-dataImportante-add',
  templateUrl: 'dataImportante-add.html'
})
export class DataImportanteAddPage {

  btpDataImportante: BtpDataImportante;
  listDataImportante: Array<BtpDataImportante>;
  formCadastroDataImportante: any;
  lstEnumGrauParentesco: Array<EnumGrauParentesco>;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public modalCtrl: ModalController, public storage: Storage,
    public dataImportanteService: DataImportanteService,
    public alertCtrl: AlertController, public utils: UtilsService,
    public formBuilder: FormBuilder, public viewCtrl: ViewController,
    public dataImportanteWS: DataImportanteWsProvider) {

    this.inicializar();

  }


  private inicializar() {
    this.btpDataImportante = this.navParams.get('btpDataImportante');

    if (this.btpDataImportante == null) {
      this.btpDataImportante = new BtpDataImportante();
      this.btpDataImportante.sexo = null;
    }

    if (this.lstEnumGrauParentesco == null) {
      this.lstEnumGrauParentesco = new Array<EnumGrauParentesco>();
      this.lstEnumGrauParentesco.push(EnumGrauParentesco.Filho);
      this.lstEnumGrauParentesco.push(EnumGrauParentesco.Filha);
      this.lstEnumGrauParentesco.push(EnumGrauParentesco.Irmão);
      this.lstEnumGrauParentesco.push(EnumGrauParentesco.Irmã);
      this.lstEnumGrauParentesco.push(EnumGrauParentesco.Pai);
      this.lstEnumGrauParentesco.push(EnumGrauParentesco.Mãe);
      this.lstEnumGrauParentesco.push(EnumGrauParentesco.Neto);
      this.lstEnumGrauParentesco.push(EnumGrauParentesco.Filha);
    }

    if (this.btpDataImportante.cliente == null) {
      this.btpDataImportante.cliente = new BtpCliente();
      this.btpDataImportante.cliente.nome = "";
    }

    this.formCadastroDataImportante = this.formBuilder.group({
      nomeCliente: [this.btpDataImportante.cliente.nome, Validators.compose([Validators.required])],
      dscDataImportante: [this.btpDataImportante.dscDataImportante, Validators.compose([Validators.required])],
      dscGrauParentesco: [this.btpDataImportante.dscGrauParentesco],
      sexo: [this.btpDataImportante.sexo],
      enumGrauParentesco: [this.btpDataImportante.enumGrauParentesco],
      diaMes: [this.btpDataImportante.diaMes == null ? moment().format() : this.btpDataImportante.diaMes, Validators.compose([Validators.required])]
    });

  }

  ionViewDidLoad() {

  }

  rangeAnosData(): Array<number> {
    let data = new Date()
    let anoAtual: number = data.getFullYear();

    let anos: Array<number> = new Array();
    anos.push(anoAtual - 1);
    anos.push(anoAtual);
    for (var _i = 0; _i < 4; _i++) {
      anoAtual++;
      anos.push(anoAtual);
    }
    return anos;
  }

  abrirModalClientes() {
    let modal = this.modalCtrl.create(PopupListarClientesPage);
    modal.onDidDismiss(data => {
      if (data != null) {
        this.btpDataImportante.cliente = data;
      }
    });
    modal.present();
  }

  fecharModalDataImportante() {
    this.viewCtrl.dismiss();
  }


  salvar() {
    if (this.formCadastroDataImportante.valid) {
      //recuperar valores do form
      this.btpDataImportante.diaMes = this.formCadastroDataImportante.value.diaMes;
      this.btpDataImportante.dscDataImportante = this.formCadastroDataImportante.value.dscDataImportante;
      this.btpDataImportante.tipDataImportante = this.formCadastroDataImportante.value.tipDataImportante;
      this.btpDataImportante.enumGrauParentesco = this.formCadastroDataImportante.value.enumGrauParentesco;
      this.btpDataImportante.sexo = this.formCadastroDataImportante.value.sexo;

      if (this.btpDataImportante.idDataImportante == null) {
        this.incluir();
      } else {
        this.alterar();
      }
    }
  }

  incluir() {
    this.btpDataImportante.isServidor = false;
    this.utils.getUsuarioLogado().then(usuario => {

      this.btpDataImportante.usuario = usuario;
      this.utils.showLoading();
      this.dataImportanteService.incluir(this.btpDataImportante)
        .then(response => {

          //após inserir produtos no banco envia data importante para Servidor
          this.enviarDataImportanteServidor().then(() => {
            this.viewCtrl.dismiss();
            this.utils.closeLoading();
          }).catch((err) => {
            this.viewCtrl.dismiss();
            this.utils.closeLoading();
          });

        }).catch(error => {
          console.error(error.message);
          console.log("erro incluir Data importante");
          this.utils.closeLoading();
        });
    });

  }

  alterar() {
    this.btpDataImportante.isServidor = false;
    this.utils.showLoading();
    this.dataImportanteService.update(this.btpDataImportante).then(response => {
      this.enviarDataImportanteServidor().then(() => {
        this.utils.voltarParaListagem(this.viewCtrl);
      }).catch(() => {
        this.utils.voltarParaListagem(this.viewCtrl);
      });
    }).catch(error => {
      console.log("erro alterar data importante");
      this.utils.closeLoading();
    });
  }


  deletarDataImportante() {

    let confirm = this.alertCtrl.create({
      title: 'Excluir',
      message: 'Deseja excluir essa data importante?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
            this.btpDataImportante.ativo = false;
            this.alterar();
          }
        }
      ]
    });
    confirm.present();
  }

  private enviarDataImportanteServidor() {
    return this.dataImportanteWS.enviarDataImportanteServidor().catch((err) => {
      console.log('erro enviar data importante ws');
      console.error(err.message);
      return Promise.reject(true);
    });
  }

}
