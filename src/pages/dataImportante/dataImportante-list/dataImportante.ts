import { DataImportanteAddPage } from '../dataImportante-add/dataImportante-add';
import { BtpDataImportante } from './../../../model/btpDataImportante';
import { Storage } from '@ionic/storage';

import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { EnumGrauParentesco } from "../../../model/enum-grauParentesco";
import { DataImportanteService } from '../../../providers/data-importante/dataImportante-service';

@Component({
  selector: 'dataImportante',
  templateUrl: 'dataImportante.html'
})
export class DataImportantePage {

  listDataImportante: Array<BtpDataImportante>;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public modalCtrl: ModalController, public dataImportanteService: DataImportanteService,
    public storage: Storage) {
    if (this.listDataImportante == null) {
      this.listDataImportante = new Array();
    }
  }

  ionViewDidLoad() {
    this.getAllDataImportante();
  }

  getAllDataImportante() {
    this.storage.get('idUsuarioLogado').then((data) => {
      this.dataImportanteService.getAll(data)
        .then(listDataImportante => {
          this.listDataImportante = listDataImportante;
        });
    });
  }

  incluir() {
    let dataImportanteModal = this.modalCtrl.create(DataImportanteAddPage);
    this.abrirModalDataImportante(dataImportanteModal);
  }

  editar(btpDataImportante) {
    let dataImportanteModal = this.modalCtrl.create(DataImportanteAddPage, { btpDataImportante: btpDataImportante });
    this.abrirModalDataImportante(dataImportanteModal);
  }

  private abrirModalDataImportante(modal) {
    modal.onDidDismiss(data => {
      this.getAllDataImportante();
    });
    modal.present();
  }

  getEnumString(key: any): string {

    return EnumGrauParentesco[key];
  }

}
