import { Storage } from '@ionic/storage';
import { AuthFirebaseService } from './../../providers/auth-firebase-service';
import { Component } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';
import { Usuario } from '../../model/usuario'
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'page-cadastro',
  templateUrl: 'cadastro.html'
})
export class CadastroPage {

  usuario: Usuario;
  masks: any;
  phoneNumber: any = "";
  formCadastroUsuario: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder,
    public authFirebaseService: AuthFirebaseService, public storage: Storage, public menuCtrl:MenuController) {
    this.usuario = new Usuario();
    this.masks = {
      foneMask: ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/],
      cpfMask: [/[0-9]/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/]
    };

    this.formCadastroUsuario = formBuilder.group({
      nomeUsuario: ['', Validators.compose([Validators.required])],
      senhaUsuario: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      emailUsuario: ['', Validators.compose([Validators.required])],
      telefoneUsuario: ['', Validators.compose([Validators.required])],
      cpfUsuario: ['', Validators.compose([Validators.required])],
      sexoUsuario: ['', Validators.compose([Validators.required])],
      enderecoUsuario: ['', Validators.compose([Validators.required])]
    });
  }

}
