import { MensagemProvider } from './../../providers/mensagem-provider';
import { ProdutoAddPage } from './../produto-add/produto-add';
import { Storage } from '@ionic/storage';
import { Produto } from './../../model/produto';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { ProdutoService } from "../../providers/produto/produto-service";

@Component({
  selector: 'page-popup-listar-produtos',
  templateUrl: 'popup-listar-produtos.html'
})
export class PopupListarProdutosPage {
  lstProdutos: Array<Produto>;
  lstProdutosNoFilter: Array<Produto>;
  listProdutosJaAdicionados: Array<Produto>;
  filtroProdutoNome: string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public produtoService: ProdutoService, public storage: Storage,
    public viewCtrl: ViewController, public modalCtrl: ModalController,
    public mensagemProvider: MensagemProvider) {
    if (this.lstProdutos == null) {
      this.lstProdutos = new Array();
    }

    this.listProdutosJaAdicionados = this.navParams.get('listProdutosJaAdicionados');

  }

  ionViewDidLoad() {
    console.log("aberto popup listar produtos");
    this.getAllProdutos();
  }

  getAllProdutos() {
    this.storage.get('idUsuarioLogado').then((data) => {
      this.produtoService.getAll(data)
        .then(clientes => {
          this.lstProdutos = clientes;
          this.lstProdutosNoFilter = clientes;
        });
    });
  }

  escolherProduto(btpProduto) {

    let produtoSelecionado: boolean = false;
    console.log(this.listProdutosJaAdicionados);
    if (this.listProdutosJaAdicionados != null && this.listProdutosJaAdicionados.length > 0) {
      this.listProdutosJaAdicionados.forEach(produto => {
        if (produto.produtoId == btpProduto.produtoId) {
          produtoSelecionado = true;
        }
      });
    }
    console.log(produtoSelecionado);
    if (produtoSelecionado) {
      this.mensagemProvider.addMensagemErro("Produto já adicionado! ", 3000, 'top');
    }
    else {
      console.log("fechar popup listar produtos");
      this.viewCtrl.dismiss(btpProduto);
    }

  }

  filtrarProdutos() {
    this.lstProdutos = this.lstProdutosNoFilter.filter((item) => {
      return item.produtoDsc.toLowerCase().indexOf(this.filtroProdutoNome.toLowerCase()) > -1;
    });
  }

  fechar() {
    this.viewCtrl.dismiss();
  }

  incluirNovoProduto() {
    let modal = this.modalCtrl.create(ProdutoAddPage);
    modal.onDidDismiss(data => {
      this.getAllProdutos();
    });
    modal.present();
  }
}
