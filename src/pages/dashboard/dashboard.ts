import { UtilsService } from './../../providers/utils-service';
import { EnviarDadosWs } from './../../providers/enviar-dados-ws';
import { DownloadWsProvider } from './../../providers/download-ws';
import { DashboardService } from './../../providers/dashboard-service';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Platform, AlertController, ModalController } from 'ionic-angular';
import { Chart } from 'chart.js';
import { Storage } from '@ionic/storage';
import { ModalMensagemPage } from "../modal-mensagem/modal-mensagem";

@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html'
})
export class DashboardPage {

  @ViewChild('doughnutCanvas') doughnutCanvas;
  doughnutChart: any;

  public unregisterBackButtonAction: any;
  public alert: any;

  vlrReceitasAReceberMes: number = 0;
  vlrReceitasRecebidoMes: number = 0;
  vlrReceitasAReceberTotal: number = 0;
  vlrReceitasRecebidoTotal: number = 0;

  vlrDespesasAPagarMes: number = 0;
  vlrDespesasPagasMes: number = 0;
  vlrDespesasAPagarTotal: number = 0;
  vlrDespesasPagasTotal: number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, public dashboardProvider: DashboardService,
    public storage: Storage, public platform: Platform, public alertCtrl: AlertController,
    public downloadWs: DownloadWsProvider, public enviarDadosWs: EnviarDadosWs, public modalCtrl: ModalController,
    public utils: UtilsService) {
  }

  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
    this.calcularValores();
  }

  ionViewWillLeave() {
    // Unregister the custom back button action for this page
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }

  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }

  private customHandleBackButton(): void {
    this.showAlert();
  }

  showAlert() {
    this.alert = this.alertCtrl.create({
      title: 'Sair?',
      message: 'Deseja sair do APP?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            this.alert = null;
          }
        },
        {
          text: 'Sair',
          handler: () => {
            this.platform.exitApp();
          }
        }
      ]
    });
    this.alert.present();
  }

  calcularValores() {
    this.storage.get('usuarioLogado').then((usuario) => {
      //------------receitas---------------
      this.dashboardProvider.getValorReceitasAReceberMes(usuario._idUsuario, new Date()).then((vlr: number) => {
        if (!vlr) {
          vlr = 0;
        }
        this.vlrReceitasAReceberMes = vlr;
      });;
      this.dashboardProvider.getValorReceitasRecebidasMes(usuario._idUsuario, new Date()).then((vlr: number) => {
        if (!vlr) {
          vlr = 0;
        }
        this.vlrReceitasRecebidoMes = vlr;
      });
      this.dashboardProvider.getValorReceitasAReceberTotal(usuario._idUsuario).then((vlr: number) => {
        if (!vlr) {
          vlr = 0;
        }
        this.vlrReceitasAReceberTotal = vlr;
      });
      this.dashboardProvider.getValorReceitasRecebidasTotal(usuario._idUsuario).then((vlr: number) => {
        if (!vlr) {
          vlr = 0;
        }
        this.vlrReceitasRecebidoTotal = vlr;
      });

      //------------despesas---------------
      this.dashboardProvider.getValorDespesasAPagarMes(usuario._idUsuario, new Date()).then((vlr: number) => {
        if (!vlr) {
          vlr = 0;
        } this.vlrDespesasAPagarMes = vlr;
      });;
      this.dashboardProvider.getValorDespesasPagasMes(usuario._idUsuario, new Date()).then((vlr: number) => {
        if (!vlr) {
          vlr = 0;
        } this.vlrDespesasPagasMes = vlr;
      });
      this.dashboardProvider.getValorDespesasAPagarTotal(usuario._idUsuario).then((vlr: number) => {
        if (!vlr) {
          vlr = 0;
        } this.vlrDespesasAPagarTotal = vlr
      });
      this.dashboardProvider.getValorDespesasPagasTotal(usuario._idUsuario).then((vlr: number) => {
        if (!vlr) {
          vlr = 0;
        } this.vlrDespesasPagasTotal = vlr
      });
    });
  }

  ionViewDidLoad() {
    this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
      type: 'doughnut',
      data: {
        labels: ["Dias Percorridos", "Dias restantes"],
        datasets: [{
          label: '# of Votes',
          data: [12, 19],
          backgroundColor: [
            'rgba(54, 162, 235, 0.5)',
            'rgba(54, 162, 235, 0.2)'
          ],
          borderColor: [
            'rgba(54, 162, 235, 1)'
          ],
          hoverBackgroundColor: [
            "#36A2EB",
            "#36A2EB"
          ]
        }]
      },
      options: {
        rotation: 1 * Math.PI,
        circumference: 1 * Math.PI,
        title: {
          position: 'bottom',
          display: true,
          text: 'Tempo restante assinatura'
        }
      }
    });
  }

  download() {
    this.utils.showLoading();
    this.utils.getUsuarioLogado().then((usuario) => {
      console.log('aaaaaaaaaaaaaaaaaaa'+usuario.idUsuario);
      this.downloadWs.downloadDados(usuario.idUsuario).then(() => {
        this.utils.closeLoading();
        this.modalCtrl.create(ModalMensagemPage, { mensagem: 'Dados baixados com sucesso!', tipo: 'sucesso' }).present();
      })
        .catch(error => {
          this.utils.closeLoading();
          this.modalCtrl.create(ModalMensagemPage, { mensagem: error.message, tipo: 'erro' }).present();
        });
    });
  }

  enviarDados() {
    this.utils.showLoading();
    this.enviarDadosWs.enviarDados()
      .then(() => {
        this.utils.closeLoading();
        this.modalCtrl.create(ModalMensagemPage, { mensagem: 'Dados enviados com sucesso!', tipo: 'sucesso' }).present();
      })
      .catch(error => {
        this.utils.closeLoading();
        this.modalCtrl.create(ModalMensagemPage, { mensagem: error.message, tipo: 'erro' }).present();
      });
  }

}
