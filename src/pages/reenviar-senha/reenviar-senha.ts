import { ModalMensagemPage } from './../modal-mensagem/modal-mensagem';
import { UtilsService } from './../../providers/utils-service';
import { AuthFirebaseService } from './../../providers/auth-firebase-service';
import { Component } from '@angular/core';
import { NavController, AlertController, ModalController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmailValidator } from '../../validators/email';

@Component({
  selector: 'page-reenviar-senha',
  templateUrl: 'reenviar-senha.html',
})
export class ReenviarSenhaPage {

  public resetPasswordForm: FormGroup;

  constructor(public auth:AuthFirebaseService, public formBuilder: FormBuilder,
    public nav: NavController, public alertCtrl: AlertController, public utils:UtilsService,
    public modalCtrl:ModalController) {

    this.resetPasswordForm = formBuilder.group({
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
    })
  }

  resetPassword(){
    if (!this.resetPasswordForm.valid){
      console.log(this.resetPasswordForm.value);
    } else {
      this.auth.resetPasword(this.resetPasswordForm.value.email)
      .then((user) => {
        let alert = this.alertCtrl.create({
          message: "We just sent you a reset link to your email",
          buttons: [
            {
              text: "Ok",
              role: 'cancel',
              handler: () => {
                this.nav.pop();
              }
            }
          ]
        });
        alert.present();
      }, (error) => {
        var errorMessage: string = error.message;
        this.modalCtrl.create(ModalMensagemPage, { mensagem: errorMessage, tipo: 'erro' }).present();
      });
    }
  }

}