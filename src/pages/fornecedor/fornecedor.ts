import { Component } from '@angular/core';
import { NavController, ModalController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { BtpFornecedor } from './../../model/fornecedor';
import { FornecedorService } from './../../providers/fornecedor/fornecedor-service';
import { FornecedorAddPage } from './../fornecedor-add/fornecedor-add';

@Component({
  selector: 'page-fornecedor',
  templateUrl: 'fornecedor.html'
})

export class FornecedorPage {

  lstFornecedores: Array<BtpFornecedor>;
  lstFornecedoresNoFilter: Array<BtpFornecedor>;
  renderSearchBar: Boolean = false;
  filtroFornecedorNome: string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public modalCtrl: ModalController, public fornecedorService: FornecedorService,
    public storage: Storage) {
    if (this.lstFornecedores == null) {
      this.lstFornecedores = new Array();
    }
  }

  ionViewDidLoad() {
    this.getAllFornecedores();
  }

  getAllFornecedores() {
    this.storage.get('idUsuarioLogado').then((data) => {
      this.fornecedorService.getAll(data)
        .then(lst => {
          this.lstFornecedores = lst;
          this.lstFornecedoresNoFilter = lst;
        });
    });
  }

  adicionarFornecedor() {
    let modal = this.modalCtrl.create(FornecedorAddPage);
    this.abrirModalFornecedor(modal);
  }

  editarFornecedor(fornecedor: BtpFornecedor) {
    let modal = this.modalCtrl.create(FornecedorAddPage, { fornecedor: fornecedor });
    this.abrirModalFornecedor(modal);
  }

  private abrirModalFornecedor(modal) {
    modal.onDidDismiss(data => {
      this.getAllFornecedores();
    });
    modal.present();
  }

  exibirSearchBar() {
    if (this.renderSearchBar) {
      this.renderSearchBar = false;
      document.getElementById("contentFornecedor").style.top = "0px";
    } else {
      this.renderSearchBar = true;
      document.getElementById("contentFornecedor").style.top = "50px";
    }
  }

  filtrarFornecedores() {
    this.lstFornecedores = this.lstFornecedoresNoFilter.filter((item) => {
      return item.nomeFornecedor.toLowerCase().indexOf(this.filtroFornecedorNome.toLowerCase()) > -1;
    });
  }
}