import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-modal-mensagem',
  templateUrl: 'modal-mensagem.html',
})
export class ModalMensagemPage {

  public mensagem: string;
  public tipo: string;
  public imagem: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.mensagem = this.navParams.get("mensagem");
    this.tipo = this.navParams.get("tipo");
    
    if(this.tipo=='sucesso'){
      this.imagem = 'assets/img/logo2.png'
    }
    else if(this.tipo=='advertencia'){
      this.imagem = 'assets/img/logo2.png'
    }
    else if(this.tipo=='erro'){
      this.imagem = 'assets/img/logo2.png'
    }
  }

  fecharPopup() {
    this.viewCtrl.dismiss();
  }

}
