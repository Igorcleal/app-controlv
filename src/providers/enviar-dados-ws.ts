import { EstoqueWsProvider } from './estoque/estoque-ws';
import { PagamentoReceitaWs } from './receita/pagamento-receita-ws';
import { PagamentoDespesaWsProvider } from './despesa/pagamento-despesa-ws';
import { DespesaWsProvider } from './despesa/despesa-ws';
import { ReceitaWsProvider } from './receita/receita-ws';
import { VendaWsProvider } from './venda/venda-ws';
import { VisitaWsProvider } from './visita/visita-ws';
import { DataImportanteWsProvider } from './data-importante/dataImportante-ws';
import { ProdutoWsProvider } from './produto/produto-ws';
import { FormaPagamentoWsProvider } from './forma-pagamento/forma-pagamento-ws';
import { FornecedorWsProvider } from './fornecedor/fornecedor-ws';
import { ContaWS } from './conta/conta-ws';
import { ClienteWsProvider } from './cliente/cliente-ws';
import { CategoriaWsProvider } from './categoria/categoria-ws';
import { Injectable } from "@angular/core";
import { CompraWsProvider } from "./compra/compra-ws";

@Injectable()
export class EnviarDadosWs {

    constructor(public categoriaWs: CategoriaWsProvider,
        public clienteWs: ClienteWsProvider,
        public contaWs: ContaWS,
        public fornecedorWs: FornecedorWsProvider,
        public formaPagamentoWs: FormaPagamentoWsProvider,
        public produtoWs: ProdutoWsProvider,
        public estoqueWs: EstoqueWsProvider,
        public dataImportanteWs: DataImportanteWsProvider,
        public visitaWs: VisitaWsProvider,
        public vendaWs: VendaWsProvider,
        public compraWs: CompraWsProvider,
        public receitaWs: ReceitaWsProvider,
        public despesaWs: DespesaWsProvider,
        public pagamentoDespesaWs: PagamentoDespesaWsProvider,
        public pagamentoReceitaWs: PagamentoReceitaWs) {

    }

    enviarDados() {

        return this.enviarCrudsBasicos().then(() => {
            let promises = [];

            promises.push(this.vendaWs.enviarVendasServidorSemDependencias().then(() => {
                return this.receitaWs.enviarReceitasServidorSemDependencias().then(() => {
                    return this.pagamentoReceitaWs.enviarPagamentosReceitasServidorSemDependencias();
                })
            }));

            promises.push(this.compraWs.enviarComprasServidorSemDependencias().then(() => {
                return this.despesaWs.enviarDespesasServidorSemDependencias().then(() => {
                    return this.pagamentoDespesaWs.enviarPagamentosDespesasServidorSemDependencias();
                })
            }));
            return Promise.all(promises);
        }).then(()=>{
            console.log('Success enviar Dados');
        }).catch(err=>{
            console.error('Err enviar dados');
            return Promise.reject(err)
        });

    }

    private enviarCrudsBasicos() {
        let promises = []
        promises.push(this.categoriaWs.enviarCategoriasServidor().then(() => {

            let promises = [];
            promises.push(this.clienteWs.enviarClientesServidorSemDependencias().then(() => {
                
                let promises = []
                promises.push(this.visitaWs.enviarVisitasServidorSemDependencias());
                promises.push(this.dataImportanteWs.enviarDataImportanteServidorSemDependencias());
                return Promise.all(promises);
            }));

            promises.push(this.produtoWs.enviarProdutoServidorSemDependencias().then(() => {
                return this.estoqueWs.enviarEstoqueServidorSemDependencias();
            }));
            return Promise.all(promises);
        }));

        promises.push(this.contaWs.enviarContasServidor());
        promises.push(this.formaPagamentoWs.enviarFormasPagamentosServidor());
        promises.push(this.fornecedorWs.enviarFornecedoresServidor());

        //envia cruds básicos
        return Promise.all(promises)

    }


}