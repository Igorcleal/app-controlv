import { SQLite } from 'ionic-native';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import * as moment from 'moment';

import { DbProvider } from '../db-provider';
import { BtpDataImportante } from '../../model/btpDataImportante';
import { BtpCliente } from '../../model/cliente';
import { Usuario } from '../../model/usuario';

@Injectable()
export class DataImportanteService {

  public db: SQLite = null;

  constructor(public http: Http, public dbProvider: DbProvider) {
    this.db = dbProvider.db;
  }

  getAll(userId: number) {
    let sql = `SELECT di.*, c.nome, c.cliId
                FROM datasImportantes di
                INNER JOIN clientes c
                on c.cliId = di.cliId
                WHERE di.userId = ?
                AND di.ativo = ?`;

    return this.db.executeSql(sql, [userId, true])
      .then(response => {
        console.log("teste-getAllDatasImportantes");
        let datasImportantes = [];

        for (let index = 0; index < response.rows.length; index++) {
          let btpDataImportante: BtpDataImportante = response.rows.item(index);
          let btpCliente: BtpCliente = new BtpCliente();
          btpCliente.cliId = response.rows.item(index).cliId;
          btpCliente.nome = response.rows.item(index).nome;
          btpDataImportante.cliente = btpCliente;
          btpDataImportante.enumGrauParentesco = response.rows.item(index).grauParentesco;

          let usuario = new Usuario();
          usuario.idUsuario = response.rows.item(index).userId;
          btpDataImportante.usuario = usuario;

          datasImportantes.push(btpDataImportante);
          console.log('id: ' + btpDataImportante.idDataImportante);
          console.log("date:" + moment().format(response.rows.item(index).diaMes));
          console.log("date:" + response.rows.item(index).diaMes);
        }
        console.log("getAll Datas importantes.......")
        return Promise.resolve(datasImportantes);
      })
      .catch(e => {
        console.log(e.msg);
        return null;
      });
  }

  getDatasImportantesPorData(userId: number, data: Date) {
    let sql = `SELECT di.*, c.nome, c.cliId
                FROM datasImportantes di
                INNER JOIN clientes c
                on c.cliId = di.cliId
                WHERE di.userId = ? 
                AND strftime('%m%Y',di.diaMes) = ?
                AND di.ativo = ? `;

    return this.db.executeSql(sql, [userId, this.dbProvider.meses[data.getMonth()] + "" + data.getFullYear(), true])
      .then(response => {
        let datasImportantes = [];

        for (let index = 0; index < response.rows.length; index++) {
          console.log(response.rows.item(index).cliId);
          let btpDataImportante: BtpDataImportante = response.rows.item(index);
          let btpCliente: BtpCliente = new BtpCliente();
          btpCliente.cliId = response.rows.item(index).cliId;
          btpCliente.nome = response.rows.item(index).nome;
          btpDataImportante.cliente = btpCliente;
          btpDataImportante.enumGrauParentesco = response.rows.item(index).grauParentesco;

          let usuario = new Usuario();
          usuario.idUsuario = response.rows.item(index).userId;
          btpDataImportante.usuario = usuario;

          datasImportantes.push(btpDataImportante);

          console.log("date:" + response.rows.item(index).diaMes);
        }
        console.log("getAll Datas importantes.......")
        return Promise.resolve(datasImportantes);
      })
      .catch(e => {
        console.log(e.msg);
        return null;
      });
  }

  incluir(btpDataImportante: BtpDataImportante) {

    let sql = `INSERT INTO datasImportantes(userId,
               cliId,
               dscDataImportante,
               tipDataImportante,
               dscGrauParentesco,
               sexo,
               diaMes,
               grauParentesco,
               ativo, isServidor)
                VALUES(?,?,?,?,?,?,?,?,?,?)`;

    console.log("Inserindo Data importante");
    console.log(btpDataImportante.diaMes);
    btpDataImportante.ativo = true;
    return this.db.executeSql(sql,
      [btpDataImportante.usuario.idUsuario,
      btpDataImportante.cliente.cliId,
      btpDataImportante.dscDataImportante,
      btpDataImportante.tipDataImportante,
      btpDataImportante.dscGrauParentesco,
      btpDataImportante.sexo,
      btpDataImportante.diaMes,
      btpDataImportante.enumGrauParentesco,
      btpDataImportante.ativo,
      btpDataImportante.isServidor]
    );
  }

  delete(btpDataImportante: BtpDataImportante) {
    let sql = 'DELETE FROM datasImportantes WHERE idDataImportante=?';
    console.log('iddata importante' + btpDataImportante.idDataImportante);
    return this.db.executeSql(sql, [btpDataImportante.idDataImportante]);
  }

  update(btpDataImportante: BtpDataImportante) {

    let sql = `UPDATE datasImportantes SET
               cliId =?,
               dscDataImportante =?,
               tipDataImportante =?,
               dscGrauParentesco =?,
               sexo =?,
               diaMes =?,
               grauParentesco =?,
               ativo=?,
               isServidor=?
               WHERE idDataImportante =?`;


    return this.db.executeSql(sql, [btpDataImportante.cliente.cliId, btpDataImportante.dscDataImportante,
    btpDataImportante.tipDataImportante, btpDataImportante.dscGrauParentesco,
    btpDataImportante.sexo, btpDataImportante.diaMes,
    btpDataImportante.enumGrauParentesco,
    btpDataImportante.ativo,
    btpDataImportante.isServidor,
    btpDataImportante.idDataImportante
    ]);
  }

  getDataImportanteNaoServidor(userId: number) {
    let sql = `SELECT di.*, c.nome, c.cliId
                FROM datasImportantes di
                INNER JOIN clientes c
                on c.cliId = di.cliId
                WHERE di.userId = ?
                AND di.isServidor = ?`;

    return this.db.executeSql(sql, [userId, false])
      .then(response => {
        console.log("teste-getAllDatasImportantes");
        let datasImportantes = [];

        for (let index = 0; index < response.rows.length; index++) {
          let btpDataImportante: BtpDataImportante = response.rows.item(index);
          let btpCliente: BtpCliente = new BtpCliente();
          btpCliente.cliId = response.rows.item(index).cliId;
          btpCliente.nome = response.rows.item(index).nome;
          btpDataImportante.cliente = btpCliente;
          btpDataImportante.enumGrauParentesco = response.rows.item(index).grauParentesco;

          let usuario = new Usuario();
          usuario.idUsuario = response.rows.item(index).userId;
          btpDataImportante.usuario = usuario;

          datasImportantes.push(btpDataImportante);
        }
        console.log("getAll Datas importantes.......")
        return Promise.resolve(datasImportantes);
      })
      .catch(e => {
        console.log(e.msg);
        return null;
      });
  }

  alterarDataImportanteGravadoServidor(btpDataImportantes: BtpDataImportante[]) {
    let promises = [];

    btpDataImportantes.forEach(btpDataImportante => {
      btpDataImportante.isServidor = true;
      promises.push(this.update(btpDataImportante));
    });

    return Promise.all(promises);
  }

  inserirDatasImportantesDownload(lista: Array<any>) {

    let promises = [];
    lista.forEach(element => {
      let dataImportante: BtpDataImportante = element;
      dataImportante.idDataImportante = element.chave.id;
      dataImportante.usuario = new Usuario();
      dataImportante.usuario.idUsuario = element.chave.idUsuario;

      dataImportante.cliente = new BtpCliente();
      dataImportante.cliente.cliId = element.cliId;

      dataImportante.enumGrauParentesco = element.codGrauParentesco;

      dataImportante.isServidor = true;
      console.log(dataImportante);
      promises.push(this.incluirComId(dataImportante));
    });
    return Promise.all(promises);
  }

  incluirComId(btpDataImportante: BtpDataImportante) {

    let sql = `INSERT INTO datasImportantes(
               idDataImportante,
               userId,
               cliId,
               dscDataImportante,
               tipDataImportante,
               dscGrauParentesco,
               sexo,
               diaMes,
               grauParentesco,
               ativo, isServidor)
                VALUES(?,?,?,?,?,?,?,?,?,?,?)`;

    return this.db.executeSql(sql,
      [
        btpDataImportante.idDataImportante,
        btpDataImportante.usuario.idUsuario,
        btpDataImportante.cliente.cliId,
        btpDataImportante.dscDataImportante,
        btpDataImportante.tipDataImportante,
        btpDataImportante.dscGrauParentesco,
        btpDataImportante.sexo,
        btpDataImportante.diaMes,
        btpDataImportante.enumGrauParentesco,
        btpDataImportante.ativo,
        btpDataImportante.isServidor]
    );


  }

}
