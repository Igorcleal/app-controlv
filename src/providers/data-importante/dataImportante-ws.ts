import { UtilsService } from './../utils-service';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { HttpProvider } from './../http/http-provider';
import { Usuario } from '../../model/usuario';
import { ClienteWsProvider } from './../cliente/cliente-ws';
import { Constants } from "../../app/constants";
import { DataImportanteService } from './dataImportante-service';
import { BtpDataImportante } from '../../model/btpDataImportante';

@Injectable()
export class DataImportanteWsProvider {

  constructor(public http: HttpProvider,
    public utils: UtilsService,
    public dataImportanteService: DataImportanteService,
    public clienteWs: ClienteWsProvider) {
  }

  enviarDependencias() {
    let promises = [];
    promises.push(this.clienteWs.enviarClientesServidor());

    return Promise.all(promises);
  }

  enviarDataImportanteServidor() {

    return this.enviarDependencias().then(() => {

      return this.getDataImportanteNaoServidor().then((btpDataImportantes: BtpDataImportante[]) => {
        if (btpDataImportantes == null || btpDataImportantes.length == 0) {
          return Promise.resolve(null);
        }
        //envia data importante para WS
        return this.http.post(`${Constants.urlWs}dataImportante/salvarLst`, btpDataImportantes).then(() => {
          //caso dê certo, altera no sqlite data importante para dataImportante gravadas no servidor
          return this.dataImportanteService.alterarDataImportanteGravadoServidor(btpDataImportantes);
        });
      });
    });
  }

  enviarDataImportanteServidorSemDependencias() {
    return this.getDataImportanteNaoServidor().then((btpDataImportantes: BtpDataImportante[]) => {
      if (btpDataImportantes == null || btpDataImportantes.length == 0) {
        return Promise.resolve(null);
      }
      //envia data importante para WS
      return this.http.post(`${Constants.urlWs}dataImportante/salvarLst`, btpDataImportantes).then(() => {
        //caso dê certo, altera no sqlite data importante para dataImportante gravadas no servidor
        return this.dataImportanteService.alterarDataImportanteGravadoServidor(btpDataImportantes);
      });
    });
  }

  getDataImportanteNaoServidor() {
    return this.utils.getUsuarioLogado().then((usuario: Usuario) => {

      return this.dataImportanteService.getDataImportanteNaoServidor(usuario.idUsuario);
    });
  }

}
