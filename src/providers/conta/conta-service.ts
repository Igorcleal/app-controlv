import { BtpConta } from './../../model/btpConta';
import { DbProvider } from '../db-provider';
import { HttpProvider } from '../http/http-provider';
import { SQLite } from 'ionic-native';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Usuario } from "../../model/usuario";

@Injectable()
export class ContaService {

  public db: SQLite = null;
  constructor(public http: HttpProvider, public dbProvider: DbProvider) {
    this.db = dbProvider.db;
  }

  getAll(userId: number) {

    let sql = `SELECT *
               FROM contas c
               WHERE c.userId = ?
               AND c.ativo = ? `;

    return this.db.executeSql(sql, [userId, true])
      .then(response => {
        let contas = [];
        for (let index = 0; index < response.rows.length; index++) {
          let conta: BtpConta = response.rows.item(index);
          conta.usuario = new Usuario();
          conta.usuario.idUsuario = response.rows.item(index).userId;
          contas.push(conta);
        }
        console.log("executou consulta: getAll Fornecedores................");
        return Promise.resolve(contas);
      });
  }

  getContasNaoServidor(userId: number) {
    let sql = `SELECT *
               FROM contas c
               WHERE c.userId = ?
               AND c.isServidor = ?`;

    return this.db.executeSql(sql, [userId, false])
      .then(response => {
        let contas = [];
        for (let index = 0; index < response.rows.length; index++) {
          let conta: BtpConta = response.rows.item(index);
          conta.usuario = new Usuario();
          conta.usuario.idUsuario = response.rows.item(index).userId;
          contas.push(conta);
        }
        console.log("executou consulta: getAll Fornecedores................");
        return Promise.resolve(contas);
      });
  }

  alterarContasGravadasServidor(contas: BtpConta[]) {

    let promises = [];

    contas.forEach(conta => {
      conta.isServidor = true;
      promises.push(this.alterar(conta));
    });

    return Promise.all(promises);
  }

  incluir(btpConta: BtpConta) {
    let sql = `INSERT INTO contas( 
               userId ,
               contaDsc,
               isServidor,
               ativo )    
    VALUES(?,?,?,?)`;

    console.log(btpConta);

    btpConta.isServidor = false;
    btpConta.ativo = true;

    return this.db.executeSql(sql, [
      btpConta.usuario.idUsuario,
      btpConta.contaDsc,
      btpConta.isServidor,
      btpConta.ativo]);
  }



  alterar(btpConta: BtpConta) {
    let sql = `UPDATE contas 
           SET contaDsc=?, isServidor = ?, ativo = ?
    WHERE contaId=?`;
    
    return this.db.executeSql(sql, [btpConta.contaDsc,
    btpConta.isServidor,
    btpConta.ativo,
    btpConta.contaId]);
  }

  deletar(btpConta: BtpConta) {
    let sql = 'DELETE FROM contas WHERE contaId=?';
    return this.db.executeSql(sql, [btpConta.contaId]);
  }

  inserirContasDownload(userId:string , contas:Array<any>){
    
    let promises = [];
    contas.forEach(element => {
      let btpConta:BtpConta = element
      btpConta.contaId = element.chave.id;
      btpConta.usuario = new Usuario();
      btpConta.usuario.idUsuario = element.chave.idUsuario;
      btpConta.isServidor;
      promises.push(this.incluirComId(btpConta));
    });
    return Promise.all(promises);
  }

  incluirComId(btpConta: BtpConta) {
    let sql = `INSERT INTO contas( 
               contaId,
               userId ,
               contaDsc,
               isServidor,
               ativo )    
    VALUES(?,?,?,?,?)`;

    return this.db.executeSql(sql, [
      btpConta.contaId,
      btpConta.usuario.idUsuario,
      btpConta.contaDsc,
      btpConta.isServidor,
      btpConta.ativo]);
  }

}
