import { Constants } from './../../app/constants';
import { HttpProvider } from './../http/http-provider';
import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { ContaService } from './conta-service';
import { BtpConta } from './../../model/btpConta';

@Injectable()
export class ContaWS {

  constructor(public http: HttpProvider, public contaService: ContaService, public storage: Storage) {
  }

  incluir(btpConta: BtpConta) {
    return this.http.post(`${Constants.urlWs}conta`, btpConta);
  }

  enviarContasServidor() {
    //recupera contas que nao foram gravadas no servdor ainda
    return this.getContasNaoServidor().then((contas: BtpConta[]) => {
      
      if (contas == null || contas.length == 0) {
        return Promise.resolve(null);
      }
      
      //envia contas para WS
      return this.http.post(`${Constants.urlWs}conta/salvarLst`, contas).then(()=>{
        //caso dê certo, altera no sqlite contas para contas gravadas no servidor
        return this.contaService.alterarContasGravadasServidor(contas);
      });
    });
  }

  getContasNaoServidor() {
    return this.storage.get('idUsuarioLogado').then((userId) => {
      return this.contaService.getContasNaoServidor(userId);
    });
  }

  alterar(btpConta: BtpConta) {
   /* let btpContaEnviar:BtpConta;
    btpContaEnviar.usuario = btpConta.usuario;
    btpContaEnviar.contaDsc = btpConta.contaDsc;
    btpContaEnviar.contaId = btpConta.contaId;*/

    return this.http.put(`${Constants.urlWs}conta`, btpConta).then(() => {
      btpConta.isServidor = true;
      return this.contaService.alterar(btpConta);
    }).catch((err) => {
      btpConta.isServidor = false;
      console.log(err.message);
      console.error('Conta não enviada para WS');
      return this.contaService.alterar(btpConta);
    })
  }

}
