import { BtpPagamentoReceita } from './../model/btpPagamentoReceita';
import { DbProvider } from './db-provider';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { Categoria } from './../model/categoria';
import { SQLite } from 'ionic-native';
import { Usuario } from "../model/usuario";
import { BtpReceita } from "../model/btpReceita";

@Injectable()
export class PagamentoService {

    public db: SQLite = null;

    constructor(public http: Http, public dbProvider: DbProvider) {
        this.db = dbProvider.db;

    }

    openDatabase() {
        return this.db.openDatabase({
            name: 'controlv.db',
            location: 'default'
        });
    }

    getPagamentosReceitas(receitaId: number, userId: number) {
        let sql = `SELECT *
                FROM pagamento_receitas
                WHERE receitaId = ? 
                AND userId = ?
                AND ativo = ? `;
        return this.db.executeSql(sql, [receitaId, userId, true])
            .then(response => {
                let pagamentos = [];
                for (let index = 0; index < response.rows.length; index++) {
                    let pagamento: BtpPagamentoReceita = response.rows.item(index);

                    let usuario = new Usuario();
                    usuario.idUsuario = userId;
                    pagamento.usuario = usuario;

                    let receita = new BtpReceita();
                    receita.receitaId = response.rows.item(index).receitaId;
                    pagamento.btpReceita = receita;

                    pagamentos.push(pagamento);
                }
                return Promise.resolve(pagamentos);
            });
    }

    getPagamentosReceitasNaoServidor(userId: number) {
        let sql = `SELECT *
                FROM pagamento_receitas
                WHERE userId = ? 
                AND isServidor = ?`;
        return this.db.executeSql(sql, [userId, false])
            .then(response => {
                let pagamentos = [];
                for (let index = 0; index < response.rows.length; index++) {

                    let pagamento: BtpPagamentoReceita = response.rows.item(index);

                    let usuario = new Usuario();
                    usuario.idUsuario = userId;
                    pagamento.usuario = usuario;

                    let receita = new BtpReceita();
                    receita.receitaId = response.rows.item(index).receitaId;
                    pagamento.btpReceita = receita;

                    pagamentos.push(pagamento);
                }
                console.log('list pagamentos nao servidor:');
                console.log(pagamentos);
                return Promise.resolve(pagamentos);
            });
    }

    incluirPagamentosReceita(pagamentos: Array<BtpPagamentoReceita>) {
        return this.db.transaction(tx => {
            let promises = [];
            pagamentos.forEach(pagamento => {
                if (pagamento.idPagamento == null) {
                    pagamento.isServidor = false;
                    pagamento.ativo = true;
                    promises.push(this.incluirPagamentoReceita(pagamento));
                }
            });
            return Promise.all(promises);
        });
    }

    incluirPagamentoReceita(btpPagamento: BtpPagamentoReceita) {
        let sql = `INSERT INTO pagamento_receitas(userId, valorPagamento, datPagamento, receitaId, isServidor, ativo) 
            VALUES(?,?,?,?,?,?)`;
        return this.db.executeSql(sql, [btpPagamento.usuario.idUsuario,
        btpPagamento.valorPagamento,
        new Date(btpPagamento.datPagamento).toISOString(),
        btpPagamento.btpReceita.receitaId,
        btpPagamento.isServidor,
        btpPagamento.ativo]).then(() => {
            console.log("Sucess incluir pagamento");
        }).catch(() => { console.error("Erro incluir Pagamento") });
    }

    excluirPagamentosReceita(pagamentos: Array<BtpPagamentoReceita>) {
        return this.db.transaction(tx => {
            let promises = [];
            pagamentos.forEach(pagamento => {
                pagamento.ativo = false;
                pagamento.isServidor = false;
                promises.push(this.alterar(pagamento));
            });
            return Promise.all(promises);
        });
    }

    delete(btpPagamento: BtpPagamentoReceita) {
        let sql = 'DELETE FROM pagamento_receitas WHERE idPagamento=?';
        return this.db.executeSql(sql, [btpPagamento.idPagamento]);
    }

    update(categoria: Categoria) {
        let sql = 'UPDATE categorias SET CategoriaDsc=?, cor=? WHERE CategoriaId=?';
        return this.db.executeSql(sql, [categoria.categoriaDsc, categoria.cor, categoria.categoriaId]);
    }

    alterar(btpPagamento: BtpPagamentoReceita) {
        let sql = `UPDATE pagamento_receitas
            SET userId = ?,
            valorPagamento = ? ,
            datPagamento = ? ,
            receitaId = ?,
            isServidor = ?,
            ativo = ? 
            WHERE idPagamento = ?`;

        return this.db.executeSql(sql, [btpPagamento.usuario.idUsuario,
        btpPagamento.valorPagamento,
        new Date(btpPagamento.datPagamento).toISOString(),
        btpPagamento.btpReceita.receitaId,
        btpPagamento.isServidor,
        btpPagamento.ativo,
        btpPagamento.idPagamento]).then(() => {
            console.log("Sucess alterar pagamento");
        }).catch((err) => {
            console.error("Erro alterar Pagamento")
            console.log(err.message);
            return Promise.reject(err);
        });
    }

    alterarPagamentosReceitasGravadoServidor(pagamentos: BtpPagamentoReceita[]) {
        let promises = [];

        pagamentos.forEach(pagamento => {
            pagamento.isServidor = true;
            promises.push(this.alterar(pagamento));
        });

        return Promise.all(promises);
    }

    inserirPagamentosReceitasDownload(lista: Array<any>) {
        let promises = [];
        lista.forEach(element => {
            let pagamento: BtpPagamentoReceita = element;
            pagamento.idPagamento = element.chave.id;
            pagamento.usuario = new Usuario();
            pagamento.usuario.idUsuario = element.chave.idUsuario;

            pagamento.btpReceita = new BtpReceita();
            pagamento.btpReceita.receitaId = element.receitaId;

            pagamento.isServidor = true;
            promises.push(this.incluirComId(pagamento));
        });
        return Promise.all(promises);
    }

    incluirComId(btpPagamento: BtpPagamentoReceita) {
        let sql = `INSERT INTO pagamento_receitas(
            idPagamento,
            userId, 
            valorPagamento, 
            datPagamento, 
            receitaId, 
            isServidor, 
            ativo) 
            VALUES(?,?,?,?,?,?,?)`;
        return this.db.executeSql(sql, [
            btpPagamento.idPagamento,
            btpPagamento.usuario.idUsuario,
            btpPagamento.valorPagamento,
            btpPagamento.datPagamento,
            btpPagamento.btpReceita.receitaId,
            btpPagamento.isServidor,
            btpPagamento.ativo]);
    }

}
