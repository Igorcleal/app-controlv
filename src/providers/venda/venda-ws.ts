import { EstoqueWsProvider } from './../estoque/estoque-ws';
import { BtpItemVenda } from './../../model/btpItemVenda';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { HttpProvider } from "../http/http-provider";
import { CategoriaWsProvider } from "../categoria/categoria-ws";
import { VendaService } from "./venda-service";
import { UtilsService } from "../utils-service";
import { BtpVenda } from "../../model/btpVenda";
import { Constants } from "../../app/constants";
import { Usuario } from "../../model/usuario";
import { ItemVendaService } from "./item-venda-service";
import { ClienteWsProvider } from "../cliente/cliente-ws";
import { ItemFormaPagamentoVendaService } from "./item-forma-pagamento-venda-service";
import { BtpItemFormaPagamentoVenda } from "../../model/btpItemFormaPagamentoVenda";
import { FormaPagamentoWsProvider } from "../forma-pagamento/forma-pagamento-ws";
import { ReceitaWsProvider } from "../receita/receita-ws";

@Injectable()
export class VendaWsProvider {

  private usuario: Usuario;

  constructor(public http: HttpProvider,
    public utils: UtilsService,
    public vendaService: VendaService,
    public categoriaWs: CategoriaWsProvider,
    public clienteWs: ClienteWsProvider,
    public itemVendaService: ItemVendaService,
    public itemFormaPagamentoVendaService: ItemFormaPagamentoVendaService,
    public formaPagamentoWs: FormaPagamentoWsProvider,
    public receitaWs:ReceitaWsProvider, public estoqueWs:EstoqueWsProvider) {
  }

  enviarDependencias() {
    let promises = [];
    promises.push(this.formaPagamentoWs.enviarFormasPagamentosServidor());
    promises.push(this.clienteWs.enviarClientesServidorSemDependencias());
    promises.push(this.estoqueWs.enviarEstoqueServidorSemDependencias());
    promises.push(this.receitaWs.enviarReceitasServidor());

    return Promise.all(promises);
  }

  enviarVendasServidor() {

    return this.enviarDependencias().then(() => {

      return this.getVendasNaoServidor().then((vendas: BtpVenda[]) => {
        if (vendas == null || vendas.length == 0) {
          return Promise.resolve(null);
        }

        let promises = [];
        vendas.forEach(venda => {
          promises.push(this.itemVendaService.getItensVenda(venda, this.usuario).then((itensVenda: Array<BtpItemVenda>) => {
            venda.listItemVenda = itensVenda;
          }));
          promises.push(this.itemFormaPagamentoVendaService.getItensFormaPagamentoVenda(venda, this.usuario).then((itensFormaPagamento: Array<BtpItemFormaPagamentoVenda>) => {
            venda.listItemPagamentoVenda = itensFormaPagamento;
          }))
        });

        return Promise.all(promises).then(() => {
          //envia produtos para WS
          return this.http.post(`${Constants.urlWs}venda/salvarLst`, vendas).then(() => {
            //caso dê certo, altera no sqlite produtos para produtos gravadas no servidor
            return this.vendaService.alterarVendasGravadoServidor(vendas);
          });
        });

      });
    });
  }

  enviarVendasServidorSemDependencias() {

      return this.getVendasNaoServidor().then((vendas: BtpVenda[]) => {
        if (vendas == null || vendas.length == 0) {
          return Promise.resolve(null);
        }

        let promises = [];
        vendas.forEach(venda => {
          promises.push(this.itemVendaService.getItensVenda(venda, this.usuario).then((itensVenda: Array<BtpItemVenda>) => {
            venda.listItemVenda = itensVenda;
          }));
          promises.push(this.itemFormaPagamentoVendaService.getItensFormaPagamentoVenda(venda, this.usuario).then((itensFormaPagamento: Array<BtpItemFormaPagamentoVenda>) => {
            venda.listItemPagamentoVenda = itensFormaPagamento;
          }))
        });

        return Promise.all(promises).then(() => {
          //envia produtos para WS
          return this.http.post(`${Constants.urlWs}venda/salvarLst`, vendas).then(() => {
            //caso dê certo, altera no sqlite produtos para produtos gravadas no servidor
            return this.vendaService.alterarVendasGravadoServidor(vendas);
          });
        });
      });
  }

  getVendasNaoServidor() {
    return this.utils.getUsuarioLogado().then((usuario: Usuario) => {
      this.usuario = usuario;
      return this.vendaService.getVendasNaoServidor(this.usuario.idUsuario);
    });
  }

}
