import { SQLite } from 'ionic-native';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { BtpCliente } from './../../model/cliente';
import { BtpReceita } from './../../model/btpReceita';
import { StatusVenda } from '../../model/status-venda';
import { DbProvider } from './../db-provider';
import { BtpVenda } from "../../model/btpVenda";
import { BtpItemVenda } from "../../model/btpItemVenda";
import { Produto } from "../../model/produto";
import { BtpItemFormaPagamentoVenda } from '../../model/btpItemFormaPagamentoVenda';
import { Usuario } from "../../model/usuario";
import { BtpFormaPagamento } from "../../model/btpFormaPagamento";
import { BtpEstoque } from "../../model/btpEstoque";

@Injectable()
export class VendaService {

  public db: SQLite = null;
  constructor(public http: Http, public dbProvider: DbProvider) {
    this.db = dbProvider.db;
  }

  getAll(userId: number) {

    let sql = `SELECT 
               v.*, cli.nome, cli.cliId
               FROM vendas v
               INNER JOIN clientes cli
                ON v.cliId = cli.cliId
             
               WHERE v.userId = ?
               ORDER BY cli.nome`;

    return this.db.executeSql(sql, [userId])
      .then(response => {
        let vendas = [];
        for (let index = 0; index < response.rows.length; index++) {
          let btpVenda: BtpVenda = response.rows.item(index);
          btpVenda.idVenda = response.rows.item(index).vendaId;
          btpVenda.datVenda = response.rows.item(index).dataVenda;
          btpVenda.datVendaString = response.rows.item(index).dataVenda;
          btpVenda.vlrTotal = response.rows.item(index).vlrTotal;

          let usuario = new Usuario();
          usuario.idUsuario = response.rows.item(index).userId;

          btpVenda.usuario = usuario;

          if (btpVenda.vlrTotal == null) { btpVenda.vlrTotal = 0 }

          btpVenda.statusVenda = response.rows.item(index).statusVenda;

          let btpCliente = new BtpCliente();
          btpCliente.cliId = response.rows.item(index).cliId;
          btpCliente.nome = response.rows.item(index).nome;
          btpCliente.usuario = usuario;
          btpVenda.btpCliente = btpCliente;

          vendas.push(btpVenda);
        }

        console.log("executou consulta: getAll vendas................");
        return Promise.resolve(vendas);
      });
  }

  getAllByDate(userId: number, data: Date) {

    let sql = `SELECT 
               v.*, v.statusVenda, cli.nome, cli.cliId,
               (select sum(iv.vlrTotal) from itens_venda iv 
                  where iv.vendaId = v.vendaId and iv.ativo = ? ) as vlrTotal
               FROM vendas v
               INNER JOIN clientes cli
                ON v.cliId = cli.cliId
               WHERE v.userId = ?
               AND v.ativo = ?
               AND strftime('%m%Y',v.dataVenda) = ?
               ORDER BY cli.nome`;

    return this.db.executeSql(sql, [true, userId, true, this.dbProvider.meses[data.getMonth()] + "" + data.getFullYear()])
      .then(response => {
        let vendas = [];
        for (let index = 0; index < response.rows.length; index++) {
          let btpVenda: BtpVenda = response.rows.item(index);
          btpVenda.idVenda = response.rows.item(index).vendaId;
          btpVenda.datVenda = response.rows.item(index).dataVenda;
          btpVenda.datVendaString = response.rows.item(index).dataVenda;
          btpVenda.vlrTotal = response.rows.item(index).vlrTotal;

          let usuario = new Usuario();
          usuario.idUsuario = response.rows.item(index).userId;

          btpVenda.usuario = usuario;

          if (btpVenda.vlrTotal == null) { btpVenda.vlrTotal = 0 }

          btpVenda.statusVenda = response.rows.item(index).statusVenda;

          let btpCliente = new BtpCliente();
          btpCliente.cliId = response.rows.item(index).cliId;
          btpCliente.nome = response.rows.item(index).nome;
          btpCliente.usuario = usuario;
          btpVenda.btpCliente = btpCliente;

          vendas.push(btpVenda);
        }

        console.log("executou consulta: getAll vendas................");
        return Promise.resolve(vendas);
      });
  }

  getVendasNaoServidor(userId: number) {

    let sql = `SELECT 
               v.*, v.statusVenda, cli.nome, cli.cliId,
               (select sum(iv.vlrTotal) from itens_venda iv 
                  where iv.vendaId = v.vendaId and iv.ativo = ? ) as vlrTotal
               FROM vendas v
               INNER JOIN clientes cli
                ON v.cliId = cli.cliId
               WHERE v.userId = ?
               AND v.isServidor = ?
               ORDER BY cli.nome`;

    return this.db.executeSql(sql, [true, userId, false])
      .then(response => {
        let vendas = [];
        for (let index = 0; index < response.rows.length; index++) {
          let btpVenda: BtpVenda = response.rows.item(index);
          btpVenda.idVenda = response.rows.item(index).vendaId;
          btpVenda.datVenda = response.rows.item(index).dataVenda;
          btpVenda.datVendaString = response.rows.item(index).dataVenda;
          btpVenda.vlrTotal = response.rows.item(index).vlrTotal;

          let usuario = new Usuario();
          usuario.idUsuario = response.rows.item(index).userId;

          btpVenda.usuario = usuario;

          if (btpVenda.vlrTotal == null) { btpVenda.vlrTotal = 0 }

          btpVenda.statusVenda = response.rows.item(index).statusVenda;

          let btpCliente = new BtpCliente();
          btpCliente.cliId = response.rows.item(index).cliId;
          btpCliente.nome = response.rows.item(index).nome;
          btpCliente.usuario = usuario;
          btpVenda.btpCliente = btpCliente;

          vendas.push(btpVenda);
        }

        console.log("executou consulta: getAll vendas................");
        return Promise.resolve(vendas);
      });
  }

  incluirVenda(btpVenda: BtpVenda) {
    let sql = `INSERT INTO vendas(userId,
               cliId, dataVenda, vlrTotal,statusVenda,
               isServidor, ativo)
                VALUES(?,?,?,?,?,?,?)`;

    console.info('Inserindo Venda');
    console.log(btpVenda.usuario.idUsuario);
    console.log(btpVenda.btpCliente.cliId);
    console.log(new Date(btpVenda.datVenda).toISOString());
    console.log(btpVenda.vlrTotal);
    console.log(btpVenda.statusVenda);

    let idInserido: number;
    btpVenda.ativo = true;

    return this.db.transaction((tx) => {
      return tx.executeSql(sql, [btpVenda.usuario.idUsuario, btpVenda.btpCliente.cliId,
      btpVenda.datVendaString, btpVenda.vlrTotal, btpVenda.statusVenda,
      btpVenda.isServidor, btpVenda.ativo], (tx, resultSet) => {
        idInserido = resultSet.insertId;
        btpVenda.idVenda = idInserido;
      }, function (tx, error) {
        console.log('INSERT error: ' + error.message);
      });
    }).then((response) => {
      return Promise.resolve(idInserido);
    });
  }

  deletarVendaCompleta(btpVenda: BtpVenda) {

    let promises = [];

    promises.push(this.deletarVenda(btpVenda));
    promises.push(this.deletarReceitasVenda(btpVenda));
    promises.push(this.deletarItensVenda(btpVenda));
    promises.push(this.deletarPagamentosVenda(btpVenda));

    return Promise.all(promises);
  }

  deletarVenda(btpVenda: BtpVenda) {
    let sql = 'DELETE FROM vendas WHERE vendaId=?';
    return this.db.executeSql(sql, [btpVenda.idVenda]).then(() => {
      console.log("Success venda excluida");
      return Promise.resolve(true);
    }).catch(() => {
      console.error("erro excluir venda");
      return Promise.reject(true);
    });
  }

  deletarItensVenda(btpVenda: BtpVenda) {
    let sql = 'DELETE FROM itens_venda WHERE vendaId=?';
    return this.db.executeSql(sql, [btpVenda.idVenda]).then(() => {
      console.log("Success excluído produtos da venda");
      return Promise.resolve(true);
    })
      .catch((err) => {
        console.error(err.message);
      });
  }

  deletarPagamentosVenda(btpVenda: BtpVenda) {
    let sql = 'DELETE FROM item_forma_pag_venda WHERE vendaId=?';
    return this.db.executeSql(sql, [btpVenda.idVenda]).then(() => {
      console.log("Success excluído pagamentos da venda");
      return Promise.resolve(true);
    })
      .catch((err) => {
        console.error(err.message);
        return Promise.reject(true);
      });
  }

  deletarReceitasVenda(btpVenda: BtpVenda) {

    let sql = 'SELECT r.receitaId as receitaId FROM receitas r WHERE vendaId=?';

    return this.db.executeSql(sql, [btpVenda.idVenda]).then(response => {

      let promises = [];

      for (let index = 0; index < response.rows.length; index++) {
        let idReceita: number = response.rows.item(index).receitaId

        //exclui pagamentos das receitas
        promises.push(this.deletarPagamentosReceitas(idReceita));
      }

      //exclui receitas geradas da venda
      return Promise.all(promises).then(() => {
        let sql = 'DELETE FROM receitas WHERE vendaId=?';
        return this.db.executeSql(sql, [btpVenda.idVenda]).then(() => {
          console.log("Excluido receitas geradas da venda");
          return Promise.resolve(true);
        })
          .catch((err) => {
            console.log("Erro excluir receita da venda");
            console.error(err.message);
            return Promise.reject(true);
          });
      });
    });

  }

  deletarPagamentosReceitas(idReceita: number) {
    let sql = 'DELETE FROM pagamento_receitas WHERE receitaId=?';
    return this.db.executeSql(sql, [idReceita]).then(() => {
      console.log("Excluido pagamentos gerados da receita da venda");
      return Promise.resolve(true);
    }).catch((err) => {
      console.log("Erro excluir pagamento da receita");
      console.error(err.message);
      return Promise.reject(true);
    });
  }

  alterarVenda(btpVenda: BtpVenda) {

    let sql = `UPDATE vendas SET
               cliId =?,
               dataVenda =?,
               vlrTotal =?,
               statusVenda=?,
               isServidor = ?,
               ativo = ?
               WHERE vendaId =?`;
    console.log(btpVenda.vlrTotal);
    return this.db.executeSql(sql, [btpVenda.btpCliente.cliId, new Date(btpVenda.datVenda).toISOString(),
    btpVenda.vlrTotal, btpVenda.statusVenda, btpVenda.isServidor, btpVenda.ativo,
    btpVenda.idVenda]);
  }

  alterarStatusVenda(vendaId: number, statusVenda: StatusVenda) {

    let sql = `UPDATE vendas SET             
               statusVenda =?
               WHERE vendaId =?`;

    return this.db.executeSql(sql, [statusVenda, vendaId]);
  }

  incluirItemVenda(btpItemVenda: BtpItemVenda) {
    let sql = `INSERT INTO itens_venda(userId,
               vendaId,
               produtoId,
               itemQtd,
               vlrUnitario,
               vlrTotal, 
               idEstoque,
               ativo)
                VALUES(?,?,?,?,?,?,?,?)`;

    let idEstoque: number = null;
    if (btpItemVenda.btpEstoque != null && btpItemVenda.btpEstoque.idEstoque != null) {
      idEstoque = btpItemVenda.btpEstoque.idEstoque;
    }

    return this.db.executeSql(sql, [btpItemVenda.usuario.idUsuario,
    btpItemVenda.btpVenda.idVenda, btpItemVenda.produto.produtoId,
    btpItemVenda.qtdItemVenda, btpItemVenda.vlrItemVenda, btpItemVenda.vlrTotalItemVenda,
      idEstoque, btpItemVenda.ativo]);
  }

  incluirItemFormaPagamentoVenda(btpItemFormaPagamentoVenda: BtpItemFormaPagamentoVenda) {
    let sql = `INSERT INTO item_forma_pag_venda(userId,
               vendaId,
               numParcela,
               datVencimento,
               datPagamento,
               vlrParcela,
               dscObservacao,
               idFormaPagamento,
               ativo)
                VALUES(?,?,?,?,?,?,?,?,?)`;

    console.log("Inserindo Item da forma de pgt");
    console.log(btpItemFormaPagamentoVenda.usuario.idUsuario);
    return this.db.executeSql(sql, [btpItemFormaPagamentoVenda.usuario.idUsuario,
    btpItemFormaPagamentoVenda.btpVenda.idVenda, btpItemFormaPagamentoVenda.numParcela,
    btpItemFormaPagamentoVenda.datVencimentoString, btpItemFormaPagamentoVenda.datPagamento,
    btpItemFormaPagamentoVenda.vlrParcela,
    btpItemFormaPagamentoVenda.dscObservacao,
    btpItemFormaPagamentoVenda.btpFormaPagamento.idFormaPagamento,
    btpItemFormaPagamentoVenda.ativo]);
  }


  alterarItemFormaPagamentoVenda(btpItemFormaPagamentoVenda: BtpItemFormaPagamentoVenda) {
    let sql = `UPDATE item_forma_pag_venda 
               SET  numParcela = ?,
                datVencimento = ?,
                datPagamento =?,
                vlrParcela =?,
                dscObservacao =?,
                idFormaPagamento =?,
                ativo=?
               WHERE itemId=?`;

    console.log("Alterando Item venda");
    return this.db.executeSql(sql, [btpItemFormaPagamentoVenda.numParcela,
    new Date(btpItemFormaPagamentoVenda.datVencimento).toISOString(),
    btpItemFormaPagamentoVenda.datPagamento,
    btpItemFormaPagamentoVenda.vlrParcela,
    btpItemFormaPagamentoVenda.dscObservacao,
    btpItemFormaPagamentoVenda.btpFormaPagamento.idFormaPagamento,
    btpItemFormaPagamentoVenda.ativo,
    btpItemFormaPagamentoVenda.itemId
    ]).catch(err => {
      console.log(err.message);
      return Promise.reject(true);
    });
  }

  alterarItemVenda(btpItemVenda: BtpItemVenda) {
    let sql = `UPDATE itens_venda 
               SET  itemQtd = ?,
               vlrUnitario = ?,
               vlrTotal =?,
               idEstoque = ?,
               ativo = ?
                WHERE itemVendaId=?`;

    let idEstoque: number = null;
    if (btpItemVenda.btpEstoque != null && btpItemVenda.btpEstoque.idEstoque != null) {
      idEstoque = btpItemVenda.btpEstoque.idEstoque;
    }

    console.log("Alterando Item venda");
    return this.db.executeSql(sql, [btpItemVenda.qtdItemVenda,
    btpItemVenda.vlrItemVenda,
    btpItemVenda.vlrTotalItemVenda, idEstoque, btpItemVenda.ativo,
    btpItemVenda.idItemVenda]);
  }

  deletarItemVenda(itemVendaId: number) {
    let sql = 'DELETE FROM itens_venda WHERE itemVendaId=?';

    return this.db.executeSql(sql, [itemVendaId]);
  }

  deletarItemFormaPagamentoVenda(itemId: number) {
    let sql = 'DELETE FROM item_forma_pag_venda WHERE itemId=?';

    return this.db.executeSql(sql, [itemId]);
  }

  getItensVendaAtivo(vendaId: number) {
    let sql = `SELECT iv.*, p.produtoId, p.produtoDsc, est.idEstoque, est.dscEstoque
                FROM itens_venda iv
                INNER JOIN produtos p
                on p.produtoId = iv.produtoId
                LEFT JOIN estoques est
                on est.idEstoque = iv.idEstoque
                WHERE iv.vendaId = ?
                AND iv.ativo = ? `;

    console.log(vendaId + "teste idVenda");
    return this.db.executeSql(sql, [vendaId, true])
      .then(response => {

        let itensVenda = [];
        let btpItemVenda: BtpItemVenda;
        let produto: Produto;
        let btpEstoque: BtpEstoque;

        console.log("Teste antes de entrar");
        for (let index = 0; index < response.rows.length; index++) {

          console.log(response.rows.item(index).itemVendaId);

          console.log("Teste depois de entrar");

          btpItemVenda = response.rows.item(index);
          btpItemVenda.idItemVenda = response.rows.item(index).itemVendaId;
          btpItemVenda.qtdItemVenda = response.rows.item(index).itemQtd;
          btpItemVenda.vlrItemVenda = response.rows.item(index).vlrUnitario;
          btpItemVenda.vlrTotalItemVenda = response.rows.item(index).vlrTotal;

          produto = new Produto();
          produto.produtoId = response.rows.item(index).produtoId;
          produto.produtoDsc = response.rows.item(index).produtoDsc;

          btpEstoque = new BtpEstoque();
          btpEstoque.idEstoque = response.rows.item(index).idEstoque;
          btpEstoque.dscEstoque = response.rows.item(index).dscEstoque;

          btpItemVenda.produto = produto;
          btpItemVenda.btpEstoque = btpEstoque;
          console.log(produto.produtoDsc + " descricao do produto.");
          itensVenda.push(btpItemVenda);
        }


        console.log(itensVenda.length);
        console.log("getAll Itens Venda.......");
        return Promise.resolve(itensVenda);
      })
      .catch(e => {
        console.error(e.message);
        console.log("Erro recuperar itens da venda: " + e.msg);
        return null;
      });
  }

  getItensFormaPagamento(vendaId: number) {
    let sql = `SELECT ifv.*, fpg.idFormaPagamento, fpg.descricao
                FROM item_forma_pag_venda ifv
                INNER JOIN formas_pagamento fpg
                ON fpg.idFormaPagamento = ifv.idFormaPagamento
                WHERE ifv.vendaId = ?
                AND ifv.ativo = ?`;

    return this.db.executeSql(sql, [vendaId, true])
      .then(response => {

        let itensFormaPagamento = [];
        let btpItemFormaPagamentoVenda: BtpItemFormaPagamentoVenda;
        let usuario: Usuario;
        let btpVenda: BtpVenda;
        let btpFormaPagamento: BtpFormaPagamento;

        for (let index = 0; index < response.rows.length; index++) {

          console.log(response.rows.item(index).itemId);

          btpItemFormaPagamentoVenda = response.rows.item(index);
          btpItemFormaPagamentoVenda.itemId = response.rows.item(index).itemId;
          btpItemFormaPagamentoVenda.numParcela = response.rows.item(index).numParcela;
          btpItemFormaPagamentoVenda.vlrParcela = response.rows.item(index).vlrParcela;
          btpItemFormaPagamentoVenda.datPagamento = response.rows.item(index).datPagamento;
          btpItemFormaPagamentoVenda.datVencimento = response.rows.item(index).datVencimento;
          btpItemFormaPagamentoVenda.datVencimentoString = response.rows.item(index).datVencimento;
          btpItemFormaPagamentoVenda.dscObservacao = response.rows.item(index).dscObservacao;

          usuario = new Usuario();
          usuario.idUsuario = response.rows.item(index).userId;

          btpVenda = new BtpVenda();
          btpVenda.idVenda = response.rows.item(index).idVenda;

          btpFormaPagamento = new BtpFormaPagamento();
          btpFormaPagamento.idFormaPagamento = response.rows.item(index).idFormaPagamento;
          btpFormaPagamento.descricao = response.rows.item(index).descricao;
          btpItemFormaPagamentoVenda.btpFormaPagamento = btpFormaPagamento;

          btpItemFormaPagamentoVenda.btpVenda = btpVenda;
          itensFormaPagamento.push(btpItemFormaPagamentoVenda);
        }


        console.log(itensFormaPagamento.length);
        console.log("getAll Item Forma Pagamento Venda.......");
        return Promise.resolve(itensFormaPagamento);
      })
      .catch(e => {
        console.log("Erro recuperar itens da venda: " + e.msg);
        return null;
      });
  }

  incluirReceita(btpReceita: BtpReceita) {
    let sql = `INSERT INTO receitas( 
               userId,
               receita,
               receitaDta, 
               receitaVlr,
               receitaVlrPago,
               receitaPaga,
               categoriaId,
               vendaId,
               receitaIdPai,
               contaId,
               isServidor,
               ativo )    
    VALUES(?,?,?,?,?,?,?,?,?,?,?,?)`;

    let idVenda: number = null;
    let receitaIdPai: number = null;
    if (btpReceita.btpReceitaPai) {
      receitaIdPai = btpReceita.btpReceitaPai.receitaId;
    }

    if (btpReceita != null && btpReceita.btpVenda != null && btpReceita.btpVenda.idVenda != null) {
      idVenda = btpReceita.btpVenda.idVenda
    }

    btpReceita.ativo = true;

    let idInserido: number;
    return this.db.transaction((tx) => {
      return tx.executeSql(sql, [btpReceita.usuario.idUsuario,
      btpReceita.receita,
      btpReceita.receitaDtaString,
      btpReceita.receitaVlr,
      btpReceita.receitaVlrPago,
      btpReceita.receitaPaga,
      btpReceita.categoria.categoriaId,
        idVenda,
        receitaIdPai,
      btpReceita.btpConta.contaId,
      btpReceita.isServidor,
      btpReceita.ativo], (tx, resultSet) => {
        idInserido = resultSet.insertId;
      }, function (tx, error) {
        console.log('INSERT error: ' + error.message);
      });
    }).then((response) => {
      return Promise.resolve(idInserido);
    });
  }

  verificarExistePagamento(idVenda: number) {
    let sql = `SELECT r.receitaId as receitaId 
                FROM receitas r 
                INNER JOIN pagamento_receitas pr
                ON pr.receitaId = r.receitaId
                WHERE vendaId=?`;

    return this.db.executeSql(sql, [idVenda]).then(response => {

      if (response.rows != null && response.rows.length > 0) {
        return Promise.resolve(true);
      }
      return Promise.resolve(false);
    }).catch(() => {
      return Promise.reject(true);
    });
  }

  alterarVendasGravadoServidor(vendas: BtpVenda[]) {
    let promises = [];

    vendas.forEach(venda => {
      venda.isServidor = true;
      promises.push(this.alterarVenda(venda));
    });

    return Promise.all(promises);
  }

  inserirVendasDownload(lista: Array<any>) {

    let promises = [];
    lista.forEach(element => {
      promises.push(this.inserirVenda(element));
    });
    return Promise.all(promises);
  }

  private inserirVenda(element) {
    let venda: BtpVenda = element;
    venda.idVenda = element.chave.id;
    venda.usuario = new Usuario();
    venda.usuario.idUsuario = element.chave.idUsuario;
    venda.isServidor = true;

    venda.btpCliente = new BtpCliente();
    venda.btpCliente.cliId = element.cliId;

    switch(element.statusVenda){
      case 'FECHADO':
        venda.statusVenda = StatusVenda.FECHADO;
        break;
      case 'PENDENTE':
        venda.statusVenda = StatusVenda.PENDENTE;
        break;
    }

    let promises = [];

    promises.push(this.incluirVendaComId(venda));

    venda.listItemVenda.forEach(itemVenda => {
      promises.push(this.inserirItemVenda(itemVenda));
    });

    venda.listItemPagamentoVenda.forEach(itemPagamentoVenda => {
      promises.push(this.inserirItemFormaPagamentoVenda(itemPagamentoVenda));
    });

    return Promise.all(promises);
  }

  private inserirItemVenda(element) {
    let itemVenda: BtpItemVenda = element;
    itemVenda.idItemVenda = element.chave.id;
    itemVenda.usuario = new Usuario();
    itemVenda.usuario.idUsuario = element.chave.idUsuario;

    itemVenda.produto = new Produto();
    itemVenda.produto.produtoId = element.produtoId;

    itemVenda.btpEstoque = new BtpEstoque();
    itemVenda.btpEstoque.idEstoque = element.idEstoque;

    itemVenda.btpVenda = new BtpVenda();
    itemVenda.btpVenda.idVenda = element.idVenda;

    return this.incluirItemProdutoVendaComId(itemVenda);
  }

  private inserirItemFormaPagamentoVenda(element) {
    let itemFormaPagamentoVenda: BtpItemFormaPagamentoVenda = element;
    itemFormaPagamentoVenda.itemId = element.chave.id;
    itemFormaPagamentoVenda.usuario = new Usuario();
    itemFormaPagamentoVenda.usuario.idUsuario = element.chave.idUsuario;

    itemFormaPagamentoVenda.btpFormaPagamento = new BtpFormaPagamento();
    itemFormaPagamentoVenda.btpFormaPagamento.idFormaPagamento = element.idFormaPagamento;

    itemFormaPagamentoVenda.btpVenda = new BtpVenda();
    itemFormaPagamentoVenda.btpVenda.idVenda = element.idVenda;

    return this.incluirItemFormaPagamentoVendaComId(itemFormaPagamentoVenda);
  }

  incluirVendaComId(btpVenda: BtpVenda) {
    let sql = `INSERT INTO vendas(
               vendaId,userId,
               cliId, dataVenda, vlrTotal,statusVenda,
               isServidor, ativo)
                VALUES(?,?,?,?,?,?,?,?)`;

    let idInserido: number;

    console.log(btpVenda);

    return this.db.transaction((tx) => {
      return tx.executeSql(sql, [
        btpVenda.idVenda,
        btpVenda.usuario.idUsuario, btpVenda.btpCliente.cliId,
        btpVenda.datVenda, btpVenda.vlrTotal, btpVenda.statusVenda,
        btpVenda.isServidor, btpVenda.ativo], (tx, resultSet) => {
          idInserido = resultSet.insertId;
          btpVenda.idVenda = idInserido;
        }, function (tx, error) {
          console.log('INSERT error: ' + error.message);
        });
    }).then((response) => {
      return Promise.resolve(idInserido);
    });
  }

  incluirItemProdutoVendaComId(btpItemVenda: BtpItemVenda) {
    let sql = `INSERT INTO itens_venda(
               itemVendaId,
               userId,
               vendaId,
               produtoId,
               itemQtd,
               vlrUnitario,
               vlrTotal, 
               idEstoque,
               ativo)
                VALUES(?,?,?,?,?,?,?,?,?)`;

    let idEstoque: number = null;
    if (btpItemVenda.btpEstoque != null && btpItemVenda.btpEstoque.idEstoque != null) {
      idEstoque = btpItemVenda.btpEstoque.idEstoque;
    }

    return this.db.executeSql(sql, [
      btpItemVenda.idItemVenda,
      btpItemVenda.usuario.idUsuario,
      btpItemVenda.btpVenda.idVenda, btpItemVenda.produto.produtoId,
      btpItemVenda.qtdItemVenda, btpItemVenda.vlrItemVenda, btpItemVenda.vlrTotalItemVenda,
      idEstoque, btpItemVenda.ativo]);
  }

  incluirItemFormaPagamentoVendaComId(btpItemFormaPagamentoVenda: BtpItemFormaPagamentoVenda) {
    let sql = `INSERT INTO item_forma_pag_venda(
               itemId,
               userId,
               vendaId,
               numParcela,
               datVencimento,
               datPagamento,
               vlrParcela,
               dscObservacao,
               idFormaPagamento,
               ativo)
                VALUES(?,?,?,?,?,?,?,?,?,?)`;

    return this.db.executeSql(sql, [
      btpItemFormaPagamentoVenda.itemId,
      btpItemFormaPagamentoVenda.usuario.idUsuario,
      btpItemFormaPagamentoVenda.btpVenda.idVenda, btpItemFormaPagamentoVenda.numParcela,
      btpItemFormaPagamentoVenda.datVencimento, btpItemFormaPagamentoVenda.datPagamento,
      btpItemFormaPagamentoVenda.vlrParcela,
      btpItemFormaPagamentoVenda.dscObservacao,
      btpItemFormaPagamentoVenda.btpFormaPagamento.idFormaPagamento,
      btpItemFormaPagamentoVenda.ativo]);
  }

}