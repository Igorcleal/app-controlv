import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { DbProvider } from './../db-provider';
import { BtpVenda } from './../../model/btpVenda';
import { SQLite } from "ionic-native/dist/es5";
import { BtpItemVenda } from "../../model/btpItemVenda";
import { Produto } from "../../model/produto";
import { BtpEstoque } from "../../model/btpEstoque";
import { Usuario } from "../../model/usuario";

@Injectable()
export class ItemVendaService {

  public db: SQLite = null;

  constructor(public http: Http, public dbProvider: DbProvider) {
    this.db = dbProvider.db;
  }

  getItensVenda(venda: BtpVenda, usuario: Usuario) {
    let idVenda: number = venda.idVenda
    let sql = `SELECT iv.*, p.produtoId, p.produtoDsc, est.idEstoque, est.dscEstoque
                FROM itens_venda iv
                INNER JOIN produtos p
                on p.produtoId = iv.produtoId
                LEFT JOIN estoques est
                on est.idEstoque = iv.idEstoque
                WHERE iv.vendaId = ?`;

    return this.db.executeSql(sql, [idVenda])
      .then(response => {

        let itensVenda = [];
        let btpItemVenda: BtpItemVenda;
        let produto: Produto;
        let btpEstoque: BtpEstoque;

        for (let index = 0; index < response.rows.length; index++) {

          btpItemVenda = response.rows.item(index);
          btpItemVenda.idItemVenda = response.rows.item(index).itemVendaId;
          btpItemVenda.qtdItemVenda = response.rows.item(index).itemQtd;
          btpItemVenda.vlrItemVenda = response.rows.item(index).vlrUnitario;
          btpItemVenda.vlrTotalItemVenda = response.rows.item(index).vlrTotal;
          btpItemVenda.usuario = usuario;

          produto = new Produto();
          produto.produtoId = response.rows.item(index).produtoId;
          produto.produtoDsc = response.rows.item(index).produtoDsc;
          produto.usuario = usuario;

          btpEstoque = new BtpEstoque();
          btpEstoque.idEstoque = response.rows.item(index).idEstoque;
          btpEstoque.dscEstoque = response.rows.item(index).dscEstoque;
          btpEstoque.usuario = usuario;

          btpItemVenda.produto = produto;
          btpItemVenda.btpEstoque = btpEstoque;

          itensVenda.push(btpItemVenda);
        }


        console.log(itensVenda.length);
        console.log("getAll Itens Venda.......");
        return Promise.resolve(itensVenda);
      })
      .catch(e => {
        console.log("Erro recuperar itens da venda: " + e.msg);
        return null;
      });
  }
}