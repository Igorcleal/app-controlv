import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { DbProvider } from './../db-provider';
import { BtpVenda } from './../../model/btpVenda';
import { SQLite } from "ionic-native/dist/es5";
import { Usuario } from "../../model/usuario";
import { BtpItemFormaPagamentoVenda } from "../../model/btpItemFormaPagamentoVenda";
import { BtpFormaPagamento } from "../../model/btpFormaPagamento";

@Injectable()
export class ItemFormaPagamentoVendaService {

  public db: SQLite = null;

  constructor(public http: Http, public dbProvider: DbProvider) {
    this.db = dbProvider.db;
  }

  getItensFormaPagamentoVenda(venda: BtpVenda, usuario: Usuario) {
    let sql = `SELECT ifv.*, fpg.idFormaPagamento, fpg.descricao
                FROM item_forma_pag_venda ifv
                INNER JOIN formas_pagamento fpg
                ON fpg.idFormaPagamento = ifv.idFormaPagamento
                WHERE ifv.vendaId = ?`;

    return this.db.executeSql(sql, [venda.idVenda])
      .then(response => {

        let itensFormaPagamento = [];
        let btpItemFormaPagamentoVenda: BtpItemFormaPagamentoVenda;
        let usuario: Usuario;
        let btpFormaPagamento: BtpFormaPagamento;

        for (let index = 0; index < response.rows.length; index++) {

          console.log(response.rows.item(index).itemId);

          btpItemFormaPagamentoVenda = response.rows.item(index);
          btpItemFormaPagamentoVenda.datVencimentoString = response.rows.item(index).datVencimento;

          usuario = new Usuario();
          usuario.idUsuario = response.rows.item(index).userId;
          btpItemFormaPagamentoVenda.usuario = usuario;

          btpFormaPagamento = new BtpFormaPagamento();
          btpFormaPagamento.idFormaPagamento = response.rows.item(index).idFormaPagamento;
          btpFormaPagamento.descricao = response.rows.item(index).descricao;
          btpFormaPagamento.usuario = usuario;
          
          btpItemFormaPagamentoVenda.btpFormaPagamento = btpFormaPagamento;

          itensFormaPagamento.push(btpItemFormaPagamentoVenda);
        }


        console.log("getAll Item Forma Pagamento Venda.......");
        return Promise.resolve(itensFormaPagamento);
      })
      .catch(e => {
        console.error(e.message);
        console.log("Erro recuperar itens da venda: " + e.msg);
        return null;
      });
  }
}