import { BtpDespesa } from './../model/btpDespesa';
import { BtpPagamentoDespesa } from './../model/btpPagamentoDespesa';
import { DbProvider } from './db-provider';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { SQLite } from 'ionic-native';
import { Usuario } from "../model/usuario";

@Injectable()
export class PagamentoDespesaService {

    public db: SQLite = null;

    constructor(public http: Http, public dbProvider: DbProvider) {
        this.db = dbProvider.db;

    }

    openDatabase() {
        return this.db.openDatabase({
            name: 'controlv.db',
            location: 'default'
        });
    }

    getPagamentosDespesas(despesaId: number, userId: number) {
        let sql = `SELECT *
                FROM pagamento_despesas
                WHERE despesaId = ? 
                AND userId = ? 
                AND ativo = ?`;
        return this.db.executeSql(sql, [despesaId, userId, true])
            .then(response => {
                let pagamentos = [];
                for (let index = 0; index < response.rows.length; index++) {
                    let pagamento: BtpPagamentoDespesa = response.rows.item(index);

                    let usuario = new Usuario();
                    usuario.idUsuario = userId;
                    pagamento.usuario = usuario;

                    let despesa = new BtpDespesa();
                    despesa.despesaId = response.rows.item(index).despesaId;
                    pagamento.btpDespesa = despesa;

                    pagamentos.push(pagamento);
                }
                return Promise.resolve(pagamentos);
            });
    }

    getPagamentosDespesasNaoServidor(userId: number) {
        let sql = `SELECT *
                FROM pagamento_despesas
                WHERE userId = ? 
                AND isServidor = ?`;
        return this.db.executeSql(sql, [userId, false])
            .then(response => {
                let pagamentos = [];
                for (let index = 0; index < response.rows.length; index++) {

                    let pagamento: BtpPagamentoDespesa = response.rows.item(index);

                    let usuario = new Usuario();
                    usuario.idUsuario = userId;
                    pagamento.usuario = usuario;

                    let despesa = new BtpDespesa();
                    despesa.despesaId = response.rows.item(index).despesaId;
                    pagamento.btpDespesa = despesa;

                    pagamentos.push(pagamento);
                }
                console.log('list pagamentos nao servidor:');
                console.log(pagamentos);
                return Promise.resolve(pagamentos);
            });
    }

    incluirPagamentosDespesa(pagamentos: Array<BtpPagamentoDespesa>) {
        return this.db.transaction(tx => {

            let promises = [];

            pagamentos.forEach(pagamento => {
                if (pagamento.idPagamento == null) {
                    pagamento.isServidor = false;
                    pagamento.ativo = true;
                    promises.push(this.incluirPagamentoDespesa(pagamento));
                }
            });

            return Promise.all(promises);
        });
    }

    incluirPagamentoDespesa(btpPagamento: BtpPagamentoDespesa) {
        let sql = `INSERT INTO pagamento_despesas
            (userId, 
            valorPagamento, 
            datPagamento, 
            despesaId,
            isServidor,
            ativo) 
            VALUES(?,?,?,?,?,?)`;

        btpPagamento.ativo = true;

        return this.db.executeSql(sql, [btpPagamento.usuario.idUsuario,
        btpPagamento.valorPagamento,
        new Date(btpPagamento.datPagamento).toISOString(),
        btpPagamento.btpDespesa.despesaId,
        btpPagamento.isServidor,
        btpPagamento.ativo]).then(() => {
            console.log("Sucess incluir pagamento");
        }).catch((err) => {
            console.log(err.message);
            console.error("Erro incluir Pagamento")
        });
    }

    excluirPagamentosDespesa(pagamentos: Array<BtpPagamentoDespesa>) {
        return this.db.transaction(tx => {
            let promises = [];
            pagamentos.forEach(pagamento => {
                pagamento.ativo = false;
                pagamento.isServidor = false;
                promises.push(this.alterar(pagamento));
            });
            return Promise.all(promises);
        });
    }

    delete(btpPagamento: BtpPagamentoDespesa) {
        let sql = 'DELETE FROM pagamento_despesas WHERE idPagamento=?';
        return this.db.executeSql(sql, [btpPagamento.idPagamento]);
    }

    alterar(btpPagamento: BtpPagamentoDespesa) {
        let sql = `UPDATE pagamento_despesas
            SET userId = ?,
            valorPagamento = ? ,
            datPagamento = ? ,
            despesaId = ?,
            isServidor = ?,
            ativo = ? 
            WHERE idPagamento = ?`;

        return this.db.executeSql(sql, [btpPagamento.usuario.idUsuario,
        btpPagamento.valorPagamento,
        new Date(btpPagamento.datPagamento).toISOString(),
        btpPagamento.btpDespesa.despesaId,
        btpPagamento.isServidor,
        btpPagamento.ativo,
        btpPagamento.idPagamento]).then(() => {
            console.log("Sucess incluir pagamento");
        }).catch(() => {
            console.error("Erro incluir Pagamento")
        });
    }

    alterarPagamentosDespesasGravadoServidor(pagamentos: BtpPagamentoDespesa[]) {
        let promises = [];

        pagamentos.forEach(pagamento => {
            pagamento.isServidor = true;
            promises.push(this.alterar(pagamento));
        });

        return Promise.all(promises);
    }

    inserirPagamentosDespesasDownload(lista: Array<any>) {
        let promises = [];
        lista.forEach(element => {
            let pagamento: BtpPagamentoDespesa = element;
            pagamento.idPagamento = element.chave.id;
            pagamento.usuario = new Usuario();
            pagamento.usuario.idUsuario = element.chave.idUsuario;

            pagamento.btpDespesa = new BtpDespesa();
            pagamento.btpDespesa.despesaId = element.despesaId;

            pagamento.isServidor = true;
            promises.push(this.incluirComId(pagamento));
        });
        return Promise.all(promises);
    }

    incluirComId(btpPagamento: BtpPagamentoDespesa) {
        let sql = `INSERT INTO pagamento_despesas
            (
            idPagamento,
            userId, 
            valorPagamento, 
            datPagamento, 
            despesaId,
            isServidor,
            ativo) 
            VALUES(?,?,?,?,?,?,?)`;

        return this.db.executeSql(sql, [
            btpPagamento.idPagamento,
            btpPagamento.usuario.idUsuario,
            btpPagamento.valorPagamento,
            btpPagamento.datPagamento,
            btpPagamento.btpDespesa.despesaId,
            btpPagamento.isServidor,
            btpPagamento.ativo]);
    }

}
