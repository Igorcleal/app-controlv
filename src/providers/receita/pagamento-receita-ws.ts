import { PagamentoService } from './../pagamento-provider';
import { ReceitaWsProvider } from './receita-ws';
import { HttpProvider } from './../http/http-provider';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { Usuario } from '../../model/usuario';
import { UtilsService } from './../utils-service';
import { Constants } from "../../app/constants";
import { BtpPagamentoReceita } from "../../model/btpPagamentoReceita";

@Injectable()
export class PagamentoReceitaWs {

  constructor(public http: HttpProvider, public utils: UtilsService,
    public pagamentoReceitaService: PagamentoService,
    public receitaWs: ReceitaWsProvider,) {
  }

  enviarDependencias() {
    let promises = [];
    promises.push(this.receitaWs.enviarReceitasServidor());

    return Promise.all(promises);
  }

  enviarPagamentosReceitasServidor() {

    return this.enviarDependencias().then(() => {

      return this.getPagamentosReceitasNaoServidor().then((pagamentosReceitas: BtpPagamentoReceita[]) => {

        if (pagamentosReceitas == null || pagamentosReceitas.length == 0) {
          return Promise.resolve(null);
        }

        //envia produtos para WS
        return this.http.post(`${Constants.urlWs}pagamentoReceita/salvarLst`, pagamentosReceitas).then(() => {
          //caso dê certo, altera no sqlite produtos para produtos gravadas no servidor
          return this.pagamentoReceitaService.alterarPagamentosReceitasGravadoServidor(pagamentosReceitas);
        });
      });
    });
  }

  enviarPagamentosReceitasServidorSemDependencias() {
      return this.getPagamentosReceitasNaoServidor().then((pagamentosReceitas: BtpPagamentoReceita[]) => {

        if (pagamentosReceitas == null || pagamentosReceitas.length == 0) {
          return Promise.resolve(null);
        }

        //envia produtos para WS
        return this.http.post(`${Constants.urlWs}pagamentoReceita/salvarLst`, pagamentosReceitas).then(() => {
          //caso dê certo, altera no sqlite produtos para produtos gravadas no servidor
          return this.pagamentoReceitaService.alterarPagamentosReceitasGravadoServidor(pagamentosReceitas);
        });
      });
  }

  getPagamentosReceitasNaoServidor() {
    return this.utils.getUsuarioLogado().then((usuario: Usuario) => {
      return this.pagamentoReceitaService.getPagamentosReceitasNaoServidor(usuario.idUsuario);
    });
  }

}
