import { HttpProvider } from './../http/http-provider';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { Usuario } from '../../model/usuario';
import { UtilsService } from './../utils-service';
import { Constants } from "../../app/constants";
import { CategoriaWsProvider } from "../categoria/categoria-ws";
import { ContaWS } from "../conta/conta-ws";
import { ReceitaService } from "./receita-service";
import { BtpReceita } from "../../model/btpReceita";

@Injectable()
export class ReceitaWsProvider {

  constructor(public http: HttpProvider, public utils: UtilsService, public receitaService: ReceitaService,
    public categoriaWs: CategoriaWsProvider, public contaWs: ContaWS) {
  }

  enviarDependencias() {
    let promises = [];
    promises.push(this.categoriaWs.enviarCategoriasServidor());
    promises.push(this.contaWs.enviarContasServidor());

    return Promise.all(promises);
  }

  enviarReceitasServidor() {

    return this.enviarDependencias().then(() => {

      return this.getReceitasNaoServidor().then((receitas: BtpReceita[]) => {
        if (receitas == null || receitas.length == 0) {
          return Promise.resolve(null);
        }

        //envia produtos para WS
        return this.http.post(`${Constants.urlWs}receita/salvarLst`, receitas).then(() => {
          //caso dê certo, altera no sqlite produtos para produtos gravadas no servidor
          return this.receitaService.alterarReceitasGravadoServidor(receitas);
        });
      });
    });
  }

  enviarReceitasServidorSemDependencias() {
    return this.getReceitasNaoServidor().then((receitas: BtpReceita[]) => {
      if (receitas == null || receitas.length == 0) {
        return Promise.resolve(null);
      }

      //envia produtos para WS
      return this.http.post(`${Constants.urlWs}receita/salvarLst`, receitas).then(() => {
        //caso dê certo, altera no sqlite produtos para produtos gravadas no servidor
        return this.receitaService.alterarReceitasGravadoServidor(receitas);
      });
    });
  }

  getReceitasNaoServidor() {
    return this.utils.getUsuarioLogado().then((usuario: Usuario) => {
      return this.receitaService.getReceitasNaoServidor(usuario.idUsuario);
    });
  }

}
