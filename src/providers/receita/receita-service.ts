import { SQLite } from 'ionic-native';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { BtpConta } from '../../model/btpConta';
import { BtpCliente } from './../../model/cliente';
import { BtpVenda } from './../../model/btpVenda';
import { Categoria } from './../../model/categoria';
import { BtpReceita } from './../../model/btpReceita';
import { DbProvider } from './../db-provider';
import { Usuario } from "../../model/usuario";

@Injectable()
export class ReceitaService {

  public db: SQLite = null;
  constructor(public http: Http, public dbProvider: DbProvider) {
    this.db = dbProvider.db;
  }

  getAll(userId: number) {
    let sql = `SELECT r.*, c.categoriaId, c.categoriaDsc as categoriaDsc, c.tipo as tipo, c.cor as cor,
                v.vendaId as vendaId, cli.nome as nomeCliente, cli.cliId, ct.contaId, ct.contaDsc
               FROM receitas r
                LEFT JOIN categorias c
                ON c.categoriaId = r.categoriaId
                LEFT JOIN contas ct
                ON ct.contaId = r.contaId
               LEFT JOIN vendas v
                ON v.vendaId = r.vendaId
               LEFT JOIN clientes cli
                ON cli.cliId = v.cliId
               WHERE r.userId = ?
               AND r.ativo = ?
               ORDER BY receitaDta DESC`;

    return this.db.executeSql(sql, [userId, true])
      .then(response => {
        let receitas = [];
        for (let index = 0; index < response.rows.length; index++) {
          let btpReceita: BtpReceita = response.rows.item(index);

          let usuario: Usuario = new Usuario();
          usuario.idUsuario = response.rows.item(index).userId;
          btpReceita.usuario = usuario;

          let categoria = new Categoria();
          categoria.categoriaId = response.rows.item(index).categoriaId;
          categoria.categoriaDsc = response.rows.item(index).categoriaDsc;
          categoria.tipo = response.rows.item(index).tipo;
          categoria.cor = response.rows.item(index).cor;
          categoria.usuario = usuario;
          btpReceita.categoria = categoria;

          let btpVenda: BtpVenda = new BtpVenda();
          btpVenda.idVenda = response.rows.item(index).vendaId;
          btpVenda.usuario = usuario;

          let btpCliente: BtpCliente = new BtpCliente();
          btpCliente.nome = response.rows.item(index).nomeCliente;
          btpCliente.cliId = response.rows.item(index).cliId;
          btpCliente.usuario = usuario;
          btpVenda.btpCliente = btpCliente;

          let btpConta = new BtpConta();
          btpConta.contaDsc = response.rows.item(index).contaDsc;
          btpConta.contaId = response.rows.item(index).contaId;
          btpConta.usuario = usuario;
          btpReceita.btpConta = btpConta;

          btpReceita.btpVenda = btpVenda;

          receitas.push(btpReceita);
        }
        console.log("executou consulta: getAll todas receitas................");
        return Promise.resolve(receitas);
      });
  }

  getReceitasNaoServidor(userId: number) {
    let sql = `SELECT r.*, c.categoriaId, c.categoriaDsc as categoriaDsc, c.tipo as tipo, c.cor as cor,
                v.vendaId as vendaId, cli.nome as nomeCliente, cli.cliId, ct.contaId, ct.contaDsc
               FROM receitas r
               LEFT JOIN categorias c
                ON c.categoriaId = r.categoriaId
               LEFT JOIN contas ct
                ON ct.contaId = r.contaId
               LEFT JOIN vendas v
                ON v.vendaId = r.vendaId
               LEFT JOIN clientes cli
                ON cli.cliId = v.cliId
              WHERE r.userId = ?
               and r.isServidor = ?
               ORDER BY r.receitaId DESC`;

    return this.db.executeSql(sql, [userId, false])
      .then(response => {
        let receitas = [];
        for (let index = 0; index < response.rows.length; index++) {

          let btpReceita: BtpReceita = response.rows.item(index);

          let usuario: Usuario = new Usuario();
          usuario.idUsuario = response.rows.item(index).userId;
          btpReceita.usuario = usuario;

          let categoria = new Categoria();
          categoria.categoriaId = response.rows.item(index).categoriaId;
          categoria.categoriaDsc = response.rows.item(index).categoriaDsc;
          categoria.tipo = response.rows.item(index).tipo;
          categoria.cor = response.rows.item(index).cor;
          categoria.usuario = usuario;
          btpReceita.categoria = categoria;

          let btpVenda: BtpVenda = new BtpVenda();
          btpVenda.idVenda = response.rows.item(index).vendaId;
          btpVenda.usuario = usuario;

          let btpCliente: BtpCliente = new BtpCliente();
          btpCliente.nome = response.rows.item(index).nomeCliente;
          btpCliente.cliId = response.rows.item(index).cliId;
          btpCliente.usuario = usuario;
          btpVenda.btpCliente = btpCliente;

          let btpConta = new BtpConta();
          btpConta.contaDsc = response.rows.item(index).contaDsc;
          btpConta.contaId = response.rows.item(index).contaId;
          btpConta.usuario = usuario;
          btpReceita.btpConta = btpConta;

          btpReceita.btpVenda = btpVenda;

          receitas.push(btpReceita);
        }
        console.log("executou consulta: getAll data receitas................");
        return Promise.resolve(receitas);
      }).catch(err => {
        console.error(err.message);
        return Promise.reject(true);
      });
  }

  getAllByDate(userId: number, data: Date) {
    let sql = `SELECT r.*, c.categoriaId, c.categoriaDsc as categoriaDsc, c.tipo as tipo, c.cor as cor,
                v.vendaId as vendaId, cli.nome as nomeCliente, cli.cliId, ct.contaId, ct.contaDsc
               FROM receitas r
               LEFT JOIN categorias c
                ON c.categoriaId = r.categoriaId
               LEFT JOIN contas ct
                ON ct.contaId = r.contaId
               LEFT JOIN vendas v
                ON v.vendaId = r.vendaId
               LEFT JOIN clientes cli
                ON cli.cliId = v.cliId
              WHERE r.userId = ?
               AND r.ativo = ?
               AND strftime('%m%Y',r.receitaDta) = ?
               ORDER BY r.receitaDta DESC`;

    return this.db.executeSql(sql, [userId, true, this.dbProvider.meses[data.getMonth()] + "" + data.getFullYear()])
      .then(response => {
        let receitas = [];
        for (let index = 0; index < response.rows.length; index++) {

          let btpReceita: BtpReceita = response.rows.item(index);
          let categoria = new Categoria();
          categoria.categoriaId = response.rows.item(index).categoriaId;
          categoria.categoriaDsc = response.rows.item(index).categoriaDsc;
          categoria.tipo = response.rows.item(index).tipo;
          categoria.cor = response.rows.item(index).cor;
          btpReceita.categoria = categoria;

          let btpVenda: BtpVenda = new BtpVenda();
          btpVenda.idVenda = response.rows.item(index).vendaId;

          let btpCliente: BtpCliente = new BtpCliente();
          btpCliente.nome = response.rows.item(index).nomeCliente;
          btpCliente.cliId = response.rows.item(index).cliId;
          btpVenda.btpCliente = btpCliente;

          let btpConta = new BtpConta();
          btpConta.contaDsc = response.rows.item(index).contaDsc;
          btpConta.contaId = response.rows.item(index).contaId;
          btpReceita.btpConta = btpConta;

          btpReceita.btpVenda = btpVenda;

          receitas.push(btpReceita);
        }
        console.log("executou consulta: getAll data receitas................");
        return Promise.resolve(receitas);
      }).catch(err => {
        console.error(err.message);
        return Promise.reject(true);
      });
  }

  incluirReceitaPeriodica(btpReceita: BtpReceita) {
    let sql = `INSERT INTO receitas( 
               userId,
               receita,
               receitaDta, 
               receitaVlr,
               receitaVlrPago,
               receitaPaga,
               categoriaId,
               vendaId,
               receitaIdPai,
               contaId,
               isServidor,
               ativo )    
    VALUES(?,?,?,?,?,?,?,?,?,?,?,?)`;

    let idVenda: number = null;

    if (btpReceita != null && btpReceita.btpVenda != null && btpReceita.btpVenda.idVenda != null) {
      idVenda = btpReceita.btpVenda.idVenda
    }

    let receitaIdPai: number = null;
    if (btpReceita.btpReceitaPai) {
      receitaIdPai = btpReceita.btpReceitaPai.receitaId;
    }

    btpReceita.ativo = true;

    return this.db.executeSql(sql, [btpReceita.usuario.idUsuario,
    btpReceita.receita,
    new Date(btpReceita.receitaDta).toISOString(),
    btpReceita.receitaVlr,
    btpReceita.receitaVlrPago,
    btpReceita.receitaPaga,
    btpReceita.categoria.categoriaId,
      idVenda, receitaIdPai, btpReceita.btpConta.contaId,
    btpReceita.isServidor, btpReceita.ativo]);
  }

  incluir(btpReceita: BtpReceita) {
    let sql = `INSERT INTO receitas( 
               userId,
               receita,
               receitaDta, 
               receitaVlr,
               receitaVlrPago,
               receitaPaga,
               categoriaId,
               vendaId,
               receitaIdPai,
               contaId,
               isServidor,
               ativo )    
    VALUES(?,?,?,?,?,?,?,?,?,?,?,?)`;

    let idVenda: number = null;
    let receitaIdPai: number = null;
    if (btpReceita.btpReceitaPai) {
      receitaIdPai = btpReceita.btpReceitaPai.receitaId;
    }

    if (btpReceita != null && btpReceita.btpVenda != null && btpReceita.btpVenda.idVenda != null) {
      idVenda = btpReceita.btpVenda.idVenda
    }

    btpReceita.ativo = true;

    let idInserido: number;
    return this.db.transaction((tx) => {
      return tx.executeSql(sql, [btpReceita.usuario.idUsuario,
      btpReceita.receita,
      btpReceita.receitaDta,
      btpReceita.receitaVlr,
      btpReceita.receitaVlrPago,
      btpReceita.receitaPaga,
      btpReceita.categoria.categoriaId,
        idVenda,
        receitaIdPai,
      btpReceita.btpConta.contaId,
      btpReceita.isServidor,
      btpReceita.ativo], (tx, resultSet) => {
        idInserido = resultSet.insertId;
      }, function (tx, error) {
        console.log('INSERT error: ' + error.message);
      });
    }).then((response) => {
      return Promise.resolve(idInserido);
    });
  }

  alterar(btpReceita: BtpReceita) {
    let sql = `UPDATE receitas 
           SET receita = ?,
               receitaDta = ?, 
               receitaVlr = ?,
               receitaVlrPago = ?,
               receitaPaga = ?,
               categoriaId = ?,
               contaId = ?,
               isServidor = ?,
               ativo = ?
          WHERE receitaId=?`;

    return this.db.executeSql(sql, [btpReceita.receita, btpReceita.receitaDta,
    btpReceita.receitaVlr, btpReceita.receitaVlrPago, btpReceita.receitaPaga,
    btpReceita.categoria.categoriaId, btpReceita.btpConta.contaId,
    btpReceita.isServidor, btpReceita.ativo, btpReceita.receitaId]);
  }

  deletar(btpReceita: BtpReceita) {
    let sql = 'DELETE FROM receitas WHERE receitaId=?';
    return this.db.executeSql(sql, [btpReceita.receitaId]);
  }

  alterarReceitasGravadoServidor(receitas: BtpReceita[]) {
    let promises = [];

    receitas.forEach(receita => {
      receita.isServidor = true;
      promises.push(this.alterar(receita));
    });

    return Promise.all(promises);
  }

  inserirReceitasDownload(lista: Array<any>) {

    let promises = [];
    lista.forEach(element => {
      let receita: BtpReceita = element;
      receita.receitaId = element.chave.id;
      receita.usuario = new Usuario();
      receita.usuario.idUsuario = element.chave.idUsuario;

      receita.categoria = new Categoria();
      receita.categoria.categoriaId = element.categoriaId;

      receita.btpConta = new BtpConta();
      receita.btpConta.contaId = element.contaId;

      receita.btpVenda = new BtpVenda();
      receita.btpVenda.idVenda = element.vendaId;

      receita.btpReceitaPai = new BtpReceita();
      receita.btpReceitaPai.receitaId = element.receitaId;

      receita.isServidor = true;
      promises.push(this.incluirComId(receita));
    });
    return Promise.all(promises);
  }

  incluirComId(btpReceita: BtpReceita) {
    let sql = `INSERT INTO receitas( 
               receitaId,
               userId,
               receita,
               receitaDta, 
               receitaVlr,
               receitaVlrPago,
               receitaPaga,
               categoriaId,
               vendaId,
               receitaIdPai,
               contaId,
               isServidor,
               ativo )    
    VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)`;

    let idVenda: number = null;
    let receitaIdPai: number = null;
    if (btpReceita.btpReceitaPai) {
      receitaIdPai = btpReceita.btpReceitaPai.receitaId;
    }

    if (btpReceita != null && btpReceita.btpVenda != null && btpReceita.btpVenda.idVenda != null) {
      idVenda = btpReceita.btpVenda.idVenda
    }

    let idInserido: number;
    return this.db.transaction((tx) => {
      return tx.executeSql(sql, [
        btpReceita.receitaId,
        btpReceita.usuario.idUsuario,
        btpReceita.receita,
        btpReceita.receitaDta,
        btpReceita.receitaVlr,
        btpReceita.receitaVlrPago,
        btpReceita.receitaPaga,
        btpReceita.categoria.categoriaId,
        idVenda,
        receitaIdPai,
        btpReceita.btpConta.contaId,
        btpReceita.isServidor,
        btpReceita.ativo], (tx, resultSet) => {
          idInserido = resultSet.insertId;
        }, function (tx, error) {
          console.log('INSERT error: ' + error.message);
        });
    }).then((response) => {
      return Promise.resolve(idInserido);
    });
  }

}
