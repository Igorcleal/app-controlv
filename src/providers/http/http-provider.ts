import { Constants } from './../../app/constants';
import { NetworkProvider } from './../network/network-provider';
import { Injectable } from '@angular/core';
import { Http, RequestOptionsArgs } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/timeout';

@Injectable()
export class HttpProvider {

  constructor(public http: Http, public network: NetworkProvider) {
    console.log('Hello HttpProvider Provider');
  }

  getHeaders(authorization) {
    return {
      // 'Accept': 'application/json',
      // 'Content-Type': 'application/json',
      'Authorization': 'Basic ' + authorization
    }
  }

  post(url: string, body: any, options?: RequestOptionsArgs) {

    if (this.network.existeConexao()==true) {
      return this.http.post(url, body)
              .timeout(Constants.timeout)
              .map(result => result.json()).toPromise();
    }
    else{
      return Promise.reject(true);
    }
  }

  put(url: string, body: any, options?: RequestOptionsArgs) {

    if (this.network.existeConexao()==true) {
      return this.http.put(url, body)
              .timeout(Constants.timeout)
              .map(result => result.json()).toPromise();
    }
    else{
      return Promise.reject(true);
    }
  }

  get(url: string, options?: any): Promise<any> {
    return this.http.get(url)
    .timeout(Constants.timeout)
    .map(result => result.json()).toPromise();
  }

}
