import { HttpProvider } from './../http/http-provider';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { Usuario } from '../../model/usuario';
import { UtilsService } from './../utils-service';
import { Constants } from "../../app/constants";
import { DespesaService } from "./despesa-service";
import { BtpDespesa } from "../../model/btpDespesa";
import { CategoriaWsProvider } from "../categoria/categoria-ws";
import { ContaWS } from "../conta/conta-ws";

@Injectable()
export class DespesaWsProvider {

  constructor(public http: HttpProvider, public utils: UtilsService, public despesaService: DespesaService,
    public categoriaWs: CategoriaWsProvider, public contaWs: ContaWS) {
  }

  enviarDependencias() {
    let promises = [];
    promises.push(this.categoriaWs.enviarCategoriasServidor());
    promises.push(this.contaWs.enviarContasServidor());

    return Promise.all(promises);
  }

  enviarDespesasServidor() {

    return this.enviarDependencias().then(() => {

      return this.getDespesasNaoServidor().then((despesas: BtpDespesa[]) => {

        console.log('Despesas:');
        console.log(despesas);

        if (despesas == null || despesas.length == 0) {
          return Promise.resolve(null);
        }

        //envia produtos para WS
        return this.http.post(`${Constants.urlWs}despesa/salvarLst`, despesas).then(() => {
          //caso dê certo, altera no sqlite produtos para produtos gravadas no servidor
          return this.despesaService.alterarDespesasGravadoServidor(despesas);
        });
      });
    });
  }

  enviarDespesasServidorSemDependencias() {

      return this.getDespesasNaoServidor().then((despesas: BtpDespesa[]) => {

        if (despesas == null || despesas.length == 0) {
          return Promise.resolve(null);
        }

        //envia produtos para WS
        return this.http.post(`${Constants.urlWs}despesa/salvarLst`, despesas).then(() => {
          //caso dê certo, altera no sqlite produtos para produtos gravadas no servidor
          return this.despesaService.alterarDespesasGravadoServidor(despesas);
        });
    });
  }

  getDespesasNaoServidor() {
    console.log(2);
    return this.utils.getUsuarioLogado().then((usuario: Usuario) => {
      return this.despesaService.getDespesasNaoServidor(usuario.idUsuario);
    });
  }

}
