import { BtpPagamentoDespesa } from './../../model/btpPagamentoDespesa';
import { PagamentoDespesaService } from './../pagamento-despesa-provider';
import { HttpProvider } from './../http/http-provider';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { Usuario } from '../../model/usuario';
import { UtilsService } from './../utils-service';
import { Constants } from "../../app/constants";
import { DespesaWsProvider } from "./despesa-ws";

@Injectable()
export class PagamentoDespesaWsProvider {

  constructor(public http: HttpProvider, public utils: UtilsService,
    public pagamentoDespesaService: PagamentoDespesaService,
    public despesaWs: DespesaWsProvider) {
  }

  enviarDependencias() {
    let promises = [];
    promises.push(this.despesaWs.enviarDespesasServidor());

    return Promise.all(promises);
  }

  enviarPagamentosDespesasServidor() {

    return this.enviarDependencias().then(() => {

      return this.getPagamentosDespesasNaoServidor().then((pagamentosDespesas: BtpPagamentoDespesa[]) => {

        if (pagamentosDespesas == null || pagamentosDespesas.length == 0) {
          return Promise.resolve(null);
        }

        //envia produtos para WS
        return this.http.post(`${Constants.urlWs}pagamentoDespesa/salvarLst`, pagamentosDespesas).then(() => {
          //caso dê certo, altera no sqlite produtos para produtos gravadas no servidor
          return this.pagamentoDespesaService.alterarPagamentosDespesasGravadoServidor(pagamentosDespesas);
        });
      });
    });
  }

  enviarPagamentosDespesasServidorSemDependencias() {

    return this.getPagamentosDespesasNaoServidor().then((pagamentosDespesas: BtpPagamentoDespesa[]) => {
      if (pagamentosDespesas == null || pagamentosDespesas.length == 0) {
        return Promise.resolve(null);
      }
      //envia produtos para WS
      return this.http.post(`${Constants.urlWs}pagamentoDespesa/salvarLst`, pagamentosDespesas).then(() => {
        //caso dê certo, altera no sqlite produtos para produtos gravadas no servidor
        return this.pagamentoDespesaService.alterarPagamentosDespesasGravadoServidor(pagamentosDespesas);
      });
    });
  }

  getPagamentosDespesasNaoServidor() {
    return this.utils.getUsuarioLogado().then((usuario: Usuario) => {
      return this.pagamentoDespesaService.getPagamentosDespesasNaoServidor(usuario.idUsuario);
    });
  }

}
