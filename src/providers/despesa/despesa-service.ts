import { SQLite } from 'ionic-native';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { BtpConta } from './../../model/btpConta';
import { Categoria } from './../../model/categoria';
import { BtpDespesa } from './../../model/btpDespesa';
import { DbProvider } from './../db-provider';
import { BtpCompra } from "../../model/btpCompra";
import { Usuario } from "../../model/usuario";

@Injectable()
export class DespesaService {

  public db: SQLite = null;
  constructor(public http: Http, public dbProvider: DbProvider) {
    this.db = dbProvider.db;
  }

  getAll(userId: number) {
    let sql = `SELECT d.*, c.categoriaId, c.categoriaDsc as categoriaDsc, c.tipo as tipo, c.cor as cor, com.compraId,
                 ct.contaId, ct.contaDsc
               FROM despesas d
               LEFT JOIN categorias c
                ON c.categoriaId = d.categoriaId
               LEFT JOIN contas ct
                ON ct.contaId = d.contaId
               LEFT JOIN compras com
                ON com.compraId = d.compraId
               WHERE d.userId = ?
               ORDER BY despesaDta DESC`;

    return this.db.executeSql(sql, [userId])
      .then(response => {
        let despesas = [];
        for (let index = 0; index < response.rows.length; index++) {
          let btpDespesa: BtpDespesa = response.rows.item(index);

          let usuario: Usuario = new Usuario();
          usuario.idUsuario = response.rows.item(index).userId;
          btpDespesa.usuario = usuario;

          let btpDespesaPai = new BtpDespesa();
          btpDespesaPai.despesaId = response.rows.item(index).despesaIdPai;
          btpDespesaPai.usuario = usuario;
          btpDespesa.btpDespesaPai = btpDespesaPai;

          let categoria = new Categoria();
          categoria.categoriaId = response.rows.item(index).categoriaId;
          categoria.categoriaDsc = response.rows.item(index).categoriaDsc;
          categoria.tipo = response.rows.item(index).tipo;
          categoria.cor = response.rows.item(index).cor;
          categoria.usuario = usuario;
          btpDespesa.categoria = categoria;

          let btpConta = new BtpConta();
          btpConta.contaDsc = response.rows.item(index).contaDsc;
          btpConta.contaId = response.rows.item(index).contaId;
          btpConta.usuario = usuario;
          btpDespesa.btpConta = btpConta;

          let compra: BtpCompra = new BtpCompra();
          compra.idCompra = response.rows.item(index).compraId;
          compra.usuario = usuario;
          btpDespesa.btpCompra = compra;

          despesas.push(btpDespesa);
        }
        console.log("executou consulta: getAll despesas................");
        return Promise.resolve(despesas);
      });
  }

  getDespesasNaoServidor(userId: number) {
    let sql = `SELECT d.*, 
                c.categoriaId, c.categoriaDsc as categoriaDsc, c.tipo as tipo, c.cor as cor,
                com.compraId,
                ct.contaId, ct.contaDsc
               FROM despesas d
               LEFT JOIN categorias c
                ON c.categoriaId = d.categoriaId
               LEFT JOIN contas ct
                ON ct.contaId = d.contaId
               LEFT JOIN compras com
                ON com.compraId = d.compraId
               WHERE d.userId = ?
               AND d.isServidor = ?
               ORDER BY d.despesaDta DESC`;
    console.log(userId);
    return this.db.executeSql(sql, [userId, false])
      .then(response => {
        let despesas = [];
        console.log(response.rows.length + "registros nao estão no servidor");

        for (let index = 0; index < response.rows.length; index++) {
          let btpDespesa: BtpDespesa = response.rows.item(index);

          let usuario: Usuario = new Usuario();
          usuario.idUsuario = response.rows.item(index).userId;
          btpDespesa.usuario = usuario;

          let btpDespesaPai = new BtpDespesa();
          btpDespesaPai.despesaId = response.rows.item(index).despesaIdPai;
          btpDespesaPai.usuario = usuario;
          btpDespesa.btpDespesaPai = btpDespesaPai;

          let categoria = new Categoria();
          categoria.categoriaId = response.rows.item(index).categoriaId;
          categoria.categoriaDsc = response.rows.item(index).categoriaDsc;
          categoria.tipo = response.rows.item(index).tipo;
          categoria.cor = response.rows.item(index).cor;
          categoria.usuario = usuario;
          btpDespesa.categoria = categoria;

          let btpConta = new BtpConta();
          btpConta.contaDsc = response.rows.item(index).contaDsc;
          btpConta.contaId = response.rows.item(index).contaId;
          btpConta.usuario = usuario;
          btpDespesa.btpConta = btpConta;

          let compra: BtpCompra = new BtpCompra();
          compra.idCompra = response.rows.item(index).compraId;
          compra.usuario = usuario;
          btpDespesa.btpCompra = compra;

          despesas.push(btpDespesa);
        }
        console.log("executou consulta: getAll despesas................");
        return Promise.resolve(despesas);
      }).catch(err => {
        console.log(err.message);
        return Promise.reject(true);
      });
  }

  getAllByDate(userId: number, data: Date) {
    let sql = `SELECT d.*, c.categoriaId, c.categoriaDsc as categoriaDsc, c.tipo as tipo, c.cor as cor, com.compraId,
                ct.contaId, ct.contaDsc
               FROM despesas d
               LEFT JOIN categorias c
                ON c.categoriaId = d.categoriaId
               LEFT JOIN contas ct
                ON ct.contaId = d.contaId
               LEFT JOIN compras com
                ON com.compraId = d.compraId
               WHERE d.userId = ?
               AND strftime('%m%Y',d.despesaDta) = ?
               AND d.ativo = ?
               ORDER BY d.despesaDta DESC`;
    return this.db.executeSql(sql, [userId, this.dbProvider.meses[data.getMonth()] + "" + data.getFullYear(), true])
      .then(response => {
        let despesas = [];
        for (let index = 0; index < response.rows.length; index++) {
          let btpDespesa: BtpDespesa = response.rows.item(index);

          let usuario: Usuario = new Usuario();
          usuario.idUsuario = response.rows.item(index).userId;
          btpDespesa.usuario = usuario;

          let btpDespesaPai = new BtpDespesa();
          btpDespesaPai.despesaId = response.rows.item(index).despesaIdPai;
          btpDespesaPai.usuario = usuario;
          btpDespesa.btpDespesaPai = btpDespesaPai;

          let categoria = new Categoria();
          categoria.categoriaId = response.rows.item(index).categoriaId;
          categoria.categoriaDsc = response.rows.item(index).categoriaDsc;
          categoria.tipo = response.rows.item(index).tipo;
          categoria.cor = response.rows.item(index).cor;
          categoria.usuario = usuario;
          btpDespesa.categoria = categoria;

          let btpConta = new BtpConta();
          btpConta.contaDsc = response.rows.item(index).contaDsc;
          btpConta.contaId = response.rows.item(index).contaId;
          btpConta.usuario = usuario;
          btpDespesa.btpConta = btpConta;

          let compra: BtpCompra = new BtpCompra();
          compra.idCompra = response.rows.item(index).compraId;
          compra.usuario = usuario;
          btpDespesa.btpCompra = compra;

          despesas.push(btpDespesa);
        }
        console.log("executou consulta: getAll despesas................");
        return Promise.resolve(despesas);
      }).catch(err => {
        console.log(err.message);
        return Promise.reject(true);
      });
  }

  incluir(btpDespesa: BtpDespesa) {
    let sql = `INSERT INTO despesas( 
               userId,
               despesa,
               despesaDta, 
               despesaVlr,
               despesaVlrPago,
               despesaPaga,
               categoriaId, 
               compraId,
               contaId,
               isServidor,
               ativo )    
    VALUES(?,?,?,?,?,?,?,?,?,?,?)`;

    console.log(btpDespesa)

    let idInserido: number;

    let idCompra: number = null;

    if (btpDespesa.btpCompra != null && btpDespesa.btpCompra.idCompra != null) {
      idCompra = btpDespesa.btpCompra.idCompra;
    }

    let despesaIdPai: number = null;
    if (btpDespesa.btpDespesaPai) {
      despesaIdPai = btpDespesa.btpDespesaPai.despesaId;
    }

    btpDespesa.ativo = true;

    return this.db.transaction((tx) => {
      return tx.executeSql(sql, [btpDespesa.usuario.idUsuario,
      btpDespesa.despesa,
      btpDespesa.despesaDta,
      btpDespesa.despesaVlr,
      btpDespesa.despesaValorPago,
      btpDespesa.despesaPaga,
      btpDespesa.categoria.categoriaId,
        idCompra,
      btpDespesa.btpConta.contaId,
      btpDespesa.isServidor,
      btpDespesa.ativo], (tx, resultSet) => {
        idInserido = resultSet.insertId;
      }, function (tx, error) {
        console.log('INSERT error: ' + error.message);
      });
    }).then((response) => {
      console.log('inseriiiiiiiu');
      return Promise.resolve(idInserido);
    });
  }

  incluirDespesasPeriodicas(btpDespesa: BtpDespesa) {
    let sql = `INSERT INTO despesas( 
               userId,
               despesa,
               despesaDta, 
               despesaVlr,
               despesaVlrPago,
               despesaPaga,
               categoriaId,
               despesaIdPai,
               contaId, 
               isServidor,
               ativo)    
    VALUES(?,?,?,?,?,?,?,?,?,?,?)`;

    btpDespesa.ativo = true;

    let despesaIdPai: number = null;
    if (btpDespesa.btpDespesaPai) {
      despesaIdPai = btpDespesa.btpDespesaPai.despesaId;
    }

    return this.db.executeSql(sql, [btpDespesa.usuario.idUsuario,
    btpDespesa.despesa,
    new Date(btpDespesa.despesaDta).toISOString(),
    btpDespesa.despesaVlr,
    btpDespesa.despesaValorPago,
    btpDespesa.despesaPaga,
    btpDespesa.categoria.categoriaId,
    btpDespesa.btpDespesaPai.despesaId,
    btpDespesa.btpConta.contaId,
    btpDespesa.isServidor,
    btpDespesa.ativo]);
  }

  alterar(btpDespesa: BtpDespesa) {
    let sql = `UPDATE despesas 
           SET despesa = ?,
               despesaDta = ?, 
               despesaVlr = ?,
               despesaVlrPago = ?,
               despesaPaga = ?,
               categoriaId = ?,
               contaId = ?,
               isServidor = ?,
               ativo = ?
          WHERE despesaId=?`;

    return this.db.executeSql(sql, [btpDespesa.despesa,
    btpDespesa.despesaDta, btpDespesa.despesaVlr, btpDespesa.despesaValorPago, btpDespesa.despesaPaga,
    btpDespesa.categoria.categoriaId, btpDespesa.btpConta.contaId, btpDespesa.isServidor, btpDespesa.ativo,
    btpDespesa.despesaId]);
  }

  deletar(btpDespesa: BtpDespesa) {
    let sql = 'DELETE FROM despesas WHERE despesaId=?';
    return this.db.executeSql(sql, [btpDespesa.despesaId]);
  }

  alterarDespesasGravadoServidor(despesas: BtpDespesa[]) {
    let promises = [];

    despesas.forEach(despesa => {
      despesa.isServidor = true;
      promises.push(this.alterar(despesa));
    });

    return Promise.all(promises);
  }

  inserirDespesasDownload(lista: Array<any>) {

    let promises = [];
    lista.forEach(element => {
      let despesa: BtpDespesa = element;
      despesa.despesaId = element.chave.id;
      despesa.usuario = new Usuario();
      despesa.usuario.idUsuario = element.chave.idUsuario;

      despesa.categoria = new Categoria();
      despesa.categoria.categoriaId = element.categoriaId;

      despesa.btpConta = new BtpConta();
      despesa.btpConta.contaId = element.contaId;

      despesa.btpCompra = new BtpCompra();
      despesa.btpCompra.idCompra = element.compraId;

      despesa.btpDespesaPai = new BtpDespesa();
      despesa.btpDespesaPai.despesaId = element.despesaId;

      despesa.isServidor = true;
      promises.push(this.incluirComId(despesa));
    });
    return Promise.all(promises);
  }

  incluirComId(btpDespesa: BtpDespesa) {
    let sql = `INSERT INTO despesas( 
               despesaId,
               userId,
               despesa,
               despesaDta, 
               despesaVlr,
               despesaVlrPago,
               despesaPaga,
               categoriaId, 
               compraId,
               contaId,
               isServidor,
               ativo )    
    VALUES(?,?,?,?,?,?,?,?,?,?,?,?)`;

    console.log(btpDespesa)

    let idInserido: number;

    let idCompra: number = null;

    if (btpDespesa.btpCompra != null && btpDespesa.btpCompra.idCompra != null) {
      idCompra = btpDespesa.btpCompra.idCompra;
    }

    let despesaIdPai: number = null;
    if (btpDespesa.btpDespesaPai) {
      despesaIdPai = btpDespesa.btpDespesaPai.despesaId;
    }

    return this.db.transaction((tx) => {
      return tx.executeSql(sql, [
        btpDespesa.despesaId,
        btpDespesa.usuario.idUsuario,
        btpDespesa.despesa,
        btpDespesa.despesaDta,
        btpDespesa.despesaVlr,
        btpDespesa.despesaValorPago,
        btpDespesa.despesaPaga,
        btpDespesa.categoria.categoriaId,
        idCompra,
        btpDespesa.btpConta.contaId,
        btpDespesa.isServidor,
        btpDespesa.ativo], (tx, resultSet) => {
          idInserido = resultSet.insertId;
        }, function (tx, error) {
          console.log('INSERT error: ' + error.message);
        });
    }).then((response) => {
      return Promise.resolve(idInserido);
    });
  }


}