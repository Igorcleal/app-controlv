import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Device } from '@ionic-native/device';

import { Constants } from "../../app/constants";
import { HttpProvider } from './../http/http-provider';
import { Usuario } from '../../model/usuario';
import { UtilsService } from './../utils-service';

@Injectable()
export class UsuarioWsProvider {

  constructor(public http: HttpProvider, public utils: UtilsService, public device:Device) {
  }

  cadastrarUsuario(usuario:Usuario){
    return this.http.post(`${Constants.urlWs}usuario`, usuario);
  }

  atualizarUidPhone(usuario){

    usuario.uidUltimoLogin = this.device.uuid;

    return this.http.post(`${Constants.urlWs}usuario/logar`, usuario);
  }

}
