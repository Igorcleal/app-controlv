import { Usuario } from '../../model/usuario';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { DbProvider } from './../db-provider';
import { Categoria } from './../../model/categoria';
import { TipoCategoria } from './../../model/tipo-categoria';
import { SQLite } from 'ionic-native';

@Injectable()
export class CategoriaService {

  public db: SQLite = null;

  constructor(public http: Http, public dbProvider: DbProvider) {
    this.db = dbProvider.db;

  }

  openDatabase() {
    return this.db.openDatabase({
      name: 'controlv.db',
      location: 'default'
    });
  }

  getAll(tipo: TipoCategoria, userId: number) {
    let sql = `SELECT c.*
                FROM categorias c
                WHERE c.tipo = ?
                AND ativo = ?`;
    return this.db.executeSql(sql, [tipo, true])
      .then(response => {
        let categorias = [];
        for (let index = 0; index < response.rows.length; index++) {
          let categoria: Categoria = response.rows.item(index);
          categoria.usuario = new Usuario();
          categoria.usuario.idUsuario = response.rows.item(index).userId;
          categorias.push(categoria);
        }
        return Promise.resolve(categorias);
      });
  }

  getCategoriasNaoServidor(userId: number) {
    let sql = `SELECT c.*
                FROM categorias c
                WHERE isServidor = ?`;
    return this.db.executeSql(sql, [false])
      .then(response => {
        let categorias = [];
        for (let index = 0; index < response.rows.length; index++) {
          let categoria: Categoria = response.rows.item(index);
          categoria.usuario = new Usuario();
          categoria.usuario.idUsuario = response.rows.item(index).userId;

          categorias.push(response.rows.item(index));
        }
        return Promise.resolve(categorias);
      });
  }

  getCategoria(idCategoria: number) {
    let sql = `SELECT CategoriaId, CategoriaDsc, tipo 
                FROM categorias
                WHERE categoriaId=?`;
    return this.db.executeSql(sql, [idCategoria])
      .then(response => {
        let categoria: Categoria;
        for (let index = 0; index < response.rows.length; index++) {
          categoria = response.rows.item(index);
        }
        return Promise.resolve(categoria);
      });
  }

  incluirCategoria(categoria: Categoria) {
    let sql = 'INSERT INTO categorias(UserId, CategoriaDsc, tipo, cor, isServidor, ativo) VALUES(?,?,?,?,?,?)';

    categoria.ativo = true;

    return this.db.executeSql(sql, [categoria.usuario.idUsuario,
    categoria.categoriaDsc,
    categoria.tipo,
    categoria.cor,
    categoria.isServidor,
    categoria.ativo]);
  }

  delete(categoria: Categoria) {
    let sql = 'DELETE FROM categorias WHERE CategoriaId=?';
    return this.db.executeSql(sql, [categoria.categoriaId]);
  }

  update(categoria: Categoria) {
    let sql = `UPDATE categorias 
               SET CategoriaDsc = ?, cor = ? , isServidor = ? , ativo = ?
               WHERE CategoriaId=?`;
    return this.db.executeSql(sql, [categoria.categoriaDsc,
    categoria.cor,
    categoria.isServidor,
    categoria.ativo,
    categoria.categoriaId]);
  }

  alterarCategoriasGravadoServidor(categorias: Categoria[]) {

    let promises = [];

    categorias.forEach(categoria => {
      categoria.isServidor = true;
      promises.push(this.update(categoria));
    });

    return Promise.all(promises);
  }

  inserirCategoriasBasicas(idUsuario: string) {
    let promises = [];
    
    promises.push(this.inserirCategoriaDespesasReceitas(idUsuario));
    promises.push(this.inserirCategoriasClientes(idUsuario));
    promises.push(this.inserirCategoriasProdutos(idUsuario));
  }


  inserirCategoriaDespesasReceitas(idUsuario: string) {
    let sql = `SELECT count(*) as count
               FROM categorias
               WHERE tipo = 2`;
    return this.db.executeSql(sql, []).then(response => {
      let count = response.rows.item(0).count;
      if (count == 0) {
        let sql = 'INSERT INTO categorias(CategoriaDsc, userId, tipo, cor, isServidor, ativo) VALUES(?,?,?,?,?,?)';

        this.db.transaction(
          tx => {
            tx.executeSql(sql, ['Alimentação', idUsuario, TipoCategoria.despesas_receitas, '#4767a4'], (tx, resultSet) => {
            }, function (tx, error) {
              console.log('INSERT error: ' + error.message);
            });
            tx.executeSql(sql, ['Educação', idUsuario, TipoCategoria.despesas_receitas, 'red', false, true]);
            tx.executeSql(sql, ['Lazer', idUsuario, TipoCategoria.despesas_receitas, 'orange', false, true]);
            tx.executeSql(sql, ['Pagamentos', idUsuario, TipoCategoria.despesas_receitas, 'gold', false, true]);
            tx.executeSql(sql, ['Saúde', idUsuario, TipoCategoria.despesas_receitas, 'gray', false, true]);
            tx.executeSql(sql, ['Transporte', idUsuario, TipoCategoria.despesas_receitas, 'green', false, true]);
            tx.executeSql(sql, ['Outros', idUsuario, TipoCategoria.despesas_receitas, 'blue', false, true]);
          });
      }
    })
  }

  inserirCategoriasProdutos(idUsuario: string) {
    let sql = `SELECT count(*) as count
               FROM categorias
               WHERE tipo = 0`;
    return this.db.executeSql(sql, []).then(response => {
      let count = response.rows.item(0).count;
      if (count == 0) {
        let sql = 'INSERT INTO categorias(CategoriaDsc, userId, tipo, cor, isServidor, ativo) VALUES(?,?,?,?,?,?)';

        this.db.transaction(
          tx => {
            tx.executeSql(sql, ['Doces', idUsuario, TipoCategoria.produto, 'red', false, true]);
            tx.executeSql(sql, ['Eletrônicos', idUsuario, TipoCategoria.produto, 'orange', false, true]);
            tx.executeSql(sql, ['Ferramentas', idUsuario, TipoCategoria.produto, 'gold', false, true]);
            tx.executeSql(sql, ['Filmes', idUsuario, TipoCategoria.produto, 'gray', false, true]);
            tx.executeSql(sql, ['Fitness', idUsuario, TipoCategoria.produto, 'green', false, true]);
            tx.executeSql(sql, ['Flores', idUsuario, TipoCategoria.produto, 'lightgreen', false, true]);
            tx.executeSql(sql, ['Games', idUsuario, TipoCategoria.produto, 'darkblue', false, true]);
            tx.executeSql(sql, ['Perfumaria', idUsuario, TipoCategoria.produto, 'blue', false, true]);
            tx.executeSql(sql, ['Vestuário', idUsuario, TipoCategoria.produto, 'lightblue', false, true]);
          });
      }
    })
  }

  inserirCategoriasClientes(idUsuario: string) {
    let sql = `SELECT count(*) as count
               FROM categorias
               WHERE tipo = 1`;
    return this.db.executeSql(sql, []).then(response => {
      let count = response.rows.item(0).count;
      if (count == 0) {
        let sql = 'INSERT INTO categorias(CategoriaDsc, userId, tipo, cor, isServidor, ativo) VALUES(?,?,?,?,?,?)';

        this.db.transaction(
          tx => {
            tx.executeSql(sql, ['Comum', idUsuario, TipoCategoria.cliente, 'red', false, true]);
            tx.executeSql(sql, ['Especial', idUsuario, TipoCategoria.cliente, 'darkblue', false, true]);
          });
      }
    })
  }

  inserirCategoriasDownload(userId: string, lista: Array<any>) {

    let promises = [];
    lista.forEach(element => {
      let categoria: Categoria = element
      categoria.categoriaId = element.chave.id;
      categoria.usuario = new Usuario();
      categoria.usuario.idUsuario = element.chave.idUsuario;

      categoria.isServidor = true;

      switch (element.tipo) {
        case 'DESPESA_RECEITA':
          categoria.tipo = TipoCategoria.despesas_receitas;
          break;
        case 'PRODUTO':
          categoria.tipo = TipoCategoria.produto;
          break;
        case 'CLIENTE':
          categoria.tipo = TipoCategoria.cliente;
          break;
      }

      promises.push(this.incluirComId(categoria));
    });
    return Promise.all(promises);
  }

  incluirComId(categoria: Categoria) {
    let sql = 'INSERT INTO categorias(categoriaId,UserId, CategoriaDsc, tipo, cor, isServidor, ativo) VALUES(?,?,?,?,?,?,?)';

    return this.db.executeSql(sql, [
      categoria.categoriaId,
      categoria.usuario.idUsuario,
      categoria.categoriaDsc,
      categoria.tipo,
      categoria.cor,
      categoria.isServidor,
      categoria.ativo]);
  }

}
