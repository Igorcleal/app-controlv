import { HttpProvider } from './../http/http-provider';
import { Categoria } from '../../model/categoria';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { Usuario } from '../../model/usuario';
import { UtilsService } from './../utils-service';
import { CategoriaService } from './../categoria/categoria-service';
import { Constants } from '../../app/constants';

@Injectable()
export class CategoriaWsProvider {

  constructor(public http: HttpProvider, public categoriaService: CategoriaService, public utils: UtilsService) {
  }

  enviarCategoriasServidor() {

    return this.getCategoriasNaoServidor().then((categorias: Categoria[]) => {

      if (categorias == null || categorias.length == 0) {
        return Promise.resolve(null);
      }

      //envia categorias para WS
      return this.http.post(`${Constants.urlWs}categoria/salvarLst`, categorias).then(() => {
        //caso dê certo, altera no sqlite registro isServidor = true
        return this.categoriaService.alterarCategoriasGravadoServidor(categorias);
      });
    });

  }

  getCategoriasNaoServidor() {
    return this.utils.getUsuarioLogado().then((usuario: Usuario) => {

      return this.categoriaService.getCategoriasNaoServidor(usuario.idUsuario);
    });
  }

}
