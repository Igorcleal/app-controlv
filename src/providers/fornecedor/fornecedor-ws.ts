import { HttpProvider } from './../http/http-provider';
import { BtpFornecedor } from './../../model/fornecedor';
import { FornecedorService } from './fornecedor-service';
import {Usuario} from '../../model/usuario';
import { UtilsService } from './../utils-service';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Constants } from "../../app/constants";

@Injectable()
export class FornecedorWsProvider {

  constructor(public http: HttpProvider, public utils:UtilsService, public fornecedorService:FornecedorService) {
  }

  enviarFornecedoresServidor(){

    return this.getFornecedoresNaoServidor().then((fornecedores: BtpFornecedor[]) => {
      //envia contas para WS
      return this.http.post(`${Constants.urlWs}fornecedor/salvarLst`, fornecedores).then(()=>{
        //caso dê certo, altera no sqlite fornecedores para fornecedores gravadas no servidor
        return this.fornecedorService.alterarFornecedoresGravadoServidor(fornecedores);
      });
    });

  }

  getFornecedoresNaoServidor() {
    return this.utils.getUsuarioLogado().then((usuario:Usuario) => {
      
      return this.fornecedorService.getFornecedoresNaoServidor(usuario.idUsuario);
    });
  }

}
