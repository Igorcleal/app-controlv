import { Usuario } from '../../model/usuario';
import { BtpFornecedor } from './../../model/fornecedor';
import { DbProvider } from './../db-provider';
import { SQLite } from 'ionic-native';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class FornecedorService {

  public db: SQLite = null;
  constructor(public http: Http, public dbProvider: DbProvider) {
    this.db = dbProvider.db;
  }

  getAll(userId: number) {

    let sql = `SELECT *
               FROM fornecedores f
               WHERE f.userId = ?
               AND f.ativo = ?`;

    return this.db.executeSql(sql, [userId, true])
      .then(response => {
        let fornecedores = [];
        for (let index = 0; index < response.rows.length; index++) {
          let fornecedor: BtpFornecedor = response.rows.item(index);
          fornecedor.usuario = new Usuario();
          fornecedor.usuario.idUsuario = response.rows.item(index).userId;

          fornecedores.push(fornecedor);
        }
        console.log("executou consulta: getAll Fornecedores................");
        return Promise.resolve(fornecedores);
      }).catch((err)=>{
        console.log(err.message);
        return Promise.reject(true);
      });
  }

  getFornecedoresNaoServidor(userId: number) {
    let sql = `SELECT *
               FROM fornecedores f
               WHERE f.userId = ?
               AND f.isServidor = ?`;

    return this.db.executeSql(sql, [userId, false])
      .then(response => {
        let fornecedores = [];
        for (let index = 0; index < response.rows.length; index++) {
          let fornecedor: BtpFornecedor = response.rows.item(index);
          fornecedor.usuario = new Usuario();
          fornecedor.usuario.idUsuario = response.rows.item(index).userId;
          fornecedores.push(fornecedor);
        }
        console.log("executou consulta: getAll Fornecedores................");
        return Promise.resolve(fornecedores);
      });
  }

  incluir(fornecedor: BtpFornecedor) {
    let sql = `INSERT INTO fornecedores( 
               userId ,
               nomeFornecedor , 
               endereco ,
               cnpj ,
               telefone,
               isServidor,
               ativo )    
    VALUES(?,?,?,?,?,?,?)`;

    fornecedor.ativo = true;

    return this.db.executeSql(sql, [
      fornecedor.usuario.idUsuario,
      fornecedor.nomeFornecedor,
      fornecedor.endereco,
      fornecedor.cnpj,
      fornecedor.telefone,
      fornecedor.isServidor,
      fornecedor.ativo]).catch(err => {
        console.log(err.message);
        return Promise.reject(true);
      });
  }

  alterar(fornecedor: BtpFornecedor) {
    let sql = `UPDATE fornecedores 
           SET nomeFornecedor=? , 
               endereco=? ,
               cnpj=? ,
               telefone=?,
               isServidor=?,
               ativo=?
    WHERE fornId=?`;

    return this.db.executeSql(sql, [fornecedor.nomeFornecedor,
    fornecedor.endereco,
    fornecedor.cnpj,
    fornecedor.telefone,
    fornecedor.isServidor,
    fornecedor.ativo,
    fornecedor.fornId]).catch(err => {
      console.log(err.message);
      return Promise.reject(true);
    });
  }

  deletar(fornecedor: BtpFornecedor) {
    let sql = 'DELETE FROM fornecedores WHERE fornId=?';
    return this.db.executeSql(sql, [fornecedor.fornId]);
  }

  alterarFornecedoresGravadoServidor(fornecedores: BtpFornecedor[]) {

    let promises = [];

    fornecedores.forEach(fornecedor => {
      fornecedor.isServidor = true;
      promises.push(this.alterar(fornecedor));
    });

    return Promise.all(promises);
  }

  inserirFornecedoresDownload(lista: Array<any>) {

    let promises = [];
    lista.forEach(element => {
      let fornecedor: BtpFornecedor = element;
      fornecedor.fornId = element.chave.id;
      fornecedor.usuario = new Usuario();
      fornecedor.usuario.idUsuario = element.chave.idUsuario;

      fornecedor.isServidor = true;
      promises.push(this.incluirComId(fornecedor));
    });
    return Promise.all(promises);
  }

  incluirComId(fornecedor: BtpFornecedor) {
    let sql = `INSERT INTO fornecedores(
               fornId, 
               userId ,
               nomeFornecedor , 
               endereco ,
               cnpj ,
               telefone,
               isServidor,
               ativo )    
    VALUES(?,?,?,?,?,?,?,?)`;

    return this.db.executeSql(sql, [
      fornecedor.fornId,
      fornecedor.usuario.idUsuario,
      fornecedor.nomeFornecedor,
      fornecedor.endereco,
      fornecedor.cnpj,
      fornecedor.telefone,
      fornecedor.isServidor,
      fornecedor.ativo]).catch(err => {
        console.log(err.message);
        return Promise.reject(true);
      });
  }

}
