import { Facebook } from '@ionic-native/facebook';
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';
import { Injectable } from '@angular/core';
import { LoadingController } from 'ionic-angular';
import { GooglePlus } from '@ionic-native/google-plus';
import * as firebase from 'firebase';

import { Usuario } from "../model/usuario";
import { UsuarioWsProvider } from "./usuario/usuario-ws";

@Injectable()
export class AuthFirebaseService {

    constructor(public loadingCtrl: LoadingController, public angfire: AngularFire,
        public usuarioWs: UsuarioWsProvider, public facebook: Facebook, public google: GooglePlus) {

    }

    cadastrarUsuario(usuario: Usuario) {
        //cria user no firebase
        return firebase.auth().createUserWithEmailAndPassword(usuario.email, usuario.senha).then((response) => {
            console.log('user criado com sucesso no firebase');
            console.log(firebase.auth().currentUser)

            usuario.idUsuario = response.uid;

            //cria user no WS
            return this.usuarioWs.cadastrarUsuario(usuario).then(() => {
                response.sendEmailVerification();
                return Promise.resolve(response);
            })
                .catch(err => {
                    //caso nao consiga criar no ws remove do firebase
                    firebase.auth().currentUser.delete();
                    return Promise.reject(err);
                });

        }).catch((err) => {
            console.error(err.message);
            err.message = "Problema ao se conectar com servidor, favor tentar em instantes!";
            return Promise.reject(err);
        });
    }

    login(email: string, password: string) {
        return this.angfire.auth.login({
            email: email,
            password: password
        },
            {
                provider: AuthProviders.Password,
                method: AuthMethods.Password
            });
    }

    resetPasword(email: string) {
        return firebase.auth().sendPasswordResetEmail(email).then(() => {
            console.log('success enviar e-mail');
            return Promise.resolve(true);
        }).catch(err => {
            console.log(err.message);
            return Promise.reject(err);
        });
    }

    facebookLogin(): Promise<any> {
        return this.facebook.login(['email'])
            .then(response => {
                const facebookCredential = firebase.auth.FacebookAuthProvider
                    .credential(response.authResponse.accessToken);

                return firebase.auth().signInWithCredential(facebookCredential)
                    .then(success => {
                        console.log("Firebase success: " + JSON.stringify(success));
                        return Promise.resolve(success);
                    });

            });
    }

    googleLogin(): Promise<any> {
        return this.google.login({
            'webClientId': '997374192032-mrns461ek3k2mv6itb2csrf48dfpbr92.apps.googleusercontent.com',
            'offline': true
        })
            .then(response => {
                console.log('passou');
                const googleCredential = firebase.auth.GoogleAuthProvider
                    .credential(response.idToken);

                return firebase.auth().signInWithCredential(googleCredential)
                    .then(success => {
                        console.log("Firebase success: " + JSON.stringify(success));
                        return Promise.resolve(success);
                    });

            });
    }

    logout() {
        return firebase.auth().signOut();
    }

}