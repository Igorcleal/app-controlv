import {Usuario} from '../model/usuario';
import { Injectable } from '@angular/core';
import {ViewController, LoadingController} from 'ionic-angular';
import { Storage } from '@ionic/storage';


@Injectable()
export class UtilsService {

    loading: any;

    constructor(public loadingCtrl: LoadingController, public storage: Storage) {

    }

    getUsuarioLogado() {
        return this.storage.get('idUsuarioLogado').then((idUsuario) => {
            let usuario = new Usuario();
            usuario.idUsuario = idUsuario;
            return Promise.resolve(usuario);
        });
    }

    showLoading() {
        this.loading = this.loadingCtrl.create({
            content: "Aguarde..."
        });
        this.loading.present();
    }

    closeLoading() {
        this.loading.dismiss();
    }

    voltarParaListagem(viewCtrl: ViewController) {
        viewCtrl.dismiss();
        this.closeLoading();
      }

}