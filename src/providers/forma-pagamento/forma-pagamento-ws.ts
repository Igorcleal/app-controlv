import { HttpProvider } from './../http/http-provider';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { UtilsService } from './../utils-service';
import { Usuario } from '../../model/usuario';
import { Constants } from "../../app/constants";
import { FormaPagamentoService } from "./forma-pagamento-service";
import { BtpFormaPagamento } from "../../model/btpFormaPagamento";

@Injectable()
export class FormaPagamentoWsProvider {

  constructor(public http: HttpProvider, public utils: UtilsService, public formaPagamentoService: FormaPagamentoService) {
  }

  enviarFormasPagamentosServidor() {

    return this.getFormasPagamentosNaoServidor().then((formasPagamentos: BtpFormaPagamento[]) => {
      
      if (formasPagamentos == null || formasPagamentos.length == 0) {
        return Promise.resolve(null);
      }
      
      //envia contas para WS
      return this.http.post(`${Constants.urlWs}formaPagamento/salvarLst`, formasPagamentos).then(() => {
        //caso dê certo, altera no sqlite contas para contas gravadas no servidor
        return this.formaPagamentoService.alterarFormasPagamentosGravadoServidor(formasPagamentos);
      });
    });

  }

  getFormasPagamentosNaoServidor() {
    return this.utils.getUsuarioLogado().then((usuario: Usuario) => {

      return this.formaPagamentoService.getFormasPagamentoNaoServidor(usuario.idUsuario);
    });
  }

}
