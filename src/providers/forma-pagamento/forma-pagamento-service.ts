import { SQLite } from 'ionic-native';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { DbProvider } from './../db-provider';
import { BtpFormaPagamento } from './../../model/btpFormaPagamento';
import { Usuario } from "../../model/usuario";

@Injectable()
export class FormaPagamentoService {

  public db: SQLite = null;

  constructor(public http: Http, public dbProvider: DbProvider) {
    this.db = dbProvider.db;
  }

  getAll(userId: number) {
    let sql = `SELECT * 
                FROM formas_pagamento fp
                WHERE fp.userId = ?
                AND fp.ativo = ? `;
    return this.db.executeSql(sql, [userId, true])
      .then(response => {
        let formasPagamento = [];
        for (let index = 0; index < response.rows.length; index++) {

          let formaPagamento: BtpFormaPagamento = response.rows.item(index);

          let usuario: Usuario = new Usuario();
          usuario.idUsuario = response.rows.item(index).userId;

          formaPagamento.usuario = usuario;

          formasPagamento.push(formaPagamento);

        }
        return Promise.resolve(formasPagamento);
      });
  }

  getFormasPagamentoNaoServidor(userId: number) {
    let sql = `SELECT * 
                FROM formas_pagamento fp
                WHERE fp.userId = ? 
                AND fp.isServidor = ?`;
    return this.db.executeSql(sql, [userId, false])
      .then(response => {
        let formasPagamento = [];
        for (let index = 0; index < response.rows.length; index++) {

          let formaPagamento: BtpFormaPagamento = response.rows.item(index);

          let usuario: Usuario = new Usuario();
          usuario.idUsuario = response.rows.item(index).userId;

          formaPagamento.usuario = usuario;

          formasPagamento.push(formaPagamento);

        }
        return Promise.resolve(formasPagamento);
      });
  }

  getFormaPagamento(idFormaPagamento: number) {
    let sql = `SELECT * 
                FROM formas_pagamento
                WHERE idFormaPagamento = ?`;
    return this.db.executeSql(sql, [idFormaPagamento])
      .then(response => {
        let formaPagamento;
        formaPagamento = response.rows.item(0);
        return Promise.resolve(formaPagamento);
      });
  }

  incluir(btpFormaPagamento: BtpFormaPagamento) {
    let sql = `INSERT INTO formas_pagamento(userId, descricao, isServidor, ativo) 
                VALUES(?,?,?,?)`;

    btpFormaPagamento.ativo = true;

    return this.db.executeSql(sql, [btpFormaPagamento.usuario.idUsuario,
    btpFormaPagamento.descricao,
    btpFormaPagamento.isServidor,
    btpFormaPagamento.ativo]);
  }

  delete(btpFormaPagamento: BtpFormaPagamento) {
    let sql = 'DELETE FROM formas_pagamento WHERE idFormaPagamento=?';
    return this.db.executeSql(sql, [btpFormaPagamento.idFormaPagamento]);
  }

  update(btpFormaPagamento: BtpFormaPagamento) {
    let sql = `UPDATE formas_pagamento 
                SET descricao=? , isServidor=?, ativo=?
                WHERE idFormaPagamento=?`;
    return this.db.executeSql(sql, [btpFormaPagamento.descricao,
    btpFormaPagamento.isServidor,
    btpFormaPagamento.ativo,
    btpFormaPagamento.idFormaPagamento]);
  }

  alterarFormasPagamentosGravadoServidor(formasPagamentos: BtpFormaPagamento[]) {

    let promises = [];

    formasPagamentos.forEach(registro => {
      registro.isServidor = true;
      promises.push(this.update(registro));
    });

    return Promise.all(promises);
  }

  inserirFormasPagamentosBasicas(userId: number) {
    let sql = `SELECT count(*) as count
               FROM formas_pagamento`;
    return this.db.executeSql(sql, []).then(response => {
      let count = response.rows.item(0).count;
      console.log('-------------------------')
      console.log('contador:' + count);
      if (count == 0) {
        let sql = 'INSERT INTO formas_pagamento(descricao, userId, isServidor, ativo) VALUES(?,?,?,?)';

        this.db.transaction(
          tx => {
            tx.executeSql(sql, ['Dinheiro', userId, false, true]);
            tx.executeSql(sql, ['Cheque', userId, false, true]);
            tx.executeSql(sql, ['Cartão de Crédito/Débito', userId, false, true]);
            tx.executeSql(sql, ['Recibo', userId, false, true]);
          });
      }
    });
  }

  inserirFormaPagamentoDownload(lista: Array<any>) {

    let promises = [];
    lista.forEach(element => {
      let formaPagamento: BtpFormaPagamento = element;
      formaPagamento.idFormaPagamento = element.chave.id;
      formaPagamento.usuario = new Usuario();
      formaPagamento.usuario.idUsuario = element.chave.idUsuario;

      formaPagamento.isServidor = true;
      promises.push(this.incluirComId(formaPagamento));
    });
    return Promise.all(promises);
  }

  incluirComId(btpFormaPagamento: BtpFormaPagamento) {
    let sql = `INSERT INTO formas_pagamento(idFormaPagamento, userId, descricao, isServidor, ativo) 
                VALUES(?,?,?,?,?)`;

    return this.db.executeSql(sql, [
      btpFormaPagamento.idFormaPagamento,
      btpFormaPagamento.usuario.idUsuario,
      btpFormaPagamento.descricao,
      btpFormaPagamento.isServidor,
      btpFormaPagamento.ativo]);
  }

}

