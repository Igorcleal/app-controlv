import { Usuario } from './../../model/usuario';
import { SQLite } from 'ionic-native';
import { Injectable } from '@angular/core';

import { BtpVisita } from './../../model/btpVisita';
import { DbProvider } from './../db-provider';
import { BtpCliente } from "../../model/cliente";

@Injectable()
export class VisitaService {

  public db: SQLite = null;

  constructor(public dbProvider: DbProvider) {
    this.db = dbProvider.db;
  }

  getAll(userId: number) {
    let sql = `SELECT v.*, c.nome, c.cliId
                FROM visitas v
                INNER JOIN clientes c
                on c.cliId = v.cliId
                WHERE v.userId = ?
                AND v.ativo = ?
                ORDER BY v.dataVisita desc`;

    return this.db.executeSql(sql, [userId, true])
      .then(response => {
        let visitas = [];

        for (let index = 0; index < response.rows.length; index++) {
          let btpVisita: BtpVisita = response.rows.item(index);

          let usuario: Usuario = new Usuario();
          usuario.idUsuario = response.rows.item(index).userId
          btpVisita.btpUsuario = usuario;

          let btpCliente: BtpCliente = new BtpCliente();
          btpCliente.cliId = response.rows.item(index).cliId;
          btpCliente.nome = response.rows.item(index).nome;
          btpCliente.usuario = usuario;

          btpVisita.btpCliente = btpCliente;

          visitas.push(btpVisita);
        }
        console.log("getAll Visitas.......")
        return Promise.resolve(visitas);
      })
      .catch(e => {
        console.log(e.msg);
        return null;
      });
  }

  getVisitasNaoServidor(userId: number) {
    let sql = `SELECT v.*, c.nome, c.cliId
                FROM visitas v
                INNER JOIN clientes c
                on c.cliId = v.cliId
                WHERE v.userId = ?
                and v.isServidor = ?
                ORDER BY v.dataVisita desc`;

    return this.db.executeSql(sql, [userId, false])
      .then(response => {
        let visitas = [];

        for (let index = 0; index < response.rows.length; index++) {
          let btpVisita: BtpVisita = response.rows.item(index);
          btpVisita.dataVisitaString = response.rows.item(index).dataVisita;

          let usuario: Usuario = new Usuario();
          usuario.idUsuario = response.rows.item(index).userId
          btpVisita.btpUsuario = usuario;

          let btpCliente: BtpCliente = new BtpCliente();
          btpCliente.cliId = response.rows.item(index).cliId;
          btpCliente.nome = response.rows.item(index).nome;
          btpCliente.usuario = usuario;

          btpVisita.btpCliente = btpCliente;

          visitas.push(btpVisita);
        }
        console.log("getAll Visitas.......")
        return Promise.resolve(visitas);
      })
      .catch(e => {
        console.log(e.msg);
        return null;
      });
  }

  getVisitasPorData(userId: number, data: Date) {
    let sql = `SELECT v.*, c.nome, c.cliId
                FROM visitas v
                INNER JOIN clientes c
                on c.cliId = v.cliId
                WHERE v.userId = ? 
                AND v.ativo = ?
                ORDER BY v.dataVisita desc`;

    return this.db.executeSql(sql, [userId, true/*, this.dbProvider.meses[data.getMonth()] + "" + data.getFullYear()*/])
      .then(response => {
        let visitas = [];

        for (let index = 0; index < response.rows.length; index++) {
          let btpVisita: BtpVisita = response.rows.item(index);
          btpVisita.dataVisitaString = response.rows.item(index).dataVisita;

          let usuario: Usuario = new Usuario();
          usuario.idUsuario = response.rows.item(index).userId
          btpVisita.btpUsuario = usuario;

          let btpCliente: BtpCliente = new BtpCliente();
          btpCliente.cliId = response.rows.item(index).cliId;
          btpCliente.nome = response.rows.item(index).nome;
          btpCliente.usuario = usuario;
          btpVisita.btpCliente = btpCliente;
          visitas.push(btpVisita);

          console.log("data:" + response.rows.item(index).dataVisita);
        }
        console.log("getAll Visitas.......")
        return Promise.resolve(visitas);
      })
      .catch(e => {
        console.log(e.msg);
        return null;
      });
  }

  incluir(btpVisita: BtpVisita) {
    let sql = `INSERT INTO visitas(userId,
               cliId,
               localVisita,
               dataVisita,
               horaVisita,
               isServidor,
               ativo)
                VALUES(?,?,?,?,?,?,?)`;

    btpVisita.ativo = true;

    console.log("Inserindo Visita");
    return this.db.executeSql(sql,
      [btpVisita.btpUsuario.idUsuario,
      btpVisita.btpCliente.cliId,
      btpVisita.localVisita,
      btpVisita.dataVisitaString,
      btpVisita.horaVisita,
      btpVisita.isServidor,
      btpVisita.ativo]
    );
  }

  delete(btpVisita: BtpVisita) {
    let sql = 'DELETE FROM visitas WHERE idVisita=?';
    return this.db.executeSql(sql, [btpVisita.idVisita]);
  }

  update(btpVisita: BtpVisita) {

    let sql = `UPDATE visitas SET
               cliId =?,
               localVisita =?,
               dataVisita =?,
               horaVisita = ?,
               isServidor = ?,
               ativo = ?
               WHERE idVisita =?`;

    return this.db.executeSql(sql, [btpVisita.btpCliente.cliId, btpVisita.localVisita,
    btpVisita.dataVisitaString, btpVisita.horaVisita,
    btpVisita.isServidor, btpVisita.ativo, btpVisita.idVisita]);
  }

  alterarVisitasGravadoServidor(visitas: BtpVisita[]) {

    let promises = [];

    visitas.forEach(registro => {
      registro.isServidor = true;
      promises.push(this.update(registro));
    });

    return Promise.all(promises);
  }

  inserirVisitasDownload(lista: Array<any>) {

    let promises = [];
    lista.forEach(element => {
      let visita: BtpVisita = element;
      visita.idVisita = element.chave.id;
      visita.btpUsuario = new Usuario();
      visita.btpUsuario.idUsuario = element.chave.idUsuario;

      visita.btpCliente = new BtpCliente();
      visita.btpCliente.cliId = element.cliId;

      visita.isServidor = true;
      promises.push(this.incluirComId(visita));
    });
    return Promise.all(promises);
  }

  incluirComId(btpVisita: BtpVisita) {
    let sql = `INSERT INTO visitas(
               idVisita,
               userId,
               cliId,
               localVisita,
               dataVisita,
               horaVisita,
               isServidor,
               ativo)
                VALUES(?,?,?,?,?,?,?,?)`;

    return this.db.executeSql(sql,
      [btpVisita.idVisita,
      btpVisita.btpUsuario.idUsuario,
      btpVisita.btpCliente.cliId,
      btpVisita.localVisita,
      btpVisita.dataVisita,
      btpVisita.horaVisita,
      btpVisita.isServidor,
      btpVisita.ativo]
    );
  }

}
