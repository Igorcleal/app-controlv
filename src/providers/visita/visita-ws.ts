import { HttpProvider } from './../http/http-provider';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { UtilsService } from './../utils-service';
import { Usuario } from '../../model/usuario';
import { Constants } from "../../app/constants";
import { VisitaService } from "./visita-service";
import { BtpVisita } from "../../model/btpVisita";

@Injectable()
export class VisitaWsProvider {

  constructor(public http: HttpProvider, public utils: UtilsService, public visitaService: VisitaService) {
  }

  enviarVisitasServidor() {
    return this.getVisitasNaoServidor().then((visitas: BtpVisita[]) => {
      //envia contas para WS
      return this.http.post(`${Constants.urlWs}visita/salvarLst`, visitas).then(() => {
        //caso dê certo, altera no sqlite registros gravado no servidor
        return this.visitaService.alterarVisitasGravadoServidor(visitas);
      });
    });
  }

  enviarVisitasServidorSemDependencias() {
    return this.getVisitasNaoServidor().then((visitas: BtpVisita[]) => {
      //envia contas para WS
      return this.http.post(`${Constants.urlWs}visita/salvarLst`, visitas).then(() => {
        //caso dê certo, altera no sqlite registros gravado no servidor
        return this.visitaService.alterarVisitasGravadoServidor(visitas);
      });
    });
  }

  getVisitasNaoServidor() {
    return this.utils.getUsuarioLogado().then((usuario: Usuario) => {
      return this.visitaService.getVisitasNaoServidor(usuario.idUsuario);
    });
  }

}
