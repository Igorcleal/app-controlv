import { HttpProvider } from './../http/http-provider';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { Usuario } from '../../model/usuario';
import { UtilsService } from './../utils-service';
import { Constants } from "../../app/constants";
import { BtpEstoque } from '../../model/btpEstoque';
import { BtpItemEstoque } from '../../model/btpItemEstoque';
import { EstoqueService } from './estoque-service';

@Injectable()
export class EstoqueWsProvider {

  private usuario: Usuario;

  constructor(public http: HttpProvider,
    public utils: UtilsService,
    public estoqueService: EstoqueService) {
  }

  enviarEstoqueServidorSemDependencias() {

    return this.getEstoquesNaoServidor().then((estoques: BtpEstoque[]) => {
      if (estoques == null || estoques.length == 0) {
        return Promise.resolve(null);
      }
      let promises = [];

      estoques.forEach(estoque => {
        promises.push(this.estoqueService.getItensEstoqueTodos(estoque.idEstoque, this.usuario.idUsuario).then((itensEstoque: Array<BtpItemEstoque>) => {
          estoque.lstItemEstoque = itensEstoque;
          console.log('testando lst item estoque');
          console.log(itensEstoque);
        }));
      });

      return Promise.all(promises).then(() => {
        //envia estoque para WS
        return this.http.post(`${Constants.urlWs}estoque/salvarLst`, estoques).then(() => {
          //caso dê certo, altera no sqlite estoques para estoques gravadas no servidor
          return this.estoqueService.alterarEstoqueGravadoServidor(estoques);
        });
      });
    });
  }

  getEstoquesNaoServidor() {
    return this.utils.getUsuarioLogado().then((usuario: Usuario) => {
      this.usuario = usuario;
      return this.estoqueService.getEstoquesNaoServidor(usuario.idUsuario);
    });
  }

}
