import { SQLite } from 'ionic-native';
import { DbProvider } from '../db-provider';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { BtpEstoque } from '../../model/btpEstoque';
import { Usuario } from '../../model/usuario';
import { BtpItemEstoque } from '../../model/btpItemEstoque';
import { Produto } from '../../model/produto';


@Injectable()
export class EstoqueService {

  public db: SQLite = null;

  constructor(public http: Http, public dbProvider: DbProvider) {
    this.db = dbProvider.db;
  }

  getAll(userId: number) {
    let sql = `SELECT e.*
                FROM estoques e
                WHERE e.userId = ?
                AND e.ativo = ?`;

    return this.db.executeSql(sql, [userId, true])
      .then(response => {

        let lstEstoques = [];
        let btpEstoque: BtpEstoque;
        for (let index = 0; index < response.rows.length; index++) {
          console.log(response.rows.item(index).cliId);
          btpEstoque = response.rows.item(index);

          btpEstoque.usuario = new Usuario();
          btpEstoque.usuario.idUsuario = response.rows.item(index).userId;

          lstEstoques.push(btpEstoque);
        }
        console.log("getAll Estoques.......")
        console.log(lstEstoques);
        return Promise.resolve(lstEstoques);
      })
      .catch(e => {
        console.log(e.msg);
        return null;
      });
  }

  getEstoquesNaoServidor(userId: number) {
    let sql = `SELECT e.*
                FROM estoques e
                WHERE e.userId = ?
                AND e.isServidor = ?`;

    return this.db.executeSql(sql, [userId, false])
      .then(response => {

        let lstEstoques = [];
        let btpEstoque: BtpEstoque;
        for (let index = 0; index < response.rows.length; index++) {
          console.log(response.rows.item(index).cliId);
          btpEstoque = response.rows.item(index);

          btpEstoque.usuario = new Usuario();
          btpEstoque.usuario.idUsuario = response.rows.item(index).userId;

          lstEstoques.push(btpEstoque);
        }
        console.log("getAll Estoques.......")
        return Promise.resolve(lstEstoques);
      })
      .catch(e => {
        console.log(e.msg);
        return null;
      });
  }

  incluirEstoque(btpEstoque: BtpEstoque) {
    let sql = `INSERT INTO estoques(dscEstoque,
               userId,
               isServidor,
               ativo )
                VALUES(?,?,?,?)`;
    console.log("Inserindo Estoque");
    btpEstoque.ativo = true;

    return this.db.transaction(
      tx => {
        tx.executeSql(sql, [btpEstoque.dscEstoque, btpEstoque.usuario.idUsuario, btpEstoque.isServidor,
        btpEstoque.ativo], (tx, resultSet) => {
          btpEstoque.idEstoque = resultSet.insertId;
          return Promise.resolve(true);
        }, function (tx, error) {
          console.log('INSERT Estoque error: ' + error.message);
          return Promise.reject(true);
        });
      });

  }

  alterarEstoque(btpEstoque: BtpEstoque) {
    let sql = `UPDATE estoques 
           SET dscEstoque=?,
           isServidor=?,
           ativo=?
    WHERE idEstoque=?`;
    return this.db.executeSql(sql, [btpEstoque.dscEstoque, btpEstoque.isServidor, btpEstoque.ativo,
    btpEstoque.idEstoque]);
  }

  deletarEstoque(idEstoque: number) {
    let sql = 'DELETE FROM estoques WHERE idEstoque=?';
    return this.db.executeSql(sql, [idEstoque]);
  }

  incluirItemEstoque(btpItemEstoque: BtpItemEstoque) {
    let sql = `INSERT INTO itens_estoque(userId,
               idEstoque,
               produtoId,
               qtdEstoque,
               ativo)
                VALUES(?,?,?,?,?)`;

    console.log("Inserindo Item do estoque");
    console.log(btpItemEstoque);

    btpItemEstoque.ativo = true;

    return this.db.executeSql(sql, [btpItemEstoque.usuario.idUsuario,
    btpItemEstoque.btpEstoque.idEstoque, btpItemEstoque.produto.produtoId,
    btpItemEstoque.qtdEstoque, btpItemEstoque.ativo]);
  }

  alterarItemEstoque(btpItemEstoque: BtpItemEstoque) {
    let sql = `UPDATE itens_estoque 
               SET  qtdEstoque = ?,
               ativo = ?
                WHERE itemEstoqueId=?`;
    console.log("qtd" + btpItemEstoque.qtdEstoque);
    console.log("id" + btpItemEstoque.idItemEstoque);
    console.log("Alterando Item Estoque");
    return this.db.executeSql(sql, [btpItemEstoque.qtdEstoque, btpItemEstoque.ativo, btpItemEstoque.idItemEstoque]);
  }

  alterarEstoqueIsServidor(idEstoque: number, isServidor: boolean) {
    let sql = `UPDATE estoques
               SET  isServidor = ?
              WHERE idEstoque = ?`;
    return this.db.executeSql(sql, [isServidor, idEstoque]);
  }

  deletarItemEstoque(itemEstoqueId: number) {
    let sql = 'DELETE FROM itens_estoque WHERE itemEstoqueId=?';
    console.log("" + itemEstoqueId);
    return this.db.executeSql(sql, [itemEstoqueId]);
  }

  getItensEstoqueTodos(idEstoque: number, idUsuario: number) {
    let sql = `SELECT ie.*, p.produtoId, p.produtoDsc
                FROM itens_estoque ie
                INNER JOIN produtos p
                on p.produtoId = ie.produtoId
                WHERE ie.idEstoque = ?
                AND ie.userId = ?`;


    return this.db.executeSql(sql, [idEstoque, idUsuario])
      .then(response => {

        let itensEstoque = [];
        let btpItemEstoque: BtpItemEstoque;
        let produto: Produto;

        for (let index = 0; index < response.rows.length; index++) {
          console.log(response.rows.item(index).itemEstoqueId);
          btpItemEstoque = response.rows.item(index);
          btpItemEstoque.qtdEstoque = response.rows.item(index).qtdEstoque;
          btpItemEstoque.idItemEstoque = response.rows.item(index).itemEstoqueId;

          produto = new Produto();
          produto.produtoId = response.rows.item(index).produtoId;
          produto.produtoDsc = response.rows.item(index).produtoDsc;
          btpItemEstoque.produto = produto;

          btpItemEstoque.usuario = new Usuario();
          btpItemEstoque.usuario.idUsuario = response.rows.item(index).userId;

          itensEstoque.push(btpItemEstoque);
        }

        for (let index = 0; index < itensEstoque.length; index++) {
          console.log(itensEstoque[index].idItemEstoque);
        }

        console.log("getAll Itens Estoque.......");
        return Promise.resolve(itensEstoque);
      })
      .catch(e => {
        console.log("Erro recuperar itens do estoque: " + e.msg);
        return null;
      });
  }

  getItensEstoque(idEstoque: number, idUsuario: number) {
    let sql = `SELECT ie.*, p.produtoId, p.produtoDsc
                FROM itens_estoque ie
                INNER JOIN produtos p
                on p.produtoId = ie.produtoId
                WHERE ie.idEstoque = ?
                AND ie.userId = ?
                AND ie.ativo = ? `;

    console.log('idEstoque:' + idEstoque);
    console.log('idUsuario:' + idUsuario);

    return this.db.executeSql(sql, [idEstoque, idUsuario, true])
      .then(response => {

        let itensEstoque = [];
        let btpItemEstoque: BtpItemEstoque;
        let produto: Produto;

        for (let index = 0; index < response.rows.length; index++) {
          console.log(response.rows.item(index).itemEstoqueId);
          btpItemEstoque = response.rows.item(index);
          btpItemEstoque.qtdEstoque = response.rows.item(index).qtdEstoque;
          btpItemEstoque.idItemEstoque = response.rows.item(index).itemEstoqueId;

          produto = new Produto();
          produto.produtoId = response.rows.item(index).produtoId;
          produto.produtoDsc = response.rows.item(index).produtoDsc;
          btpItemEstoque.produto = produto;

          btpItemEstoque.usuario = new Usuario();
          btpItemEstoque.usuario.idUsuario = response.rows.item(index).userId;

          itensEstoque.push(btpItemEstoque);
        }

        for (let index = 0; index < itensEstoque.length; index++) {
          console.log(itensEstoque[index].idItemEstoque);
        }

        console.log("getAll Itens Estoque.......");
        console.log(itensEstoque);
        return Promise.resolve(itensEstoque);
      })
      .catch(e => {
        console.log("Erro recuperar itens do estoque: " + e.msg);
        return null;
      });
  }

  consultarEstoquePorProduto(produtoId: number) {
    let sql = `SELECT e.*,
                ie.qtdEstoque as qtdEstoque
                FROM estoques e
                INNER JOIN itens_estoque ie
                ON e.idEstoque = ie.idEstoque
                INNER JOIN produtos p
                on p.produtoId = ie.produtoId
                WHERE p.produtoId = ? 
                AND e.ativo = ?
                AND ie.ativo = ?`;

    return this.db.executeSql(sql, [produtoId, true, true])
      .then(response => {

        let lstEstoques = new Array<BtpEstoque>();
        let btpEstoque: BtpEstoque;
        for (let index = 0; index < response.rows.length; index++) {
          console.log(response.rows.item(index).cliId);
          btpEstoque = response.rows.item(index);
          lstEstoques.push(btpEstoque);
        }
        console.log(response.rows.length)
        console.log("getAll Estoques.......")
        return Promise.resolve(lstEstoques);
      })
      .catch(e => {
        console.log("Erro ao consultar os estoques por produto: " + e.msg);
        return null;
      });
  }


  consultarItemEstoquePorCodProdutoCodEstoque(idEstoque: number, produtoId: number) {
    let sql = `SELECT ie.*
                FROM itens_estoque ie
                INNER JOIN produtos p
                on p.produtoId = ie.produtoId
                WHERE p.produtoId = ?
                AND ie.idEstoque = ? `;

    console.log("consultado estoque " + idEstoque);
    console.log("consultado produto " + produtoId);

    return this.db.executeSql(sql, [produtoId, idEstoque])
      .then(response => {

        let lstBtpItemEstoque = new Array<BtpItemEstoque>();
        let btpItemEstoque: BtpItemEstoque;
        let produto: Produto;
        for (let index = 0; index < response.rows.length; index++) {

          btpItemEstoque = response.rows.item(index);
          btpItemEstoque.qtdEstoque = response.rows.item(index).qtdEstoque;
          btpItemEstoque.idItemEstoque = response.rows.item(index).itemEstoqueId;

          produto = new Produto();
          produto.produtoId = response.rows.item(index).produtoId;
          produto.produtoDsc = response.rows.item(index).produtoDsc;
          btpItemEstoque.produto = produto;

          lstBtpItemEstoque.push(btpItemEstoque);
        }
        console.log(response.rows.length);
        console.log("getAll itens de estoque.......");
        return Promise.resolve(lstBtpItemEstoque);
      })
      .catch(e => {
        console.log("Erro ao consultar itens de estoque por produto: " + e.msg);
        return null;
      });
  }

  removerQtdItemEstoque(produtoId: number, idEstoque: number, qtdEstoque: number) {

    this.consultarItemEstoquePorCodProdutoCodEstoque(idEstoque, produtoId).then(lstBtpItemEstoque => {

      if (lstBtpItemEstoque != null && lstBtpItemEstoque.length > 0) {
        lstBtpItemEstoque.forEach(element => {

          if (element.qtdEstoque != null) {
            let qtdNovoEstoque: number = 0;
            let idItemEstoque: number = 0;

            qtdNovoEstoque = element.qtdEstoque - qtdEstoque;
            idItemEstoque = element.idItemEstoque;

            console.log("id item:" + idItemEstoque);
            console.log("qtd novo estoque:" + qtdNovoEstoque);

            this.atualizarQtdItemEstoque(idItemEstoque, qtdNovoEstoque);
          }
        });
      }

    });
  }

  adicionarQtdItemEstoque(produtoId: number, idEstoque: number, qtdEstoque: number) {

    this.consultarItemEstoquePorCodProdutoCodEstoque(idEstoque, produtoId).then(lstBtpItemEstoque => {

      if (lstBtpItemEstoque != null && lstBtpItemEstoque.length > 0) {
        lstBtpItemEstoque.forEach(element => {

          if (element.qtdEstoque != null) {
            let qtdNovoEstoque: number = 0;
            let idItemEstoque: number = 0;

            qtdNovoEstoque = element.qtdEstoque + qtdEstoque;
            idItemEstoque = element.idItemEstoque;

            console.log("id item:" + idItemEstoque);
            console.log("qtd novo estoque:" + qtdNovoEstoque);

            this.atualizarQtdItemEstoque(idItemEstoque, qtdNovoEstoque);
          }
        });
      }

    });
  }

  atualizarQtdItemEstoque(idItemEstoque: number, qtdEstoque: number) {
    let sql = `UPDATE itens_estoque 
                SET  qtdEstoque = ?
               WHERE itemEstoqueId=?`;
    console.log("qtd" + qtdEstoque);
    console.log("id" + idItemEstoque);
    console.log("Alterando Item Estoque");
    return this.db.executeSql(sql, [qtdEstoque, idItemEstoque]);
  }

  alterarEstoqueGravadoServidor(estoques: BtpEstoque[]) {
    let promises = [];

    estoques.forEach(estoque => {
      estoque.isServidor = true;
      promises.push(this.alterarEstoque(estoque));
    });

    return Promise.all(promises);
  }

  inserirEstoquesDownload(lista: Array<any>) {

    let promises = [];
    lista.forEach(element => {
      promises.push(this.inserirEstoque(element));
    });
    return Promise.all(promises);
  }

  private inserirEstoque(element) {
    let estoque: BtpEstoque = element;
    estoque.idEstoque = element.chave.id;
    estoque.usuario = new Usuario();
    estoque.usuario.idUsuario = element.chave.idUsuario;
    estoque.isServidor = true;

    estoque.lstItemEstoque = element.lstItemEstoque;

    let promises = [];

    promises.push(this.incluirEstoqueComId(estoque));

    estoque.lstItemEstoque.forEach(itemEstoqueAux => {

      promises.push(this.inserirItemEstoque(itemEstoqueAux));

    });

    return Promise.all(promises);
    
  }

  private inserirItemEstoque(element) {
    let itemEstoque: BtpItemEstoque = element;
    itemEstoque.idItemEstoque = element.chave.id;
    itemEstoque.usuario = new Usuario();
    itemEstoque.usuario.idUsuario = element.chave.idUsuario;
    itemEstoque.isServidor = true;

    itemEstoque.produto = new Produto();
    itemEstoque.produto.produtoId = element.produtoId;

    itemEstoque.btpEstoque = new BtpEstoque();
    itemEstoque.btpEstoque.idEstoque = element.idEstoque;

    return this.incluirItemEstoqueComId(itemEstoque);
  }

  incluirEstoqueComId(btpEstoque: BtpEstoque) {
    let sql = `INSERT INTO estoques(
               idEstoque,
               dscEstoque,
               userId,
               isServidor,
               ativo )
                VALUES(?,?,?,?,?)`;

    return this.db.transaction(
      tx => {
        tx.executeSql(sql, [
          btpEstoque.idEstoque,
          btpEstoque.dscEstoque,
          btpEstoque.usuario.idUsuario,
          btpEstoque.isServidor,
          btpEstoque.ativo], (tx, resultSet) => {
            btpEstoque.idEstoque = resultSet.insertId;
            return Promise.resolve(true);
          }, function (tx, error) {
            console.log('INSERT Estoque error: ' + error.message);
            return Promise.reject(true);
          });
      });
  }

  incluirItemEstoqueComId(btpItemEstoque: BtpItemEstoque) {
    let sql = `INSERT INTO itens_estoque(
               itemEstoqueId,         
               userId,
               idEstoque,
               produtoId,
               qtdEstoque,
               ativo)
                VALUES(?,?,?,?,?,?)`;

    return this.db.executeSql(sql, [
      btpItemEstoque.idItemEstoque,
      btpItemEstoque.usuario.idUsuario,
      btpItemEstoque.btpEstoque.idEstoque, btpItemEstoque.produto.produtoId,
      btpItemEstoque.qtdEstoque, btpItemEstoque.ativo]);
  }

}
