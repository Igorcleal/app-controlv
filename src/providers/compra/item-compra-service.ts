import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { DbProvider } from './../db-provider';
import { SQLite } from "ionic-native/dist/es5";
import { Produto } from "../../model/produto";
import { BtpEstoque } from "../../model/btpEstoque";
import { Usuario } from "../../model/usuario";
import { BtpItemProdutoCompra } from "../../model/btpItemProdutoCompra";

@Injectable()
export class ItemCompraService {

  public db: SQLite = null;

  constructor(public http: Http, public dbProvider: DbProvider) {
    this.db = dbProvider.db;
  }

  getItemProdutoCompra(usuario:Usuario,compraId: number) {
    let sql = `SELECT pc.*, p.produtoId, p.produtoDsc, est.idEstoque, est.dscEstoque
                FROM produto_compra pc
                INNER JOIN produtos p
                on p.produtoId = pc.produtoId
                LEFT JOIN estoques est
                on est.idEstoque = pc.idEstoque
                WHERE pc.compraId = ?
                AND pc.userId = ?`;

    return this.db.executeSql(sql, [compraId, usuario.idUsuario])
      .then(response => {

        let itensProdutoCompra = [];
        let btpItemProdutoCompra: BtpItemProdutoCompra;
        let produto: Produto;
        let btpEstoque: BtpEstoque;

        for (let index = 0; index < response.rows.length; index++) {

          console.log(response.rows.item(index).produtoCompraId);

          btpItemProdutoCompra = response.rows.item(index);
          btpItemProdutoCompra.idItemCompra = response.rows.item(index).produtoCompraId;
          btpItemProdutoCompra.qtdItemCompra = response.rows.item(index).itemQtd;
          btpItemProdutoCompra.vlrItemCompra = response.rows.item(index).vlrUnitario;
          btpItemProdutoCompra.vlrTotalItemCompra = response.rows.item(index).vlrTotal;
          btpItemProdutoCompra.usuario = usuario;

          produto = new Produto();
          produto.produtoId = response.rows.item(index).produtoId;
          produto.produtoDsc = response.rows.item(index).produtoDsc;
          produto.usuario = usuario;
          btpItemProdutoCompra.produto = produto;

          btpEstoque = new BtpEstoque();
          btpEstoque.idEstoque = response.rows.item(index).idEstoque;
          btpEstoque.dscEstoque = response.rows.item(index).dscEstoque

          btpItemProdutoCompra.produto = produto;
          btpItemProdutoCompra.btpEstoque = btpEstoque;

          itensProdutoCompra.push(btpItemProdutoCompra);
        }


        console.log("getAll Itens produto compra.......");
        return Promise.resolve(itensProdutoCompra);
      })
      .catch(e => {
        console.log("Erro recuperar itens da Compra: " + e.msg);
        return null;
      });
  }
}