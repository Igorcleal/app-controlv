import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { DbProvider } from './../db-provider';
import { SQLite } from "ionic-native/dist/es5";
import { Usuario } from "../../model/usuario";
import { BtpItemFormaPagamentoCompra } from "../../model/btpItemFormaPagamentoCompra";
import { BtpCompra } from "../../model/btpCompra";
import { BtpFormaPagamento } from "../../model/btpFormaPagamento";

@Injectable()
export class ItemFormaPagamentoCompraService {

  public db: SQLite = null;

  constructor(public http: Http, public dbProvider: DbProvider) {
    this.db = dbProvider.db;
  }

  getItemPagamentoCompra(usuario:Usuario,compraId: number) {
    let sql = `SELECT ifc.*, fpg.idFormaPagamento, fpg.descricao
                FROM item_forma_pag_compra ifc
                INNER JOIN formas_pagamento fpg
                ON fpg.idFormaPagamento = ifc.idFormaPagamento
                WHERE ifc.compraId = ?
                AND ifc.userId = ?`;

    return this.db.executeSql(sql, [compraId, usuario.idUsuario])
      .then(response => {

        let itensFormaPagamento = [];
        let btpItemFormaPagamentoCompra: BtpItemFormaPagamentoCompra;
        let usuario: Usuario;
        let btpCompra: BtpCompra;
        let btpFormaPagamento: BtpFormaPagamento;

        for (let index = 0; index < response.rows.length; index++) {

          console.log(response.rows.item(index).itemId);

          btpItemFormaPagamentoCompra = response.rows.item(index);
          btpItemFormaPagamentoCompra.datVencimentoString = response.rows.item(index).datVencimento;

          usuario = new Usuario();
          usuario.idUsuario = response.rows.item(index).userId;
          btpItemFormaPagamentoCompra.usuario = usuario;

          btpCompra = new BtpCompra();
          btpCompra.idCompra = response.rows.item(index).idCompra;

          btpFormaPagamento = new BtpFormaPagamento();
          btpFormaPagamento.idFormaPagamento = response.rows.item(index).idFormaPagamento;
          btpFormaPagamento.descricao = response.rows.item(index).descricao;
          btpItemFormaPagamentoCompra.btpFormaPagamento = btpFormaPagamento;

          btpCompra.idCompra = response.rows.item(index).idCompra;
          
          itensFormaPagamento.push(btpItemFormaPagamentoCompra);
        }

        console.log("getAll Item Forma Pagamento Compra.......");
        return Promise.resolve(itensFormaPagamento);
      })
      .catch(e => {
        console.log(e.message);
        console.log("Erro recuperar itens da compra: " + e.msg);
        return Promise.reject(true);
      });
  }
}