import { SQLite } from 'ionic-native';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { BtpDespesa } from './../../model/btpDespesa';
import { BtpFormaPagamento } from '../../model/btpFormaPagamento';
import { DbProvider } from './../db-provider';
import { Usuario } from "../../model/usuario";
import { BtpCompra } from "../../model/btpCompra";
import { BtpFornecedor } from "../../model/fornecedor";
import { StatusCompra } from "../../model/status-compra";
import { BtpItemProdutoCompra } from "../../model/btpItemProdutoCompra";
import { BtpItemFormaPagamentoCompra } from "../../model/btpItemFormaPagamentoCompra";
import { Produto } from "../../model/produto";
import { BtpEstoque } from "../../model/btpEstoque";

@Injectable()
export class CompraService {

  public db: SQLite = null;
  constructor(public http: Http, public dbProvider: DbProvider) {
    this.db = dbProvider.db;
  }

  getAll(userId: number) {

    let sql = `SELECT 
               c.*, for.nomeFornecedor, for.fornId
               FROM compras c
               INNER JOIN fornecedores for
                ON c.fornId = for.fornId
             
               WHERE c.userId = ?
               ORDER BY for.nomeFornecedor`;

    return this.db.executeSql(sql, [userId])
      .then(response => {
        let compras = [];
        for (let index = 0; index < response.rows.length; index++) {
          let btpcompra: BtpCompra = response.rows.item(index);
          btpcompra.idCompra = response.rows.item(index).compraId;
          btpcompra.datCompra = response.rows.item(index).dataCompra;
          btpcompra.datCompraString = response.rows.item(index).dataCompra;
          btpcompra.vlrTotal = response.rows.item(index).vlrTotal;
          btpcompra.statusCompra = response.rows.item(index).statusCompra;

          let btpFornecedor = new BtpFornecedor();
          btpFornecedor.fornId = response.rows.item(index).fornId;
          btpFornecedor.nomeFornecedor = response.rows.item(index).nomeFornecedor;
          btpcompra.btpFornecedor = btpFornecedor;

          compras.push(btpcompra);
        }

        console.log("executou consulta: getAll compras................");
        return Promise.resolve(compras);
      });
  }

  incluirCompra(btpCompra: BtpCompra) {
    let sql = `INSERT INTO compras(userId,
               fornId, 
               statusCompra, 
               dataCompra,
               isServidor,
               ativo)
                VALUES(?,?,?,?,?,?)`;
    let idInserido: number;

    btpCompra.ativo = true;

    return this.db.transaction((tx) => {
      return tx.executeSql(sql, [btpCompra.usuario.idUsuario,
      btpCompra.btpFornecedor.fornId,
      btpCompra.statusCompra,
      btpCompra.datCompraString,
      btpCompra.isServidor,
      btpCompra.ativo], (tx, resultSet) => {
        idInserido = resultSet.insertId;
        btpCompra.idCompra = resultSet.insertId;
      }, function (tx, error) {
        console.log('INSERT error: ' + error.message);
      });
    }).then((response) => {
      return Promise.resolve(idInserido);
    });
  }

  alterarCompra(btpCompra: BtpCompra) {

    let sql = `UPDATE compras SET
               fornId =?,
               dataCompra =?,
               vlrTotal =?,
               statusCompra =?,
               isServidor = ?,
               ativo = ?
               WHERE compraId =?`;

    return this.db.executeSql(sql, [btpCompra.btpFornecedor.fornId,
    new Date(btpCompra.datCompra).toISOString(),
    btpCompra.vlrTotal,
    btpCompra.statusCompra,
    btpCompra.isServidor,
    btpCompra.ativo,
    btpCompra.idCompra
    ]);
  }

  alterarStatusCompra(idCompra: number, statusCompra: StatusCompra) {

    let sql = `UPDATE compras SET             
               statusCompra =?
               WHERE compraId =?`;

    return this.db.executeSql(sql, [statusCompra, idCompra]);
  }

  alterarVlrCompra(idCompra: number, vlrTotal: number) {

    let sql = `UPDATE compras SET             
               vlrTotal =?
               WHERE compraId =?`;

    return this.db.executeSql(sql, [vlrTotal, idCompra]);
  }

  incluirItemProdutoCompra(btpItemProdutoCompra: BtpItemProdutoCompra) {
    let sql = `INSERT INTO produto_compra(userId,
               compraId,
               produtoId,
               itemQtd,
               vlrUnitario,
               vlrTotal, 
               idEstoque,
                ativo)
                VALUES(?,?,?,?,?,?,?,?)`;

    let idEstoque: number = null;
    if (btpItemProdutoCompra.btpEstoque != null && btpItemProdutoCompra.btpEstoque.idEstoque != null) {
      idEstoque = btpItemProdutoCompra.btpEstoque.idEstoque;
    }

    btpItemProdutoCompra.ativo = true;

    console.log("Inserindo Item produtoCompra");
    console.log(btpItemProdutoCompra.qtdItemCompra);
    return this.db.executeSql(sql, [btpItemProdutoCompra.usuario.idUsuario,
    btpItemProdutoCompra.btpCompra.idCompra, btpItemProdutoCompra.produto.produtoId,
    btpItemProdutoCompra.qtdItemCompra, btpItemProdutoCompra.vlrItemCompra,
    btpItemProdutoCompra.vlrTotalItemCompra, idEstoque,
    btpItemProdutoCompra.ativo]);
  }

  alterarItemProdutoCompra(btpItemProdutoCompra: BtpItemProdutoCompra) {
    let sql = `UPDATE produto_compra 
               SET  itemQtd = ?,
               vlrUnitario = ?,
               vlrTotal =?,
               idEstoque =?,
               ativo = ?
               WHERE produtoCompraId=?`;

    let idEstoque: number = null;
    if (btpItemProdutoCompra.btpEstoque != null && btpItemProdutoCompra.btpEstoque.idEstoque != null) {
      idEstoque = btpItemProdutoCompra.btpEstoque.idEstoque;
    }

    console.log("Alterando Item produtoCompra");
    return this.db.executeSql(sql, [btpItemProdutoCompra.qtdItemCompra,
    btpItemProdutoCompra.vlrItemCompra,
    btpItemProdutoCompra.vlrTotalItemCompra,
      idEstoque,
    btpItemProdutoCompra.ativo,
    btpItemProdutoCompra.idItemCompra]);
  }

  deletarItemCompra(itemCompraId: number) {
    let sql = 'DELETE FROM produto_compra WHERE produtoCompraId=?';

    return this.db.executeSql(sql, [itemCompraId]);
  }

  incluirItemFormaPagamentoCompra(btpItemFormaPagamentoCompra: BtpItemFormaPagamentoCompra) {
    let sql = `INSERT INTO item_forma_pag_compra(userId,
               compraId,
               numParcela,
               datVencimento,
               datPagamento,
               vlrParcela,
               dscObservacao,
               idFormaPagamento,
               ativo)
                VALUES(?,?,?,?,?,?,?,?,?)`;

    btpItemFormaPagamentoCompra.ativo = true;

    console.log("Inserindo Item da forma de pgt");
    return this.db.executeSql(sql, [btpItemFormaPagamentoCompra.usuario.idUsuario,
    btpItemFormaPagamentoCompra.btpCompra.idCompra, btpItemFormaPagamentoCompra.numParcela,
    btpItemFormaPagamentoCompra.datVencimentoString, btpItemFormaPagamentoCompra.datPagamento,
    btpItemFormaPagamentoCompra.vlrParcela,
    btpItemFormaPagamentoCompra.dscObservacao,
    btpItemFormaPagamentoCompra.btpFormaPagamento.idFormaPagamento,
    btpItemFormaPagamentoCompra.ativo]);
  }

  deletarItemFormaPagamentoCompra(itemId: number) {
    let sql = 'DELETE FROM item_forma_pag_Compra WHERE itemId=?';

    return this.db.executeSql(sql, [itemId]);
  }

  alterarItemFormaPagamentoCompra(BtpItemFormaPagamentoCompra: BtpItemFormaPagamentoCompra) {
    let sql = `UPDATE item_forma_pag_compra
               SET  numParcela = ?,
                datVencimento = ?,
                datPagamento =?,
                vlrParcela =?,
                dscObservacao =?,
                idFormaPagamento =?,
                ativo = ?
               WHERE itemId=?`;

    console.log("Alterando Item Compra");
    return this.db.executeSql(sql, [BtpItemFormaPagamentoCompra.numParcela,
    BtpItemFormaPagamentoCompra.datVencimentoString,
    BtpItemFormaPagamentoCompra.datPagamento,
    BtpItemFormaPagamentoCompra.vlrParcela,
    BtpItemFormaPagamentoCompra.dscObservacao,
    BtpItemFormaPagamentoCompra.btpFormaPagamento.idFormaPagamento,
    BtpItemFormaPagamentoCompra.ativo,
    BtpItemFormaPagamentoCompra.itemId
    ]);
  }


  getItemProdutoCompra(userId: number, compraId: number) {
    let sql = `SELECT pc.*, p.produtoId, p.produtoDsc, est.idEstoque, est.dscEstoque
                FROM produto_compra pc
                INNER JOIN produtos p
                on p.produtoId = pc.produtoId
                LEFT JOIN estoques est
                on est.idEstoque = pc.idEstoque
                WHERE pc.compraId = ?
                AND pc.userId = ?
                AND pc.ativo = ?`;

    console.log(compraId + "teste idCompra");
    return this.db.executeSql(sql, [compraId, userId, true])
      .then(response => {

        let itensProdutoCompra = [];
        let btpItemProdutoCompra: BtpItemProdutoCompra;
        let produto: Produto;
        let btpEstoque: BtpEstoque;

        for (let index = 0; index < response.rows.length; index++) {

          console.log(response.rows.item(index).produtoCompraId);

          btpItemProdutoCompra = response.rows.item(index);
          btpItemProdutoCompra.idItemCompra = response.rows.item(index).produtoCompraId;
          btpItemProdutoCompra.qtdItemCompra = response.rows.item(index).itemQtd;
          btpItemProdutoCompra.vlrItemCompra = response.rows.item(index).vlrUnitario;
          btpItemProdutoCompra.vlrTotalItemCompra = response.rows.item(index).vlrTotal;

          let usuario = new Usuario();
          usuario.idUsuario = response.rows.item(index).userId;
          btpItemProdutoCompra.usuario = usuario;

          produto = new Produto();
          produto.produtoId = response.rows.item(index).produtoId;
          produto.produtoDsc = response.rows.item(index).produtoDsc;
          btpItemProdutoCompra.produto = produto;

          btpEstoque = new BtpEstoque();
          btpEstoque.idEstoque = response.rows.item(index).idEstoque;
          btpEstoque.dscEstoque = response.rows.item(index).dscEstoque

          btpItemProdutoCompra.produto = produto;
          btpItemProdutoCompra.btpEstoque = btpEstoque;

          console.log(produto.produtoDsc + " descricao do produto.");
          itensProdutoCompra.push(btpItemProdutoCompra);
        }


        console.log(itensProdutoCompra.length);
        console.log("getAll Itens produto compra.......");
        return Promise.resolve(itensProdutoCompra);
      })
      .catch(e => {
        console.log("Erro recuperar itens da Compra: " + e.msg);
        return null;
      });
  }

  getItensFormaPagamento(compraId: number) {
    let sql = `SELECT ifc.*, fpg.idFormaPagamento, fpg.descricao
                FROM item_forma_pag_compra ifc
                INNER JOIN formas_pagamento fpg
                ON fpg.idFormaPagamento = ifc.idFormaPagamento
                WHERE ifc.compraId = ?
                AND ifc.ativo = ?`;

    console.log(compraId + "teste compraId");
    return this.db.executeSql(sql, [compraId, true])
      .then(response => {

        let itensFormaPagamento = [];
        let btpItemFormaPagamentoCompra: BtpItemFormaPagamentoCompra;
        let usuario: Usuario;
        let btpCompra: BtpCompra;
        let btpFormaPagamento: BtpFormaPagamento;

        for (let index = 0; index < response.rows.length; index++) {

          console.log(response.rows.item(index).itemId);

          btpItemFormaPagamentoCompra = response.rows.item(index);
          btpItemFormaPagamentoCompra.itemId = response.rows.item(index).itemId;
          btpItemFormaPagamentoCompra.numParcela = response.rows.item(index).numParcela;
          btpItemFormaPagamentoCompra.vlrParcela = response.rows.item(index).vlrParcela;
          btpItemFormaPagamentoCompra.datPagamento = response.rows.item(index).datPagamento;
          btpItemFormaPagamentoCompra.datVencimentoString = response.rows.item(index).datVencimento;
          btpItemFormaPagamentoCompra.datVencimento = response.rows.item(index).datVencimento;
          btpItemFormaPagamentoCompra.dscObservacao = response.rows.item(index).dscObservacao;

          usuario = new Usuario();
          usuario.idUsuario = response.rows.item(index).userId;

          btpCompra = new BtpCompra();
          btpCompra.idCompra = response.rows.item(index).idCompra;

          btpFormaPagamento = new BtpFormaPagamento();
          btpFormaPagamento.idFormaPagamento = response.rows.item(index).idFormaPagamento;
          btpFormaPagamento.descricao = response.rows.item(index).descricao;
          btpItemFormaPagamentoCompra.btpFormaPagamento = btpFormaPagamento;

          btpCompra.idCompra = response.rows.item(index).idCompra;
          btpItemFormaPagamentoCompra.btpCompra = btpCompra;
          itensFormaPagamento.push(btpItemFormaPagamentoCompra);
        }

        console.log(itensFormaPagamento.length);
        console.log("getAll Item Forma Pagamento Compra.......");
        return Promise.resolve(itensFormaPagamento);
      })
      .catch(e => {
        console.log("Erro recuperar itens da compra: " + e.msg);
        return null;
      });
  }

  getComprasNaoServidor(userId: number) {

    let sql = `SELECT 
               c.*, c.statusCompra, for.nomeFornecedor, for.fornId,
               (select sum(pc.vlrTotal) from produto_compra pc 
                  where pc.compraId = c.compraId
                  AND pc.ativo = ? ) as vlrTotal
               FROM compras c
               INNER JOIN fornecedores for
                ON c.fornId = for.fornId
               WHERE c.userId = ?
                AND c.isServidor = ?
               ORDER BY for.nomeFornecedor`;

    return this.db.executeSql(sql, [true, userId, false])
      .then(response => {
        console.log(response);
        let compras = [];
        for (let index = 0; index < response.rows.length; index++) {
          let btpCompra: BtpCompra = response.rows.item(index);
          btpCompra.idCompra = response.rows.item(index).compraId;
          btpCompra.datCompra = response.rows.item(index).dataCompra;
          btpCompra.datCompraString = response.rows.item(index).dataCompra;
          btpCompra.vlrTotal = response.rows.item(index).vlrTotal;
          btpCompra.statusCompra = response.rows.item(index).statusCompra;

          if (btpCompra.vlrTotal == null) {
            btpCompra.vlrTotal = 0
          }

          let usuario = new Usuario();
          usuario.idUsuario = response.rows.item(index).userId;
          btpCompra.usuario = usuario;

          let btpFornecedor = new BtpFornecedor();
          btpFornecedor.fornId = response.rows.item(index).fornId;
          btpFornecedor.nomeFornecedor = response.rows.item(index).nomeFornecedor;
          btpFornecedor.usuario = usuario;
          btpCompra.btpFornecedor = btpFornecedor;

          compras.push(btpCompra);
        }

        console.log("executou consulta: getComprasNaoServido................");
        return Promise.resolve(compras);
      });
  }

  getAllByDate(userId: number, data: Date) {

    let sql = `SELECT 
               c.*, c.statusCompra, for.nomeFornecedor, for.fornId,
               (select sum(pc.vlrTotal) from produto_compra pc 
                  where pc.compraId = c.compraId
                   and pc.ativo = ? ) as vlrTotal
               FROM compras c
               INNER JOIN fornecedores for
                ON c.fornId = for.fornId
               WHERE c.userId = ?
               AND strftime('%m%Y',c.dataCompra) = ?
               ORDER BY for.nomeFornecedor`;

    return this.db.executeSql(sql, [true, userId, this.dbProvider.meses[data.getMonth()] + "" + data.getFullYear()])
      .then(response => {
        let compras = [];
        for (let index = 0; index < response.rows.length; index++) {
          let btpCompra: BtpCompra = response.rows.item(index);
          btpCompra.idCompra = response.rows.item(index).compraId;
          btpCompra.datCompra = response.rows.item(index).dataCompra;
          btpCompra.datCompraString = response.rows.item(index).dataCompra;
          btpCompra.vlrTotal = response.rows.item(index).vlrTotal;

          if (btpCompra.vlrTotal == null) { btpCompra.vlrTotal = 0 }

          btpCompra.statusCompra = response.rows.item(index).statusCompra;

          let btpFornecedor = new BtpFornecedor();
          btpFornecedor.fornId = response.rows.item(index).fornId;
          btpFornecedor.nomeFornecedor = response.rows.item(index).nomeFornecedor;
          btpCompra.btpFornecedor = btpFornecedor;

          compras.push(btpCompra);
        }

        console.log("executou consulta: getAll compras................");
        return Promise.resolve(compras);
      });
  }

  deletarCompraCompleta(btpCompra: BtpCompra) {

    let promises = [];

    promises.push(this.deletarCompra(btpCompra));
    promises.push(this.deletarDespesasCompra(btpCompra));
    promises.push(this.deletarItensCompra(btpCompra));
    promises.push(this.deletarPagamentosCompra(btpCompra));

    return Promise.all(promises);
  }

  deletarCompra(btpCompra: BtpCompra) {
    let sql = 'DELETE FROM Compras WHERE compraId=?';
    return this.db.executeSql(sql, [btpCompra.idCompra]).then(() => {
      console.log("Success Compra excluida");
      return Promise.resolve(true);
    }).catch(() => {
      console.error("erro excluir Compra");
      return Promise.reject(true);
    });
  }

  deletarItensCompra(btpCompra: BtpCompra) {
    let sql = 'DELETE FROM produto_compra WHERE CompraId=?';
    return this.db.executeSql(sql, [btpCompra.idCompra]).then(() => {
      console.log("Success excluído produtos da Compra");
      return Promise.resolve(true);
    })
      .catch((err) => {
        console.error(err.message);
      });
  }

  deletarPagamentosCompra(btpCompra: BtpCompra) {
    let sql = 'DELETE FROM item_forma_pag_Compra WHERE CompraId=?';
    return this.db.executeSql(sql, [btpCompra.idCompra]).then(() => {
      console.log("Success excluído pagamentos da Compra");
      return Promise.resolve(true);
    })
      .catch((err) => {
        console.error(err.message);
        return Promise.reject(true);
      });
  }

  deletarDespesasCompra(btpCompra: BtpCompra) {

    let sql = 'SELECT d.despesaId as despesaId FROM despesas d WHERE CompraId=?';

    return this.db.executeSql(sql, [btpCompra.idCompra]).then(response => {

      let promises = [];

      for (let index = 0; index < response.rows.length; index++) {
        let idDespesa: number = response.rows.item(index).despesaId

        //exclui pagamentos das despesas
        promises.push(this.deletarPagamentosDespesas(idDespesa));
      }

      //exclui despesas geradas da Compra
      return Promise.all(promises).then(() => {
        let sql = 'DELETE FROM despesas WHERE CompraId=?';
        return this.db.executeSql(sql, [btpCompra.idCompra]).then(() => {
          console.log("Excluido despesas geradas da Compra");
          return Promise.resolve(true);
        })
          .catch((err) => {
            console.log("Erro excluir despesa da Compra");
            console.error(err.message);
            return Promise.reject(true);
          });
      });
    });

  }

  deletarPagamentosDespesas(idDespesa: number) {
    let sql = 'DELETE FROM pagamento_despesas WHERE despesaId=?';
    return this.db.executeSql(sql, [idDespesa]).then(() => {
      console.log("Excluido pagamentos gerados da despesa da Compra");
      return Promise.resolve(true);
    }).catch((err) => {
      console.log("Erro excluir pagamento da despesa");
      console.error(err.message);
      return Promise.reject(true);
    });
  }

  incluirDespesa(btpDespesa: BtpDespesa) {
    let sql = `INSERT INTO despesas( 
               userId,
               despesa,
               despesaDta, 
               despesaVlr,
               despesaVlrPago,
               despesaPaga,
               categoriaId, 
               compraId,
               contaId,
               isServidor,
               ativo )    
    VALUES(?,?,?,?,?,?,?,?,?,?,?)`;

    let idInserido: number;

    let idCompra: number = null;

    if (btpDespesa.btpCompra != null && btpDespesa.btpCompra.idCompra != null) {
      idCompra = btpDespesa.btpCompra.idCompra;
    }

    let despesaIdPai: number = null;
    if (btpDespesa.btpDespesaPai) {
      despesaIdPai = btpDespesa.btpDespesaPai.despesaId;
    }

    btpDespesa.ativo = true;

    return this.db.transaction((tx) => {
      return tx.executeSql(sql, [btpDespesa.usuario.idUsuario,
      btpDespesa.despesa,
      btpDespesa.despesaDtaString,
      btpDespesa.despesaVlr,
      btpDespesa.despesaValorPago,
      btpDespesa.despesaPaga,
      btpDespesa.categoria.categoriaId,
        idCompra, btpDespesa.btpConta.contaId,
      btpDespesa.isServidor,
      btpDespesa.ativo], (tx, resultSet) => {
        idInserido = resultSet.insertId;
      }, function (tx, error) {
        console.log('INSERT error: ' + error.message);
      });
    }).then((response) => {
      return Promise.resolve(idInserido);
    });
  }

  verificarExistePagamento(idCompra: number) {
    let sql = `SELECT d.despesaId 
                FROM despesas d 
                INNER JOIN pagamento_despesas pd
                ON pd.despesaId = d.despesaId
                WHERE compraId=?`;

    return this.db.executeSql(sql, [idCompra]).then(response => {

      if (response.rows != null && response.rows.length > 0) {
        return Promise.resolve(true);
      }
      return Promise.resolve(false);
    }).catch(() => {
      return Promise.reject(true);
    });
  }

  alterarComprasGravadoServidor(compras: BtpCompra[]) {
    let promises = [];

    compras.forEach(compra => {
      compra.isServidor = true;
      promises.push(this.alterarCompra(compra));
    });

    return Promise.all(promises);
  }

  inserirComprasDownload(lista: Array<any>) {

    let promises = [];
    lista.forEach(element => {
      promises.push(this.inserirCompra(element));
    });
    return Promise.all(promises);
  }

  private inserirCompra(element) {
    let compra: BtpCompra = element;
    compra.idCompra = element.chave.id;
    compra.usuario = new Usuario();
    compra.usuario.idUsuario = element.chave.idUsuario;
    compra.isServidor = true;

    compra.btpFornecedor = new BtpFornecedor();
    compra.btpFornecedor.fornId = element.fornId;

    compra.listItemCompra = element.listItemCompra;

    switch(element.statusCompra){
      case 'FECHADO':
        compra.statusCompra = StatusCompra.FECHADO;
        break;
      case 'PENDENTE':
        compra.statusCompra = StatusCompra.PENDENTE;
        break;
    }

    let promises = [];

    promises.push(this.incluirCompraComId(compra));

    compra.listItemCompra.forEach(itemCompra => {
      promises.push(this.inserirItemCompra(itemCompra));
    });

    compra.listItemPagamentoCompra.forEach(itemPagamentoCompra => {
      promises.push(this.inserirItemFormaPagamentoCompra(itemPagamentoCompra));
    });

    return Promise.all(promises);
  }

  private inserirItemCompra(element) {
    let itemCompra: BtpItemProdutoCompra = element;
    itemCompra.idItemCompra = element.chave.id;
    itemCompra.usuario = new Usuario();
    itemCompra.usuario.idUsuario = element.chave.idUsuario;

    itemCompra.produto = new Produto();
    itemCompra.produto.produtoId = element.produtoId;

    itemCompra.btpEstoque = new BtpEstoque();
    itemCompra.btpEstoque.idEstoque = element.idEstoque;

    itemCompra.btpCompra = new BtpCompra();
    itemCompra.btpCompra.idCompra = element.compraId;

    return this.incluirItemProdutoCompraComId(itemCompra);
  }

  private inserirItemFormaPagamentoCompra(element) {
    let itemFormaPagamentoCompra: BtpItemFormaPagamentoCompra = element;
    itemFormaPagamentoCompra.itemId = element.chave.id;
    itemFormaPagamentoCompra.usuario = new Usuario();
    itemFormaPagamentoCompra.usuario.idUsuario = element.chave.idUsuario;

    itemFormaPagamentoCompra.btpFormaPagamento = new BtpFormaPagamento();
    itemFormaPagamentoCompra.btpFormaPagamento.idFormaPagamento = element.idFormaPagamento;

    itemFormaPagamentoCompra.btpCompra = new BtpCompra();
    itemFormaPagamentoCompra.btpCompra.idCompra = element.compraId;

    return this.incluirItemFormaPagamentoCompraComId(itemFormaPagamentoCompra);
  }

  incluirCompraComId(btpCompra: BtpCompra) {
    let sql = `INSERT INTO compras(
               compraId,
               userId,
               fornId, 
               statusCompra, 
               dataCompra,
               isServidor,
               ativo)
                VALUES(?,?,?,?,?,?,?)`;
    let idInserido: number;

    console.log(btpCompra);

    return this.db.transaction((tx) => {
      return tx.executeSql(sql, [
        btpCompra.idCompra,
        btpCompra.usuario.idUsuario,
        btpCompra.btpFornecedor.fornId,
        btpCompra.statusCompra,
        btpCompra.datCompra,
        btpCompra.isServidor,
        btpCompra.ativo], (tx, resultSet) => {
          idInserido = resultSet.insertId;
          btpCompra.idCompra = resultSet.insertId;
        }, function (tx, error) {
          console.log('INSERT error: ' + error.message);
        });
    }).then((response) => {
      return Promise.resolve(idInserido);
    });
  }

  incluirItemProdutoCompraComId(btpItemProdutoCompra: BtpItemProdutoCompra) {
    let sql = `INSERT INTO produto_compra(
               produtoCompraId,
               userId,
               compraId,
               produtoId,
               itemQtd,
               vlrUnitario,
               vlrTotal, 
               idEstoque,
                ativo)
                VALUES(?,?,?,?,?,?,?,?,?)`;

    let idEstoque: number = null;
    if (btpItemProdutoCompra.btpEstoque != null && btpItemProdutoCompra.btpEstoque.idEstoque != null) {
      idEstoque = btpItemProdutoCompra.btpEstoque.idEstoque;
    }


    return this.db.executeSql(sql, [
      btpItemProdutoCompra.idItemCompra,
      btpItemProdutoCompra.usuario.idUsuario,
      btpItemProdutoCompra.btpCompra.idCompra, btpItemProdutoCompra.produto.produtoId,
      btpItemProdutoCompra.qtdItemCompra, btpItemProdutoCompra.vlrItemCompra,
      btpItemProdutoCompra.vlrTotalItemCompra, idEstoque,
      btpItemProdutoCompra.ativo]);
  }

  incluirItemFormaPagamentoCompraComId(btpItemFormaPagamentoCompra: BtpItemFormaPagamentoCompra) {
    let sql = `INSERT INTO item_forma_pag_compra(
               itemId,
               userId,
               compraId,
               numParcela,
               datVencimento,
               datPagamento,
               vlrParcela,
               dscObservacao,
               idFormaPagamento,
               ativo)
                VALUES(?,?,?,?,?,?,?,?,?,?)`;

    console.log(btpItemFormaPagamentoCompra);

    return this.db.executeSql(sql, [
      btpItemFormaPagamentoCompra.itemId,
      btpItemFormaPagamentoCompra.usuario.idUsuario,
      btpItemFormaPagamentoCompra.btpCompra.idCompra, btpItemFormaPagamentoCompra.numParcela,
      btpItemFormaPagamentoCompra.datVencimento, btpItemFormaPagamentoCompra.datPagamento,
      btpItemFormaPagamentoCompra.vlrParcela,
      btpItemFormaPagamentoCompra.dscObservacao,
      btpItemFormaPagamentoCompra.btpFormaPagamento.idFormaPagamento,
      btpItemFormaPagamentoCompra.ativo]);
  }

}