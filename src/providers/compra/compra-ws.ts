import {EstoqueWsProvider} from '../estoque/estoque-ws';
import { ClienteWsProvider } from './../cliente/cliente-ws';
import { FormaPagamentoWsProvider } from './../forma-pagamento/forma-pagamento-ws';
import { HttpProvider } from './../http/http-provider';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { Usuario } from '../../model/usuario';
import { UtilsService } from './../utils-service';
import { Constants } from "../../app/constants";
import { CategoriaWsProvider } from "../categoria/categoria-ws";
import { CompraService } from "./compra-service";
import { BtpCompra } from "../../model/btpCompra";
import { ItemCompraService } from "./item-compra-service";
import { BtpItemProdutoCompra } from "../../model/btpItemProdutoCompra";
import { BtpItemFormaPagamentoCompra } from "../../model/btpItemFormaPagamentoCompra";
import { ItemFormaPagamentoCompraService } from "./item-forma-pagamento-compra-service";
import { DespesaWsProvider } from "../despesa/despesa-ws";

@Injectable()
export class CompraWsProvider {

  private usuario: Usuario;

  constructor(public http: HttpProvider,
    public utils: UtilsService,
    public compraService: CompraService,
    public categoriaWs: CategoriaWsProvider,
    public despesaWs: DespesaWsProvider,
    public itemCompraService: ItemCompraService,
    public itemFormaPagamentoCompraService: ItemFormaPagamentoCompraService,
    public formaPagamentoWs: FormaPagamentoWsProvider,
    public clienteWs: ClienteWsProvider,
    public estoqueWs: EstoqueWsProvider) {
  }

  enviarDependencias() {
    let promises = [];
    promises.push(this.formaPagamentoWs.enviarFormasPagamentosServidor());
    promises.push(this.clienteWs.enviarClientesServidorSemDependencias());
    promises.push(this.estoqueWs.enviarEstoqueServidorSemDependencias());
    promises.push(this.despesaWs.enviarDespesasServidor());

    return Promise.all(promises);
  }

  enviarComprasServidor() {

    return this.enviarDependencias().then(() => {

      return this.getComprasNaoServidor().then((compras: BtpCompra[]) => {
        console.log(compras);
        if (compras == null || compras.length == 0) {
          return Promise.resolve(null);
        }

        let promises = [];

        compras.forEach(compra => {
          promises.push(this.itemCompraService.getItemProdutoCompra(this.usuario, compra.idCompra).then((itensCompra: Array<BtpItemProdutoCompra>) => {
            compra.listItemCompra = itensCompra;
            console.log('itensCompra')
            console.log(itensCompra)
          }));

          promises.push(this.itemFormaPagamentoCompraService.getItemPagamentoCompra(this.usuario, compra.idCompra).then((itensPagamentoCompra: Array<BtpItemFormaPagamentoCompra>) => {
            compra.listItemPagamentoCompra = itensPagamentoCompra;
            console.log('itensPagamentoCompra')
            console.log(itensPagamentoCompra)
          }));
        });


        return Promise.all(promises).then(() => {
          console.log(compras)
          //envia produtos para WS
          return this.http.post(`${Constants.urlWs}compra/salvarLst`, compras).then(() => {
            //caso dê certo, altera no sqlite produtos para produtos gravadas no servidor
            return this.compraService.alterarComprasGravadoServidor(compras);
          });
        });
      });
    });
  }

  enviarComprasServidorSemDependencias() {

    return this.enviarDependencias().then(() => {

      return this.getComprasNaoServidor().then((compras: BtpCompra[]) => {
        console.log(compras);
        if (compras == null || compras.length == 0) {
          return Promise.resolve(null);
        }

        let promises = [];

        compras.forEach(compra => {
          promises.push(this.itemCompraService.getItemProdutoCompra(this.usuario, compra.idCompra).then((itensCompra: Array<BtpItemProdutoCompra>) => {
            compra.listItemCompra = itensCompra;
            console.log('itensCompra')
            console.log(itensCompra)
          }));

          promises.push(this.itemFormaPagamentoCompraService.getItemPagamentoCompra(this.usuario, compra.idCompra).then((itensPagamentoCompra: Array<BtpItemFormaPagamentoCompra>) => {
            compra.listItemPagamentoCompra = itensPagamentoCompra;
            console.log('itensPagamentoCompra')
            console.log(itensPagamentoCompra)
          }));
        });


        return Promise.all(promises).then(() => {
          console.log(compras)
          //envia produtos para WS
          return this.http.post(`${Constants.urlWs}compra/salvarLst`, compras).then(() => {
            //caso dê certo, altera no sqlite produtos para produtos gravadas no servidor
            return this.compraService.alterarComprasGravadoServidor(compras);
          });
        });
      });
    });
  }

  getComprasNaoServidor() {
    return this.utils.getUsuarioLogado().then((usuario: Usuario) => {
      this.usuario = usuario;
      return this.compraService.getComprasNaoServidor(usuario.idUsuario);
    });
  }

}
