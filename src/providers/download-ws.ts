import { PagamentoService } from './pagamento-provider';
import { PagamentoDespesaService } from './pagamento-despesa-provider';
import { DespesaService } from './despesa/despesa-service';
import { ReceitaService } from './receita/receita-service';
import { BtpReceita } from './../model/btpReceita';
import { BtpDespesa } from './../model/btpDespesa';
import { VendaService } from './venda/venda-service';
import { CompraService } from './compra/compra-service';
import { EstoqueService } from './estoque/estoque-service';
import { BtpEstoque } from './../model/btpEstoque';
import { DataImportanteService } from './data-importante/dataImportante-service';
import { VisitaService } from './visita/visita-service';
import { BtpVisita } from './../model/btpVisita';
import { BtpDataImportante } from './../model/btpDataImportante';
import { FormaPagamentoService } from './forma-pagamento/forma-pagamento-service';
import { BtpFormaPagamento } from './../model/btpFormaPagamento';
import { Produto } from './../model/produto';
import { FornecedorService } from './fornecedor/fornecedor-service';
import { ProdutoService } from './produto/produto-service';
import { ClienteService } from './cliente/cliente-service';
import { BtpCliente } from './../model/cliente';
import { CategoriaService } from './categoria/categoria-service';
import { DbProvider } from './db-provider';
import { BtpConta } from './../model/btpConta';
import { ContaService } from './conta/conta-service';
import { Categoria } from '../model/categoria';
import { Constants } from '../app/constants';
import { HttpProvider } from './http/http-provider';
import { Injectable } from '@angular/core';
import { BtpCompra } from "../model/btpCompra";
import { BtpVenda } from "../model/btpVenda";

@Injectable()
export class DownloadWsProvider {

    public db;
    public userId: string;
    constructor(public http: HttpProvider, public dbProvider: DbProvider,
        public contaService: ContaService,
        public categoriaService: CategoriaService,
        public clienteService: ClienteService,
        public produtoService: ProdutoService,
        public fornecedorService: FornecedorService,
        public formaPagamentoService: FormaPagamentoService,
        public visitaService: VisitaService,
        public dataImportanteService: DataImportanteService,
        public estoqueService: EstoqueService,
        public compraService: CompraService,
        public vendaService: VendaService,
        public receitaService: ReceitaService,
        public despesaService: DespesaService,
        public pagamentoDespesaService: PagamentoDespesaService,
        public pagamentoService: PagamentoService
    ) {
        this.db = dbProvider.db;
    }

    downloadDados(userId: string) {

        this.userId = userId;

        return this.reiniciarBanco().then(() => {

            let promises = [];

            promises.push(this.downloadContas());
            promises.push(this.downloadCategorias());
            promises.push(this.downloadClientes());
            promises.push(this.downloadFornecedores());
            promises.push(this.downloadProdutos());
            promises.push(this.downloadFormaPagamento());
            promises.push(this.downloadVisitas());
            promises.push(this.downloadDatasImportantes());
            promises.push(this.downloadEstoques());
            promises.push(this.downloadCompras());
            promises.push(this.downloadVendas());
            promises.push(this.downloadReceitas());
            promises.push(this.downloadPagamentosReceitas());
            promises.push(this.downloadDespesas());
            promises.push(this.downloadPagamentosDespesas());

            return Promise.all(promises);
        }).catch((err) => {
            console.error(err.message);
            console.error('download error');
            return Promise.reject(err);
        })


    }

    reiniciarBanco() {
        //deleta banco, reinicia e cria as tabelas
        return this.dbProvider.deleteDatabase().then(() => {
            return this.dbProvider.openDatabase().then(() => {
                return this.dbProvider.createTable();
            })
        })
    }

    downloadContas() {
        return this.http.get(`${Constants.urlWs}conta/usuario/${this.userId}`).then((contas: Array<BtpConta>) => {
            return this.contaService.inserirContasDownload(this.userId, contas).then(() => {
                console.log('success download contas');
            }).catch((err) => {
                console.error(err.message);
                Promise.reject(err)
            });
        })
    }

    downloadCategorias() {
        return this.http.get(`${Constants.urlWs}categoria/usuario/${this.userId}`).then((lista: Array<Categoria>) => {
            return this.categoriaService.inserirCategoriasDownload(this.userId, lista).then(() => {
                console.log('success download categorias');
            }).catch((err) => {
                console.error(err.message);
                Promise.reject(err)
            });
        })
    }

    downloadClientes() {
        return this.http.get(`${Constants.urlWs}cliente/usuario/${this.userId}`).then((lista: Array<BtpCliente>) => {
            console.log(lista);
            console.log(this.userId);
            console.log('azzzzcxzczcxzczcxzcxzcxzxcxzczc');
            return this.clienteService.inserirClientesDownload(lista).then(() => {
                console.log('success download clientes');
            }).catch((err) => {
                console.error(err.message);
                Promise.reject(err)
            });
        })
    }

    downloadProdutos() {
        return this.http.get(`${Constants.urlWs}produto/usuario/${this.userId}`).then((lista: Array<Produto>) => {
            return this.produtoService.inserirProdutosDownload(lista).then(() => {
                console.log('success download produtos');
            }).catch((err) => {
                console.error(err.message);
                Promise.reject(err)
            });
        })
    }

    downloadFornecedores() {
        return this.http.get(`${Constants.urlWs}fornecedor/usuario/${this.userId}`).then((lista: Array<BtpCliente>) => {
            return this.fornecedorService.inserirFornecedoresDownload(lista).then(() => {
                console.log('success download fornecedores');
            }).catch((err) => {
                console.error(err.message);
                Promise.reject(err)
            });
        })
    }

    downloadFormaPagamento() {
        return this.http.get(`${Constants.urlWs}formaPagamento/usuario/${this.userId}`).then((lista: Array<BtpFormaPagamento>) => {
            return this.formaPagamentoService.inserirFormaPagamentoDownload(lista).then(() => {
                console.log('success download forma pagamento');
            }).catch((err) => {
                console.error(err.message);
                Promise.reject(err)
            });
        })
    }

    downloadDatasImportantes() {
        return this.http.get(`${Constants.urlWs}dataImportante/usuario/${this.userId}`).then((lista: Array<BtpDataImportante>) => {
            return this.dataImportanteService.inserirDatasImportantesDownload(lista).then(() => {
                console.log('success download data importante');
            }).catch((err) => {
                console.log('error download data importante');
                console.error(err.message);
                Promise.reject(err)
            });
        })
    }

    downloadVisitas() {
        return this.http.get(`${Constants.urlWs}visita/usuario/${this.userId}`).then((lista: Array<BtpVisita>) => {
            return this.visitaService.inserirVisitasDownload(lista).then(() => {
                console.log('success download visitas');
            }).catch((err) => {
                console.error(err.message);
                Promise.reject(err)
            });
        })
    }

    downloadEstoques() {
        return this.http.get(`${Constants.urlWs}estoque/usuario/${this.userId}`).then((lista: Array<BtpEstoque>) => {
            return this.estoqueService.inserirEstoquesDownload(lista).then(() => {
                console.log('success download estoque');
            }).catch((err) => {
                console.error(err.message);
                Promise.reject(err)
            });
        })
    }

    downloadCompras() {
        return this.http.get(`${Constants.urlWs}compra/usuario/${this.userId}`).then((lista: Array<BtpCompra>) => {
            return this.compraService.inserirComprasDownload(lista).then(() => {
                console.log('success download Compras');
            }).catch((err) => {
                console.error(err.message);
                Promise.reject(err)
            });
        })
    }

    downloadVendas() {
        return this.http.get(`${Constants.urlWs}venda/usuario/${this.userId}`).then((lista: Array<BtpVenda>) => {
            return this.vendaService.inserirVendasDownload(lista).then(() => {
                console.log('success download vendas');
            }).catch((err) => {
                console.log('error download vendas');
                console.error(err.message);
                Promise.reject(err)
            });
        })
    }

    downloadReceitas() {
        return this.http.get(`${Constants.urlWs}receita/usuario/${this.userId}`).then((lista: Array<BtpReceita>) => {
            return this.receitaService.inserirReceitasDownload(lista).then(() => {
                console.log('success download receitas');
            }).catch((err) => {
                console.log('error download receitas');
                console.error(err.message);
                Promise.reject(err)
            });
        })
    }

    downloadDespesas() {
        return this.http.get(`${Constants.urlWs}despesa/usuario/${this.userId}`).then((lista: Array<BtpDespesa>) => {
            return this.despesaService.inserirDespesasDownload(lista).then(() => {
                console.log('success download despesas');
            }).catch((err) => {
                console.log('error download despesas');
                console.error(err.message);
                Promise.reject(err)
            });
        })
    }

    downloadPagamentosDespesas() {
        return this.http.get(`${Constants.urlWs}pagamentoDespesa/usuario/${this.userId}`).then((lista: Array<BtpDespesa>) => {
            return this.pagamentoDespesaService.inserirPagamentosDespesasDownload(lista).then(() => {
                console.log('success download pagamentos despesas');
            }).catch((err) => {
                console.log('error download pagamentos  despesas');
                console.error(err.message);
                Promise.reject(err)
            });
        })
    }

    downloadPagamentosReceitas() {
        return this.http.get(`${Constants.urlWs}pagamentoReceita/usuario/${this.userId}`).then((lista: Array<BtpDespesa>) => {
            return this.pagamentoService.inserirPagamentosReceitasDownload(lista).then(() => {
                console.log('success download pagamentos  receitas');
            }).catch((err) => {
                console.log('error download pagamentos receitas');
                console.error(err.message);
                Promise.reject(err)
            });
        })
    }

}