import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLite } from 'ionic-native';

@Injectable()
export class DbProvider {

  public db: SQLite = null;

  public meses: string[] = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];

  constructor(public http: Http) {
    this.db = new SQLite();
  }

  openDatabase() {
    return this.db.openDatabase({
      name: 'controlv.db',
      location: 'default' // the location field is required
    });
  }

  deleteDatabase() {
    return SQLite.deleteDatabase({
      name: 'controlv.db',
      location: 'default' // the location field is required
    });
  }

  createTable() {
    //this.habilitarForeignKey();
    //this.dropTable('receitas');
    let promises = [];
    
    promises.push(this.createTableCategoria());
    promises.push(this.createTableProduto());
    promises.push(this.createTableCliente());
    promises.push(this.createTableFormaPagamento());
    promises.push(this.createTableFornecedor());
    promises.push(this.createTablePedido());
    promises.push(this.createTableItensPedido());
    promises.push(this.createTableDataImportante());
    promises.push(this.createTableConta());
    promises.push(this.createTableReceita());
    promises.push(this.createTableDespesa());
    promises.push(this.createTablePagamentoDespesa());
    promises.push(this.createTablePagamentoReceita());
    promises.push(this.createTableEstoque());
    promises.push(this.createTableItensEstoque());
    promises.push(this.createTableVenda());
    promises.push(this.createTableItensVenda());
    promises.push(this.createTableVisitas());
    promises.push(this.createTableItemFormaPagamentoVenda());
    promises.push(this.createTableCompra());
    promises.push(this.createTableProdutoCompra());
    promises.push(this.createTableItemFormaPagamentoCompra());
    promises.push(this.triggerCategoria());

    return Promise.all(promises);

  }

  habilitarForeignKey() {
    this.db.executeSql('PRAGMA foreign_keys = ON;', []).then((res) => {
      console.log("PRAGMA res: " + JSON.stringify(res))
      console.log("PRAGMA res: " + JSON.stringify(res.rows))
      console.log("PRAGMA res: " + JSON.stringify(res.rows.item(0)))
    });
  }

  triggerCategoria() {

    let sql = `CREATE TRIGGER IF NOT EXISTS TG_BI_CONTAS AFTER INSERT ON CONTAS
                FOR EACH ROW

                BEGIN
                  UPDATE CONTAS SET contaId = (SELECT ifnull(MAX(c2.contaId),1) FROM CONTAS c2 where c2.userId = NEW.userId ) 
                  WHERE contaId = NEW.contaId;
                END;
                `;

    this.db.executeSql(sql, []).then((res) => {
      console.log('SUCCESS CRIAR TRIGGER');
    }).catch(err=>{
      console.log(err.message);
    });
  }

  dropTable(table: string) {
    let sql = `DROP TABLE ` + table;
    return this.db.executeSql(sql, []);
  }

  createTableFornecedor() {
    let sql = `CREATE TABLE IF NOT EXISTS 
    fornecedores(fornId INTEGER PRIMARY KEY AUTOINCREMENT, 
               userId TEXT ,
               nomeFornecedor TEXTs,
               endereco TEXT, 
               cnpj TEXT, 
               telefone TEXT,
               isServidor BOOLEAN,
               ativo BOOLEAN)`;
    return this.db.executeSql(sql, []);
  }

  createTableCategoria() {
    let sql = `CREATE TABLE IF NOT EXISTS 
    categorias(categoriaId INTEGER PRIMARY KEY, 
               userId TEXT ,
               categoriaDsc TEXT, 
               tipo TEXT,
               cor TEXT,
               isServidor BOOLEAN,
               ativo BOOLEAN)`;
    return this.db.executeSql(sql, []);
  }

  createTableProduto() {
    let sql = `CREATE TABLE IF NOT EXISTS 
    produtos(produtoId INTEGER PRIMARY KEY, 
               userId TEXT ,
               categoriaId INTEGER, 
               produtoDsc TEXT,
               produtoUnd INTEGER,
               durabilidade INTEGER,
               precoUnitario INTEGER,
               precoUltCompra INTEGER,
               caracteristicas TEXT,
               isServidor BOOLEAN,
               ativo BOOLEAN)`;
    return this.db.executeSql(sql, []);
  }

  createTableCliente() {
    let sql = `CREATE TABLE IF NOT EXISTS 
    clientes(cliId INTEGER PRIMARY KEY AUTOINCREMENT, 
               userId TEXT ,
               categoriaId INTEGER, 
               nome TEXT,
               cpf TEXT,
               sexo TEXT,
               endereco TEXT,
               celular TEXT,
               email TEXT,
               dataNasc DATE,
               isServidor BOOLEAN,
               ativo BOOLEAN)`;
    return this.db.executeSql(sql, []);
  }

  createTableFormaPagamento() {
    let sql = `CREATE TABLE IF NOT EXISTS 
    formas_pagamento(idFormaPagamento INTEGER PRIMARY KEY AUTOINCREMENT, 
               userId TEXT ,
               descricao TEXT,
               isServidor BOOLEAN,
               ativo BOOLEAN)`;
    return this.db.executeSql(sql, []);
  }

  createTablePedido() {
    let sql = `CREATE TABLE IF NOT EXISTS 
    pedidos(pedidoId INTEGER PRIMARY KEY AUTOINCREMENT, 
               userId TEXT ,
               cliId INTEGER,
               dataPedido DATE,
               statusPedido INTEGER)`;
    return this.db.executeSql(sql, []);
  }

  createTableItensPedido() {
    let sql = `CREATE TABLE IF NOT EXISTS 
    itens_pedido(itemId INTEGER PRIMARY KEY AUTOINCREMENT, 
               userId TEXT ,
               pedidoId INTEGER ,
               produtoId INTEGER,
               qtdSolicitada INTEGER,
               qtdRecebida INTEGER,
               valor REAL)`;
    return this.db.executeSql(sql, []);
  }

  createTableProdutosPedido() {
    let sql = `CREATE TABLE IF NOT EXISTS 
    produtosPedidos(pedidoId INTEGER, 
               userId TEXT ,
               produtoId INTEGER,
               qtdProduto REAL,
               qtdAtendidaProduto REAL,
               valor REAL,
               PRIMARY KEY (pedidoId,produtoId))`;
    return this.db.executeSql(sql, []);
  }

  createTableDataImportante() {
    let sql = `CREATE TABLE IF NOT EXISTS 
    datasImportantes(idDataImportante INTEGER PRIMARY KEY AUTOINCREMENT, 
               userId TEXT ,
               cliId INTEGER,
               dscDataImportante TEXT, 
               tipDataImportante TEXT,
               dscGrauParentesco TEXT,
               sexo TEXT,
               diaMes DATE,
               grauParentesco INTEGER,
               isServidor BOOLEAN,
               ativo BOOLEAN,
               FOREIGN KEY(cliId) REFERENCES clientes (cliId) )`;
    return this.db.executeSql(sql, []).then(response => {
      console.log("criou table data importante");
    }).catch(error => {
      console.log("erro criar table data importante");
      console.error(error.message);
    });
  }

  createTableVisitas() {
    let sql = `CREATE TABLE IF NOT EXISTS 
    visitas(idVisita INTEGER PRIMARY KEY AUTOINCREMENT, 
               userId TEXT ,
               cliId INTEGER,
               localVisita TEXT, 
               dataVisita DATE,
               horaVisita TEXT,
               isServidor BOOLEAN,
               ativo BOOLEAN,
               FOREIGN KEY(cliId) REFERENCES clientes (cliId))`;
    return this.db.executeSql(sql, []).then(response => {
      console.log("criou table compra");
    }).catch(error => {
      console.log("erro criar table compra");
      console.error(error.message);
    });
  }

  createTableDespesa() {
    let sql = `CREATE TABLE IF NOT EXISTS 
    despesas(despesaId INTEGER PRIMARY KEY AUTOINCREMENT, 
               userId TEXT ,
               despesa TEXT,
               despesaDta DATE, 
               despesaVlr INTEGER,
               despesaVlrPago INTEGER,
               despesaPaga INTEGER,
               categoriaId INTEGER,
               compraId INTEGER,
               despesaIdPai INTEGER,
               contaId INTEGER,
               isServidor BOOLEAN,
               ativo BOOLEAN,
               FOREIGN KEY(categoriaId) REFERENCES categorias (categoriaId),
               FOREIGN KEY(contaId) REFERENCES contas (contaId))`;

    return this.db.executeSql(sql, []).then(response => {
      console.log("criou table despesa");
    }).catch(error => {
      console.log("erro criar table despesa");
      console.error(error.message);
    });
  }

  createTablePagamentoReceita() {
    let sql = `CREATE TABLE IF NOT EXISTS 
    pagamento_receitas(idPagamento INTEGER PRIMARY KEY AUTOINCREMENT, 
               userId TEXT ,
               valorPagamento INTEGER,
               datPagamento DATE, 
               receitaId INTEGER,
               isServidor BOOLEAN,
               ativo BOOLEAN)`;

    return this.db.executeSql(sql, []);
  }

  createTablePagamentoDespesa() {
    let sql = `CREATE TABLE IF NOT EXISTS 
    pagamento_despesas(idPagamento INTEGER PRIMARY KEY AUTOINCREMENT, 
               userId TEXT ,
               valorPagamento INTEGER,
               datPagamento DATE, 
               despesaId INTEGER,
               isServidor BOOLEAN,
               ativo BOOLEAN)`;

    return this.db.executeSql(sql, []);
  }

  createTableEstoque() {
    let sql = `CREATE TABLE IF NOT EXISTS 
    estoques(idEstoque INTEGER PRIMARY KEY AUTOINCREMENT, 
             dscEstoque TEXT, 
             userId TEXT,
             isServidor BOOLEAN,
             ativo BOOLEAN )`;
    return this.db.executeSql(sql, []);
  }

  createTableItensEstoque() {
    let sql = `CREATE TABLE IF NOT EXISTS 
    itens_estoque(itemEstoqueId INTEGER PRIMARY KEY AUTOINCREMENT, 
               userId TEXT ,
               idEstoque INTEGER ,
               produtoId INTEGER,
               qtdEstoque INTEGER,
               ativo BOOLEAN,
               FOREIGN KEY(produtoId) REFERENCES produtos (produtoId))`;
    return this.db.executeSql(sql, []);
  }

  createTableItensVenda() {
    let sql = `CREATE TABLE IF NOT EXISTS 
    itens_venda(itemVendaId INTEGER PRIMARY KEY AUTOINCREMENT, 
               userId TEXT ,
               vendaId INTEGER ,
               produtoId INTEGER,
               itemQtd INTEGER,
               vlrUnitario REAL,
               vlrTotal REAL,
               idEstoque INTEGER,
               isServidor BOOLEAN,
               ativo BOOLEAN,
               FOREIGN KEY(produtoId) REFERENCES produtos (produtoId),
               FOREIGN KEY(idEstoque) REFERENCES estoques (idEstoque))`;
    return this.db.executeSql(sql, []).then(response => {
      console.log("criou table itens venda");
    }).catch(error => {
      console.log("erro criar table itens venda");
      console.error(error.message);
    });
  }

  createTableVenda() {
    let sql = `CREATE TABLE IF NOT EXISTS 
    vendas(vendaId INTEGER PRIMARY KEY AUTOINCREMENT, 
               userId TEXT ,
               cliId INTEGER ,
               dataVenda DATE,
               vlrTotal REAL, 
               statusVenda INTEGER,
               isServidor BOOLEAN,
               ativo BOOLEAN,
               FOREIGN KEY(cliId) REFERENCES clientes (cliId))`;

    return this.db.executeSql(sql, []).then(response => {
      console.log("criou table venda");
    }).catch(error => {
      console.log("erro criar table venda");
      console.error(error.message);
    });;
  }

  createTableReceita() {
    let sql = `CREATE TABLE IF NOT EXISTS 
    receitas(receitaId INTEGER PRIMARY KEY AUTOINCREMENT, 
               userId TEXT ,
               receita TEXT,
               receitaDta DATE, 
               receitaVlr INTEGER,
               receitaVlrPago INTEGER,
               receitaPaga INTEGER,
               categoriaId INTEGER,
               vendaId INTEGER,
               receitaIdPai INTEGER,
               contaId INTEGER,
               isServidor BOOLEAN,
               ativo BOOLEAN,
               FOREIGN KEY(categoriaId) REFERENCES categorias (categoriaId),
               FOREIGN KEY(contaId) REFERENCES contas (contaId))`;
    return this.db.executeSql(sql, []).then(response => {
      console.log("criou table receita");
    }).catch(error => {
      console.log("erro criar table receita");
      console.error(error.message);
    });
  }

  createTableConta() {
    let sql = `CREATE TABLE IF NOT EXISTS 
    contas(contaId INTEGER PRIMARY KEY, 
               userId TEXT ,
               contaDsc TEXT,
               cor TEXT,
               isServidor BOOLEAN,
               ativo BOOLEAN)`;
    return this.db.executeSql(sql, []);
  }

  createTableItemFormaPagamentoVenda() {
    let sql = `CREATE TABLE IF NOT EXISTS 
    item_forma_pag_venda(itemId INTEGER PRIMARY KEY AUTOINCREMENT, 
               userId TEXT,
               vendaId INTEGER,
               numParcela INTEGER,
               datVencimento DATE,
               datPagamento DATE,
               vlrParcela INTEGER,
               vlrPago INTEGER,
               dscObservacao TEXT,
               idFormaPagamento INTEGER,
               ativo BOOLEAN,
               FOREIGN KEY(idFormaPagamento) REFERENCES formas_pagamento (idFormaPagamento) )`;
    return this.db.executeSql(sql, []).then(response => {
      console.log("criou table forma pagamento venda");
    }).catch(error => {
      console.log("erro criar table forma pagamento venda");
      console.error(error.message);
    });
  }

  createTableCompra() {
    let sql = `CREATE TABLE IF NOT EXISTS 
    compras(compraId INTEGER PRIMARY KEY AUTOINCREMENT, 
               userId TEXT ,
               fornId INTEGER ,
               dataCompra DATE,
               vlrTotal REAL, 
               statusCompra INTEGER,
               isServidor BOOLEAN,
               ativo BOOLEAN,
               FOREIGN KEY(fornId) REFERENCES fornecedores (fornId) )`;

    return this.db.executeSql(sql, []).then(response => {
      console.log("criou table compra");
    }).catch(error => {
      console.log("erro criar table compra");
      console.error(error.message);
    });
  }

  createTableProdutoCompra() {
    let sql = `CREATE TABLE IF NOT EXISTS 
    produto_compra(produtoCompraId INTEGER PRIMARY KEY AUTOINCREMENT, 
               userId TEXT ,
               compraId INTEGER ,
               produtoId INTEGER,
               itemQtd INTEGER,
               vlrUnitario REAL,
               vlrTotal REAL,
               idEstoque INTEGER,
               ativo BOOLEAN,
               FOREIGN KEY(produtoId) REFERENCES produtos (produtoId),
               FOREIGN KEY(idEstoque) REFERENCES estoques (idEstoque))`;
    return this.db.executeSql(sql, []).then(response => {
      console.log("criou table itens compra");
    }).catch(error => {
      console.log("erro criar table itens compra");
      console.error(error.message);
    });
  }

  createTableItemFormaPagamentoCompra() {
    let sql = `CREATE TABLE IF NOT EXISTS 
    item_forma_pag_compra(itemId INTEGER PRIMARY KEY AUTOINCREMENT, 
               userId TEXT,
               compraId INTEGER,
               numParcela INTEGER,
               datVencimento DATE,
               datPagamento DATE,
               vlrParcela INTEGER,
               vlrPago INTEGER,
               dscObservacao TEXT,
               idFormaPagamento INTEGER,
               ativo BOOLEAN,
               FOREIGN KEY(idFormaPagamento) REFERENCES formas_pagamento (idFormaPagamento)  )`;
    return this.db.executeSql(sql, []).then(response => {
      console.log("criou table forma pagamento compra");
    }).catch(error => {
      console.log("erro criar table forma pagamento compra");
      console.error(error.message);
    });
  }

}