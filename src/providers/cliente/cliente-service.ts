import { SQLite } from 'ionic-native';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import * as moment from 'moment';

import { BtpCliente } from './../../model/cliente';
import { Categoria } from './../../model/categoria';
import { DbProvider } from './../db-provider';
import { Usuario } from "./../../model/usuario";

@Injectable()
export class ClienteService {

  public db: SQLite = null;
  constructor(public http: Http, public dbProvider: DbProvider) {
    this.db = dbProvider.db;
  }

  getAll(userId: number) {

    let sql = `SELECT c.cliId, c.nome , c.cpf ,
                c.sexo, c.endereco, c.celular, c.email , c.dataNasc, c.isServidor ,c.ativo,
                cat.categoriaId as categoriaId, cat.CategoriaDsc as categoriaDsc, 
                cat.tipo as tipo, cat.cor as cor, c.userId
               FROM clientes c
               LEFT JOIN categorias cat
                ON cat.categoriaId = c.categoriaId
               WHERE c.userId = ?
                AND c.ativo = ?
               ORDER BY nome`;

    return this.db.executeSql(sql, [userId, true])
      .then(response => {
        let clientes = [];
        for (let index = 0; index < response.rows.length; index++) {
          let btpCliente: BtpCliente = response.rows.item(index);
          let categoria = new Categoria();

          let usuario: Usuario = new Usuario();
          usuario.idUsuario = response.rows.item(index).userId;

          categoria.categoriaId = response.rows.item(index).categoriaId;
          categoria.categoriaDsc = response.rows.item(index).categoriaDsc;
          categoria.tipo = response.rows.item(index).tipo;
          categoria.cor = response.rows.item(index).cor;
          categoria.usuario = usuario;
          btpCliente.categoria = categoria;

          btpCliente.usuario = usuario;

          clientes.push(btpCliente);
        }
        console.log("executou consulta: getAll clientes................");
        return Promise.resolve(clientes);
      });
  }

  getClientesNaoServidor(userId: number) {

    let sql = `SELECT c.cliId, c.nome , c.cpf ,
                c.sexo, c.endereco, c.celular, c.email , c.dataNasc, c.isServidor ,c.ativo,
                cat.categoriaId as categoriaId, cat.CategoriaDsc as categoriaDsc, 
                cat.tipo as tipo, cat.cor as cor, c.userId
               FROM clientes c
               LEFT JOIN categorias cat
                ON cat.categoriaId = c.categoriaId
               WHERE c.userId = ?
                AND c.isServidor = ?
               ORDER BY nome`;

    return this.db.executeSql(sql, [userId, false])
      .then(response => {
        let clientes = [];
        for (let index = 0; index < response.rows.length; index++) {
          let btpCliente: BtpCliente = response.rows.item(index);
          let categoria = new Categoria();

          let usuario: Usuario = new Usuario();
          usuario.idUsuario = response.rows.item(index).userId;

          categoria.categoriaId = response.rows.item(index).categoriaId;
          categoria.categoriaDsc = response.rows.item(index).categoriaDsc;
          categoria.tipo = response.rows.item(index).tipo;
          categoria.cor = response.rows.item(index).cor;
          categoria.usuario = usuario;

          btpCliente.categoria = categoria;

          btpCliente.usuario = usuario;

          clientes.push(btpCliente);
        }
        console.log("executou consulta: getAll clientes................");
        return Promise.resolve(clientes);
      });
  }

  incluir(btpCliente: BtpCliente) {
    let sql = `INSERT INTO clientes( 
               userId ,
               categoriaId , 
               nome ,
               cpf ,
               sexo ,
               endereco ,
               celular ,
               email ,
               dataNasc,
               isServidor,
               ativo )    
    VALUES(?,?,?,?,?,?,?,?,?,?,?)`;

    btpCliente.ativo = true;

    return this.db.executeSql(sql, [btpCliente.usuario.idUsuario,
    btpCliente.categoria.categoriaId,
    btpCliente.nome,
    btpCliente.cpf,
    btpCliente.sexo,
    btpCliente.endereco,
    btpCliente.celular,
    btpCliente.email,
    new Date(btpCliente.dataNasc).toISOString(),
    btpCliente.isServidor,
    btpCliente.ativo]);
  }

  alterar(btpCliente: BtpCliente) {
    let sql = `UPDATE clientes 
           SET categoriaId=? , 
               nome=? ,
               cpf=? ,
               sexo=? ,
               endereco=? ,
               celular=? ,
               email=? ,
               dataNasc=?,
               isServidor = ?,
               ativo = ?
    WHERE cliId=?`;

    return this.db.executeSql(sql, [btpCliente.categoria.categoriaId,
    btpCliente.nome, btpCliente.cpf, btpCliente.sexo, btpCliente.endereco,
    btpCliente.celular, btpCliente.email, btpCliente.dataNasc, btpCliente.isServidor,
    btpCliente.ativo, btpCliente.cliId]);
  }

  deletar(btpCliente: BtpCliente) {
    let sql = 'DELETE FROM clientes WHERE cliId=?';
    return this.db.executeSql(sql, [btpCliente.cliId]);
  }

  alterarClientesGravadoServidor(clientes: BtpCliente[]) {

    let promises = [];

    clientes.forEach(cliente => {
      cliente.isServidor = true;
      promises.push(this.alterar(cliente));
    });

    return Promise.all(promises);
  }

  inserirClientesDownload(lista: Array<any>) {

    let promises = [];
    lista.forEach(element => {
      let cliente: BtpCliente = element
      cliente.cliId = element.chave.id;
      cliente.usuario = new Usuario();
      cliente.usuario.idUsuario = element.chave.idUsuario;
      
      cliente.categoria = new Categoria();
      cliente.categoria.categoriaId = element.categoriaId;

      cliente.isServidor = true;
      promises.push(this.incluirComId(cliente));
    });
    return Promise.all(promises);
  }

  incluirComId(btpCliente: BtpCliente) {
    let sql = `INSERT INTO clientes(
               cliId,
               userId ,
               categoriaId , 
               nome ,
               cpf ,
               sexo ,
               endereco ,
               celular ,
               email ,
               dataNasc,
               isServidor,
               ativo )    
    VALUES(?,?,?,?,?,?,?,?,?,?,?,?)`;

    return this.db.executeSql(sql, [
      btpCliente.cliId,
      btpCliente.usuario.idUsuario,
      btpCliente.categoria.categoriaId,
      btpCliente.nome,
      btpCliente.cpf,
      btpCliente.sexo,
      btpCliente.endereco,
      btpCliente.celular,
      btpCliente.email,
      new Date(btpCliente.dataNasc).toISOString(),
      btpCliente.isServidor,
      btpCliente.ativo]);
  }

}
