import { CategoriaWsProvider } from './../categoria/categoria-ws';
import { HttpProvider } from './../http/http-provider';
import { Usuario } from '../../model/usuario';
import { UtilsService } from './../utils-service';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Constants } from "../../app/constants";
import { ClienteService } from "./cliente-service";
import { BtpCliente } from "../../model/cliente";

@Injectable()
export class ClienteWsProvider {

  constructor(public http: HttpProvider, public utils: UtilsService, public clienteService: ClienteService,
    public categoriaWs: CategoriaWsProvider) {
  }

  enviarDependencias() {
    let promises = [];
    promises.push(this.categoriaWs.enviarCategoriasServidor());
    return Promise.all(promises);
  }

  enviarClientesServidor() {
    return this.enviarDependencias().then(() => {
      return this.getClientesNaoServidor().then((clientes: BtpCliente[]) => {

        if (clientes == null || clientes.length == 0) {
          return Promise.resolve(null);
        }

        //envia contas para WS
        return this.http.post(`${Constants.urlWs}cliente/salvarLst`, clientes).then(() => {
          //caso dê certo, altera no sqlite contas para contas gravadas no servidor
          return this.clienteService.alterarClientesGravadoServidor(clientes);
        });
      });
    });
  }

  enviarClientesServidorSemDependencias() {
      return this.getClientesNaoServidor().then((clientes: BtpCliente[]) => {

        if (clientes == null || clientes.length == 0) {
          return Promise.resolve(null);
        }

        //envia contas para WS
        return this.http.post(`${Constants.urlWs}cliente/salvarLst`, clientes).then(() => {
          //caso dê certo, altera no sqlite contas para contas gravadas no servidor
          return this.clienteService.alterarClientesGravadoServidor(clientes);
        });
    });
  }

  getClientesNaoServidor() {
    return this.utils.getUsuarioLogado().then((usuario: Usuario) => {

      return this.clienteService.getClientesNaoServidor(usuario.idUsuario);
    });
  }

}
