import { ToastController } from 'ionic-angular';
import { Injectable } from '@angular/core';

@Injectable()
export class MensagemProvider {

    static MSG_INFORMACAO:string="msgInformacao";
    static MSG_ADVERTENCIA:string="msgAdvertencia";
    static MSG_SUCESSO:string="msgSucesso";
    static MSG_ERRO:string="msgErro";

    static TOP:string="top";
    static BOT:string="bottom"
    static MIDLE:string="middle";

    constructor(public toastCtrl: ToastController) {

    }

    addMensagemSucesso(msg:string, duration:number, position: string) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: duration,
            cssClass: MensagemProvider.MSG_SUCESSO,
            position: position,
            dismissOnPageChange: true
        });
        return toast.present();
    }

    addMensagemInformacao(msg:string, duration:number, position: string) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: duration,
            cssClass: MensagemProvider.MSG_INFORMACAO,
            position: position,
            dismissOnPageChange: true
        });
        return toast.present();
    }

    addMensagemAdvertencia(msg:string, duration:number, position: string) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: duration,
            cssClass: MensagemProvider.MSG_ADVERTENCIA,
            position: position,
            dismissOnPageChange: true
        });
        return toast.present();
    }

    addMensagemErro(msg:string, duration:number, position: string) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: duration,
            cssClass: 'msgErro',
            position: position,
            dismissOnPageChange: true
        });
        return toast.present();
    }

}