import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { SQLite } from 'ionic-native';

import { DbProvider } from "../db-provider";
import { Produto } from "../../model/produto";
import { Categoria } from "../../model/categoria";
import { Usuario } from "../../model/usuario";


@Injectable()
export class ProdutoService {

  public db: SQLite = null;

  constructor(public http: Http, public dbProvider: DbProvider) {
    this.db = dbProvider.db;

  }

  getAll(userId: number) {
    let sql = `SELECT p.*,
    c.categoriaId as categoriaId, c.categoriaDsc as categoriaDsc, c.tipo as tipo
    FROM produtos p
    LEFT JOIN categorias c
    on c.categoriaId = p.categoriaId
    WHERE p.userId = ?
    AND p.ativo = ?
    ORDER BY produtoDsc`;
    return this.db.executeSql(sql, [userId, true])
      .then(response => {
        let produtos = [];
        for (let index = 0; index < response.rows.length; index++) {
          let produto: Produto = response.rows.item(index);
          console.log(response.rows.item(index).caracteristicas);

          let usuario = new Usuario();
          usuario.idUsuario = response.rows.item(index).userId;
          produto.usuario = usuario;

          let categoria = new Categoria();
          categoria.categoriaId = response.rows.item(index).categoriaId;
          categoria.categoriaDsc = response.rows.item(index).categoriaDsc;
          categoria.tipo = response.rows.item(index).tipo;
          categoria.usuario = usuario;

          produto.categoria = categoria;
          produtos.push(produto);
        }
        console.log(produtos);
        return Promise.resolve(produtos);
      })
  }

  incluirProduto(produto: Produto) {
    let sql = `INSERT INTO produtos(UserId, categoriaId, produtoDsc, produtoUnd, durabilidade, precoUnitario, precoUltCompra, caracteristicas, ativo, isServidor)     
    VALUES(?,?,?,?,?,?,?,?,?,?)`;

    console.log('id: ' + produto.categoria.categoriaId);
    produto.ativo = true;
    return this.db.executeSql(sql, [produto.usuario.idUsuario,
    produto.categoria.categoriaId,
    produto.produtoDsc,
    produto.produtoUnd,
    produto.durabilidade,
    produto.precoUnitario,
    produto.precoUltCompra,
    produto.caracteristicas,
    produto.ativo,
    produto.isServidor]);
  }

  delete(produto: Produto) {
    let sql = 'DELETE FROM produtos WHERE produtoId=?';
    return this.db.executeSql(sql, [produto.produtoId]);
  }

  update(produto: Produto) {
    let sql = `UPDATE produtos 
    SET produtoDsc=?, 
    produtoUnd=?, 
    durabilidade=?, 
    precoUnitario=?, 
    precoUltCompra=?,
    categoriaId=? ,
    caracteristicas=?,
    ativo=?,
    isServidor=?
    WHERE produtoId=?`;
    console.log(produto.caracteristicas);
    return this.db.executeSql(sql, [produto.produtoDsc, produto.produtoUnd,
    produto.durabilidade, produto.precoUnitario, produto.precoUltCompra,
    produto.categoria.categoriaId, produto.caracteristicas, produto.ativo, produto.isServidor, produto.produtoId]);
  }

  alterarProdutossGravadoServidor(produtos: Produto[]) {
    let promises = [];

    produtos.forEach(produto => {
      produto.isServidor = true;
      promises.push(this.update(produto));
    });

    return Promise.all(promises);
  }

  getProdutosNaoServidor(userId: number) {
    let sql = `SELECT p.*,
    c.categoriaId as categoriaId, c.categoriaDsc as categoriaDsc, c.tipo as tipo
    FROM produtos p
    LEFT JOIN categorias c
    on c.categoriaId = p.categoriaId
    WHERE p.userId = ?
    AND p.isServidor = ?
    ORDER BY produtoDsc`;
    return this.db.executeSql(sql, [userId, false])
      .then(response => {
        let produtos = [];
        for (let index = 0; index < response.rows.length; index++) {
          let produto: Produto = response.rows.item(index);
          console.log(response.rows.item(index).caracteristicas);
          let usuario = new Usuario();
          usuario.idUsuario = response.rows.item(index).userId;
          produto.usuario = usuario;

          let categoria = new Categoria();
          categoria.categoriaId = response.rows.item(index).categoriaId;
          categoria.categoriaDsc = response.rows.item(index).categoriaDsc;
          categoria.tipo = response.rows.item(index).tipo;
          categoria.usuario = usuario;

          produto.categoria = categoria;
          produtos.push(produto);
        }
        console.log(produtos);
        return Promise.resolve(produtos);
      })
  }

  inserirProdutosDownload(lista: Array<any>) {

    let promises = [];
    lista.forEach(element => {
      let produto: Produto = element;
      produto.produtoId = element.chave.id;
      produto.usuario = new Usuario();
      produto.usuario.idUsuario = element.chave.idUsuario;

      produto.categoria = new Categoria();
      produto.categoria.categoriaId = element.categoriaId;

      produto.isServidor = true;
      promises.push(this.incluirComId(produto));
    });
    return Promise.all(promises);
  }

  incluirComId(produto: Produto) {
    let sql = `INSERT INTO produtos(
      produtoId,
      UserId, 
      categoriaId, 
      produtoDsc, 
      produtoUnd, 
      durabilidade, 
      precoUnitario, 
      precoUltCompra, 
      caracteristicas, 
      ativo, 
      isServidor)     
    VALUES(?,?,?,?,?,?,?,?,?,?,?)`;

    return this.db.executeSql(sql, [
      produto.produtoId,
      produto.usuario.idUsuario,
      produto.categoria.categoriaId,
      produto.produtoDsc,
      produto.produtoUnd,
      produto.durabilidade,
      produto.precoUnitario,
      produto.precoUltCompra,
      produto.caracteristicas,
      produto.ativo,
      produto.isServidor]);
  }

}