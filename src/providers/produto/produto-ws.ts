import { HttpProvider } from './../http/http-provider';
import { Usuario } from '../../model/usuario';
import { UtilsService } from './../utils-service';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { CategoriaWsProvider } from './../categoria/categoria-ws';
import { Constants } from "../../app/constants";
import { ProdutoService } from "./produto-service";
import { Produto } from "../../model/produto";

@Injectable()
export class ProdutoWsProvider {

  constructor(public http: HttpProvider, public utils: UtilsService, public produtoService: ProdutoService,
    public categoriaWs: CategoriaWsProvider) {
  }

  enviarDependencias() {
    let promises = [];
    promises.push(this.categoriaWs.enviarCategoriasServidor());
    return Promise.all(promises);
  }

  enviarProdutoServidor() {

    return this.enviarDependencias().then(() => {
      return this.getProdutosNaoServidor().then((produtos: Produto[]) => {
        if (produtos == null || produtos.length == 0) {
          return Promise.resolve(null);
        }
        //envia produtos para WS
        return this.http.post(`${Constants.urlWs}produto/salvarLst`, produtos).then(() => {
          //caso dê certo, altera no sqlite produtos para produtos gravadas no servidor
          return this.produtoService.alterarProdutossGravadoServidor(produtos);
        });
      });
    });
  }

  enviarProdutoServidorSemDependencias() {

    return this.getProdutosNaoServidor().then((produtos: Produto[]) => {
      if (produtos == null || produtos.length == 0) {
        return Promise.resolve(null);
      }
      //envia produtos para WS
      return this.http.post(`${Constants.urlWs}produto/salvarLst`, produtos).then(() => {
        //caso dê certo, altera no sqlite produtos para produtos gravadas no servidor
        return this.produtoService.alterarProdutossGravadoServidor(produtos);
      });
    });
  }

  getProdutosNaoServidor() {
    return this.utils.getUsuarioLogado().then((usuario: Usuario) => {

      return this.produtoService.getProdutosNaoServidor(usuario.idUsuario);
    });
  }

}
