import { Network } from '@ionic-native/network';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

@Injectable()
export class NetworkProvider {

  constructor(public network: Network) {
  }

  existeConexao():boolean{
    console.log(this.network.type);
    if(this.network.type == 'none'){
      return false;
    }
    return true;
  }
}
