import { Produto } from './../model/produto';
import { StatusPedido } from './../model/status-pedido';
import { BtpItemPedido } from './../model/btpItemPedido';
import { BtpCliente } from './../model/cliente';
import { BtpPedido } from './../model/btpPedido';
import { DbProvider } from './db-provider';
import { SQLite } from 'ionic-native';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class PedidoService {

  public db: SQLite = null;

  constructor(public http: Http, public dbProvider: DbProvider) {
    this.db = dbProvider.db;
  }

  getAll(userId: number) {
    let sql = `SELECT p.*, c.nome
                FROM pedidos p
                INNER JOIN clientes c
                on c.cliId = p.cliId
                WHERE p.userId = ?`;

    return this.db.executeSql(sql, [userId])
      .then(response => {
        console.log("teste");
        let pedidos = [];
        for (let index = 0; index < response.rows.length; index++) {
          console.log(response.rows.item(index).cliId);
          let btpPedido: BtpPedido = response.rows.item(index);
          let btpCliente: BtpCliente = new BtpCliente();
          btpCliente.cliId = response.rows.item(index).cliId;
          btpCliente.nome = response.rows.item(index).nome;
          btpPedido.btpCliente = btpCliente;
          pedidos.push(btpPedido);
        }
        console.log("getAll Pedidos.......")
        return Promise.resolve(pedidos);
      })
      .catch(e => {
        console.log(e.msg);
        return null;
      });
  }

  getAllPorStatus(userId: number, statusPedido: StatusPedido) {
    let sql = `SELECT p.*, c.nome, 
                (SELECT SUM(ip.qtdSolicitada*ip.valor) FROM itens_pedido ip WHERE ip.pedidoId = p.pedidoId) as totalPedido
                FROM pedidos p
                INNER JOIN clientes c
                on c.cliId = p.cliId
                WHERE p.userId = ? AND p.statusPedido = ?`;

    return this.db.executeSql(sql, [userId, statusPedido])
      .then(response => {
        let pedidos = [];
        for (let index = 0; index < response.rows.length; index++) {
          let btpPedido: BtpPedido = response.rows.item(index);
          let btpCliente: BtpCliente = new BtpCliente();
          btpCliente.cliId = response.rows.item(index).cliId;
          btpCliente.nome = response.rows.item(index).nome;
          btpPedido.btpCliente = btpCliente;
          pedidos.push(btpPedido);
        }
        console.log("getAll Pedidos.......")
        return Promise.resolve(pedidos);
      })
      .catch(e => {
        console.log(e.msg);
        return null;
      });
  }

  incluir(btpPedido: BtpPedido) {
    let sql = `INSERT INTO pedidos(userId,
               cliId,
               dataPedido,
               statusPedido)
                VALUES(?,?,?,?)`;
    console.log("Inserindo Pedido");
    let idInserido: number;
    this.db.transaction(
      tx => {
        tx.executeSql(sql, [btpPedido.usuario.idUsuario,
        btpPedido.btpCliente.cliId, btpPedido.dataPedido, btpPedido.statusPedido], (tx, resultSet) => {
          btpPedido.pedidoId = resultSet.insertId;
          console.log('resultSet.insertId: ' + resultSet.insertId);
          console.log('resultSet.rowsAffected: ' + resultSet.rowsAffected);
        }, function (tx, error) {
          console.log('INSERT error: ' + error.message);
        });
      });
      return idInserido;
  }

  teste() {

  }

  alterarPedido(btpPedido: BtpPedido) {
    let sql = `UPDATE pedidos
               SET cliId=?,
               dataPedido=?,
               statusPedido=?
               WHERE pedidoId=?`;
    console.log("Update Pedido");
    return this.db.executeSql(sql, [btpPedido.btpCliente.cliId,
    btpPedido.dataPedido, btpPedido.statusPedido, btpPedido.pedidoId]);
  }

  fecharPedido(pedidoId: number) {
    let sql = `UPDATE pedidos
               SET statusPedido=?
               WHERE pedidoId=?`;
    console.log("Fechando Pedido");
    return this.db.executeSql(sql, [StatusPedido.ENCERRADO, pedidoId]);
  }

  deletarPedido(pedidoId: number) {
    let sql = 'DELETE FROM pedidos WHERE pedidoId=?';
    return this.db.executeSql(sql, [pedidoId]);
  }

  incluirItemPedido(btpItemPedido: BtpItemPedido) {
    let sql = `INSERT INTO itens_pedido(userId,
               pedidoId,
               produtoId,
               qtdSolicitada,
               qtdRecebida,
               valor)
                VALUES(?,?,?,?,?,?)`;
    console.log("Inserindo Item Pedido");
    console.log("pedido:" + btpItemPedido.btpPedido.pedidoId);

    return this.db.executeSql(sql, [btpItemPedido.usuario.idUsuario,
    btpItemPedido.btpPedido.pedidoId, btpItemPedido.produto.produtoId,
    btpItemPedido.qtdSolicitada, btpItemPedido.qtdAtendida, btpItemPedido.valor]);
  }

  alterarItemPedido(btpItemPedido: BtpItemPedido) {
    let sql = `UPDATE itens_pedido 
               SET  qtdSolicitada = ?,
                    qtdRecebida = ?,
                    valor = ?
                WHERE itemId=?`;
    console.log("Alterando Item Pedido");
    return this.db.executeSql(sql, [btpItemPedido.qtdSolicitada, btpItemPedido.qtdAtendida,
    btpItemPedido.valor, btpItemPedido.itemId]);
  }

  getItensPedido(pedidoId: number) {
    let sql = `SELECT ip.*, p.produtoId, p.produtoDsc
                FROM itens_pedido ip
                INNER JOIN produtos p
                on p.produtoId = ip.produtoId
                WHERE ip.pedidoId = ?`;

    return this.db.executeSql(sql, [pedidoId])
      .then(response => {
        let itensPedido = [];
        for (let index = 0; index < response.rows.length; index++) {
          console.log(response.rows.item(index).cliId);
          let btpItemPedido: BtpItemPedido = response.rows.item(index);
          btpItemPedido.qtdAtendida = response.rows.item(index).qtdRecebida;

          let produto: Produto = new Produto();
          produto.produtoId = response.rows.item(index).produtoId;
          produto.produtoDsc = response.rows.item(index).produtoDsc;
          btpItemPedido.produto = produto;
          itensPedido.push(btpItemPedido);
        }
        console.log("getAll Itens Pedidos.......")
        return Promise.resolve(itensPedido);
      })
      .catch(e => {
        console.log("Erro recuperar itens Pedido: " + e.msg);
        return null;
      });
  }

  deletarItemPedido(itemId: number) {
    let sql = 'DELETE FROM itens_pedido WHERE itemId=?';
    return this.db.executeSql(sql, [itemId]);
  }

}
