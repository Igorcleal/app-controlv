import { DbProvider } from './db-provider';
import { SQLite } from 'ionic-native';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class DashboardService {

  public db: SQLite = null;

  constructor(public http: Http, public dbProvider: DbProvider) {
    this.db = dbProvider.db;
  }

  getValorReceitasAReceberMes(userId: number, data: Date) {
    let sql = `SELECT sum(r.receitaVlr) - sum(ifnull(r.receitaVlrPago,0)) as valor
               FROM receitas r
              WHERE r.userId = ?
               AND strftime('%m%Y',r.receitaDta) = ?
               AND r.receitaPaga = 'false'`;

    return this.db.executeSql(sql, [userId, this.dbProvider.meses[data.getMonth()] + "" + data.getFullYear()])
      .then(response => {
        let valor: number;
        for (let index = 0; index < response.rows.length; index++) {
          valor = response.rows.item(index).valor;
        }
        console.log("executou consulta: valor receber mês atual................");
        console.log(valor);
        return Promise.resolve(valor);
      }).catch((err: Error) => {
        console.log(err.message);
        console.log(err.stack);
      });
  }

  getValorReceitasRecebidasMes(userId: number, data: Date) {
    let sql = `
            SELECT sum(valor) as valor FROM (
              SELECT sum(r.receitaVlr) as valor
               FROM receitas r
              WHERE r.userId = ?
               AND strftime('%m%Y',r.receitaDta) = ?
               AND r.receitaPaga = 'true'
               
               UNION 
               
               SELECT sum(ifnull(r.receitaVlrPago,0)) as valor
               FROM receitas r
              WHERE r.userId = ?
               AND strftime('%m%Y',r.receitaDta) = ?
               AND r.receitaPaga = 'false' 
            )`;

    return this.db.executeSql(sql, [userId, this.dbProvider.meses[data.getMonth()] + "" + data.getFullYear(),
      userId, this.dbProvider.meses[data.getMonth()] + "" + data.getFullYear()])
      .then(response => {
        let valor: number;
        for (let index = 0; index < response.rows.length; index++) {
          valor = response.rows.item(index).valor;
        }
        console.log("executou consulta: valor receber mês atual................");
        console.log(valor);
        return Promise.resolve(valor);
      }).catch((err: Error) => {
        console.log(err.message);
        console.log(err.stack);
      });
  }

  getValorReceitasAReceberTotal(userId: number) {
    let sql = `SELECT sum(r.receitaVlr) - sum(ifnull(r.receitaVlrPago,0)) as valor
               FROM receitas r
              WHERE r.userId = ?
               AND r.receitaPaga = 'false'`;

    return this.db.executeSql(sql, [userId])
      .then(response => {
        let valor: number;
        for (let index = 0; index < response.rows.length; index++) {
          valor = response.rows.item(index).valor;
        }
        console.log("executou consulta: valor receber mês atual................");
        console.log(valor);
        return Promise.resolve(valor);
      }).catch((err: Error) => {
        console.log(err.message);
        console.log(err.stack);
      });
  }

  getValorReceitasRecebidasTotal(userId: number) {
    let sql = `
            SELECT sum(valor) as valor FROM (
              SELECT sum(r.receitaVlr) as valor
               FROM receitas r
              WHERE r.userId = ?
               AND r.receitaPaga = 'true' 
               UNION 
               SELECT sum(ifnull(r.receitaVlrPago,0)) as valor
               FROM receitas r
              WHERE r.userId = ?
               AND r.receitaPaga = 'false' 
            )`;

    return this.db.executeSql(sql, [userId, userId])
      .then(response => {
        let valor: number;
        for (let index = 0; index < response.rows.length; index++) {
          valor = response.rows.item(index).valor;
        }
        console.log("executou consulta: valor receber mês atual................");
        console.log(valor);
        return Promise.resolve(valor);
      }).catch((err: Error) => {
        console.log(err.message);
        console.log(err.stack);
      });
  }

  getValorDespesasAPagarMes(userId: number, data: Date) {
    let sql = `SELECT sum(d.despesaVlr) - sum(ifnull(d.despesaVlrPago,0)) as valor
               FROM despesas d
              WHERE d.userId = ?
               AND strftime('%m%Y',d.despesaDta) = ?
               AND d.despesaPaga = 'false'`;

    return this.db.executeSql(sql, [userId, this.dbProvider.meses[data.getMonth()] + "" + data.getFullYear()])
      .then(response => {
        let valor: number;
        for (let index = 0; index < response.rows.length; index++) {
          valor = response.rows.item(index).valor;
        }
        console.log("executou consulta: valor receber mês atual................");
        console.log(valor);
        return Promise.resolve(valor);
      }).catch((err: Error) => {
        console.log(err.message);
        console.log(err.stack);
      });
  }

  getValorDespesasPagasMes(userId: number, data: Date) {
    let sql = `
            SELECT sum(valor) as valor FROM (
              SELECT sum(d.despesaVlr) as valor
               FROM despesas d
              WHERE d.userId = ?
               AND strftime('%m%Y',d.despesaDta) = ?
               AND d.despesaPaga = 'true'
               
               UNION 

               SELECT sum(ifnull(d.despesaVlrPago,0)) as valor
               FROM despesas d
              WHERE d.userId = ?
               AND strftime('%m%Y',d.despesaDta) = ?
               AND d.despesaPaga = 'false'
            )`;

    return this.db.executeSql(sql, [userId, this.dbProvider.meses[data.getMonth()] + "" + data.getFullYear(),
      userId, this.dbProvider.meses[data.getMonth()] + "" + data.getFullYear()])
      .then(response => {
        let valor: number;
        for (let index = 0; index < response.rows.length; index++) {
          valor = response.rows.item(index).valor;
        }
        console.log("executou consulta: valor receber mês atual................");
        console.log(valor);
        return Promise.resolve(valor);
      }).catch((err: Error) => {
        console.log(err.message);
        console.log(err.stack);
      });
  }

  getValorDespesasAPagarTotal(userId: number) {
    let sql = `SELECT sum(d.despesaVlr) - sum(ifnull(d.despesaVlrPago,0)) as valor
               FROM despesas d
              WHERE d.userId = ?
               AND d.despesaPaga = 'false'`;

    return this.db.executeSql(sql, [userId])
      .then(response => {
        let valor: number;
        for (let index = 0; index < response.rows.length; index++) {
          valor = response.rows.item(index).valor;
        }
        console.log("executou consulta: valor receber mês atual................");
        console.log(valor);
        return Promise.resolve(valor);
      }).catch((err: Error) => {
        console.log(err.message);
        console.log(err.stack);
      });
  }

  getValorDespesasPagasTotal(userId: number) {
    let sql = `
            SELECT sum(valor) as valor FROM (
              SELECT sum(d.despesaVlr) as valor
               FROM despesas d
              WHERE d.userId = ?
               AND d.despesaPaga = 'true'
               
               UNION 

               SELECT sum(ifnull(d.despesaVlrPago,0)) as valor
               FROM despesas d
              WHERE d.userId = ?
               AND d.despesaPaga = 'false'
            )`;

    return this.db.executeSql(sql, [userId, userId])
      .then(response => {
        let valor: number;
        for (let index = 0; index < response.rows.length; index++) {
          valor = response.rows.item(index).valor;
        }
        console.log("executou consulta: valor receber mês atual................");
        console.log(valor);
        return Promise.resolve(valor);
      }).catch((err: Error) => {
        console.log(err.message);
        console.log(err.stack);
      });
  }

}
