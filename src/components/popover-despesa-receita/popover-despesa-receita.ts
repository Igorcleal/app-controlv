import { ViewController, NavParams } from 'ionic-angular';
import { Component } from '@angular/core';

@Component({
  selector: 'popover-despesa-receita',
  templateUrl: 'popover-despesa-receita.html'
})
export class PopoverDespesaReceita {

  exibirTodos:boolean;

  constructor(public viewCtrl: ViewController, public navParams:NavParams) {
    this.exibirTodos = navParams.get("exibirTodos");
  }

  consultar(){
    this.viewCtrl.dismiss("consultar");
  }

  exibir(){
    this.viewCtrl.dismiss("exibir");
  }

}
