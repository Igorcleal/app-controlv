import { ViewController } from 'ionic-angular';
import { Component } from '@angular/core';

import { EnumTipoCalendario } from '../../model/enum-tipo-calendario';

@Component({
  selector: 'popover-calendario',
  templateUrl: 'popover-calendario.html'
})
export class PopoverCalendarioComponent {

  tipoCalendario: EnumTipoCalendario;
  EnumTipoCalendario : typeof EnumTipoCalendario = EnumTipoCalendario;

  constructor(public viewCtrl: ViewController) {}

  close(){
    this.viewCtrl.dismiss(this.tipoCalendario);
  }

  calendarioSelected(tipoCalendario:EnumTipoCalendario) {
    this.tipoCalendario = tipoCalendario;
    this.close();
  }

}
