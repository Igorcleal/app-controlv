import { ViewController } from 'ionic-angular';
import { Component } from '@angular/core';

import { TipoCategoria } from '../../model/tipo-categoria'

@Component({
  selector: 'popover-categoria',
  templateUrl: 'popover-categoria.html'
})
export class PopoverCategoriaComponent {

  tipoCategoria: TipoCategoria;
  TipoCategoria : typeof TipoCategoria = TipoCategoria;

  constructor(public viewCtrl: ViewController) {}

  close(){
    this.viewCtrl.dismiss(this.tipoCategoria);
  }

  categoriaSelecionada(tipoCategoria:TipoCategoria) {
    this.tipoCategoria = tipoCategoria;
    this.close();
  }

}
