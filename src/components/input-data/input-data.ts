import { DatePicker } from '@ionic-native/date-picker';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'input-data',
  templateUrl: 'input-data.html'
})
export class InputDataComponent {

  @Input('label')
  label: string;

  @Input('valueInicial')
  dataFormatada: string;

  @Output() selecionarData = new EventEmitter();

  constructor(private datePicker: DatePicker) {
  }

  abrirPickDate() {
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
    }).then(
      date => {
        let dataRetorno: Date = new Date();
        dataRetorno.setUTCFullYear(date.getUTCFullYear());
        dataRetorno.setUTCDate(date.getUTCDate());
        dataRetorno.setUTCMonth(date.getUTCMonth());
        this.dataFormatada = dataRetorno.getDate() + "/" + (dataRetorno.getMonth() + 1) + "/" + dataRetorno.getFullYear();
        this.selecionarData.emit(dataRetorno.toISOString());
      },
      err => console.log('Error occurred while getting date: ', err)
      );
  }

}
