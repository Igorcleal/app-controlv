import { ViewController } from 'ionic-angular';
import { Component } from '@angular/core';

@Component({
  selector: 'popover-colors',
  templateUrl: 'popover-colors.html'
})
export class PopoverColorsComponent {

  constructor(public viewCtrl:ViewController) {
  }

  pickColor(color:string){
    this.viewCtrl.dismiss(color);
  }

}
